package com.trimindi.switching.gatway;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.parse.ConfigParser;
import com.trimindi.switching.gatway.client.client.ClientConfiguration;
import com.trimindi.switching.gatway.client.client.Iso8583ClientCustom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;

/**
 * Created by sx on 10/10/17.
 * Copyright under comercial unit
 */
@Configuration
public class VisClientComponent {
    public static final Logger logger = LoggerFactory.getLogger(ClientComponent.class);

    @Value("${iso8583.vsi.connection.host}")
    private String host;

    @Value("${iso8583.vsi.connection.port}")
    private int port;

    @Value("${iso8583.vsi.connection.idleTimeout}")
    private int idleTimeout;
    @Value("${iso8583.vsi.connection.echomessage}")
    private boolean echomessage;

    @Value("${iso8583.vsi.connection.logsensitifdata}")
    private boolean logsensitifdata;
    @Value("${iso8583.vsi.connection.replayonerror}")
    private boolean replayonerror;

    @Bean(name = "vsiClient")
    public Iso8583ClientCustom<IsoMessage> iso8583Client(@Qualifier("vsiClientMessageFactory") MessageFactory<IsoMessage> messageFactory) throws IOException {
        SocketAddress socketAddress = new InetSocketAddress(host, port);
        final ClientConfiguration configuration = ClientConfiguration.newBuilder()
                .withIdleTimeout(idleTimeout)
                .withSensitiveDataFields()
                .withEchoMessageListener(echomessage)
                .withLogSensitiveData(logsensitifdata)
                .withReplyOnError(replayonerror)
                .withAddLoggingHandler(false)
                .build();

        return new Iso8583ClientCustom<>(socketAddress, configuration, messageFactory);
    }

    @Bean(name = "vsiClientMessageFactory")
    public MessageFactory<IsoMessage> clientMessageFactory() throws IOException {
        final MessageFactory<IsoMessage> messageFactory = ConfigParser.createFromClasspathConfig("spec-vsi.xml");
        messageFactory.setCharacterEncoding(StandardCharsets.UTF_8.name());
        messageFactory.setUseBinaryMessages(false);
        messageFactory.setAssignDate(false);
        messageFactory.setForceSecondaryBitmap(false);
        messageFactory.setIgnoreLastMissingField(false);
        messageFactory.setUseBinaryBitmap(false);
        messageFactory.setEtx(1);
        return messageFactory;
    }
}
