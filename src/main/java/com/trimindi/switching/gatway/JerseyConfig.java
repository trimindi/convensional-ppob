package com.trimindi.switching.gatway;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.trimindi.switching.gatway.controllers.*;
import com.trimindi.switching.gatway.exeption.*;
import com.trimindi.switching.gatway.filter.CORSFilter;
import com.trimindi.switching.gatway.filter.SecureEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;
import javax.xml.bind.JAXB;

/**
 * Created by PC on 08/08/2017.
 */
@Component
@ApplicationPath("/api/v1")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(JAXB.class);
        register(JacksonJaxbJsonProvider.class);
        register(SecureEndpoint.class);
        register(CORSFilter.class);
        register(TransaksiControllers.class);
        register(DharmawisataController.class);
        register(TrainControllers.class);
        register(HotelController.class);
        register(ShipController.class);
        register(AirlineController.class);
        register(ReconController.class);
        register(DealerControllers.class);
        register(BeanValConstrainViolationExceptionMapper.class);
        register(CustomBadRequestExeption.class);
        register(CustomExeption.class);
        register(CustomInvalidJsonParseExeption.class);
        register(CustomNotAllowedException.class);
        register(CustomNotFoundExeption.class);
        register(ServindoControllers.class);
    }
}
