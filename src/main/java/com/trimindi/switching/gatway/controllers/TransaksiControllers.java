package com.trimindi.switching.gatway.controllers;

import com.trimindi.switching.gatway.PlnProperties;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.dw.DwPPOBService;
import com.trimindi.switching.gatway.biller.dw.PpobService;
import com.trimindi.switching.gatway.biller.pdamtasik.PdamTasikService;
import com.trimindi.switching.gatway.biller.rajabiller.RajaBillerService;
import com.trimindi.switching.gatway.biller.servindo.ServindoService;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.CetakUlangService;
import com.trimindi.switching.gatway.services.PulsaPrefixService;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.thread.*;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.trimindi.switching.gatway.controllers.Billers.*;
import static com.trimindi.switching.gatway.utils.constanta.Status.PAYMENT_PROSESS;

/**
 * Created by PC on 6/17/2017.
 */
@Path("/")
@Component
@Scope(value = "request")
public class TransaksiControllers {
    public static final Logger logger = LoggerFactory.getLogger(TransaksiControllers.class);
    private final
    ProductItemService productItemService;
    private final
    TransaksiService transaksiService;

    private final
    ServindoService servindoService;
    private final
    RajaBillerService rajaBillerService;
    private final
    PlnProperties config;
    private final CetakUlangService cetakUlangService;

    private final
    PulsaPrefixService pulsaPrefixService;
    private final PartnerDepositService partnerDepositService;
    private final
    PostpaidInquiry postpaidInquiry;
    private final
    PostpaidPayment postpaidPayment;
    private final
    NontaglisInquiry nontaglisInquiry;
    private final
    NontaglistPayment nontaglistPayment;
    private final
    PrepaidInquiy prepaidInquiy;
    private final
    PrepaidPayment prepaidPayment;
    @Autowired
    DwPPOBService dwPPob;

    private final PdamTasikService pdamTasikService;
    private PartnerCredential p;
    private Transaksi transaksi;

    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    PpobService ppobService;
    @Autowired
    public TransaksiControllers(ProductItemService productItemService, TransaksiService transaksiService, PlnProperties config, ServindoService servindoService, RajaBillerService rajaBillerService, PrepaidPayment prepaidPayment, PulsaPrefixService pulsaPrefixService, PartnerDepositService partnerDepositService, PostpaidInquiry postpaidInquiry, PostpaidPayment postpaidPayment, NontaglisInquiry nontaglisInquiry, NontaglistPayment nontaglistPayment, PrepaidInquiy prepaidInquiy, CetakUlangService cetakUlangService, PdamTasikService pdamTasikService) {
        this.productItemService = productItemService;
        this.transaksiService = transaksiService;
        this.config = config;
        this.servindoService = servindoService;
        this.rajaBillerService = rajaBillerService;
        this.prepaidPayment = prepaidPayment;
        this.pulsaPrefixService = pulsaPrefixService;
        this.partnerDepositService = partnerDepositService;
        this.postpaidInquiry = postpaidInquiry;
        this.postpaidPayment = postpaidPayment;
        this.nontaglisInquiry = nontaglisInquiry;
        this.nontaglistPayment = nontaglistPayment;
        this.prepaidInquiy = prepaidInquiy;
        this.cetakUlangService = cetakUlangService;
        this.pdamTasikService = pdamTasikService;
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void transaksi(@Suspended final AsyncResponse asyncResponse,
                        @Context ContainerRequestContext security,@Valid Request req) {
        try {
            p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
            Map<String, String> params = new HashMap<>();
            req.BACK_LINK = p.getBack_link();
            params.put(Constanta.NTRANS, req.NTRANS);
            params.put(Constanta.MACHINE_TYPE,p.getMerchant_type());
            params.put(Constanta.MSSIDN,req.MSSIDN);
            params.put(Constanta.AREA , req.AREA);
            params.put(Constanta.IP_ADDRESS, String.valueOf(security.getProperty(Constanta.IP_ADDRESS)));
            params.put(Constanta.MAC, String.valueOf(security.getProperty(Constanta.MAC)));
            params.put(Constanta.LONGITUDE, String.valueOf(security.getProperty(Constanta.LONGITUDE)));
            params.put(Constanta.LATITUDE, String.valueOf(security.getProperty(Constanta.LATITUDE)));
            params.put(Constanta.DENOM, req.PRODUCT);
            params.put(Constanta.BACK_LINK, req.BACK_LINK);
            ProductItem productItem;
            switch (req.ACTION){
                case "PLN.INQUIRY":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("001")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (!productItem.isACTIVE()) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    asyncResponse.setTimeout(60, TimeUnit.SECONDS);
                    asyncResponse.setTimeoutHandler(as -> as.resume(Response.status(200).entity(ResponseCode.SERVER_TIMEOUT.setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)).setNtrans(transaksi.getNTRANS())).build()));
                    switch (productItem.getProduct_id()) {
                        case "99501":
                            if (req.MSSIDN.length() == 12) {
                                params.put(Constanta.PRODUCT_CODE, config.PAN_POSTPAID);
                                postpaidInquiry.init(asyncResponse, productItem, p, params, req);
                                postpaidInquiry.run();
                            } else {
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.INVALID_MSSIDN.setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN))).build());
                            }
                            break;
                        case "99502":
                            if (req.MSSIDN.length() == 12 || req.MSSIDN.length() == 11) {
                                params.put(Constanta.PRODUCT_CODE, config.PAN_PREPAID);
                                prepaidInquiy.init(asyncResponse, productItem, p, params, req);
                                prepaidInquiy.run();
                            } else {
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.INVALID_MSSIDN.setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN))).build());
                            }
                            break;
                        case "99504":
                            if (req.MSSIDN.length() >= 12) {
                                params.put(Constanta.PRODUCT_CODE, config.PAN_NONTAGLIST);
                                nontaglisInquiry.init(asyncResponse, productItem, p, params, req);
                                nontaglisInquiry.run();
                            } else {
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.INVALID_MSSIDN.setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN))).build());
                            }
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            break;
                    }
                    break;
                case "PLN.PAYMENT":
                    asyncResponse.setTimeout(60, TimeUnit.SECONDS);
                    asyncResponse.setTimeoutHandler(as -> {
                        if (transaksi.getST().equals(PAYMENT_PROSESS)) {
                            as.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES.setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)).setNtrans(transaksi.getNTRANS())).build());
                        } else {
                            as.resume(Response.status(200).entity(ResponseCode.SERVER_TIMEOUT.setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)).setNtrans(transaksi.getNTRANS())).build());
                        }
                    });
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("001")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (!productItem.isACTIVE()) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    transaksi = transaksiService.findById(req.NTRANS);
                    if(!Objects.equals(transaksi.getDENOM(), req.PRODUCT)){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (Objects.equals(transaksi.getST(), Status.INQUIRY)) {
                        if (partnerDepositService.bookingSaldo(transaksi)) {
                            switch (transaksi.getPRODUCT()) {
                                case "99501":
                                    params.put(Constanta.PRODUCT_CODE, config.PAN_POSTPAID);
                                    postpaidPayment.init(asyncResponse, productItem, p, params, req, transaksi);
                                    postpaidPayment.run();
                                    break;
                                case "99502":
                                    params.put(Constanta.PRODUCT_CODE, config.PAN_PREPAID);
                                    prepaidPayment.init(asyncResponse, productItem, p, params, req, transaksi);
                                    prepaidPayment.run();
                                    break;
                                case "99504":
                                    params.put(Constanta.PRODUCT_CODE, config.PAN_NONTAGLIST);
                                    nontaglistPayment.init(asyncResponse, productItem, p, params, req, transaksi);
                                    nontaglistPayment.run();
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200)
                                            .entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION
                                                    .setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                            .build());
                                    break;
                            }
                        } else {
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        }
                    }else{
                        switch (transaksi.getST()){
                            case Status.PAYMENT_PROSESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_UNDER_PROSES.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_FAILED:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_SUCCESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_DONE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                        }
                    }
                    break;
                case "CETAK.ULANG":
                    if(req.NTRANS == null){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.INVALID_BODY_REQUEST_FORMAT.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    transaksi = transaksiService.findById(req.NTRANS);
                    if (transaksi == null) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (!transaksi.getST().equals(Status.PAYMENT_SUCCESS)) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    cetakStruk(asyncResponse, transaksi);
                    break;
                case "TELKOM.INQUIRY":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("002")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    rajaBillerService.InquiryTelkom(asyncResponse,productItem,params,p);
                    break;
                case "PDAM.INQUIRY":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("003")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    switch (productItem.getROUTE()) {
                        case RAJABILLER:
                            rajaBillerService.InquiryPDAM(asyncResponse, productItem, params, p);
                            break;
                        case PDAM_TASIK:
                            asyncResponse.resume(pdamTasikService.inquiry(productItem, params, p));
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            break;
                    }
                    break;
                case "TELKOM.PAYMENT":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("002")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response
                                .status(200)
                                .entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                .build());
                        return;
                    }
                    transaksi = transaksiService.findById(req.NTRANS);
                    if(!Objects.equals(transaksi.getDENOM(), req.PRODUCT)){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (Objects.equals(transaksi.getST(), Status.INQUIRY)) {
                        if (partnerDepositService.bookingSaldo(transaksi)) {
                            switch (productItem.getROUTE()) {
                                case RAJABILLER:
                                    rajaBillerService.PaymentTelkom(asyncResponse, transaksi, params);
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                                    break;
                            }
                        } else {
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            return;
                        }
                    }else{
                        switch (transaksi.getST()){
                            case Status.PAYMENT_PROSESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_UNDER_PROSES.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_FAILED:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_SUCCESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_DONE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                        }
                    }
                    break;
                case "PDAM.PAYMENT":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("003")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    transaksi = transaksiService.findById(req.NTRANS);
                    if(!Objects.equals(transaksi.getDENOM(), req.PRODUCT)){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (Objects.equals(transaksi.getST(), Status.INQUIRY)) {
                        if (partnerDepositService.bookingSaldo(transaksi)) {
                            switch (productItem.getROUTE()) {
                                case RAJABILLER:
                                    rajaBillerService.PaymentPDAM(asyncResponse, transaksi, params);
                                    break;
                                case PDAM_TASIK:
                                    asyncResponse.resume(pdamTasikService.payment(transaksi, p));
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                                    break;
                            }
                        } else {
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            return;
                        }
                    }else{
                        switch (transaksi.getST()){
                            case Status.PAYMENT_PROSESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_UNDER_PROSES.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_FAILED:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_SUCCESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_DONE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                        }
                    }
                    break;
                case "VOUCHER.PULSA":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("004")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }

                    if (pulsaPrefixService.isValidNumberProduct(req.PRODUCT, req.MSSIDN)) {
                        List<Transaksi> prosesTransaksi = transaksiService.check_if_transaksi_under_progress(req.MSSIDN, req.PRODUCT);
                        if (prosesTransaksi.size() == 0) {

                            switch (productItem.getROUTE()) {
                                case SERVINDO:
                                    servindoService.beliPulsa(asyncResponse, productItem, params, p);
                                    break;
                                case DI:
                                    dwPPob.Pulsa(asyncResponse, productItem, p, params);
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                                    break;
                            }
                        } else {
                            asyncResponse.resume(
                                    Response.status(200).entity(
                                            ResponseCode.PAYMENT_UNDER_PROSES
                                                    .setNtrans(prosesTransaksi.get(0).getNTRANS())
                                                    .setProduct(req.PRODUCT)
                                                    .setMssidn(req.MSSIDN)
                                                    .setSaldo(partnerDepositService.findById(p.getPartner_id())
                                                            .getBALANCE())
                                    ).build());
                            return;
                        }
                    } else {
                        asyncResponse.resume(Response.status(200).entity(new ResponseCode("8888", "NOMOR TIDAK SESUAI DENGAN OPERATOR").setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    break;
                case "PULSA.PASCA.INQUIRY":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("007")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (!productItem.isACTIVE()) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    switch (productItem.getROUTE()) {
                        case SERVINDO:
                            break;
                        case RAJABILLER:
                            rajaBillerService.InquiryPulsaPasca(asyncResponse, productItem, params, p);
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            break;
                    }
                    break;

                case "PULSA.PASCA.PAYMENT":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("007")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (!productItem.isACTIVE()) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    transaksi = transaksiService.findById(req.NTRANS);
                    if (!Objects.equals(transaksi.getDENOM(), req.PRODUCT)) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (Objects.equals(transaksi.getST(), Status.INQUIRY)) {
                        if (partnerDepositService.bookingSaldo(transaksi)) {
                            switch (productItem.getROUTE()) {
                                case RAJABILLER:
                                    rajaBillerService.PaymentPulsaPasca(asyncResponse, transaksi, params);
                                    break;
                                case PDAM_TASIK:
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                                    break;
                            }
                        } else {
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            return;
                        }
                    } else {
                        switch (transaksi.getST()) {
                            case Status.PAYMENT_PROSESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_UNDER_PROSES.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_FAILED:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_SUCCESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_DONE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                        }
                    }
                    break;
                case "VOUCHER.DATA":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("005")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if(!productItem.isACTIVE()){
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    List<Transaksi> prosesTransaksi = transaksiService.check_if_transaksi_under_progress(req.MSSIDN, req.PRODUCT);
                    if (prosesTransaksi.size() == 0) {
                        switch (productItem.getROUTE()) {
                            case SERVINDO:
                                servindoService.beliData(asyncResponse, productItem, params, p);
                                break;
                            case RAJABILLER:
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                                break;
                        }
                    }else{
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES.setNtrans(prosesTransaksi.get(0).getNTRANS()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                    }
                    break;
                case "BPJS.INQUIRY":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("008")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (!productItem.isACTIVE()) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    switch (productItem.getROUTE()) {
                        case SERVINDO:
                            break;
                        case RAJABILLER:
                            rajaBillerService.InquiryBpjs(asyncResponse, productItem, params, p);
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            break;
                    }
                    break;
                case "BPJS.PAYMENT":
                    productItem = productItemService.findById(req.PRODUCT);
                    if (productItem == null || !productItem.getFk_biller().equalsIgnoreCase("008")) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (!productItem.isACTIVE()) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    transaksi = transaksiService.findById(req.NTRANS);
                    if (!Objects.equals(transaksi.getDENOM(), req.PRODUCT)) {
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                        return;
                    }
                    if (Objects.equals(transaksi.getST(), Status.INQUIRY)) {
                        if (partnerDepositService.bookingSaldo(transaksi)) {
                            switch (productItem.getROUTE()) {
                                case RAJABILLER:
                                    rajaBillerService.PaymentBpjs(asyncResponse, transaksi, params, req);
                                    break;
                                case PDAM_TASIK:
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                                    break;
                            }
                        } else {
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                            return;
                        }
                    } else {
                        switch (transaksi.getST()) {
                            case Status.PAYMENT_PROSESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_UNDER_PROSES.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_FAILED:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                            case Status.PAYMENT_SUCCESS:
                                asyncResponse.resume(
                                        Response
                                                .status(200)
                                                .entity(ResponseCode.PAYMENT_DONE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE()))
                                                .build());
                                break;
                        }
                    }
                    break;
                default:
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
                    break;
            }
        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setProduct(req.PRODUCT).setMssidn(req.MSSIDN).setSaldo(partnerDepositService.findById(p.getPartner_id()).getBALANCE())).build());
        }
    }

    private void cetakStruk(AsyncResponse asyncResponse, Transaksi transaksi) {
        if (transaksi.getPRT() > 3) {
            asyncResponse.resume(Response.status(200).entity(ResponseCode.MAXIMUM_PRINTED_RECIPT).build());
        } else {
            ProductItem productItem = productItemService.findById(transaksi.getDENOM());
            switch (productItem.getFk_biller()) {
                case "001":
                    switch (productItem.getROUTE()) {
                        case ARANET:
                            switch (productItem.getProduct_id()) {
                                case "99501":
                                    asyncResponse.resume(cetakUlangService.cetakUlangPlnPostpaidAranet(transaksi));
                                    break;
                                case "99502":
                                    asyncResponse.resume(cetakUlangService.cetakUlangPlnPrepaidAranet(transaksi));
                                    break;
                                case "99504":
                                    asyncResponse.resume(cetakUlangService.cetakUlangPlnNontaglistAranet(transaksi));
                                    break;
                            }
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                            break;
                    }
                    break;
                case "002":
                    try {
                        switch (productItem.getROUTE()) {
                            case RAJABILLER:
                                Response response = cetakUlangService.cetakUlangTelkomRajabiller(transaksi);
                                asyncResponse.resume(response);
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                                break;
                        }
                    } catch (JAXBException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    break;
                case "003":
                    try {
                        Response response;
                        switch (productItem.getROUTE()) {
                            case RAJABILLER:
                                response = cetakUlangService.cetakUlangPdamRajabiller(transaksi);
                                asyncResponse.resume(response);
                                break;
                            case PDAM_TASIK:
                                response = cetakUlangService.cetakUlangPdamTasik(transaksi);
                                asyncResponse.resume(response);
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                                break;
                        }

                    } catch (JAXBException | IOException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    break;
                case "004":
                    try {
                        Response response;
                        switch (productItem.getROUTE()) {
                            case SERVINDO:
                                response = cetakUlangService.cetakUlangPulsaServindo(transaksi);
                                asyncResponse.resume(response);
                                break;
                            case DI:
                                response = cetakUlangService.cetakUlangPulsaDI(transaksi);
                                asyncResponse.resume(response);
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                                break;
                        }

                    } catch (IOException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    break;
                case "005":
                    try {
                        Response response;
                        switch (productItem.getROUTE()) {
                            case SERVINDO:
                                response = cetakUlangService.cetakUlangDataServindo(transaksi);
                                asyncResponse.resume(response);
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                                break;
                        }
                    } catch (IOException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    break;
                case "006":
                    switch (productItem.getROUTE()) {
                        case VSI:
                            cetakUlangService.cetakUlangMultifinance(asyncResponse, transaksi);
                            break;
                        default:
                            asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                            break;
                    }
                    break;
                case "007":
                    try {
                        Response response;
                        switch (productItem.getROUTE()) {
                            case RAJABILLER:
                                response = cetakUlangService.cetakUlangPulsaPascaRajabiller(transaksi);
                                asyncResponse.resume(response);
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                                break;
                        }

                    } catch (JAXBException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    break;
                case "008":
                    try {
                        Response response;
                        switch (productItem.getROUTE()) {
                            case RAJABILLER:
                                response = cetakUlangService.cetakUlangBpjsRajabiller(transaksi);
                                asyncResponse.resume(response);
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                                break;
                        }

                    } catch (JAXBException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    break;
                case "010":
                    try {
                        Response response;
                        switch (productItem.getROUTE()) {
                            case DI:
                                response = cetakUlangService.cetakUlangTvDharmawisata(transaksi);
                                asyncResponse.resume(response);
                                break;
                            default:
                                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                                break;
                        }
                    } catch (IOException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    break;
            }
        }

    }
}

