package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

/**
 * Created by PC on 6/17/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {
    @NotNull(message = "PRODUCT REQUIRED")
    @JsonProperty("PRODUCT")
    public String PRODUCT;
    @NotNull(message = "ACTION REQUIRED")
    @JsonProperty("ACTION")
    public String ACTION;
    @NotNull(message = "MSSIDN REQUIRED")
    @JsonProperty("MSSIDN")
    public String MSSIDN;
    @JsonProperty("NOHP")
    public String NOHP;
    @JsonProperty("NTRANS")
    public String NTRANS;
    public String AREA;
    public String BACK_LINK;

    @Override
    public String toString() {
        return "Request{" +
                "PRODUCT='" + PRODUCT + '\'' +
                ", ACTION='" + ACTION + '\'' +
                ", MSSIDN='" + MSSIDN + '\'' +
                ", NTRANS='" + NTRANS + '\'' +
                ", AREA='" + AREA + '\'' +
                '}';
    }
}
