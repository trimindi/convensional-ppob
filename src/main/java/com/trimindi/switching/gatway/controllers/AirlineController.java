package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.biller.dw.AirlineService;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage.GetAirlineBagageAndMealRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.booking.SetAirlineBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.booking.SetAirlineBookingResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.issued.SetAirlineIssuedRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.price.GetAirlinePriceRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.route.GetAirlineRouteRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule.GetAirlineScheduleRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.seat.GetAirlineSeatRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sx on 18/12/17.
 * Copyright under comercial unit
 */
@Path("/airline")
@Component
@Scope(value = "request")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class AirlineController {
    public static final Logger LOGGER = LoggerFactory.getLogger(AirlineController.class);

    @Autowired
    AirlineService airlineService;
    @Context
    private ContainerRequestContext security;
    private PartnerCredential p;
    @Autowired
    private TransaksiService transaksiService;
    @Autowired
    private PartnerDepositService partnerDepositService;
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Get Airline List
     * Operation to get list of available airline via API.
     * Get Airline Nationality List
     * Operation to get list nationality to be used in airline API request.
     * Get Airline Routes
     * Operation to get list of airline routes via API.
     * Get Airline Schedule
     * Operation to get list of airline schedule via API.
     * Get Airline AddOns : Baggage and Meal
     * Operation to get list of available baggage and meal addons in selected airline schedule .
     * Get Airline Add Ons : Seat
     * Operation to get list of available seat addons in selected airline schedule.
     * Get Airline Prices
     * Operation to get price list for selected airline schedule.
     * Set Booking Airline
     * Operation to set booking to selected airline schedule via API.
     * Set Issued Airline
     * Operation to set issued to booking airline via API.
     * Get List Airline Booking
     * Operation to get agent’s list of airline booking via API.
     * Get Airline Booking Detail
     * Operation to get information detail of agent’s airline booking transaction via API.
     *
     * @return
     * @
     */
    @POST
    @Path("/GetAirlineList")
    public Response GetAirlineList() {
        return Response.status(200).entity(airlineService.getAirlineList()).build();
    }

    @POST
    @Path("/GetAirlineNationalityList")
    public Response GetAirlineNationalityList() {
        return Response.status(200).entity(airlineService.getAirlineCountry()).build();

    }

    @POST
    @Path("/GetAirlineRoutes")
    public Response GetAirlineRoutes(GetAirlineRouteRequest getAirlineRouteRequest) {
        return Response.status(200).entity(airlineService.getAirlineRoutes(getAirlineRouteRequest)).build();

    }

    @POST
    @Path("/GetAirlineSchedule")
    public Response GetAirlineSchedule(GetAirlineScheduleRequest getAirlineScheduleRequest) {
        return airlineService.getAirlineSchedule(getAirlineScheduleRequest);

    }

    @POST
    @Path("/GetAirlineAddOnsBaggageandMeal")
    public Response GetAirlineAddOnsBaggageandMeal(GetAirlineBagageAndMealRequest getAirlineBagageAndMealRequest) {
        return Response.status(200).entity(airlineService.getAirlineBagage(getAirlineBagageAndMealRequest)).build();

    }

    @POST
    @Path("/GetAirlineAddOnsSeat")
    public Response GetAirlineAddOnsSeat(GetAirlineSeatRequest getAirlineSeatRequest) {
        return Response.status(200).entity(airlineService.getAirlineSeatMap(getAirlineSeatRequest)).build();

    }

    @POST
    @Path("/GetAirlinePrices")
    public Response GetAirlinePrices(GetAirlinePriceRequest getAirlinePriceRequest) {
        return Response.status(200).entity(airlineService.getAilinePrice(getAirlinePriceRequest)).build();

    }

    @POST
    @Path("/SetBookingAirline")
    public Response SetBookingAirline(SetAirlineBookingRequest setAirlineBookingRequest) {
        LOGGER.error("Request inbound");
        p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        return Response.status(200).entity(airlineService.setBookingAirline(setAirlineBookingRequest, p)).build();
    }

    @POST
    @Path("/SetIssuedAirline/{ntrans}")
    public Response SetIssuedAirline(@PathParam("ntrans") final String ntrans) throws ParseException, IOException {
        LOGGER.error("Request inbound");
        Response rx;
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            if (partnerDepositService.bookingSaldo(transaksi)) {
                SetAirlineBookingResponse setBookingTrainResponse = this.objectMapper.readValue(transaksi.getINQUIRY(), SetAirlineBookingResponse.class);
                SetAirlineIssuedRequest setAirlineIssuedRequest = new SetAirlineIssuedRequest();
                setAirlineIssuedRequest.setBookingCode(setBookingTrainResponse.getBookingCode());
                setAirlineIssuedRequest.setTripType(setBookingTrainResponse.getTripType());
                setAirlineIssuedRequest.setDepartDate(setBookingTrainResponse.getDepartDate());
                setAirlineIssuedRequest.setReturnDate(setBookingTrainResponse.getReturnDate());
                setAirlineIssuedRequest.setOrigin(setBookingTrainResponse.getOrigin());
                setAirlineIssuedRequest.setDestination(setBookingTrainResponse.getDestination());
                setAirlineIssuedRequest.setAirlineID(setBookingTrainResponse.getAirlineID());
                setAirlineIssuedRequest.setAirlineAccessCode("");
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX").parse(setBookingTrainResponse.getBookingDate());
                setAirlineIssuedRequest.setBookingDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
                rx = Response.status(200).entity(airlineService.setIssuedAirline(setAirlineIssuedRequest, transaksi)).build();
            } else {
                rx = Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();
            }
        } else {
            rx = Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setNtrans(transaksi.getNTRANS())).build();
        }
        return rx;
    }

    @POST
    @Path("/GetListAirlineBooking")
    public Response GetListAirlineBooking() {
        return Response.ok().build();
    }

    @POST
    @Path("/GetAirlineBookingDetail/{ntrans}")
    public Response GetAirlineBookingDetail(@PathParam("ntrans") final String ntrans) throws ParseException, IOException {
        LOGGER.error("Request inbound");
        Response rx = null;
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            if (partnerDepositService.bookingSaldo(transaksi)) {
                SetAirlineBookingResponse setAirlineBookingResponse = this.objectMapper.readValue(transaksi.getINQUIRY(), SetAirlineBookingResponse.class);
                BookingDetailRequest bookingDetailRequest = new BookingDetailRequest();
                bookingDetailRequest.setBookingCode(setAirlineBookingResponse.getBookingCode());
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX").parse(setAirlineBookingResponse.getBookingDate());
                bookingDetailRequest.setBookingDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
                rx = Response.status(200).entity(airlineService.getBookingDetail(bookingDetailRequest, transaksi)).build();
            } else {
                rx = Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();
            }
        } else {
            rx = Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setNtrans(transaksi.getNTRANS())).build();
        }
        return rx;
    }
}
