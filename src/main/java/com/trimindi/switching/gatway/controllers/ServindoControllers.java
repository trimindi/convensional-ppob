package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.servindo.ResponseServindo;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.PartnerCredentialService;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created by PC on 21/07/2017.
 */
@Path("/servindo")
@Component
@Scope(value = "request")
public class ServindoControllers {

    public static final Logger logger = LoggerFactory.getLogger(ServindoControllers.class);
    private final
    TransaksiService transaksiService;
    private final
    PartnerCredentialService partnerCredentialService;
    private final
    PartnerDepositService partnerDepositService;
    private final ObjectMapper objectMapper;
    private final CloseableHttpClient client;
    private final ProductItemService productItemService;
    private HttpPost httpPost;
    private PartnerCredential partnerCredential;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public ServindoControllers(TransaksiService transaksiService, PartnerCredentialService partnerCredentialService, PartnerDepositService partnerDepositService, ObjectMapper objectMapper, CloseableHttpClient client, ProductItemService productItemService) {
        this.transaksiService = transaksiService;
        this.partnerCredentialService = partnerCredentialService;
        this.partnerDepositService = partnerDepositService;
        this.objectMapper = objectMapper;
        this.client = client;
        this.productItemService = productItemService;
    }

    @POST
    @Path("report")
    public void report(@Suspended AsyncResponse asyncResponse, String response) {
        logger.error("Income Report hit Link -> " + response);
        ResponseServindo responseServindo;
        try {
            responseServindo = objectMapper.readValue(response, ResponseServindo.class);
            if (responseServindo != null) {
                Transaksi transaksi = transaksiService.findByBILL_REF_NUMBER(responseServindo.getRegId());
                if (transaksi != null) {
                    partnerCredential = partnerCredentialService.findById(transaksi.getUSERID());
                    httpPost = new HttpPost(partnerCredential.getBack_link());
                    responseServindo.setKodeProduk(transaksi.getDENOM());
                    BaseResponse<ResponseServindo> paymentResponse = new BaseResponse<>();
                    if (responseServindo.getStatus() == 1) {
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(response)
                                .setBILL_REF_NUMBER(responseServindo.getTn());
                        transaksiService.update(transaksi);
                        if (!(Double.compare(Double.parseDouble(responseServindo.getHarga()), (transaksi.getCHARGE() - transaksi.getFEE_CA())) == 0)) {
                            if (Double.parseDouble(responseServindo.getHarga()) != 0) {
                                productItemService.updateHarga(transaksi.getDENOM(), Double.parseDouble(responseServindo.getHarga()));
                            }
                        }
                        asyncResponse.resume(Response.status(200).entity("OK").build());
                        paymentResponse.setData(responseServindo);
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                        paymentResponse.setMessage("Successful");
                        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        sendToMitra(Response.status(200).entity(paymentResponse).build());
                    } else if (responseServindo.getStatus() == 3) {
                        partnerDepositService.reverseSaldo(transaksi, response);
                        transaksi.setST(Status.PAYMENT_FAILED)
                                .setPAYMENT(response)
                                .setBILL_REF_NUMBER(responseServindo.getTn());
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity("OK").build());
                        sendToMitra(Response.status(200).entity(ResponseCode.PAYMENT_FAILED.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                    }else{
                        asyncResponse.resume(Response.status(200).entity("OK").build());
                    }
                } else {
                    asyncResponse.resume(Response.status(200).entity("ERORR REG ID TIDAK DIKENALI").build());
                }
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            asyncResponse.resume(Response.status(200).entity("TERJADI KESALAHAN PADA SERVER").build());
        }
    }

    private void sendToMitra(Response response) {
        try {
            StringEntity entity = new StringEntity(objectMapper.writeValueAsString(response.getEntity()));
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            CloseableHttpResponse r;
            r = client.execute(httpPost);
            String respone = EntityUtils.toString(r.getEntity(), StandardCharsets.UTF_8.name());
            logger.error("<-- REPORT START  --> ");
            logger.error("SEND TO PARTNER    -> " + "[" + partnerCredential.getPartner_id() + "|" + partnerCredential.getPartner_uid() + "] ");
            logger.error("SEND TO API        -> " + partnerCredential.getBack_link());
            logger.error("WITH BODY          -> " + objectMapper.writeValueAsString(response.getEntity()));
            logger.error("RESPONSE BY CLIENT -> " + respone);
            logger.error("<-- REPORT END    --> ");
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
    }
}
