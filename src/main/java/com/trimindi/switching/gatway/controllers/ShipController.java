package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.biller.dw.ShipService;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.avability.request.GetShipAvabilityRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.request.SetShipBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.response.SetShipBookingResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.detail.request.GetShipBookingDetailRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.issued.request.SetShipIssuedRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.room.request.GetShipRoomRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.request.GetShipScheduleRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sx on 26/01/18.
 * Copyright under comercial unit
 */
@Path("/ship")
@Component
@Scope(value = "request")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ShipController {
    public static final Logger LOGGER = LoggerFactory.getLogger(ShipController.class);

    @Autowired
    ShipService shipService;
    @Context
    private ContainerRequestContext security;
    private PartnerCredential p;
    @Autowired
    private TransaksiService transaksiService;
    @Autowired
    private PartnerDepositService partnerDepositService;
    @Autowired
    private ObjectMapper objectMapper;

    @POST
    @Path("GetShipRoutes")
    public Response GetShipRoutes() {
        return shipService.GetShipRoutes();
    }

    @POST
    @Path("GetShipSchedule")
    public Response GetShipSchedule(GetShipScheduleRequest getShipScheduleRequest) {
        return shipService.GetShipSchedule(getShipScheduleRequest);
    }

    @POST
    @Path("GetShipAvailability")
    public Response GetShipAvailability(GetShipAvabilityRequest getShipAvabilityRequest) {
        return shipService.GetShipAvailability(getShipAvabilityRequest);
    }

    @POST
    @Path("GetShipRoom")
    public Response GetShipRoom(GetShipRoomRequest getShipRoomRequest) {
        return shipService.GetShipRoom(getShipRoomRequest);

    }

    @POST
    @Path("SetBookingShip")
    public Response SetBookingShip(SetShipBookingRequest setShipBookingRequest) {
        p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        return shipService.SetBookingShip(setShipBookingRequest, p);
    }

    @POST
    @Path("SetIssuedShip/{ntrans}")
    public Response SetIssuedShip(@PathParam("ntrans") final String ntrans) throws ParseException, IOException {
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            if (partnerDepositService.bookingSaldo(transaksi)) {
                SetShipBookingResponse setBookingTrainResponse = this.objectMapper.readValue(transaksi.getINQUIRY(), SetShipBookingResponse.class);
                SetShipIssuedRequest setShipIssuedRequest = new SetShipIssuedRequest();
                setShipIssuedRequest.setNumCode(setBookingTrainResponse.getNumCode());
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX").parse(setBookingTrainResponse.getBookingDateTime());
                setShipIssuedRequest.setBookingDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
                return shipService.SetIssuedShip(setShipIssuedRequest, transaksi);
            } else {
                return Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();
            }
        } else {
            return Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setNtrans(transaksi.getNTRANS())).build();
        }
    }

    @POST
    @Path("GetListShipBooking")
    public Response GetListShipBooking() {
        return Response.noContent().build();
    }

    @POST
    @Path("GetShipBookingDetail/{ntrans}")
    public Response GetShipBookingDetail(@PathParam("ntrans") final String ntrans) throws IOException, ParseException {
        LOGGER.error("Request inbound");
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            if (partnerDepositService.bookingSaldo(transaksi)) {
                SetShipBookingResponse setBookingTrainResponse = this.objectMapper.readValue(transaksi.getINQUIRY(), SetShipBookingResponse.class);
                GetShipBookingDetailRequest bookingDetailRequest = new GetShipBookingDetailRequest();
                bookingDetailRequest.setNumCode(setBookingTrainResponse.getNumCode());
                bookingDetailRequest.setBookingDate(setBookingTrainResponse.getBookingDateTime());
                return shipService.GetShipBookingDetail(bookingDetailRequest, transaksi);
            } else {
                return Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();
            }
        } else {
            return Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setNtrans(transaksi.getNTRANS())).build();
        }
    }

}
