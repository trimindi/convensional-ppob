package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.biller.dw.KaiService;
import com.trimindi.switching.gatway.biller.dw.pojo.train.booking.SetBookingTrainRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.booking.SetBookingTrainResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.issue.SetIssuedTrainRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.route.GetTrainRouteRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.schedule.GetTrainSchaduleRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.seat.GetTrainSeatMapRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.takeseat.TakeSeatRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by sx on 06/12/17.
 * Copyright under comercial unit
 */
@Path("/train")
@Component
@Scope(value = "request")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TrainControllers {
    public static final Logger LOGGER = LoggerFactory.getLogger(TrainControllers.class);

    @Autowired
    KaiService kaiService;
    @Context
    private ContainerRequestContext security;
    private PartnerCredential p;
    @Autowired
    private TransaksiService transaksiService;
    @Autowired
    private PartnerDepositService partnerDepositService;
    @Autowired
    private ObjectMapper objectMapper;
    /**
     * ->Get Train List
     * Operation to get list of available train via API.
     * ->Get Train Routes
     * Operation to get list of train routes via API.
     * ->Get Train Schedule
     * Operation to get list of train schedule via API.
     * ->Get Train Seat Map
     * Operation to get list of train seat map via API.
     * Take Seat Train (only in KAI, other train include in booking feature)
     * Operation to take train seat.
     * ->Set Booking Train
     * Operation to set booking to selected train schedule via API.
     * ->Set Issued Train
     * Operation to set issued to booking train via API.
     * ->Get List Train Booking
     * Operation to get agent’s list of train booking via API.
     * ->Get Train Booking Detail
     * Operation to get information detail of agent’s train booking transaction via API.
     */

    @POST
    @Path("/GetTrainList")
    public Response getTrainList() {
        return Response.status(200).entity(kaiService.getTrainList()).build();
    }

    @POST
    @Path("/GetTrainRoutes")
    public Response GetTrainRoutes(GetTrainRouteRequest getTrainRouteRequest) {
        return Response.status(200).entity(kaiService.getTrainRoutes(getTrainRouteRequest)).build();
    }

    @POST
    @Path("/GetTrainSchedule")
    public Response GetTrainSchedule(GetTrainSchaduleRequest getTrainSchaduleRequest) {
        return Response.status(200).entity(kaiService.getTrainSchedule(getTrainSchaduleRequest)).build();
    }

    @POST
    @Path("/GetTrainSeatMap")
    public Response GetTrainSeatMap(GetTrainSeatMapRequest getTrainSeatMapRequest) {
        return Response.status(200).entity(kaiService.getTrainSeatMap(getTrainSeatMapRequest)).build();
    }

    @POST
    @Path("/SetTrainSeatMap")
    public Response SetTrainSeatMap(TakeSeatRequest takeSeatRequest) {
        return Response.status(200).entity(kaiService.setSeatTrain(takeSeatRequest)).build();
    }

    @POST
    @Path("/SetBookingTrain")
    public Response SetBookingTrain(SetBookingTrainRequest setBookingTrainRequest) {
        p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        return Response.status(200).entity(kaiService.setBookingTrain(setBookingTrainRequest, p)).build();
    }

    @POST
    @Path("/SetIssuedTrain/{ntrans}")
    public Response SetIssuedTrain(@PathParam("ntrans") final String ntrans) throws IOException, ParseException {
        Response rx = null;
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            if(Objects.equals(transaksi.getST(), Status.INQUIRY)){
                if (partnerDepositService.bookingSaldo(transaksi)) {
                    SetBookingTrainResponse setBookingTrainResponse = this.objectMapper.readValue(transaksi.getINQUIRY(), SetBookingTrainResponse.class);
                    SetIssuedTrainRequest setIssuedTrainRequest = new SetIssuedTrainRequest();
                    setIssuedTrainRequest.setBookingCode(setBookingTrainResponse.getBookingCode());
                    Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX").parse(setBookingTrainResponse.getBookingDate());
                    setIssuedTrainRequest.setBookingDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
                    rx = Response.status(200).entity(kaiService.setIssuedTrain(setIssuedTrainRequest, transaksi)).build();
                } else {
                    rx = Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();
                }
            }else{
                rx = Response.status(200).entity(ResponseCode.PAYMENT_DONE.setMessage("NTRANS Expired ulangi booking kembali kembali").setNtrans(transaksi.getNTRANS())).build();
            }
        } else {
            rx = Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setNtrans(transaksi.getNTRANS())).build();
        }
        return rx;
    }

    @POST
    @Path("/GetListTrainBooking")
    public Response GetListTrainBooking() {
        return Response.noContent().build();
    }

    @POST
    @Path("/GetTrainBookingDetail/{ntrans}")
    public Response GetTrainBookingDetail(@PathParam("ntrans") final String ntrans) throws ParseException, IOException {
        Response rx = null;
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            SetBookingTrainResponse setBookingTrainResponse = this.objectMapper.readValue(transaksi.getINQUIRY(), SetBookingTrainResponse.class);
            BookingDetailRequest bookingDetailRequest = new BookingDetailRequest();
            bookingDetailRequest.setBookingCode(setBookingTrainResponse.getBookingCode());
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX").parse(setBookingTrainResponse.getBookingDate());
            bookingDetailRequest.setBookingDate(new SimpleDateFormat("yyyy-MM-dd").format(date));
            rx = Response.status(200).entity(kaiService.getBookingDetauils(bookingDetailRequest, transaksi)).build();
        } else {
            rx = Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setNtrans(transaksi.getNTRANS())).build();
        }
        return rx;

    }
}
