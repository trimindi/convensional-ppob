package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.PartnerCredentialService;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sx on 22/01/18.
 * Copyright under comercial unit
 */
@Component
@Scope(value = "request")
@Path("/di")
public class DharmawisataController {
    private Logger logger = LoggerFactory.getLogger(DharmawisataController.class);
    private final SlackSendMessage slackSendMessage;
    private final CloseableHttpClient client;
    private final ObjectMapper objectMapper;
    private final PartnerDepositService partnerDepositService;
    private final TransaksiService transaksiService;
    private final PartnerCredentialService partnerCredentialService;
    PartnerCredential partnerCredential;
    private HttpPost httpPost;

    @Autowired
    public DharmawisataController(SlackSendMessage slackSendMessage, CloseableHttpClient client, ObjectMapper objectMapper, PartnerDepositService partnerDepositService, TransaksiService transaksiService, PartnerCredentialService partnerCredentialService) {
        this.slackSendMessage = slackSendMessage;
        this.client = client;
        this.objectMapper = objectMapper;
        this.partnerDepositService = partnerDepositService;
        this.transaksiService = transaksiService;
        this.partnerCredentialService = partnerCredentialService;
    }

    @GET
    @Path("/report")
    public Response report(
            @QueryParam("pid") String trxid,
            @QueryParam("code") String code,
            @QueryParam("produk") String produk,
            @QueryParam("sn") String sn,
            @QueryParam("msisdn") String msisdn,
            @QueryParam("msg") String msg,
            @QueryParam("data") String data
    ) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[Trxid " + trxid);
        stringBuilder.append(" code " + code);
        stringBuilder.append(" produk " + produk);
        stringBuilder.append(" sn " + sn);
        stringBuilder.append(" msisdn " + msisdn);
        stringBuilder.append(" data " + data + " ]");
        logger.error("Income Hit From DI PUlsa \n " + stringBuilder.toString());
        Transaksi transaksi = transaksiService.findByBILL_REF_NUMBER(trxid);
        logger.error(transaksi.toString());
        if (transaksi != null) {
            partnerCredential = partnerCredentialService.findById(transaksi.getUSERID());
            httpPost = new HttpPost(partnerCredential.getBack_link());
            switch (code) {
                case "2":
                    partnerDepositService.reverseSaldo(transaksi, "");
                    transaksi.setST(Status.PAYMENT_FAILED);
                    transaksiService.update(transaksi);
                    sendToMitra(Response.status(200).entity(ResponseCode.PAYMENT_FAILED.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                    break;
                case "3":
                    partnerDepositService.reverseSaldo(transaksi, "");
                    transaksi.setST(Status.PAYMENT_FAILED);
                    transaksiService.update(transaksi);
                    sendToMitra(Response.status(200).entity(ResponseCode.PAYMENT_FAILED.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                    break;
                case "4":
                    transaksi.setST(Status.PAYMENT_SUCCESS)
                            .setBILL_REF_NUMBER(sn);
                    transaksiService.update(transaksi);
                    Map<String, String> r = new HashMap<>();
                    r.put("sn", trxid);
                    r.put("message", String.format("SUKSES! Trx %s %s berhasil. SN: %s", transaksi.getDENOM(), transaksi.getMSSIDN(), sn));
                    BaseResponse<Map<String, String>> baseResponse = new BaseResponse<>();
                    baseResponse.setData(r);
                    baseResponse.setNtrans(transaksi.getNTRANS());
                    baseResponse.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                    baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                    baseResponse.setProduct(transaksi.getDENOM());
                    baseResponse.setMessage("Successful");
                    baseResponse.setFee(0);
                    sendToMitra(Response.status(200).entity(baseResponse).build());
                    break;
            }
        }
        return Response.status(200).entity("OK").build();
    }

    private void sendToMitra(Response response) {
        try {
            StringEntity entity = new StringEntity(objectMapper.writeValueAsString(response.getEntity()));
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            CloseableHttpResponse r;
            r = client.execute(httpPost);
            String respone = EntityUtils.toString(r.getEntity(), StandardCharsets.UTF_8.name());
            logger.error("<-- REPORT START  --> ");
            logger.error("SEND TO PARTNER    -> " + "[" + partnerCredential.getPartner_id() + "|" + partnerCredential.getPartner_uid() + "] ");
            logger.error("SEND TO API        -> " + partnerCredential.getBack_link());
            logger.error("WITH BODY          -> " + objectMapper.writeValueAsString(response.getEntity()));
            logger.error("RESPONSE BY CLIENT -> " + respone);
            logger.error("<-- REPORT END    --> ");
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
    }
}
