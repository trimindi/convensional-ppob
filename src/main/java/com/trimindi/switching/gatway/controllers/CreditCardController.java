package com.trimindi.switching.gatway.controllers;

import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by sx on 19/03/18.
 * Copyright under comercial unit
 */
@Path("/cc")
@Component
@Scope(value = "request")
public class CreditCardController {
    public static final org.slf4j.Logger logger = LoggerFactory.getLogger(CreditCardController.class);

    @POST
    @Path("/")
    public Response inquiry(String string) {
        logger.error(string);
        return Response.status(200).entity(string).build();
    }

}
