package com.trimindi.switching.gatway.controllers;

import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.VSIPropoerties;
import com.trimindi.switching.gatway.client.client.Iso8583ClientCustom;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.mutifinance.Payment;
import com.trimindi.switching.gatway.utils.iso.FieldBuilder;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorMultifinance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by sx on 08/01/18.
 * Copyright under comercial unit
 */
@Component
@Scope(value = "request")
@Path("/recon")
public class ReconController {
    @Autowired
    VSIPropoerties appProperties;
    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    Iso8583ClientCustom clientCustom;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Path("/multifinance")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response reconMultifinance(@QueryParam("date") String date, @QueryParam("product") String product) throws ParseException {
        if (date == null) {
            return Response.serverError().build();
        }
        Date parseTime = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query =
                entityManager.createNativeQuery("SELECT * FROM mst_transaksi where date(time_payment) = :date AND product = :product and ST = '02' order by time_payment,mssidn ASC", Transaksi.class);
        query.setParameter("date", parseTime, TemporalType.DATE);
        query.setParameter("product", product);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DT|CENTRAL_ID|MERCHANT|REFNUM|SREFNUM|SUB_ID|BILL_COUNT|TRANS_AMOUNT|TOTAL_BILL|BILL_AMOUNT|PENALTY_FEE|ADMIN_CHARGE|CA_ID|TERMINAL_ID\n");
        List<Transaksi> transaksiList = (List<Transaksi>) query.getResultList();
        transaksiList.stream()
                .forEach(transaksi -> {
                    try {
                        if (transaksi.getPAYMENT() != null) {
                            IsoMessage isoMessage = clientCustom.getIsoMessageFactory().parseMessage(transaksi.getPAYMENT().getBytes(), 0);
                            List<Rules> bit48 = new SDE.Builder()
                                    .setPayload(isoMessage.getObjectValue(48))
                                    .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(48, true))
                                    .generate();
                            List<Rules> bit62 = new SDE.Builder()
                                    .setPayload(isoMessage.getObjectValue(62))
                                    .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(62, true))
                                    .generate();
                            bit48.addAll(bit62);
                            Payment payment = new Payment(bit48);
                            stringBuilder.append(new FieldBuilder.Builder()
                                    .addValue(isoMessage.getObjectValue(12), 14, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(isoMessage.getObjectValue(33), 7, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(isoMessage.getObjectValue(26), 4, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(payment.getGWReferenceNumber(), 32, " ", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(payment.getSWReferenceNumber(), 32, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(payment.getCustomerID(), 20, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(payment.getLastPaidPeriod(), 3, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(new DecimalFormat("#").format(payment.getBillAmount() + payment.getAdminCharges()), 17, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(new DecimalFormat("#").format(payment.getBillAmount()), 17, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(new DecimalFormat("#").format(payment.getODPenaltyFee()), 11, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(new DecimalFormat("#").format(payment.getAdminCharges()), 11, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(isoMessage.getObjectValue(32), 7, "0", "L")
                                    .addValue("", 1, "|", "L")
                                    .addValue(isoMessage.getObjectValue(41), 16, "0", "L")
                                    .addValue("\n", 2, " ", "L")
                                    .build()
                            );
                        }
                    } catch (ParseException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    } catch (UnsupportedEncodingException e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                });
        String nameFile = appProperties.SWITCHER_ID + "-" + product + "-" + new SimpleDateFormat("yyyyMMdd").format(parseTime) + ".ftr";
        return Response.status(200)
                .header("Content-Disposition",
                        "attachment; filename=\"" + nameFile + "\"")
                .entity(stringBuilder.toString()).build();
    }
}
