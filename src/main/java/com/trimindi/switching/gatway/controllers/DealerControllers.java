package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.PartnerDeposit;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.CetakUlangService;
import com.trimindi.switching.gatway.services.PartnerCredentialService;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static com.trimindi.switching.gatway.controllers.Billers.*;


/**
 * Created by PC on 06/09/2017.
 */
@Component
@Scope(value = "request")
@Path("/dealer")
public class DealerControllers {

    private static final Logger logger = LoggerFactory.getLogger(DealerControllers.class);
    private final TransaksiService transaksiService;
    private final
    PartnerCredentialService partnerCredentialService;
    private final
    PartnerDepositService partnerDepositService;
    private final ObjectMapper objectMapper;
    private final CloseableHttpClient client;
    private HttpPost httpPost;
    private final CetakUlangService cetakUlangService;
    private final SlackSendMessage slackSendMessage;
    private final ProductItemService productItemService;
    private PartnerCredential partnerCredential;

    @Autowired
    public DealerControllers(PartnerDepositService partnerDepositService, PartnerCredentialService partnerCredentialService, TransaksiService transaksiService, ObjectMapper objectMapper, CloseableHttpClient client, CetakUlangService cetakUlangService, SlackSendMessage slackSendMessage, ProductItemService productItemService) {
        this.partnerDepositService = partnerDepositService;
        this.partnerCredentialService = partnerCredentialService;
        this.transaksiService = transaksiService;
        this.objectMapper = objectMapper;
        this.client = client;
        this.cetakUlangService = cetakUlangService;
        this.slackSendMessage = slackSendMessage;
        this.productItemService = productItemService;
    }

    @Path("/saldo")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response saldo(@Context ContainerRequestContext security) {
        PartnerCredential partnerCredential = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        if (partnerCredential != null) {
            PartnerDeposit partnerDeposit = partnerDepositService.findById(partnerCredential.getPartner_id());
            if (partnerDeposit != null) {
                return Response.status(200).entity(partnerDeposit).build();
            } else {
                return Response.noContent().build();
            }
        } else {
            return Response.noContent().build();
        }

    }

    @Path("/recon/{ntrans}")
    @GET
    public Response recon(@Valid @PathParam("ntrans") String ntrans) {
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            PartnerCredential partnerCredential = partnerCredentialService.findById(transaksi.getUSERID());
            httpPost = new HttpPost(partnerCredential.getBack_link());
            try {
                partnerDepositService.reverseSaldo(transaksi, "");
                ResponseCode response = ResponseCode.PAYMENT_FAILED.setProduct(transaksi.getDENOM()).setMssidn(transaksi.getMSSIDN()).setNtrans(transaksi.getNTRANS()).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(response));
                httpPost.setEntity(entity);
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse r;
                r = client.execute(httpPost);
                String respone = EntityUtils.toString(r.getEntity(), StandardCharsets.UTF_8.name());
                logger.error("<-- REPORT START  --> ");
                logger.error("SEND TO PARTNER    -> " + "[" + partnerCredential.getPartner_id() + "|" + partnerCredential.getPartner_uid() + "] ");
                logger.error("SEND TO API        -> " + partnerCredential.getBack_link());
                logger.error("WITH BODY          -> " + objectMapper.writeValueAsString(response));
                logger.error("RESPONSE BY CLIENT -> " + respone);
                logger.error("<-- REPORT END    --> ");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return Response.ok("SUKSES").build();
        } else {
            return Response.ok("GAGAL").build();
        }
    }


    @Path("/send/report/{ntrans}")
    @GET
    public void send_report(@Suspended AsyncResponse asyncResponse, @Valid @PathParam("ntrans") String ntrans) {
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null) {
            partnerCredential = partnerCredentialService.findById(transaksi.getUSERID());
            httpPost = new HttpPost(partnerCredential.getBack_link());
            ProductItem productItem = productItemService.findById(transaksi.getDENOM());
            System.out.println(transaksi);
            switch (transaksi.getST()) {
                case Status.PAYMENT_FAILED:
                    sendToMitra(asyncResponse, Response.status(200)
                            .entity(ResponseCode.PAYMENT_FAILED.setMssidn(transaksi.getMSSIDN()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                    break;
                case Status.PAYMENT_SUCCESS:
                    switch (productItem.getFk_biller()) {
                        case "001":
                            switch (productItem.getROUTE()) {
                                case ARANET:
                                    switch (productItem.getProduct_id()) {
                                        case "99501":
                                            sendToMitra(asyncResponse, cetakUlangService.cetakUlangPlnPostpaidAranet(transaksi));
                                            break;
                                        case "99502":
                                            sendToMitra(asyncResponse, cetakUlangService.cetakUlangPlnPrepaidAranet(transaksi));
                                            break;
                                        case "99504":
                                            sendToMitra(asyncResponse, cetakUlangService.cetakUlangPlnNontaglistAranet(transaksi));
                                            break;
                                    }
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                    break;
                            }
                            break;
                        case "002":
                            try {
                                switch (productItem.getROUTE()) {
                                    case RAJABILLER:
                                        Response response = cetakUlangService.cetakUlangTelkomRajabiller(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    default:
                                        asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                        break;
                                }
                            } catch (JAXBException e) {
                                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                            }
                            break;
                        case "003":
                            try {
                                Response response;
                                switch (productItem.getROUTE()) {
                                    case RAJABILLER:
                                        response = cetakUlangService.cetakUlangPdamRajabiller(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    case PDAM_TASIK:
                                        response = cetakUlangService.cetakUlangPdamTasik(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    default:
                                        asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                        break;
                                }

                            } catch (JAXBException | IOException e) {
                                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                            }
                            break;
                        case "004":
                            try {
                                Response response;
                                switch (productItem.getROUTE()) {
                                    case SERVINDO:
                                        response = cetakUlangService.cetakUlangPulsaServindo(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    case DI:
                                        response = cetakUlangService.cetakUlangPulsaDI(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    default:
                                        asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                        break;
                                }

                            } catch (IOException e) {
                                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                            }
                            break;
                        case "005":
                            try {
                                Response response;
                                switch (productItem.getROUTE()) {
                                    case SERVINDO:
                                        response = cetakUlangService.cetakUlangDataServindo(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    default:
                                        asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                        break;
                                }
                            } catch (IOException e) {
                                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                            }
                            break;
                        case "006":
                            switch (productItem.getROUTE()) {
                                case VSI:
                                    cetakUlangService.cetakUlangMultifinance(asyncResponse, transaksi);
                                    break;
                                default:
                                    asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                    break;
                            }
                            break;
                        case "007":
                            try {
                                Response response;
                                switch (productItem.getROUTE()) {
                                    case RAJABILLER:
                                        response = cetakUlangService.cetakUlangPulsaPascaRajabiller(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    default:
                                        asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                        break;
                                }

                            } catch (JAXBException e) {
                                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                            }
                            break;
                        case "008":
                            try {
                                Response response;
                                switch (productItem.getROUTE()) {
                                    case RAJABILLER:
                                        response = cetakUlangService.cetakUlangBpjsRajabiller(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    default:
                                        asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                        break;
                                }

                            } catch (JAXBException e) {
                                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                            }
                            break;
                        case "010":
                            try {
                                Response response;
                                switch (productItem.getROUTE()) {
                                    case DI:
                                        response = cetakUlangService.cetakUlangTvDharmawisata(transaksi);
                                        sendToMitra(asyncResponse, response);
                                        break;
                                    default:
                                        asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                                        break;
                                }
                            } catch (IOException e) {
                                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                            }
                            break;
                    }
                    break;
                case Status.INQUIRY:
                    asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK VALID").build());
                    break;
                default:
                    asyncResponse.resume(Response.status(200).entity("TRANSAKSI MASIH PROSES").build());
                    break;
            }
        } else {
            asyncResponse.resume(Response.status(200).entity("TRANSAKSI TIDAK DITEMUKAN").build());
        }
    }

    private void sendToMitra(@Suspended AsyncResponse asyncResponse, Response response) {
        try {
            String content = objectMapper.writeValueAsString(response.getEntity());
            StringEntity entity = new StringEntity(content);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            CloseableHttpResponse r;
            r = client.execute(httpPost);
            String respone = EntityUtils.toString(r.getEntity(), StandardCharsets.UTF_8.name());
            logger.error("<-- REPORT START  --> ");
            logger.error("SEND TO PARTNER    -> " + "[" + partnerCredential.getPartner_id() + "|" + partnerCredential.getPartner_uid() + "] ");
            logger.error("SEND TO API        -> " + partnerCredential.getBack_link());
            logger.error("WITH BODY          -> " + objectMapper.writeValueAsString(response.getEntity()));
            logger.error("RESPONSE BY CLIENT -> " + respone);
            logger.error("<-- REPORT END    --> ");
            asyncResponse.resume(Response.status(200).entity("OK").build());

        } catch (Exception e) {
            slackSendMessage.sendMessage(e.getLocalizedMessage());
        }
    }
}
