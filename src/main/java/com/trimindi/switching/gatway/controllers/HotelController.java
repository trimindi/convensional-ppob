package com.trimindi.switching.gatway.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.biller.dw.HotelService;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.booking.request.SetHotelBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.booking.response.SetHotelBookingResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.request.GetHotelSearchRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.city.request.GetHotelCityRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.images.request.GetHotelImageRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.issued.SetIssuedHotelRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.request.GetHotelPriceRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.util.Objects;

/**
 * Created by sx on 26/01/18.
 * Copyright under comercial unit
 */
@Path("/hotel")
@Component
@Scope(value = "request")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class HotelController {
    public static final Logger logger = LoggerFactory.getLogger(HotelController.class);
    /**
     * Get Hotel Country List
     * Operation to get list country to be used in hotel API request.
     * Get Hotel Passport List
     * Operation to get list passport to be used in airline API request.
     * Get Hotel City List
     * Operation to get list city to be used in airline API request.
     * Search Available Hotel
     * Operation to get list of available hotel via API.
     * Get Hotel Image List
     * Operation to get hotel image list.
     * Get Hotel Price Detail, Commission and Policy Info
     * Operation to get detail of price, commission and cancellation policy of hotel.
     * Set Hotel Booking
     * Operation to set booking to room selected. Get List Hotel Booking
     * Operation to get agent’s list of hotel booking via API.
     * Get Hotel Booking Detail
     * Operation to get information detail of agent’s hotel booking transaction via API.
     * Get Hotel Detail Information
     * Operation to get information detail of hotel via API.
     */

    public static final Logger LOGGER = LoggerFactory.getLogger(ShipController.class);

    @Autowired
    HotelService hotelService;
    @Context
    private ContainerRequestContext security;
    private PartnerCredential p;
    @Autowired
    private TransaksiService transaksiService;
    @Autowired
    private PartnerDepositService partnerDepositService;
    @Autowired
    private ObjectMapper objectMapper;

    @POST
    @Path("/Country")
    public Response GetHotelCountryList() {
        return hotelService.GetHotelCountryList();
    }

    @POST
    @Path("/Passport")
    public Response GetHotelPassportList() {
        return hotelService.GetHotelPassportList();
    }

    @POST
    @Path("/City")
    public Response GetHotelCityList(@Valid GetHotelCityRequest getHotelCityRequest) {
        return hotelService.GetHotelCityList(getHotelCityRequest);
    }

    @POST
    @Path("/SearchAvailableHotel")
    public Response SearchAvailableHotel(@Valid GetHotelSearchRequest getHotelSearchRequest) {
        return hotelService.SearchAvailableHotel(getHotelSearchRequest);
    }

    @POST
    @Path("/ImageList")
    public Response GetHotelImageList(@Valid GetHotelImageRequest getHotelImageRequest) {
        return hotelService.GetHotelImageList(getHotelImageRequest);
    }

    @POST
    @Path("/Price")
    public Response GetHotelPrice(@Valid GetHotelPriceRequest getHotelPriceRequest) {
        return hotelService.GetHotelPrice(getHotelPriceRequest);
    }

    @POST
    @Path("/Booking")
    public Response SetHotelBooking(@Valid SetHotelBookingRequest setHotelBookingRequest) {
        p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        return hotelService.SetHotelBooking(setHotelBookingRequest, p);
    }

    @POST
    @Path("/Issued/{ntrans}")
    public Response SetIssuedHotel(@PathParam("ntrans") final String ntrans) throws IOException, ParseException {
        Response rx = null;
        Transaksi transaksi = transaksiService.findById(ntrans);
        if (transaksi != null && Objects.equals(transaksi.getST(), Status.INQUIRY)) {
            if (partnerDepositService.bookingSaldo(transaksi)) {
                SetHotelBookingResponse setHotelBookingResponse = this.objectMapper.readValue(transaksi.getINQUIRY(), SetHotelBookingResponse.class);
                SetIssuedHotelRequest setIssuedTrainRequest = new SetIssuedHotelRequest();
                setIssuedTrainRequest.setReservationNo(setHotelBookingResponse.getReservationNo());
                rx = hotelService.SetIssuedHotel(setIssuedTrainRequest, transaksi);
            } else {
                rx = Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();
            }
        } else {
            rx = Response.status(200).entity(ResponseCode.NTRANS_NOT_FOUND.setNtrans(transaksi.getNTRANS())).build();
        }
        return rx;
    }
    @POST
    @Path("/BookingDetail/{ntrans}")
    public Response GetHotelBookingDetail(@PathParam("ntrans") String ntrans) {
        Transaksi transaksi = transaksiService.findById(ntrans);
        return hotelService.GetHotelBookingDetail(transaksi);
    }
}
