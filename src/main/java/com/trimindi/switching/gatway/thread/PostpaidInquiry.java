package com.trimindi.switching.gatway.thread;

import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.PlnProperties;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.client.IsoMessageListener;
import com.trimindi.switching.gatway.client.client.Iso8583Client;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.models.*;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.postpaid.Inquiry;
import com.trimindi.switching.gatway.response.postpaid.Rincian;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.PostpaidHelper;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import com.trimindi.switching.gatway.utils.generator.MessageGenerator;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorPostPaid;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by PC on 06/09/2017.
 */
@Component
@Scope(value = "prototype")
public class PostpaidInquiry extends Thread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(NontaglisInquiry.currentThread().getName());
    private final Map<String, ResponseCode> failedCode;
    private final Map<String, ResponseCode> responseCode;
    private final Iso8583Client<IsoMessage> iso8583Client;
    private final MessageGenerator messageGenerator;
    private final PlnProperties config;
    private final ProductFeeService productFeeService;
    private final TransaksiService transaksiService;
    private final PartnerDepositService partnerDepositService;
    private Map<String, String> params;
    private IsoMessage inquiryRequest;
    private Transaksi transaksi = null;
    private AsyncResponse asyncResponse;
    private PartnerCredential partnerCredential;
    private ProductItem productItem;
    private ProductFee productFee;
    private Request request;
    private String dueDate;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public PostpaidInquiry(@Qualifier("aranetFailedCode") Map<String, ResponseCode> failedCode,
                           @Qualifier("aranetResponseCode") Map<String, ResponseCode> responseCode,
                           Iso8583Client<IsoMessage> iso8583Client,
                           MessageGenerator messageGenerator,
                           PlnProperties config,
                           ProductFeeService productFeeService,
                           TransaksiService transaksiService,
                           PartnerDepositService partnerDepositService) {
        this.failedCode = failedCode;
        this.responseCode = responseCode;
        this.iso8583Client = iso8583Client;
        this.messageGenerator = messageGenerator;
        this.config = config;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
    }

    public void init(
            @Suspended AsyncResponse asyncResponse,
            ProductItem productItem,
            PartnerCredential partnerCredential,
            Map<String, String> params,
            Request request
    ) {
        this.productItem = productItem;
        this.asyncResponse = asyncResponse;
        this.partnerCredential = partnerCredential;
        this.params = params;
        this.request = request;
    }

    @Override
    public void run() {
        super.run();
        try {
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage != null && isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(2).equals(config.PAN_POSTPAID) && isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    try {
                        if (isoMessage.getObjectValue(39).equals("0000")) {
                            transaksi = new Transaksi();
                            boolean status = true;
                            String inquiryRes = isoMessage.getObjectValue(48);
                            List<Rules> bit48 = new SDE.Builder()
                                    .setPayload(inquiryRes)
                                    .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48, status))
                                    .generate();
                            int legtht = new SDE.Builder()
                                    .setPayload(inquiryRes)
                                    .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48, status))
                                    .calculate();

                            String rincian = inquiryRes.substring(legtht, inquiryRes.length());
                            Inquiry inquiryResponse = new Inquiry(bit48, true);
                            List<Rincian> rincians = new ArrayList<>();
                            int start = 0;
                            int leghtRincian = 115;
                            double total = 0;
                            double denda = 0;
                            for (int i = 0; i < inquiryResponse.getBillStatus(); i++) {
                                String parsRincian = rincian.substring(start, start + leghtRincian);
                                List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                                Rincian r = new Rincian(rc);
                                rincians.add(r);
                                dueDate = r.getDueDate();
                                total += r.getTotalElectricityBill();
                                denda += r.getPenaltyFee();
                                start += leghtRincian;

                            }
                            total += denda;
                            inquiryResponse.setRincian(rincians);
                            /**
                             * MT = MACHINE_TYPE
                             * PC = PRODUDUCT CODE
                             * PRODUCT = PRODUCT
                             */
                            ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), productItem.getDENOM());
                            PartnerDeposit partnerDeposit = partnerDepositService.findById(partnerCredential.getPartner_id());
                            transaksi
                                    .setSTAN(isoMessage.getObjectValue(11))
                                    .setADMIN(inquiryResponse.getAdmin())
                                    .setMSSIDN_NAME(inquiryResponse.getSubscriberName())
                                    .setBILL_REF_NUMBER(inquiryResponse.getBukopinTbkReferenceNumber())
                                    .setMSSIDN(inquiryResponse.getSubscriberID())
                                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                    .setPRODUCT(params.get(Constanta.PRODUCT_CODE))
                                    .setTAGIHAN(total)
                                    .setDENOM(productItem.getDENOM())
                                    .setPRODUCT(productItem.getProduct_id())
                                    .setFEE_CA(productFee.getFEE_CA() * inquiryResponse.getBillStatus())
                                    .setFEE_DEALER((productItem.getFEE_BILLER() - productFee.getFEE_CA()) * inquiryResponse.getBillStatus())
                                    .setUSERID(partnerCredential.getPartner_uid())
                                    .setINQUIRY(isoMessage.debugString())
                                    .setCHARGE(total + inquiryResponse.getAdmin() - ((productItem.getFEE_BILLER() - productFee.getFEE_CA()) * inquiryResponse.getBillStatus()))
                                    .setDEBET(total + inquiryResponse.getAdmin() - ((productItem.getFEE_BILLER() - productFee.getFEE_CA()) * inquiryResponse.getBillStatus()))
                                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                    .setPARTNERID(partnerCredential.getPartner_id())
                                    .setDENDA(denda)
                                    .setST(Status.INQUIRY);
                            transaksiService.save(transaksi);
                            inquiryResponse.setPeriod(PostpaidHelper.generatePeriode(rincians));
                            inquiryResponse.setMeter(PostpaidHelper.generateStandMeter(rincians));
                            inquiryResponse.setTagihan(transaksi.getTAGIHAN());
                            inquiryResponse.setDenda(denda);
                            inquiryResponse.setDuedate(dueDate);
                            BaseResponse<Inquiry> inquiryBaseResponse = new BaseResponse<>();
                            inquiryBaseResponse.setMessage("Successful");
                            inquiryBaseResponse.setNtrans(transaksi.getNTRANS());
                            inquiryBaseResponse.setData(inquiryResponse);
                            inquiryBaseResponse.setSaldo(partnerDeposit.getBALANCE());
                            inquiryBaseResponse.setProduct(transaksi.getDENOM());
                            inquiryBaseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                            inquiryBaseResponse.setFee(inquiryResponse.getBillStatus() * (productItem.getFEE_BILLER() - productFee.getFEE_CA()));
                            asyncResponse.resume(
                                    Response.status(200)
                                            .entity(inquiryBaseResponse)
                                            .build()
                            );
                        } else {
                            asyncResponse.resume(
                                    Response.status(200)
                                            .entity(responseCode.get(isoMessage.getObjectValue(39).toString()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(params.get(Constanta.DENOM)))
                                            .build()
                            );
                        }
                    } catch (Exception e) {
                        slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                    }
                    return false;
                }
            });
            inquiryRequest = messageGenerator.inquiryPostpaid(params.get(Constanta.MSSIDN), params.get(Constanta.MACHINE_TYPE));
            if (!iso8583Client.isConnected()) {
                iso8583Client.connectAsync().sync().await();
            }
            iso8583Client.sendAsync(inquiryRequest);
        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(params.get(Constanta.DENOM))));
        }
    }
}
