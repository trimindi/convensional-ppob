package com.trimindi.switching.gatway.thread.multifinance;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.VSIPropoerties;
import com.trimindi.switching.gatway.client.IsoMessageListener;
import com.trimindi.switching.gatway.client.client.Iso8583ClientCustom;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.controllers.ReschedulableTimer;
import com.trimindi.switching.gatway.models.*;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.mutifinance.Payment;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.generator.VSIMessageGenerator;
import com.trimindi.switching.gatway.utils.iso.FieldBuilder;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorMultifinance;
import io.netty.channel.ChannelHandlerContext;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.trimindi.switching.gatway.utils.constanta.Status.PAYMENT_PROSESS;
import static com.trimindi.switching.gatway.utils.constanta.Status.PAYMENT_SUCCESS;

/**
 * Created by PC on 06/09/2017.
 */
@Component
@Scope(value = "prototype")
public class MultifinancePayment extends Thread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(MultifinanceInquiry.currentThread().getName());
    private final Map<String, ResponseCode> failedCode;
    private final Map<String, ResponseCode> responseCode;
    private final VSIMessageGenerator vsiMessageGenerator;
    private final VSIPropoerties config;
    private final ProductFeeService productFeeService;
    private final TransaksiService transaksiService;
    private final PartnerDepositService partnerDepositService;
    private final ObjectMapper objectMapper;
    private final CloseableHttpClient client;
    private ReschedulableTimer reschedulableTimer;
    private final Iso8583ClientCustom<IsoMessage> iso8583Client;
    private HttpPost httpPost;
    private Map<String, String> params;
    private IsoMessage paymentRequest;
    private Transaksi transaksi;
    private AsyncResponse asyncResponse;
    private PartnerCredential partnerCredential;
    private ProductItem productItem;
    private ProductFee productFee;
    private IsoMessage reversalRequest = null;
    private Request request;
    private String reversal;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public MultifinancePayment(VSIMessageGenerator vsiMessageGenerator,
                               @Qualifier("vsiFailedCode") Map<String, ResponseCode> failedCode,
                               @Qualifier("vsiResponseCode") Map<String, ResponseCode> responseCode,
                               VSIPropoerties config,
                               ProductFeeService productFeeService,
                               TransaksiService transaksiService,
                               PartnerDepositService partnerDepositService,
                               ObjectMapper objectMapper,
                               CloseableHttpClient client,
                               Iso8583ClientCustom<IsoMessage> iso8583Client) {
        this.vsiMessageGenerator = vsiMessageGenerator;
        this.failedCode = failedCode;
        this.responseCode = responseCode;
        this.config = config;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
        this.objectMapper = objectMapper;
        this.client = client;
        this.iso8583Client = iso8583Client;
    }

    public void init(
            @Suspended AsyncResponse asyncResponse,
            ProductItem productItem,
            PartnerCredential partnerCredential,
            Map<String, String> params,
            Request request,
            Transaksi transaksi
    ) {
        this.transaksi = transaksi;
        this.productItem = productItem;
        this.asyncResponse = asyncResponse;
        this.partnerCredential = partnerCredential;
        this.params = params;
        this.request = request;
        this.reschedulableTimer = new ReschedulableTimer();

    }

    @Override
    public void run() {
        super.run();
        try {
            httpPost = new HttpPost(params.get(Constanta.BACK_LINK));
            /**
             * nontaglist paymentRequest
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage != null && isoMessage.getType() == 0x2210 &&
                            isoMessage.getObjectValue(11).equals(paymentRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        if (reversalRequest == null) {
                            reschedulableTimer.stop();
                            boolean status = true;
                            List<Rules> bit48 = new SDE.Builder()
                                    .setPayload(isoMessage.getObjectValue(48))
                                    .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(48, status))
                                    .generate();
                            List<Rules> bit62 = new SDE.Builder()
                                    .setPayload(isoMessage.getObjectValue(62))
                                    .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(62, status))
                                    .generate();
                            bit48.addAll(bit62);
                            Payment paymentResponse = new Payment(bit48);
                            transaksi.setPAYMENT(isoMessage.debugString()).setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                                    .setST(PAYMENT_SUCCESS)
                                    .setBILL_REF_NUMBER(paymentResponse.getSWReferenceNumber());
                            transaksiService.save(transaksi);
                            BaseResponse<Payment> baseResponse = new BaseResponse<>();
                            PartnerDeposit partnerDeposit = partnerDepositService.findById(transaksi.getPARTNERID());
                            baseResponse.setData(paymentResponse);
                            baseResponse.setFee(transaksi.getFEE_DEALER());
                            baseResponse.setSaldo(partnerDeposit.getBALANCE());
                            baseResponse.setProduct(transaksi.getDENOM());
                            baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                            baseResponse.setNtrans(transaksi.getNTRANS());
                            baseResponse.setMessage("Successful");
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(baseResponse)
                                            .build()
                            );
                        }
                    } else {
                        if (failedCode.containsKey(isoMessage.getObjectValue(39).toString())) {
                            if (reschedulableTimer.getStep() <= 3) {
                                reversalRequest = vsiMessageGenerator.revesalMultifinance(paymentRequest, 0x2400, reversal);
//                                ctx.writeAndFlush(reversalRequest);
                                try {
                                    iso8583Client.send(reversalRequest);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                reschedulableTimer.reschedule();
                            } else {
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, isoMessage.debugString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED
                                                        .setNtrans(transaksi.getNTRANS())
                                                        .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }

                        } else {
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, isoMessage.debugString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(ResponseCode.PAYMENT_FAILED
                                                    .setNtrans(transaksi.getNTRANS())
                                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    }
                    return false;
                }
            });
            /**
             * nontaglist reverasl
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage != null && (isoMessage.getType() == 0x2410 || isoMessage.getType() == 0x2411) &&
                            isoMessage.getObjectValue(11).equals(paymentRequest.getObjectValue(11));

                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        reschedulableTimer.stop();
                        partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                        sendBack(asyncResponse,
                                Response.status(200)
                                        .entity(ResponseCode.PAYMENT_FAILED
                                                .setNtrans(transaksi.getNTRANS())
                                                .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                        .build()
                        );
                    } else {
                        if (failedCode.containsKey(isoMessage.getObjectValue(39).toString())) {
                            if (reschedulableTimer.getStep() <= 3) {
                                switch (isoMessage.getType()) {
                                    case 0x2410:
                                        reversalRequest = vsiMessageGenerator.revesalMultifinance(paymentRequest, 0x2401, reversal);
                                        break;
                                    case 0x2411:
                                        reversalRequest = vsiMessageGenerator.revesalMultifinance(paymentRequest, 0x2401, reversal);
                                        break;
                                }
                                if (reversalRequest != null) {
                                    try {
                                        iso8583Client.send(reversalRequest);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
//                                    ctx.writeAndFlush(reversalRequest);
                                    reschedulableTimer.reschedule();
                                }
                            } else {
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED
                                                        .setNtrans(transaksi.getNTRANS())
                                                        .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }
                        } else {
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(responseCode.get(isoMessage.getObjectValue(39).toString())
                                                    .setNtrans(transaksi.getNTRANS())
                                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    }
                    return false;
                }
            });

            paymentRequest = vsiMessageGenerator.paymentMultifinance(transaksi);
            reversal = new FieldBuilder.Builder()
                    .addValue("2200", 4, "0", "L")
                    .addValue(paymentRequest.getObjectValue(11), 12, "0", "L")
                    .addValue(paymentRequest.getObjectValue(12), 14, "0", "L")
                    .addValue(paymentRequest.getObjectValue(32), 7, "0", "L")
                    .build();
            iso8583Client.send(paymentRequest);
            reschedulableTimer.schedule(() -> {
                if (transaksi != null && Objects.equals(transaksi.getST(), PAYMENT_PROSESS)) {
                    switch (reschedulableTimer.getStep()) {
                        case 1:
                            reversalRequest = vsiMessageGenerator.revesalMultifinance(paymentRequest, 0x2400, reversal);
                            try {
                                iso8583Client.send(reversalRequest);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            reschedulableTimer.reschedule();
                            break;
                        case 2:
                            reversalRequest = vsiMessageGenerator.revesalMultifinance(paymentRequest, 0x2401, reversal);
                            try {
                                iso8583Client.send(reversalRequest);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            reschedulableTimer.reschedule();
                            break;
                        case 3:
                            reversalRequest = vsiMessageGenerator.revesalMultifinance(paymentRequest, 0x2401, reversal);
                            try {
                                iso8583Client.send(reversalRequest);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            reschedulableTimer.reschedule();
                            break;
                        default:
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(ResponseCode.PAYMENT_FAILED
                                                    .setNtrans(transaksi.getNTRANS())
                                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                            break;
                    }
                } else {
                    reschedulableTimer.stop();
                }
            });
        } catch (InterruptedException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            partnerDepositService.reverseSaldo(transaksi, "Cant connect to server ");
            sendBack(asyncResponse,
                    Response
                            .status(200)
                            .entity(ResponseCode
                                    .SERVER_UNAVAILABLE
                                    .setNtrans(transaksi.getNTRANS())
                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE())
                                    .setProduct(params.get(Constanta.DENOM))
                                    .setMssidn(params.get(Constanta.MSSIDN)))
                            .build());
        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            sendBack(asyncResponse,
                    Response
                            .status(200)
                            .entity(ResponseCode
                                    .SERVER_UNAVAILABLE
                                    .setNtrans(transaksi.getNTRANS())
                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE())
                                    .setProduct(params.get(Constanta.DENOM))
                                    .setMssidn(params.get(Constanta.MSSIDN)))
                            .build());
        }
    }

    private void sendBack(@Suspended AsyncResponse response, Response r) {
        if (response.isSuspended()) {
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse res;
                res = client.execute(httpPost);
                String respone = EntityUtils.toString(res.getEntity(), StandardCharsets.UTF_8.name());
                logger.error("SEND TO            -> {}", partnerCredential.getPartner_id());
                logger.error("WITH BODY          -> {}", objectMapper.writeValueAsString(r.getEntity()));
                logger.error("RESPONSE BY CLIENT -> {}", respone);
            } catch (IOException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            }
        }
    }
}
