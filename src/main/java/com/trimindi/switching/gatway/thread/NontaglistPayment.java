package com.trimindi.switching.gatway.thread;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.PlnProperties;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.client.IsoMessageListener;
import com.trimindi.switching.gatway.client.client.Iso8583Client;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.controllers.ReschedulableTimer;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.PartnerDeposit;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.nontaglist.Payment;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.generator.MessageGenerator;
import com.trimindi.switching.gatway.utils.iso.FieldBuilder;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorNonTagList;
import io.netty.channel.ChannelHandlerContext;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.trimindi.switching.gatway.utils.constanta.Status.PAYMENT_PROSESS;
import static com.trimindi.switching.gatway.utils.constanta.Status.PAYMENT_SUCCESS;

/**
 * Created by PC on 06/09/2017.
 */
@Component
@Scope(value = "prototype")
public class NontaglistPayment extends Thread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(NontaglistPayment.currentThread().getName());
    private final Map<String, ResponseCode> failedCode;
    private final Map<String, ResponseCode> responseCode;
    private final Iso8583Client<IsoMessage> iso8583Client;
    private final MessageGenerator messageGenerator;
    private final PlnProperties config;
    private final ProductFeeService productFeeService;
    private final TransaksiService transaksiService;
    private final PartnerDepositService partnerDepositService;
    private final ObjectMapper objectMapper;
    private final CloseableHttpClient client;
    private ReschedulableTimer reschedulableTimer;
    private HttpPost httpPost;
    private Map<String, String> params;
    private IsoMessage paymentRequest;
    private Transaksi transaksi;
    private AsyncResponse asyncResponse;
    private PartnerCredential partnerCredential;
    private ProductItem productItem;
    private IsoMessage reversalRequest;
    private Request request;
    private String reversal;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public NontaglistPayment(@Qualifier("aranetResponseCode") Map<String, ResponseCode> responseCode,
                             @Qualifier("aranetFailedCode") Map<String, ResponseCode> failedCode,
                             Iso8583Client<IsoMessage> iso8583Client,
                             MessageGenerator messageGenerator,
                             PlnProperties config,
                             ProductFeeService productFeeService,
                             TransaksiService transaksiService,
                             PartnerDepositService partnerDepositService,
                             ObjectMapper objectMapper, CloseableHttpClient client
    ) {
        this.responseCode = responseCode;
        this.failedCode = failedCode;
        this.iso8583Client = iso8583Client;
        this.messageGenerator = messageGenerator;
        this.config = config;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
        this.objectMapper = objectMapper;
        this.client = client;
    }


    public void init(
            @Suspended AsyncResponse asyncResponse,
            ProductItem productItem,
            PartnerCredential partnerCredential,
            Map<String, String> params,
            Request request,
            Transaksi transaksi
    ) {
        this.transaksi = transaksi;
        this.productItem = productItem;
        this.asyncResponse = asyncResponse;
        this.partnerCredential = partnerCredential;
        this.params = params;
        this.request = request;
        this.reschedulableTimer = new ReschedulableTimer();
    }

    @Override
    public void run() {
        super.run();

        try {
            httpPost = new HttpPost(params.get(Constanta.BACK_LINK));
            /**
             * nontaglist paymentRequest
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage != null && isoMessage.getType() == 0x2210 && isoMessage.getObjectValue(2).equals(config.PAN_NONTAGLIST) &&
                            isoMessage.getObjectValue(11).equals(paymentRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        if (reversalRequest == null) {
                            reschedulableTimer.stop();
                            boolean status = true;
                            List<Rules> rule = parsingRulesNontaglist(isoMessage, status);
                            Payment paymentResponse = new Payment(rule);
                            transaksi.setPAYMENT(isoMessage.debugString()).setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                                    .setST(PAYMENT_SUCCESS)
                                    .setBILL_REF_NUMBER(paymentResponse.getBukopinReferenceNumber());
                            transaksiService.save(transaksi);
                            BaseResponse<Payment> baseResponse = new BaseResponse<>();
                            paymentResponse.setTagihan(transaksi.getTAGIHAN());
                            PartnerDeposit partnerDeposit = partnerDepositService.findById(transaksi.getPARTNERID());
                            baseResponse.setData(paymentResponse);
                            baseResponse.setFee(transaksi.getFEE_DEALER());
                            baseResponse.setSaldo(partnerDeposit.getBALANCE());
                            baseResponse.setProduct(transaksi.getDENOM());
                            baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                            baseResponse.setNtrans(transaksi.getNTRANS());
                            baseResponse.setMessage("Successful");
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(baseResponse)
                                            .build()
                            );
                        }
                    } else {
                        if (failedCode.containsKey(isoMessage.getObjectValue(39).toString())) {
                            if (reschedulableTimer.getStep() <= 3) {
                                reversalRequest = messageGenerator.revesalNontaglist(paymentRequest, 0x2400, reversal);
                                ctx.writeAndFlush(reversalRequest);
                                reschedulableTimer.reschedule();
                            } else {
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, isoMessage.debugString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED
                                                        .setNtrans(transaksi.getNTRANS())
                                                        .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }

                        } else {
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, isoMessage.debugString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(ResponseCode.PAYMENT_FAILED
                                                    .setNtrans(transaksi.getNTRANS())
                                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    }
                    return false;
                }
            });
            /**
             * nontaglist reverasl
             */
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage != null && (isoMessage.getType() == 0x2410 || isoMessage.getType() == 0x2411) && isoMessage.getObjectValue(2).equals(config.PAN_NONTAGLIST) &&
                            isoMessage.getObjectValue(11).equals(paymentRequest.getObjectValue(11));

                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        reschedulableTimer.stop();
                        partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                        sendBack(asyncResponse,
                                Response.status(200)
                                        .entity(ResponseCode.PAYMENT_FAILED
                                                .setNtrans(transaksi.getNTRANS())
                                                .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                        .build()
                        );
                    } else {
                        if (failedCode.containsKey(isoMessage.getObjectValue(39).toString())) {
                            if (reschedulableTimer.getStep() <= 3) {
                                switch (isoMessage.getType()) {
                                    case 0x2410:
                                        reversalRequest = messageGenerator.revesalNontaglist(paymentRequest, 0x2401, reversal);
                                        break;
                                    case 0x2411:
                                        reversalRequest = messageGenerator.revesalNontaglist(paymentRequest, 0x2401, reversal);
                                        break;
                                }
                                if (reversalRequest != null) {
                                    ctx.writeAndFlush(reversalRequest);
                                    reschedulableTimer.reschedule();
                                }
                            } else {
                                reschedulableTimer.stop();
                                partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                                sendBack(asyncResponse,
                                        Response.status(200)
                                                .entity(ResponseCode.PAYMENT_FAILED
                                                        .setNtrans(transaksi.getNTRANS())
                                                        .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                                .build()
                                );
                            }
                        } else {
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(responseCode.get(isoMessage.getObjectValue(39).toString())
                                                    .setNtrans(transaksi.getNTRANS())
                                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                        }
                    }
                    return false;
                }
            });
            paymentRequest = messageGenerator.paymentNontaglist(transaksi);
            if (!iso8583Client.isConnected()) {
                iso8583Client.connectAsync().sync().await();
            }
            iso8583Client.sendAsync(paymentRequest);
            reversal = new FieldBuilder.Builder()
                    .addValue("2200", 4, "0", "L")
                    .addValue(paymentRequest.getObjectValue(11), 12, "0", "L")
                    .addValue(paymentRequest.getObjectValue(12), 14, "0", "L")
                    .addValue(paymentRequest.getObjectValue(32), 7, "0", "L")
                    .build();
            reschedulableTimer.schedule(() -> {
                if (transaksi != null && Objects.equals(transaksi.getST(), PAYMENT_PROSESS)) {
                    switch (reschedulableTimer.getStep()) {
                        case 1:
                            reversalRequest = messageGenerator.revesalNontaglist(paymentRequest, 0x2400, reversal);
                            iso8583Client.sendAsync(reversalRequest);
                            reschedulableTimer.reschedule();
                            break;
                        case 2:
                            reversalRequest = messageGenerator.revesalNontaglist(paymentRequest, 0x2401, reversal);
                            iso8583Client.sendAsync(reversalRequest);
                            reschedulableTimer.reschedule();
                            break;
                        case 3:
                            reversalRequest = messageGenerator.revesalNontaglist(paymentRequest, 0x2401, reversal);
                            iso8583Client.sendAsync(reversalRequest);
                            reschedulableTimer.reschedule();
                            break;
                        default:
                            reschedulableTimer.stop();
                            partnerDepositService.reverseSaldo(transaksi, reversalRequest.debugString());
                            sendBack(asyncResponse,
                                    Response.status(200)
                                            .entity(ResponseCode.PAYMENT_FAILED
                                                    .setNtrans(transaksi.getNTRANS())
                                                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                            .build()
                            );
                            break;
                    }
                } else {
                    reschedulableTimer.stop();
                }
            });
        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            sendBack(asyncResponse,
                    Response
                            .status(200)
                            .entity(
                                    ResponseCode.SERVER_UNAVAILABLE
                                            .setNtrans(transaksi.getNTRANS())
                                            .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN))).build());
        }
    }

    private List<Rules> parsingRulesNontaglist(IsoMessage d, boolean status) {
        try {
            List<Rules> bit48 = new SDE.Builder()
                    .setPayload(d.getObjectValue(48))
                    .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(48, status))
                    .generate();
            List<Rules> bit61 = new SDE.Builder()
                    .setPayload(d.getObjectValue(61))
                    .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(61, status))
                    .generate();
            List<Rules> bit62 = new SDE.Builder()
                    .setPayload(d.getObjectValue(62))
                    .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(62, status))
                    .generate();
            bit48.addAll(bit61);
            bit48.addAll(bit62);
            if (d.hasField(63)) {
                bit48.add(new Rules(d.getObjectValue(63)));
            } else {
                bit48.add(new Rules("\"Informasi Hubungi Call Center 123 Atau Hub PLN Terdekat :\""));
            }
            return bit48;
        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        return null;
    }

    private void sendBack(@Suspended AsyncResponse response, Response r) {
        if (response.isSuspended()) {
            response.resume(r);
        } else {
            try {
                StringEntity entity = new StringEntity(objectMapper.writeValueAsString(r.getEntity()));
                httpPost.setEntity(entity);
                httpPost.setHeader("Content-type", "application/json");
                CloseableHttpResponse res;
                res = client.execute(httpPost);
                String respone = EntityUtils.toString(res.getEntity(), StandardCharsets.UTF_8.name());
                logger.error("SEND TO            -> {}", partnerCredential.getPartner_id());
                logger.error("WITH BODY          -> {}", objectMapper.writeValueAsString(r.getEntity()));
                logger.error("RESPONSE BY CLIENT -> {}", respone);
            } catch (IOException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            }
        }
    }
}
