package com.trimindi.switching.gatway.thread;

import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.PlnProperties;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.client.IsoMessageListener;
import com.trimindi.switching.gatway.client.client.Iso8583Client;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.prepaid.Inquiry;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import com.trimindi.switching.gatway.utils.generator.MessageGenerator;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorPrePaid;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created by PC on 06/09/2017.
 */
@Component
@Scope(value = "prototype")
public class PrepaidInquiy extends Thread implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(NontaglisInquiry.currentThread().getName());
    private final Map<String, ResponseCode> failedCode;
    private final Map<String, ResponseCode> responseCode;
    private final Iso8583Client<IsoMessage> iso8583Client;
    private final MessageGenerator messageGenerator;
    private final PlnProperties config;
    private final ProductFeeService productFeeService;
    private final TransaksiService transaksiService;
    private final PartnerDepositService partnerDepositService;
    private Map<String, String> params;
    private IsoMessage inquiryRequest;
    private Transaksi transaksi;
    private AsyncResponse asyncResponse;
    private PartnerCredential partnerCredential;
    private ProductItem productItem;
    private ProductFee productFee;
    private Request request;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public PrepaidInquiy(@Qualifier("aranetFailedCode") Map<String, ResponseCode> failedCode,
                         @Qualifier("aranetResponseCode") Map<String, ResponseCode> responseCode,
                         Iso8583Client<IsoMessage> iso8583Client,
                         MessageGenerator messageGenerator,
                         PlnProperties config,
                         ProductFeeService productFeeService,
                         TransaksiService transaksiService,
                         PartnerDepositService partnerDepositService) {
        this.failedCode = failedCode;
        this.responseCode = responseCode;
        this.iso8583Client = iso8583Client;
        this.messageGenerator = messageGenerator;
        this.config = config;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
    }

    public void init(
            @Suspended AsyncResponse asyncResponse,
            ProductItem productItem,
            PartnerCredential partnerCredential,
            Map<String, String> params,
            Request request
    ) {
        this.productItem = productItem;
        this.asyncResponse = asyncResponse;
        this.partnerCredential = partnerCredential;
        this.params = params;
        this.request = request;
    }

    @Override
    public void run() {
        super.run();
        try {
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage != null && isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(2).equals(config.PAN_PREPAID) && isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage inquiry) {
                    if (inquiry.getObjectValue(39).equals("0000")) {
                        List<Rules> bit48 = new SDE.Builder()
                                .setPayload(inquiry.getObjectValue(48))
                                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48, true))
                                .generate();

                        List<Rules> bit62 = new SDE.Builder()
                                .setPayload(inquiry.getObjectValue(62))
                                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62, true))
                                .generate();
                        bit48.addAll(bit62);
                        Inquiry response = new Inquiry(bit48, true);
                        /**
                         * MT = MACHINE_TYPE
                         * PC = PRODUDUCT CODE
                         * PRODUCT = PRODUCT
                         */
                        ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), productItem.getDENOM());
                        Transaksi transaksi = new Transaksi()
                                .setSTAN(inquiry.getObjectValue(11))
                                .setADMIN(response.getAdmin())
                                .setMSSIDN_NAME(response.getSubscriberName())
                                .setBILL_REF_NUMBER(response.getBukopinReferenceNumber())
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMSSIDN(response.getSubscriberID())
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setTAGIHAN(productItem.getAMOUT())
                                .setFEE_CA(productFee.getFEE_CA())
                                .setFEE_DEALER((productItem.getFEE_BILLER() - productFee.getFEE_CA()))
                                .setPRODUCT(productItem.getProduct_id())
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setDENOM(productItem.getDENOM())
                                .setINQUIRY(inquiry.debugString())
                                .setCHARGE(productItem.getAMOUT() + response.getAdmin() - (productItem.getFEE_BILLER() - productFee.getFEE_CA()))
                                .setDEBET(productItem.getAMOUT() + response.getAdmin() - (productItem.getFEE_BILLER() - productFee.getFEE_CA()))
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setDENDA(0)
                                .setST(Status.INQUIRY);
                        transaksiService.save(transaksi);
                        response.setTagihan(transaksi.getTAGIHAN());
                        BaseResponse<Inquiry> inquiryResponse = new BaseResponse<>();
                        inquiryResponse.setNtrans(transaksi.getNTRANS());
                        inquiryResponse.setData(response);
                        inquiryResponse.setMessage("Successful");
                        inquiryResponse.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                        inquiryResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        inquiryResponse.setProduct(transaksi.getDENOM());
                        inquiryResponse.setFee((productItem.getFEE_BILLER() - productFee.getFEE_CA()));
                        asyncResponse.resume(
                                Response
                                        .status(200)
                                        .entity(inquiryResponse)
                                        .build()
                        );

                    } else {
                        asyncResponse.resume(Response.status(200)
                                .entity(responseCode.get(inquiry.getObjectValue(39).toString())
                                        .setMssidn(params.get(Constanta.MSSIDN))
                                        .setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE())
                                        .setProduct(params.get(Constanta.DENOM)))
                                .build());
                    }
                    return false;
                }
            });
            inquiryRequest = messageGenerator.inquiryPrepaid(params.get(Constanta.MSSIDN), params.get(Constanta.MACHINE_TYPE));
            if (!iso8583Client.isConnected()) {
                iso8583Client.connectAsync().sync().await();
            }
            iso8583Client.sendAsync(inquiryRequest);
        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(params.get(Constanta.DENOM))));
        }
    }
}
