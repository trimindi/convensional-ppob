package com.trimindi.switching.gatway.thread.multifinance;


import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.VSIPropoerties;
import com.trimindi.switching.gatway.client.IsoMessageListener;
import com.trimindi.switching.gatway.client.client.Iso8583ClientCustom;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.models.*;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.mutifinance.Inquiry;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import com.trimindi.switching.gatway.utils.generator.VSIMessageGenerator;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorMultifinance;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created by PC on 06/09/2017.
 */
@Component
@Scope(value = "prototype")
public class MultifinanceInquiry extends Thread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(MultifinanceInquiry.currentThread().getName());
    private final Map<String, ResponseCode> failedCode;
    private final Map<String, ResponseCode> responseCode;
    private final VSIMessageGenerator vsiMessageGenerator;
    private final VSIPropoerties config;
    private final ProductFeeService productFeeService;
    private final TransaksiService transaksiService;
    private final PartnerDepositService partnerDepositService;
    private final Iso8583ClientCustom<IsoMessage> iso8583Client;
    private Map<String, String> params;
    private IsoMessage inquiryRequest;
    private Transaksi transaksi = null;
    private AsyncResponse asyncResponse;
    private PartnerCredential partnerCredential;
    private ProductItem productItem;
    private Request request;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public MultifinanceInquiry(@Qualifier("vsiFailedCode") Map<String, ResponseCode> failedCode,
                               @Qualifier("vsiResponseCode") Map<String, ResponseCode> responseCode,
                               VSIMessageGenerator vsiMessageGenerator,
                               VSIPropoerties config,
                               ProductFeeService productFeeService,
                               TransaksiService transaksiService,
                               PartnerDepositService partnerDepositService,
                               Iso8583ClientCustom<IsoMessage> iso8583Client) {
        this.failedCode = failedCode;
        this.responseCode = responseCode;
        this.vsiMessageGenerator = vsiMessageGenerator;
        this.config = config;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
        this.iso8583Client = iso8583Client;
    }

    public void init(
            @Suspended AsyncResponse asyncResponse,
            ProductItem productItem,
            PartnerCredential partnerCredential,
            Map<String, String> params,
            Request request
    ) {
        this.productItem = productItem;
        this.asyncResponse = asyncResponse;
        this.partnerCredential = partnerCredential;
        this.params = params;
        this.request = request;
    }

    @Override
    public void run() {
        super.run();
        try {
            iso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage != null && isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        transaksi = new Transaksi();
                        boolean status = true;
                        List<Rules> bit48 = new SDE.Builder()
                                .setPayload(isoMessage.getObjectValue(48))
                                .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(48, status))
                                .generate();
                        List<Rules> bit62 = new SDE.Builder()
                                .setPayload(isoMessage.getObjectValue(62))
                                .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(62, status))
                                .generate();
                        bit48.addAll(bit62);
                        Inquiry inquiryResponse = new Inquiry(bit48);
                        ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), productItem.getDENOM());
                        PartnerDeposit partnerDeposit = partnerDepositService.findById(partnerCredential.getPartner_id());
                        transaksi
                                .setSTAN(isoMessage.getObjectValue(11))
                                .setADMIN(inquiryResponse.getAdminCharges())
                                .setMSSIDN_NAME(inquiryResponse.getCustomerName())
                                .setBILL_REF_NUMBER(inquiryResponse.getSWReferenceNumber())
                                .setMSSIDN(inquiryResponse.getCustomerID())
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setTAGIHAN(inquiryResponse.getODInstallmentAmount())
                                .setDENDA(inquiryResponse.getODPenaltyFee())
                                .setFEE_CA(productFee.getFEE_CA())
                                .setFEE_DEALER((productItem.getFEE_BILLER() - productFee.getFEE_CA()))
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setINQUIRY(isoMessage.debugString())
                                .setCHARGE(inquiryResponse.getBillAmount() + inquiryResponse.getAdminCharges() - (productItem.getFEE_BILLER() - productFee.getFEE_CA()))
                                .setDEBET(inquiryResponse.getBillAmount() + inquiryResponse.getAdminCharges() - (productItem.getFEE_BILLER() - productFee.getFEE_CA()))
                                .setDENOM(productItem.getDENOM())
                                .setPRODUCT(productItem.getProduct_id())
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setPRODUCT(productItem.getProduct_id())
                                .setST(Status.INQUIRY);
                        transaksiService.save(transaksi);
                        BaseResponse<Inquiry> baseResponse = new BaseResponse<>();
                        baseResponse.setData(inquiryResponse);
                        baseResponse.setNtrans(transaksi.getNTRANS());
                        baseResponse.setSaldo(partnerDeposit.getBALANCE());
                        baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        baseResponse.setProduct(transaksi.getDENOM());
                        baseResponse.setMessage("Successful");
                        baseResponse.setFee((productItem.getFEE_BILLER() - productFee.getFEE_CA()));
                        asyncResponse.resume(
                                Response.status(200)
                                        .entity(baseResponse)
                                        .build()
                        );
                    } else {
                        asyncResponse.resume(
                                Response.status(200)
                                        .entity(responseCode.get(isoMessage.getObjectValue(39).toString())
                                                .setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN)))
                                        .build()
                        );
                    }
                    return false;
                }
            });
            inquiryRequest = vsiMessageGenerator.inquiryMultifinance(params.get(Constanta.MSSIDN), params.get(Constanta.MACHINE_TYPE), productItem.getProduct_id());
            iso8583Client.send(inquiryRequest);
        } catch (InterruptedException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_TIMEOUT.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN))));

        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            asyncResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setProduct(params.get(Constanta.DENOM)).setMssidn(params.get(Constanta.MSSIDN))));
        }
    }
}
