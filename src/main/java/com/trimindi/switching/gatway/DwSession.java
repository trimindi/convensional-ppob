package com.trimindi.switching.gatway;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.biller.dw.pojo.login.LoginRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.login.LoginResponse;
import com.trimindi.switching.gatway.utils.GenericRescheduleIdle;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sx on 07/12/17.
 * Copyright under comercial unit
 */
@Component
public class DwSession {
    public static final Logger logger = LoggerFactory.getLogger(DwSession.class);
    final
    ObjectMapper objectMapper;
    final
    CloseableHttpClient client;
    @Value("${dw.username}")
    private String username;
    @Value("${dw.password}")
    private String password;
    @Value("${dw.url.login}")
    private String loginPath;
    @Value("${dw.enable}")
    private Boolean enable;
    private final LoginResponse session;
    private final SlackSendMessage slackSendMessage;
    @Autowired
    private GenericRescheduleIdle rescheduleIdle;

    @Autowired
    public DwSession(ObjectMapper objectMapper, CloseableHttpClient client, SlackSendMessage slackSendMessage) {
        this.objectMapper = objectMapper;
        this.client = client;
        this.slackSendMessage = slackSendMessage;
        this.session = new LoginResponse();
    }

    @PostConstruct
    public void schedule() {
        login();
    }

    @Bean
    GenericRescheduleIdle getRescheduleIdle() {
        return new GenericRescheduleIdle(() -> {
            login();
        }, 19);
    }

    private void login() {
        if (enable) {
            try {
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.setUserID(username);
                String token = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
                loginRequest.setToken(token);
                String pass = DigestUtils.md5Hex(password);
                String securityCode = DigestUtils.md5Hex(token + pass);
                loginRequest.setSecurityCode(securityCode);
                HttpPost httpPost = new HttpPost(loginPath);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setEntity(new StringEntity(objectMapper.writeValueAsString(loginRequest)));
                CloseableHttpResponse execute = client.execute(httpPost);
                rescheduleIdle.reschedule();
                String respone = EntityUtils.toString(execute.getEntity(), StandardCharsets.UTF_8.name());
                logger.error("Response Login Darmawisata  -> {}", respone);
                LoginResponse loginResponse = objectMapper.readValue(respone, LoginResponse.class);
                session.setAccessToken(loginResponse.getAccessToken());
                session.setUserID(loginResponse.getUserID());
            } catch (Exception e) {
                e.printStackTrace();
//            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            }
        }

    }

    public LoginResponse authKey() {
        rescheduleIdle.reschedule();
        return session;
    }

}
