package com.trimindi.switching.gatway.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by sx on 02/03/18.
 * Copyright under comercial unit
 */
public class GenericRescheduleIdle {
    public static final Logger logger = LoggerFactory.getLogger(GenericRescheduleIdle.class);
    private final Runnable task;
    private final int menit;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> sch;
    public GenericRescheduleIdle(Runnable runnable, int i) {
        this.menit = i;
        task = runnable;
        sch = scheduler.schedule(runnable, this.menit, TimeUnit.MINUTES);
    }


    public void reschedule() {
        sch.cancel(true);
        sch = scheduler.schedule(this.task, this.menit, TimeUnit.MINUTES);
    }
}
