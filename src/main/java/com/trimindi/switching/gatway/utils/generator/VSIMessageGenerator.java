package com.trimindi.switching.gatway.utils.generator;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.trimindi.switching.gatway.VSIPropoerties;
import com.trimindi.switching.gatway.client.client.Iso8583ClientCustom;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.utils.iso.FieldBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;

import static com.trimindi.switching.gatway.utils.generator.BaseHelper.amountToPay;
import static com.trimindi.switching.gatway.utils.generator.BaseHelper.date14;

/**
 * Created by PC on 27/07/2017.
 */

@Component
public class VSIMessageGenerator {
    private static final Logger logger = LoggerFactory.getLogger(VSIMessageGenerator.class);
    private final VSIPropoerties config;
    @Autowired
    Iso8583ClientCustom<IsoMessage> iso8583Client;
    private VSIStanGenerator generator;

    @Autowired
    public VSIMessageGenerator(VSIStanGenerator generator, VSIPropoerties appProperties) {
        this.generator = generator;
        this.config = appProperties;
    }

    public IsoMessage signON() {
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2800);
        isoMessage.setField(12, IsoType.ALPHA.value(date14(), 14));
        isoMessage.setField(33, IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(40, IsoType.ALPHA.value(config.SIGN_ON, 3));
        isoMessage.setField(41, IsoType.ALPHA.value(config.TERMINAL_ID, 16));
        isoMessage.setField(48, IsoType.LLLVAR.value(config.SWITCHER_ID));
        return isoMessage;
    }


    public IsoMessage signOFF() {
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2800);
        isoMessage.setField(12, IsoType.ALPHA.value(date14(), 14));
        isoMessage.setField(33, IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(40, IsoType.NUMERIC.value(config.SIGN_OFF, 3));
        isoMessage.setField(41, IsoType.ALPHA.value(config.TERMINAL_ID, 16));
        isoMessage.setField(48, IsoType.LLLVAR.value(config.SWITCHER_ID));
        return isoMessage;
    }

    public IsoMessage echoMSG() {
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2800);
        isoMessage.setField(12, IsoType.ALPHA.value(date14(), 14));
        isoMessage.setField(33, IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(40, IsoType.NUMERIC.value(config.ECHO_TEST, 3));
        isoMessage.setField(41, IsoType.ALPHA.value(config.TERMINAL_ID, 16));
        isoMessage.setField(48, IsoType.LLLVAR.value(config.SWITCHER_ID));
        return isoMessage;
    }

    public IsoMessage inquiryMultifinance(String MSSIDN, String MT, String PAN) {
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2100);
        isoMessage.setField(2, IsoType.LLVAR.value(PAN));
        isoMessage.setField(11, IsoType.ALPHA.value(generator.nextTrace(), 12));
        isoMessage.setField(12, IsoType.ALPHA.value(date14(), 14));
        isoMessage.setField(26, IsoType.ALPHA.value(MT, 4));
        isoMessage.setField(32, IsoType.LLVAR.value(config.BANK_CODE));
        isoMessage.setField(33, IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(41, IsoType.ALPHA.value(config.TERMINAL_ID, 16));
        isoMessage.setField(48, IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID, 7, "", "L")
                .addValue("", 2, "0", "L")
                .addValue(MSSIDN, 20, "", "L")
                .build()));
        isoMessage.setField(61, IsoType.LLLVAR.value("01"));
        return isoMessage;
    }

    public IsoMessage paymentMultifinance(Transaksi transaksi) {
        String payload = transaksi.getINQUIRY();
        IsoMessage inquiry = null;
        try {
            inquiry = iso8583Client.getIsoMessageFactory().parseMessage(payload.getBytes(), 0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        inquiry.setType(0x2200);
        inquiry.removeFields(39);
        inquiry.update(12, IsoType.ALPHA.value(date14(), 14));
        inquiry.update(4, IsoType.ALPHA.value(amountToPay(transaksi.getTAGIHAN() + transaksi.getADMIN() + transaksi.getDENDA()), 16));
        return inquiry;
    }

    public IsoMessage revesalMultifinance(final IsoMessage newMsg, int MTI, String bit56) {
        String date = newMsg.getObjectValue(12);
        String hex = Integer.toHexString(newMsg.getType());
        newMsg.setType(MTI);
        newMsg.update(12, IsoType.ALPHA.value(date14(), 14));
        newMsg.update(56, IsoType.LLVAR.value(bit56));
        return newMsg;
    }

    public IsoMessage inquiryTelkom(String MSSIDN, String MT) {
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2100);
        isoMessage.setField(2, IsoType.LLVAR.value(config.PAN_TELKOM));
        isoMessage.setField(11, IsoType.ALPHA.value(generator.nextTrace(), 12));
        isoMessage.setField(12, IsoType.ALPHA.value(date14(), 14));
        isoMessage.setField(26, IsoType.ALPHA.value(MT, 4));
        isoMessage.setField(32, IsoType.LLVAR.value(config.BANK_CODE));
        isoMessage.setField(33, IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(41, IsoType.ALPHA.value(config.TERMINAL_ID, 16));
        isoMessage.setField(48, IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID, 7, "", "L")
                .addValue("001001", 6, "0", "L")
                .addValue("", 15, " ", "R")
                .addValue("", 40, " ", "R")
                .addValue(MSSIDN, 13, "0", "L")
                .build()));
        return isoMessage;
    }

    public IsoMessage paymentTelkom(Transaksi transaksi) {
        String payload = transaksi.getINQUIRY();
        IsoMessage inquiry = null;
        try {
            inquiry = iso8583Client.getIsoMessageFactory().parseMessage(payload.getBytes(), 0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        inquiry.setType(0x2200);
        inquiry.removeFields(39);
        inquiry.update(12, IsoType.ALPHA.value(date14(), 14));
        inquiry.update(4, IsoType.ALPHA.value(amountToPay(transaksi.getTAGIHAN() + transaksi.getADMIN() + transaksi.getDENDA()), 16));
        return inquiry;
    }

    public IsoMessage cetakUlangMultifinance(Transaksi transaksi) {
        String payload = transaksi.getPAYMENT();
        IsoMessage inquiry = null;
        try {
            inquiry = iso8583Client.getIsoMessageFactory().parseMessage(payload.getBytes(), 0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        inquiry.setType(0x2220);
        inquiry.removeFields(39);
        inquiry.removeFields(48);
        inquiry.removeFields(62);
        inquiry.update(48, IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID, 7, "", "L")
                .addValue("00", 2, "0", "L")
                .addValue("1", 1, " ", "R")
                .addValue(transaksi.getMSSIDN(), 20, " ", "L")
                .addValue(transaksi.getBILL_REF_NUMBER(), 32, "0", "R")
                .build()));
        return inquiry;
    }
}
