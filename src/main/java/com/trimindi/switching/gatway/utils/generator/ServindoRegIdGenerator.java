package com.trimindi.switching.gatway.utils.generator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by PC on 10/08/2017.
 */
@Component
public class ServindoRegIdGenerator {
    private static final Logger logger = LoggerFactory.getLogger(ServindoRegIdGenerator.class);
    private volatile int value = 1;
    private BufferedReader bufferedReader;

    public ServindoRegIdGenerator() {

    }

    @PostConstruct
    public void init() {
        try {
            bufferedReader = new BufferedReader(new FileReader("servindo.resume"));
            value = Integer.parseInt(bufferedReader.readLine());
        } catch (IOException e) {
            logger.info("CANT FIND FILE START NEW SERVINDO NUMBER");
            create();
        }
    }

    public int getLastTrace() {
        return this.value;
    }

    @PreDestroy
    public void destory() {
        logger.info("SAVING CURENT SERVINDO NUMBER");
        create();
    }

    private void create() {
        try {
            FileWriter fileWriter = new FileWriter("servindo.resume");
            fileWriter.write(String.valueOf(getLastTrace()));
            fileWriter.flush();
            fileWriter.close();
            logger.info("SUCCESS SAVING SERVINDO NUMBER");
        } catch (IOException e) {
            logger.error("FAILED SAVING SERVINDO NUMBER");
            logger.error(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public synchronized String nextTrace() {
        ++this.value;
        if (this.value > 9999) {
            this.value = 1;
        }

        return new SimpleDateFormat("yyMMddHHmmss").format(new Date()) + StringUtils.leftPad(String.valueOf(this.value), 4, "0");
    }
}
