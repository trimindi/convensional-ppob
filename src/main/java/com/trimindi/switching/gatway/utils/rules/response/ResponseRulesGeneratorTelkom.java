package com.trimindi.switching.gatway.utils.rules.response;


import com.trimindi.switching.gatway.utils.iso.models.Rules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class ResponseRulesGeneratorTelkom {
    public static List<Rules> inquiryResponse(int bitnumber, boolean success) {
        List<Rules> R = new ArrayList<>();
        switch (bitnumber) {
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Service Type Code", 6));
                R.add(new Rules<Integer>(3, "Card Acceptor Identification Code", 15));
                R.add(new Rules<Integer>(4, "Card Acceptor Name Location", 40));
                R.add(new Rules<Integer>(5, "Customer Telephone Number", 13));
                R.add(new Rules<Integer>(6, "Card Acceptor Name Location", 40));
                R.add(new Rules<Integer>(7, "Divre Code", 2));
                R.add(new Rules<Integer>(8, "Datel Code", 4));
                R.add(new Rules<Integer>(9, "Bill Status", 1));
                R.add(new Rules<Integer>(10, "Total Admin Charge", 9));
//                R.add(new Rules<Integer>(10, "Customer name", 30));
//                R.add(new Rules<Integer>(10, "NPWP", 15));
//                R.add(new Rules<Integer>(10, "Switcher Reference Number", 15));
                break;
        }
        return R;
    }

    public static List<Rules> inquiryResponse(int bitnumber, int bill) {
        List<Rules> R = new ArrayList<>();
        switch (bitnumber) {
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Service Type Code", 6));
                R.add(new Rules<Integer>(3, "Card Acceptor Identification Code", 15));
                R.add(new Rules<Integer>(4, "Card Acceptor Name Location", 40));
                R.add(new Rules<Integer>(5, "Customer Telephone Number", 13));
                R.add(new Rules<Integer>(6, "Card Acceptor Name Location", 40));
                R.add(new Rules<Integer>(7, "Divre Code", 2));
                R.add(new Rules<Integer>(8, "Datel Code", 4));
                R.add(new Rules<Integer>(9, "Bill Status", 1));
                R.add(new Rules<Integer>(10, "Total Admin Charge", 9));
                for (int i = bill; i > 0; i--) {
                    R.add(new Rules<Integer>(14, "Reference Number #" + bill, 32));
                    R.add(new Rules<Integer>(15, "Total Bill #" + bill, 32));
                    R.add(new Rules<Integer>(16, "Admin Charge #" + bill, 32));
                }
                R.add(new Rules<Integer>(11, "Customer name", 30));
                R.add(new Rules<Integer>(12, "NPWP", 15));
                R.add(new Rules<Integer>(13, "Switcher Reference Number", 15));

                break;
        }
        return R;
    }

    public static List<Rules> paymentResponse(int bitnumber, boolean success) {
        List<Rules> R = new ArrayList<>();
        switch (bitnumber) {
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "BillerCode", 2));
                R.add(new Rules<Integer>(3, "Customer ID", 20));

                /**
                 * IF RC 0000 = SUCCESS
                 */
                if (success) {
                    R.add(new Rules<Integer>(4, "GW Reference Number", 32));
                    R.add(new Rules<String>(5, "SW Reference Number", 32));
                    R.add(new Rules<String>(6, "Customer Name", 50));
                    R.add(new Rules<String>(7, "Product Category", 2));
                    R.add(new Rules<String>(8, "Minor Unit", 1));
                    R.add(new Rules<String>(9, "Bill Amount", 12));
                    R.add(new Rules<String>(10, "Stamp Duty", 12));
                    R.add(new Rules<String>(11, "PPN", 12));
                    R.add(new Rules<String>(12, "Admin Charges", 12));
                }

                break;
            case 61:
                R.clear();
                R.add(new Rules<Integer>(1, "Bill Quantity", 2));
                break;
            case 62:
                R.clear();
                R.add(new Rules<String>(1, "Biller Reference Number", 32));
                R.add(new Rules<String>(2, "PT Name", 25));
                R.add(new Rules<String>(3, "Branch Name", 30));
                R.add(new Rules<String>(4, "Item Merk Type", 42));
                R.add(new Rules<String>(5, "Chasis Number", 25));
                R.add(new Rules<String>(6, "Car Number", 10));
                R.add(new Rules<String>(7, "Tenor", 3));
                R.add(new Rules<String>(8, "Last Paid Period", 3));
                R.add(new Rules<String>(9, "Last Paid Due Date", 3));
                R.add(new Rules<String>(10, "Minor Unit", 1));
                R.add(new Rules<String>(11, "OS Installment Amount", 12));
                R.add(new Rules<String>(12, "OD Installment Period", 3));
                R.add(new Rules<String>(13, "OD Installment Amount", 12));
                R.add(new Rules<String>(14, "OD Penalty Fee", 12));
                R.add(new Rules<String>(15, "Biller Admin Fee", 12));
                R.add(new Rules<String>(16, "Misc Fee", 12));
                R.add(new Rules<String>(17, "Minimum Pay Amount", 12));
                R.add(new Rules<String>(18, "Maximum Pay Amount", 12));
                break;
        }
        return R;
    }
}
