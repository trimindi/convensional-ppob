package com.trimindi.switching.gatway.utils;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by sx on 10/03/18.
 * Copyright under comercial unit
 */
public class StringXOR {

    public static String encode(String s, String key) {
        return base64Encode(xorWithKey(s.getBytes(), key.getBytes()));
    }


    private static byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i % key.length]);
        }
        return out;
    }

    private static String base64Encode(byte[] bytes) {
        byte[] encodedBytes = Base64.encodeBase64(bytes);
        return new String(encodedBytes);
    }
}
