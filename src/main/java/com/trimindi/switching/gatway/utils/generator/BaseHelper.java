package com.trimindi.switching.gatway.utils.generator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sx on 13/05/17.
 */
@Configuration
public class BaseHelper {
    public static final Logger logger = LoggerFactory.getLogger(BaseHelper.class);

    public static String date14()
    {
//        return new SimpleDateFormat("YYYYMMDDhhmm").format(new Date()) +""+new SimpleDateFormat("ss").format(new Date()).substring(0,1);
        return new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
    }

    public static String date8()
    {
        return new SimpleDateFormat("YYYYMMDD").format(new Date());
    }

    public static String numberToPay(double number){
        int a = (int) number;
        return String.valueOf(a);
    }

    public static String amountToPay(double amount) {
        String curency = "360";
        String minor = "0";
        String harga = new DecimalFormat("#").format(amount);
        return curency + minor + StringUtils.leftPad(harga,12,"0");
    }

    public static String valueToMinor(double amount,int minor) {
        if(amount % 1 == 0){
            StringBuilder s = new StringBuilder();
            for(int i=1;i<= minor;i++){
                s.append("0");
            }
            return new DecimalFormat("#").format(amount) + s.toString();
        }else{
            StringBuilder s = new StringBuilder();
            s.append("1");
            for(int i=1;i<= minor;i++) {
                s.append("0");
            }
            return new DecimalFormat("#").format(amount * Integer.parseInt(s.toString()));
        }
    }
    public double numberMinorUnit(String val, int min) {
        int bagi = Integer.parseInt("1" + StringUtils.leftPad("", min, "0"));
        return Double.parseDouble(val) / bagi;
    }
}
