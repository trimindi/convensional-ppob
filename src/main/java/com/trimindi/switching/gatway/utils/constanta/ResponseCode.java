package com.trimindi.switching.gatway.utils.constanta;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 13/05/17.
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseCode {
    public static final ResponseCode PAYMENT_UNDER_PROSES = new ResponseCode("2222", "PEMBAYARAN / PEMBELIAN SEDANG PROSES");
    public static final ResponseCode PAYMENT_FAILED = new ResponseCode("3333", "PEMBAYARAN / PEMBELIAN GAGAL");
    public static final ResponseCode MAXIMUM_PRINTED_RECIPT = new ResponseCode("4444", "MELEBIHI BATAS CETAK ULANG");
    public static final ResponseCode PRODUCT_UNDER_MAINTANCE = new ResponseCode("5555", "MAAF , PRODUCT SEDANG CLOSE");
    public static final ResponseCode PAYMENT_DONE = new ResponseCode("6666", "TRANSAKSI BERHASIL");
    public static final ResponseCode SERVER_UNAVAILABLE = new ResponseCode("0005", "TERJADI KESALAHAN PADA SERVER COBA BEBERAPA SAAT KEMUDIAN");
    public static final ResponseCode SERVER_TIMEOUT = new ResponseCode("0068", "WAKTU REQUEST HABIS COBA BEBERAPA SAAT LAGI");
    public static final ResponseCode PARAMETER_SALDO = new ResponseCode("0026", "MAAF SALDO TIDAK CUKUP");
    public static final ResponseCode NTRANS_NOT_FOUND = new ResponseCode("0096", "TRANSAKSI TIDAK DITEMUKAN");
    public static final ResponseCode INVALID_BODY_REQUEST_FORMAT = new ResponseCode("0030", "FORMAT PESAN TIDAK SESUAI");
    public static final ResponseCode PARAMETER_INVALID_PRODUCT_ACTION = new ResponseCode("0033", "PRODUCT TIDAK TERDAFTAR");
    public static final ResponseCode UNAUTHORIZED_REQUEST = new ResponseCode("0004", "PARTNER TIDAK TERDAFTAR");
    public static final ResponseCode INVALID_MSSIDN = new ResponseCode("0031", "ID PELANGGAN TIDAK VALID");
    public static final ResponseCode INQUIRY_FAILED = new ResponseCode("0037", "PROSES INQUIRY GAGAL COBA BEBERASPA SAAT KEMUDIAN");

    private String status;
    private String message = "";
    private String ntrans = "";
    private String product = "";
    private String mssidn = "";
    private double saldo = 0;
    private double saldoTerpotong = 0;
    public ResponseCode() {
    }

    public ResponseCode(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public double getSaldo() {
        return saldo;
    }

    public ResponseCode setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getSaldoTerpotong() {
        return saldoTerpotong;
    }

    public ResponseCode setSaldoTerpotong(double saldoTerpotong) {
        this.saldoTerpotong = saldoTerpotong;
        return this;
    }

    public String getMssidn() {
        return mssidn;
    }

    public ResponseCode setMssidn(String mssidn) {
        this.mssidn = mssidn;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public ResponseCode setProduct(String product) {
        this.product = product;
        return this;
    }

    @XmlElement(name = "ntrans",nillable = false)
    public String getNtrans() {
        return ntrans;
    }

    public ResponseCode setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public ResponseCode setStatus(String code) {
        this.status = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseCode setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return "ResponseCode{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", ntrans='" + ntrans + '\'' +
                ", product='" + product + '\'' +
                ", mssidn='" + mssidn + '\'' +
                ", saldo=" + saldo +
                ", saldoTerpotong=" + saldoTerpotong +
                '}';
    }
}
