package com.trimindi.switching.gatway;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.parse.ConfigParser;
import com.trimindi.switching.gatway.client.client.ClientConfiguration;
import com.trimindi.switching.gatway.client.client.Iso8583Client;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by PC on 08/08/2017.
 */
@Configuration
public class ClientComponent {

    public static final Logger logger = LoggerFactory.getLogger(ClientComponent.class);

    @Value("${iso8583.aranet.responseCode}")
    String aranetFilename;
    @Value("${iso8583.aranet.failedCode}")
    String aranetFailedCode;
    @Value("${iso8583.vsi.failedCode}")
    String vsiFailedCode;
    @Value("${iso8583.vsi.responseCode}")
    String vsiFileName;
    @Value("${iso8583.aranet.connection.host}")
    private String host;
    @Value("${iso8583.aranet.connection.port}")
    private int port;
    @Value("${iso8583.aranet.connection.idleTimeout}")
    private int idleTimeout;
    @Value("${iso8583.aranet.connection.echomessage}")
    private boolean echomessage;

    @Bean
    @Scope(value = "prototype")
    public CloseableHttpClient closeableHttpClient(SSLContext sslContext){
        int timeout = 80;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        CloseableHttpClient client = HttpClients.custom()
                .setDefaultRequestConfig(config)
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .build();
        return client;
    }
    @Bean
    public SSLContext sslContext(){
        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, (x509Certificates, s) -> true).build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            logger.error(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return sslContext;
    }

//    @Bean
//    ThreadPoolTaskExecutor threadPoolTaskExecutor() {
//        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
//        threadPoolTaskExecutor.setQueueCapacity(300);
//        threadPoolTaskExecutor.setCorePoolSize(100);
//        threadPoolTaskExecutor.setMaxPoolSize(1000);
//        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(true);
//        return threadPoolTaskExecutor;
//    }

    @Bean
    public CacheManager cacheManager() {
        return new EhCacheCacheManager(ehCacheCacheManager().getObject());
    }

    @Bean
    public EhCacheManagerFactoryBean ehCacheCacheManager() {
        EhCacheManagerFactoryBean cmfb = new EhCacheManagerFactoryBean();
        cmfb.setConfigLocation(new ClassPathResource("ehcache.xml"));
        cmfb.setShared(true);
        cmfb.setShared(true);
        return cmfb;
    }

    @Value("${iso8583.aranet.connection.logsensitifdata}")
    private boolean logsensitifdata;
    @Value("${iso8583.aranet.connection.replayonerror}")
    private boolean replayonerror;

    @Bean(name = "aranetClient")
    public Iso8583Client<IsoMessage> iso8583Client(@Qualifier("aranetClientMessageFactory") MessageFactory<IsoMessage> messageFactory) throws IOException {
        SocketAddress socketAddress = new InetSocketAddress(host, port);
        final ClientConfiguration configuration = ClientConfiguration.newBuilder()
                .withIdleTimeout(idleTimeout)
                .withSensitiveDataFields()
                .withEchoMessageListener(false)
                .withLogSensitiveData(false)
                .withAddLoggingHandler(true)
                .withReplyOnError(false)
                .build();

        return new Iso8583Client<>(socketAddress, configuration, messageFactory);
    }

    @Bean(name = "aranetClientMessageFactory")
    public MessageFactory<IsoMessage> clientMessageFactory() throws IOException {
        final MessageFactory<IsoMessage> messageFactory = ConfigParser.createFromClasspathConfig("spec-aranet.xml");
        messageFactory.setCharacterEncoding(StandardCharsets.UTF_8.name());
        messageFactory.setUseBinaryMessages(false);
        messageFactory.setAssignDate(false);
        messageFactory.setForceSecondaryBitmap(false);
        messageFactory.setIgnoreLastMissingField(true);
        messageFactory.setUseBinaryBitmap(false);
        return messageFactory;
    }

    @Bean(name = "aranetResponseCode")
    public Map<String, ResponseCode> responseCode() {
        return Collections.unmodifiableMap(getStringResponseCodeMap(aranetFilename));
    }

    private Map<String, ResponseCode> getStringResponseCodeMap(String filename) {
        Map<String, ResponseCode> aMap = new HashMap<>();
        try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename)) {
            final Properties properties = new Properties();
            properties.load(stream);
            properties.forEach((key, value) -> {
                aMap.put(String.valueOf(key), new ResponseCode((String) key, (String) value));
            });
        } catch (IOException | NumberFormatException e) {
            throw new IllegalStateException("Unable to Response Code dictionary", e);
        }
        return aMap;
    }

    @Bean(name = "aranetFailedCode")
    public Map<String, ResponseCode> failedCode() {
        return Collections.unmodifiableMap(getStringResponseCodeMap(aranetFailedCode));
    }

    @Bean(name = "vsiResponseCode")
    public Map<String, ResponseCode> vsiresponseCode() {
        return Collections.unmodifiableMap(getStringResponseCodeMap(vsiFileName));
    }

    @Bean(name = "vsiFailedCode")
    public Map<String, ResponseCode> vsifailedCode() {
        return Collections.unmodifiableMap(getStringResponseCodeMap(vsiFailedCode));
    }

}
