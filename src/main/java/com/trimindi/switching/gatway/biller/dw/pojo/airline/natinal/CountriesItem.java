package com.trimindi.switching.gatway.biller.dw.pojo.airline.natinal;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CountriesItem {

    @JsonProperty("countryName")
    private String countryName;

    @JsonProperty("countryID")
    private String countryID;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    @Override
    public String toString() {
        return
                "CountriesItem{" +
                        "countryName = '" + countryName + '\'' +
                        ",countryID = '" + countryID + '\'' +
                        "}";
    }
}