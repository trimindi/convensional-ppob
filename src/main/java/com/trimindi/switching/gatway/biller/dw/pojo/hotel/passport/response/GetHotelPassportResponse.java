package com.trimindi.switching.gatway.biller.dw.pojo.hotel.passport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelPassportResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("passports")
    private List<PassportsItem> passports;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("status")
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public List<PassportsItem> getPassports() {
        return passports;
    }

    public void setPassports(List<PassportsItem> passports) {
        this.passports = passports;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetHotelPassportResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",passports = '" + passports + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}