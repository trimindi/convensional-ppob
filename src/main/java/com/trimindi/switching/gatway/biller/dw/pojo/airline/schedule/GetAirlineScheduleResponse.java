package com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

public class GetAirlineScheduleResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("extraDay")
    private int extraDay;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("searchKey")
    private Object searchKey;

    @JsonProperty("journeyDepart")
    private List<JourneyDepartItem> journeyDepart;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("airlineAccessCode")
    private String airlineAccessCode;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonProperty("journeyReturn")
    private List<JourneyReturnItem> journeyReturn;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("promoCode")
    private String promoCode;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("status")
    private String status;

    @JsonIgnore
    @XmlTransient
    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public int getExtraDay() {
        return extraDay;
    }

    public void setExtraDay(int extraDay) {
        this.extraDay = extraDay;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Object getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(Object searchKey) {
        this.searchKey = searchKey;
    }

    public List<JourneyDepartItem> getJourneyDepart() {
        return journeyDepart;
    }

    public void setJourneyDepart(List<JourneyDepartItem> journeyDepart) {
        this.journeyDepart = journeyDepart;
    }

    @JsonIgnore
    @XmlTransient
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @JsonIgnore
    @XmlTransient
    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getAirlineAccessCode() {
        return airlineAccessCode;
    }

    public void setAirlineAccessCode(String airlineAccessCode) {
        this.airlineAccessCode = airlineAccessCode;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public List<JourneyReturnItem> getJourneyReturn() {
        return journeyReturn;
    }

    public void setJourneyReturn(List<JourneyReturnItem> journeyReturn) {
        this.journeyReturn = journeyReturn;
    }

    @JsonIgnore
    @XmlTransient
    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    @JsonIgnore
    @XmlTransient

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetAirlineScheduleResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",extraDay = '" + extraDay + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",searchKey = '" + searchKey + '\'' +
                        ",journeyDepart = '" + journeyDepart + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",airlineAccessCode = '" + airlineAccessCode + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",journeyReturn = '" + journeyReturn + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",promoCode = '" + promoCode + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}