package com.trimindi.switching.gatway.biller.dw.pojo.airline.route;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetAirlineRouteResponse {

    @JsonProperty("routes")
    private List<RoutesItem> routes;

    @JsonProperty("respMessage")
    @JsonIgnore
    private String respMessage;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("accessToken")
    @JsonIgnore
    private String accessToken;

    @JsonProperty("userID")
    @JsonIgnore
    private String userID;

    @JsonProperty("status")
    @JsonIgnore
    private String status;

    public List<RoutesItem> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RoutesItem> routes) {
        this.routes = routes;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetAirlineRouteResponse{" +
                        "routes = '" + routes + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}