package com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PaxBookingDetailsItem {

    @JsonProperty("bed")
    private String bed;

    @JsonProperty("paxGender")
    private String paxGender;

    @JsonProperty("fare")
    private int fare;

    @JsonProperty("paxType")
    private String paxType;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("deck")
    private String deck;

    @JsonProperty("cabin")
    private String cabin;

    @JsonProperty("ID")
    private String iD;

    @JsonProperty("paxName")
    private String paxName;

    @JsonProperty("birthDate")
    private String birthDate;

    public String getBed() {
        return bed;
    }

    public void setBed(String bed) {
        this.bed = bed;
    }

    public String getPaxGender() {
        return paxGender;
    }

    public void setPaxGender(String paxGender) {
        this.paxGender = paxGender;
    }

    public int getFare() {
        return fare;
    }

    public void setFare(int fare) {
        this.fare = fare;
    }

    public String getPaxType() {
        return paxType;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getPaxName() {
        return paxName;
    }

    public void setPaxName(String paxName) {
        this.paxName = paxName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return
                "PaxBookingDetailsItem{" +
                        "bed = '" + bed + '\'' +
                        ",paxGender = '" + paxGender + '\'' +
                        ",fare = '" + fare + '\'' +
                        ",paxType = '" + paxType + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",deck = '" + deck + '\'' +
                        ",cabin = '" + cabin + '\'' +
                        ",iD = '" + iD + '\'' +
                        ",paxName = '" + paxName + '\'' +
                        ",birthDate = '" + birthDate + '\'' +
                        "}";
    }
}