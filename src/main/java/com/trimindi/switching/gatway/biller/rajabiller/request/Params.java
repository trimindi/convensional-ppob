package com.trimindi.switching.gatway.biller.rajabiller.request;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/16/2017.
 */
@XmlRootElement
public class Params {
    private Param[] param;

    public Params(Param[] param) {
        this.param = param;
    }

    public Params() {
    }

    public Param[] getParam() {
        return param;
    }

    public void setParam(Param[] param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return "ClassPojo [param = " + param + "]";
    }

}
