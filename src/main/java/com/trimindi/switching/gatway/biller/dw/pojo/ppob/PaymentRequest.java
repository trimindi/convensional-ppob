package com.trimindi.switching.gatway.biller.dw.pojo.ppob;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentRequest {
    @JsonProperty("billingReferenceID")
    private String billingReferenceID;
    @JsonProperty("accessToken")
    private String accessToken;
    @JsonProperty("userID")
    private String userID;

    public String getBillingReferenceID() {
        return billingReferenceID;
    }

    public void setBillingReferenceID(String billingReferenceID) {
        this.billingReferenceID = billingReferenceID;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return
                "PaymentRequest{" +
                        "billingReferenceID = '" + billingReferenceID + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        "}";
    }
}
