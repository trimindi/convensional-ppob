package com.trimindi.switching.gatway.biller.dw;

import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;

import javax.ws.rs.container.AsyncResponse;
import java.util.Map;

/**
 * Created by sx on 10/03/18.
 * Copyright under comercial unit
 */
public interface DwPPOBService {
    void Pulsa(AsyncResponse response, ProductItem productItem, PartnerCredential p, Map<String, String> params);

    void InquiryCC(AsyncResponse response, ProductItem productItem, PartnerCredential p, Map<String, String> params);

    void PaymentCC(AsyncResponse response, Transaksi transaksi);
}
