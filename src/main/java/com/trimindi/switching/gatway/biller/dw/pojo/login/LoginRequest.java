package com.trimindi.switching.gatway.biller.dw.pojo.login;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginRequest {

    @JsonProperty("securityCode")
    private String securityCode;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("token")
    private String token;

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return
                "LoginRequest{" +
                        "securityCode = '" + securityCode + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",token = '" + token + '\'' +
                        "}";
    }
}