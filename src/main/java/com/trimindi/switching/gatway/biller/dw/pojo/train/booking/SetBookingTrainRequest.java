package com.trimindi.switching.gatway.biller.dw.pojo.train.booking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SetBookingTrainRequest {

    @JsonProperty("trainID")
    private String trainID;

    @JsonProperty("passengers")
    private List<Passengers> passengers;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("contactName")
    private String contactName;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("trainNumber")
    private String trainNumber;

    @JsonProperty("contactPhone")
    private String contactPhone;

    @JsonProperty("availabilityClass")
    private String availabilityClass;

    public String getTrainID() {
        return trainID;
    }

    public void setTrainID(String trainID) {
        this.trainID = trainID;
    }

    public List<Passengers> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passengers> passengers) {
        this.passengers = passengers;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getAvailabilityClass() {
        return availabilityClass;
    }

    public void setAvailabilityClass(String availabilityClass) {
        this.availabilityClass = availabilityClass;
    }

    @Override
    public String toString() {
        return
                "SetBookingTrainRequest{" +
                        "trainID = '" + trainID + '\'' +
                        ",passengers = '" + passengers + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",contactName = '" + contactName + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",trainNumber = '" + trainNumber + '\'' +
                        ",contactPhone = '" + contactPhone + '\'' +
                        ",availabilityClass = '" + availabilityClass + '\'' +
                        "}";
    }
}