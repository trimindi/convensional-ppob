package com.trimindi.switching.gatway.biller.dw.pojo.hotel.images.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelImageRequest {

    @JsonProperty("hotelID")
    @NotNull
    private String hotelID;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    public String getHotelID() {
        return hotelID;
    }

    public void setHotelID(String hotelID) {
        this.hotelID = hotelID;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return
                "GetHotelImageRequest{" +
                        "hotelID = '" + hotelID + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        "}";
    }
}