package com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class RoomRequestItem {

    @JsonProperty("childNum")
    private int childNum;

    @JsonProperty("isRequestChildBed")
    private boolean isRequestChildBed;

    @JsonProperty("childAges")
    private List<Object> childAges;

    @JsonProperty("roomType")
    private int roomType;

    public int getChildNum() {
        return childNum;
    }

    public void setChildNum(int childNum) {
        this.childNum = childNum;
    }

    public boolean isIsRequestChildBed() {
        return isRequestChildBed;
    }

    public void setIsRequestChildBed(boolean isRequestChildBed) {
        this.isRequestChildBed = isRequestChildBed;
    }

    public List<Object> getChildAges() {
        return childAges;
    }

    public void setChildAges(List<Object> childAges) {
        this.childAges = childAges;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    @Override
    public String toString() {
        return
                "RoomRequestItem{" +
                        "childNum = '" + childNum + '\'' +
                        ",isRequestChildBed = '" + isRequestChildBed + '\'' +
                        ",childAges = '" + childAges + '\'' +
                        ",roomType = '" + roomType + '\'' +
                        "}";
    }
}