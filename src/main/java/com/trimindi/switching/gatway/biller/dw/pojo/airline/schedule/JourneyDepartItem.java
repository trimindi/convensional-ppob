package com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class JourneyDepartItem {

    @JsonProperty("jiDepartTime")
    private String jiDepartTime;

    @JsonProperty("jiArrivalTime")
    private String jiArrivalTime;

    @JsonProperty("jiDestination")
    private String jiDestination;

    @JsonProperty("jiOrigin")
    private String jiOrigin;

    @JsonProperty("segment")
    private List<SegmentItem> segment;

    @JsonProperty("category")
    private String category;

    public String getJiDepartTime() {
        return jiDepartTime;
    }

    public void setJiDepartTime(String jiDepartTime) {
        this.jiDepartTime = jiDepartTime;
    }

    public String getJiArrivalTime() {
        return jiArrivalTime;
    }

    public void setJiArrivalTime(String jiArrivalTime) {
        this.jiArrivalTime = jiArrivalTime;
    }

    public String getJiDestination() {
        return jiDestination;
    }

    public void setJiDestination(String jiDestination) {
        this.jiDestination = jiDestination;
    }

    public String getJiOrigin() {
        return jiOrigin;
    }

    public void setJiOrigin(String jiOrigin) {
        this.jiOrigin = jiOrigin;
    }

    public List<SegmentItem> getSegment() {
        return segment;
    }

    public void setSegment(List<SegmentItem> segment) {
        this.segment = segment;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return
                "JourneyDepartItem{" +
                        "jiDepartTime = '" + jiDepartTime + '\'' +
                        ",jiArrivalTime = '" + jiArrivalTime + '\'' +
                        ",jiDestination = '" + jiDestination + '\'' +
                        ",jiOrigin = '" + jiOrigin + '\'' +
                        ",segment = '" + segment + '\'' +
                        ",category = '" + category + '\'' +
                        "}";
    }
}