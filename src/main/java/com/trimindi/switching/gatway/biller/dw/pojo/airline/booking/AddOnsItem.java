package com.trimindi.switching.gatway.biller.dw.pojo.airline.booking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class AddOnsItem {

    @JsonProperty("seat")
    private String seat;

    @JsonProperty("aoOrigin")
    private String aoOrigin;

    @JsonProperty("compartment")
    private String compartment;

    @JsonProperty("aoDestination")
    private String aoDestination;

    @JsonProperty("baggageString")
    private String baggageString;

    @JsonProperty("meals")
    private List<String> meals;

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getAoOrigin() {
        return aoOrigin;
    }

    public void setAoOrigin(String aoOrigin) {
        this.aoOrigin = aoOrigin;
    }

    public String getCompartment() {
        return compartment;
    }

    public void setCompartment(String compartment) {
        this.compartment = compartment;
    }

    public String getAoDestination() {
        return aoDestination;
    }

    public void setAoDestination(String aoDestination) {
        this.aoDestination = aoDestination;
    }

    public String getBaggageString() {
        return baggageString;
    }

    public void setBaggageString(String baggageString) {
        this.baggageString = baggageString;
    }

    public List<String> getMeals() {
        return meals;
    }

    public void setMeals(List<String> meals) {
        this.meals = meals;
    }

    @Override
    public String toString() {
        return
                "AddOnsItem{" +
                        "seat = '" + seat + '\'' +
                        ",aoOrigin = '" + aoOrigin + '\'' +
                        ",compartment = '" + compartment + '\'' +
                        ",aoDestination = '" + aoDestination + '\'' +
                        ",baggageString = '" + baggageString + '\'' +
                        ",meals = '" + meals + '\'' +
                        "}";
    }
}