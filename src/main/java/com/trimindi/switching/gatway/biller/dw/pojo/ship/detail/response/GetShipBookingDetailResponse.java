package com.trimindi.switching.gatway.biller.dw.pojo.ship.detail.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetShipBookingDetailResponse {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("bokingNumber")
    private Object bokingNumber;

    @JsonProperty("ticketPrice")
    private int ticketPrice;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("numCode")
    private String numCode;

    @JsonProperty("bookingDateTime")
    private String bookingDateTime;

    @JsonProperty("salesPrice")
    private int salesPrice;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("totalTicket")
    private int totalTicket;

    @JsonProperty("shipName")
    private String shipName;

    @JsonProperty("paxBookingDetails")
    private List<PaxBookingDetailsItem> paxBookingDetails;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("departDateTime")
    private String departDateTime;

    @JsonProperty("issuedDateTimeLimit")
    private String issuedDateTimeLimit;

    @JsonProperty("ticketStatus")
    private String ticketStatus;

    @JsonProperty("issuedDateTime")
    private String issuedDateTime;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("arrivalDateTime")
    private String arrivalDateTime;

    @JsonProperty("status")
    private String status;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public Object getBokingNumber() {
        return bokingNumber;
    }

    public void setBokingNumber(Object bokingNumber) {
        this.bokingNumber = bokingNumber;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public String getNumCode() {
        return numCode;
    }

    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public String getBookingDateTime() {
        return bookingDateTime;
    }

    public void setBookingDateTime(String bookingDateTime) {
        this.bookingDateTime = bookingDateTime;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public int getTotalTicket() {
        return totalTicket;
    }

    public void setTotalTicket(int totalTicket) {
        this.totalTicket = totalTicket;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public List<PaxBookingDetailsItem> getPaxBookingDetails() {
        return paxBookingDetails;
    }

    public void setPaxBookingDetails(List<PaxBookingDetailsItem> paxBookingDetails) {
        this.paxBookingDetails = paxBookingDetails;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDepartDateTime() {
        return departDateTime;
    }

    public void setDepartDateTime(String departDateTime) {
        this.departDateTime = departDateTime;
    }

    public String getIssuedDateTimeLimit() {
        return issuedDateTimeLimit;
    }

    public void setIssuedDateTimeLimit(String issuedDateTimeLimit) {
        this.issuedDateTimeLimit = issuedDateTimeLimit;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getIssuedDateTime() {
        return issuedDateTime;
    }

    public void setIssuedDateTime(String issuedDateTime) {
        this.issuedDateTime = issuedDateTime;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetShipBookingDetailResponse{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",bokingNumber = '" + bokingNumber + '\'' +
                        ",ticketPrice = '" + ticketPrice + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",numCode = '" + numCode + '\'' +
                        ",bookingDateTime = '" + bookingDateTime + '\'' +
                        ",salesPrice = '" + salesPrice + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",totalTicket = '" + totalTicket + '\'' +
                        ",shipName = '" + shipName + '\'' +
                        ",paxBookingDetails = '" + paxBookingDetails + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",departDateTime = '" + departDateTime + '\'' +
                        ",issuedDateTimeLimit = '" + issuedDateTimeLimit + '\'' +
                        ",ticketStatus = '" + ticketStatus + '\'' +
                        ",issuedDateTime = '" + issuedDateTime + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",arrivalDateTime = '" + arrivalDateTime + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}