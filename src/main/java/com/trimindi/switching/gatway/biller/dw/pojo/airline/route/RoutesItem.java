package com.trimindi.switching.gatway.biller.dw.pojo.airline.route;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class RoutesItem {

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destinationName")
    private String destinationName;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("originName")
    private String originName;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    @Override
    public String toString() {
        return
                "RoutesItem{" +
                        "origin = '" + origin + '\'' +
                        ",destinationName = '" + destinationName + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",originName = '" + originName + '\'' +
                        "}";
    }
}