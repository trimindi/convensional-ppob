package com.trimindi.switching.gatway.biller.dw.pojo.hotel.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelBookingDetail {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("bookingDetail")
    private BookingDetail bookingDetail;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("status")
    private String status;

    public GetHotelBookingDetail() {
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public BookingDetail getBookingDetail() {
        return bookingDetail;
    }

    public void setBookingDetail(BookingDetail bookingDetail) {
        this.bookingDetail = bookingDetail;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetHotelBookingDetail{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",bookingDetail = '" + bookingDetail + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}