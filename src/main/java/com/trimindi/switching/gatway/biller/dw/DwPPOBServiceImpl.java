package com.trimindi.switching.gatway.biller.dw;

import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.dw.pojo.pulsa.PulsaMaticRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.pulsa.PulsaMaticResponse;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.StringXOR;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import com.trimindi.switching.gatway.utils.generator.ServindoRegIdGenerator;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by sx on 10/03/18.
 * Copyright under comercial unit
 */
@Service
public class DwPPOBServiceImpl implements DwPPOBService {
    final
    CloseableHttpClient client;
    private final TransaksiService transaksiService;
    private final ProductFeeService productFeeService;
    private final PartnerDepositService partnerDepositService;
    private final SlackSendMessage slackSendMessage;
    private Logger logger = LoggerFactory.getLogger(DwPPOBServiceImpl.class);
    private Transaksi transaksi;
    @Value("${di.serpul.host}")
    private String host;
    @Value("${di.serpul.username}")
    private String username;
    @Value("${di.serpul.password}")
    private String password;
    @Autowired
    private ServindoRegIdGenerator servindoRegIdGenerator;

    @Autowired
    public DwPPOBServiceImpl(CloseableHttpClient client, TransaksiService transaksiService, PartnerDepositService partnerDepositService, SlackSendMessage slackSendMessage, ProductFeeService productFeeService) {
        this.client = client;
        this.transaksiService = transaksiService;
        this.partnerDepositService = partnerDepositService;
        this.slackSendMessage = slackSendMessage;
        this.productFeeService = productFeeService;
    }

    @Override
    public void Pulsa(AsyncResponse resp, ProductItem product, PartnerCredential partnerCredential, Map<String, String> params) {
        String reg_id = servindoRegIdGenerator.nextTrace();

        try {
            ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), product.getDENOM());
            transaksi = new Transaksi()
                    .setADMIN(0)
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setMSSIDN_NAME(params.get(Constanta.MSSIDN))
                    .setMSSIDN(params.get(Constanta.MSSIDN))
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                    .setPRODUCT(product.getProduct_id())
                    .setTAGIHAN(product.getAMOUT() + productFee.getFEE_CA())
                    .setDENOM(product.getDENOM())
                    .setFEE_CA(productFee.getFEE_CA())
                    .setFEE_DEALER(0)
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setCHARGE(product.getAMOUT() + productFee.getFEE_CA())
                    .setDEBET(product.getAMOUT() + productFee.getFEE_CA())
                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setDENDA(0)
                    .setST(Status.PAYMENT_PROSESS);
            transaksiService.save(transaksi);
            if (partnerDepositService.bookingSaldo(transaksi)) {
                String time = new SimpleDateFormat("HHmmss").format(new Date(System.currentTimeMillis()));
                PulsaMaticRequest pulsaMaticRequest = new PulsaMaticRequest()
                        .setCommand("TOPUP")
                        .setUserid(username)
                        .setVtype(product.getDENOM_BILLER())
                        .setMsisdn(params.get(Constanta.MSSIDN))
                        .setTime(time)
                        .setTrxid(reg_id)
                        .setSign(generateSign(time, params.get(Constanta.MSSIDN)));
                JAXBContext jc = JAXBContext.newInstance(PulsaMaticRequest.class);
                Marshaller marshaller = jc.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
                StringWriter sw = new StringWriter();
                marshaller.marshal(pulsaMaticRequest, sw);
                String response = this.process(host, sw.toString());
                JAXBContext jc2 = JAXBContext.newInstance(PulsaMaticResponse.class);
                Unmarshaller unmarshaller = jc2.createUnmarshaller();
                PulsaMaticResponse pulsaMaticResponse =
                        (PulsaMaticResponse) unmarshaller.unmarshal(new StringReader(response));
                switch (pulsaMaticResponse.getResult()) {
                    case "success":
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(response)
                                .setHOST_REF_NUMBER(reg_id)
                                .setBILL_REF_NUMBER(pulsaMaticResponse.getTrxid())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        Map<String, String> r = new HashMap<>();
                        r.put("sn", pulsaMaticResponse.getTrxid());
                        r.put("message", String.format("SUKSES! Trx %s %s berhasil. SN: %s", transaksi.getDENOM(), transaksi.getMSSIDN(), pulsaMaticResponse.getTrxid()));
                        BaseResponse<Map<String, String>> baseResponse = new BaseResponse<>();
                        baseResponse.setData(r);
                        baseResponse.setNtrans(transaksi.getNTRANS());
                        baseResponse.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                        baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        baseResponse.setProduct(transaksi.getDENOM());
                        baseResponse.setMessage("Successful");
                        baseResponse.setFee(0);
                        resp.resume(Response.status(200).entity(baseResponse).build());
                        break;
                    case "pending":
                        transaksi
                                .setPAYMENT(response)
                                .setHOST_REF_NUMBER(reg_id)
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        resp.resume(Response.status(200)
                                .entity(ResponseCode.PAYMENT_UNDER_PROSES
                                        .setNtrans(transaksi.getNTRANS())
                                        .setProduct(transaksi.getDENOM())
                                        .setMssidn(transaksi.getMSSIDN())
                                        .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE())
                                        .setSaldoTerpotong(transaksi.getCHARGE())
                                ).build());
                        break;
                    default:
                        partnerDepositService.reverseSaldo(transaksi, response);
                        resp.resume(Response.status(200).entity(
                                ResponseCode.PAYMENT_FAILED
                                        .setProduct(product.getDENOM())
                                        .setNtrans(transaksi.getNTRANS())
                                        .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE())
                                        .setMssidn(params.get(Constanta.MSSIDN))
                        ).build());
                        break;

                }
            } else {
                transaksiService.deleteById(transaksi.getNTRANS());
                resp.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO
                        .setProduct(product.getDENOM())
                        .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE())
                        .setMssidn(params.get(Constanta.MSSIDN))).build());
            }
        } catch (JAXBException | IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getMessage(), e);
            resp.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE
                    .setProduct(product.getDENOM())
                    .setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE())
                    .setMssidn(params.get(Constanta.MSSIDN))).build());
        }
    }

    @Override
    public void InquiryCC(AsyncResponse asyncResponse, ProductItem productItem, PartnerCredential partnerCredential, Map<String, String> param) {
        String reg_id = servindoRegIdGenerator.nextTrace();
        XmlRpcClientConfigImpl xmlRpcClientConfig = new XmlRpcClientConfigImpl();
        try {
            xmlRpcClientConfig.setServerURL(new URL("http://localhost/ppob2/inquiry.php"));
            Map<String, Object> params = new HashMap<>();
            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
            params.put("user", username);
            params.put("waktu", time);
            params.put("produk", productItem.getDENOM_BILLER());
            params.put("idpel", params.get(Constanta.MSSIDN));
            params.put("hppel", params.get(Constanta.NOHP));
            params.put("reffid", reg_id);
            params.put("signature", generateSignature(time));
            List<Object> register_params = new ArrayList<>();
            register_params.add(params);
            ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), productItem.getDENOM());
            XmlRpcClient xmlRpcClient = new XmlRpcClient();
            xmlRpcClient.setConfig(xmlRpcClientConfig);
            Map<String, String> response = (Map<String, String>) xmlRpcClient.execute("PPOB.Inquiry", register_params);
            switch (response.get("code")) {
                case "00":
                    Map<String, String> payload = new HashMap<>();
                    String[] res = response.get("message").split("\\|");
                    for (int i = 0; i < res.length; i++) {
                        String[] _res2 = res[i].split("\\:");
                        payload.put(_res2[0], _res2[1]);
                    }

                    transaksi = new Transaksi()
                            .setADMIN(0)
                            .setPARTNERID(partnerCredential.getPartner_id())
                            .setUSERID(partnerCredential.getPartner_uid())
                            .setMSSIDN_NAME(param.get(Constanta.MSSIDN))
                            .setMSSIDN(param.get(Constanta.MSSIDN))
                            .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                            .setMERCHANT_ID(param.get(Constanta.MACHINE_TYPE))
                            .setPRODUCT(productItem.getProduct_id())
                            .setTAGIHAN(productItem.getAMOUT() + productFee.getFEE_CA())
                            .setDENOM(productItem.getDENOM())
                            .setFEE_CA(productFee.getFEE_CA())
                            .setFEE_DEALER(0)
                            .setUSERID(partnerCredential.getPartner_uid())
                            .setCHARGE(productItem.getAMOUT() + productFee.getFEE_CA())
                            .setDEBET(productItem.getAMOUT() + productFee.getFEE_CA())
                            .setIP_ADDRESS(param.get(Constanta.IP_ADDRESS))
                            .setPARTNERID(partnerCredential.getPartner_id())
                            .setDENDA(0)
                            .setINQUIRY(response.get("response"))
                            .setST(Status.INQUIRY);
                    transaksiService.save(transaksi);
                    payload.remove("saldo");
                    payload.remove("charge");
                    BaseResponse<Map<String, String>> baseResponse = new BaseResponse<>();
                    baseResponse.setData(payload);
                    baseResponse.setNtrans(transaksi.getNTRANS());
                    baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                    baseResponse.setMessage("Successful");
                    baseResponse.setProduct(productItem.getDENOM());
                    asyncResponse.resume(baseResponse);
                    break;
                case "667":
                    asyncResponse.resume(
                            Response.status(200)
                                    .entity(ResponseCode.INQUIRY_FAILED.setMessage(response.get("message")).setProduct(param.get(Constanta.DENOM))).build());
                    break;
                case "999":
                    asyncResponse.resume(
                            Response.status(200)
                                    .entity(ResponseCode.INQUIRY_FAILED.setMessage(response.get("message")).setProduct(param.get(Constanta.DENOM))).build());
                    break;

            }

        } catch (MalformedURLException | XmlRpcException e) {
            asyncResponse.resume(
                    Response.status(200).entity(ResponseCode.INQUIRY_FAILED).build());
        }
    }

    @Override
    public void PaymentCC(AsyncResponse response, Transaksi transaksi) {

    }

    private String generateSignature(String time) {
        return new String(DigestUtils.md5(username + password + time));
    }

    private String generateSign(String time, String mssidn) {
        assert mssidn != null;
        String word = mssidn.substring(mssidn.length() - 4);
        String a = time + word;
        String b = reverse(word) + password;
        return StringXOR.encode(a, b);
    }

    private String reverse(String word) {
        char[] chs = word.toCharArray();

        int i = 0, j = chs.length - 1;
        while (i < j) {
            // swap chs[i] and chs[j]
            char t = chs[i];
            chs[i] = chs[j];
            chs[j] = t;
            i++;
            j--;
        }
        return String.valueOf(chs);
    }

    private String process(String url, String payload) throws IOException {
        logger.error("send req to       -> {}", url);
        logger.error("send req payload  -> {}", payload);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept", "application/xml");
        httpPost.setHeader("Content-type", "application/xml");
        httpPost.setEntity(new StringEntity(payload));
        CloseableHttpResponse execute = client.execute(httpPost);
        String response = EntityUtils.toString(execute.getEntity(), StandardCharsets.UTF_8.name());
        logger.error("Response req  -> {}", response);
        return response;
    }
}
