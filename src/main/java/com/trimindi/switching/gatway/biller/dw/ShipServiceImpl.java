package com.trimindi.switching.gatway.biller.dw;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.trimindi.switching.gatway.DwSession;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.avability.request.GetShipAvabilityRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.avability.response.GetShipAvabilityResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.request.SetShipBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.response.SetShipBookingResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.detail.request.GetShipBookingDetailRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.detail.response.GetShipBookingDetailResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.issued.request.SetShipIssuedRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.issued.response.SetShipIssuedResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.room.request.GetShipRoomRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.room.response.GetShipRoomResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.route.GetShipRouteResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.request.GetShipScheduleRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.response.GetShipScheduleResponse;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;

/**
 * Created by sx on 26/01/18.
 * Copyright under comercial unit
 */
@Service
public class ShipServiceImpl implements ShipService {
    public static final Logger logger = LoggerFactory.getLogger(ShipServiceImpl.class);
    private static final String SHIP = "SHIP";
    final
    ObjectMapper objectMapper;
    final
    CloseableHttpClient client;
    final
    PartnerDepositService partnerDepositService;
    final
    ProductItemService productItemService;
    final
    ProductFeeService productFeeService;
    private final TransaksiService transaksiService;
    @Autowired
    private
    DwSession session;
    @Value("${dw.url.ship.route}")
    private String shipRoute;
    @Value("${dw.url.ship.schedule}")
    private String shipListschedule;
    @Value("${dw.url.ship.avability}")
    private String shipListavability;
    @Value("${dw.url.ship.room}")
    private String shipListroom;
    @Value("${dw.url.ship.booking}")
    private String shipbooking;
    @Value("${dw.url.ship.issued}")
    private String shipissued;
    @Value("${dw.url.ship.bookingdetail}")
    private String shipDetailBooking;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public ShipServiceImpl(ObjectMapper objectMapper, CloseableHttpClient client, PartnerDepositService partnerDepositService, ProductItemService productItemService, ProductFeeService productFeeService, TransaksiService transaksiService) {
        this.objectMapper = objectMapper;
        this.client = client;
        this.partnerDepositService = partnerDepositService;
        this.productItemService = productItemService;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
    }

    private String process(String url, String payload) throws IOException {
        logger.error("send req to       -> {}", url);
        logger.error("send req payload  -> {}", payload);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setEntity(new StringEntity(payload));
        CloseableHttpResponse execute = client.execute(httpPost);
        String respone = EntityUtils.toString(execute.getEntity(), StandardCharsets.UTF_8.name());
        logger.error("Response req  -> {}", respone);
        return respone;
    }

    @Override
    public Response GetShipRoutes() {
        try {
            ObjectNode objectNode = objectMapper.createObjectNode();
            objectNode.put("userID", session.authKey().getUserID());
            objectNode.put("accessToken", session.authKey().getAccessToken());
            GetShipRouteResponse response = objectMapper.readValue(this.process(shipRoute, objectMapper.writeValueAsString(objectNode)), GetShipRouteResponse.class);
            BaseResponse<GetShipRouteResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response);
                base.setProduct(SHIP);
            } else {
                base.setStatus("0005");
                base.setProduct(SHIP);
                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetShipRouteResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(SHIP);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetShipSchedule(GetShipScheduleRequest getShipScheduleRequest) {
        try {
            getShipScheduleRequest.setAccessToken(session.authKey().getAccessToken());
            getShipScheduleRequest.setUserID(session.authKey().getUserID());
            GetShipScheduleResponse response = objectMapper.readValue(this.process(shipListschedule, objectMapper.writeValueAsString(getShipScheduleRequest)), GetShipScheduleResponse.class);
            BaseResponse<GetShipScheduleResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response);
                base.setProduct(SHIP);
            } else {
                base.setStatus("0005");
                base.setProduct(SHIP);
                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetShipRouteResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(SHIP);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetShipAvailability(GetShipAvabilityRequest getShipAvabilityRequest) {
        try {
            getShipAvabilityRequest.setAccessToken(session.authKey().getAccessToken());
            getShipAvabilityRequest.setUserID(session.authKey().getUserID());
            GetShipAvabilityResponse response = objectMapper.readValue(this.process(shipListavability, objectMapper.writeValueAsString(getShipAvabilityRequest)), GetShipAvabilityResponse.class);
            BaseResponse<GetShipAvabilityResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(SHIP);
                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setProduct(SHIP);
                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetShipAvabilityResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(SHIP);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetShipRoom(GetShipRoomRequest getShipRoomRequest) {
        try {
            getShipRoomRequest.setAccessToken(session.authKey().getAccessToken());
            getShipRoomRequest.setUserID(session.authKey().getUserID());
            GetShipRoomResponse response = objectMapper.readValue(this.process(shipListroom, objectMapper.writeValueAsString(getShipRoomRequest)), GetShipRoomResponse.class);
            BaseResponse<GetShipRoomResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response);
                base.setProduct(SHIP);

            } else {
                base.setStatus("0005");
                base.setProduct(SHIP);

                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetShipRoomResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(SHIP);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response SetBookingShip(SetShipBookingRequest setShipBookingRequest, PartnerCredential partnerCredential) {
        try {
            setShipBookingRequest.setUserID(session.authKey().getUserID());
            setShipBookingRequest.setAccessToken(session.authKey().getAccessToken());
            String payload = this.process(shipbooking, objectMapper.writeValueAsString(setShipBookingRequest));
            SetShipBookingResponse response = objectMapper.readValue(payload, SetShipBookingResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                ProductFee kai = productFeeService.findFeePartner(partnerCredential.getPartner_id(), SHIP);
                Transaksi transaksi = new Transaksi();
                transaksi.setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                        .setDATE(new Timestamp(System.currentTimeMillis()))
                        .setUSERID(partnerCredential.getPartner_uid())
                        .setPARTNERID(partnerCredential.getPartner_id())
                        .setBILL_REF_NUMBER(response.getNumCode())
                        .setDATA(response.getBookingDateTime())
                        .setST(Status.INQUIRY)
                        .setTAGIHAN(response.getTicketPrice())
                        .setDEBET(response.getSalesPrice() + kai.getFEE_CA())
                        .setCHARGE(response.getSalesPrice() + kai.getFEE_CA())
                        .setADMIN(0)
                        .setPRODUCT(SHIP)
                        .setDENOM(SHIP)
                        .setMSSIDN_NAME("")
                        .setMSSIDN("")
                        .setFEE_CA(kai.getFEE_CA())
                        .setFEE_DEALER(kai.getFEE_CA())
                        .setINQUIRY(payload);
                transaksiService.save(transaksi);
                BaseResponse<SetShipBookingResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setNtrans(transaksi.getNTRANS());
                base.setProduct(SHIP);
                base.setSaldoTerpotong(transaksi.getCHARGE());
                base.setFee(transaksi.getFEE_DEALER());
                base.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                base.setMessage("SUCCESS");
                return Response.status(200).entity(base).build();
            } else {
                BaseResponse<SetShipBookingResponse> base = new BaseResponse<>();
                base.setMessage(response.getRespMessage());
                base.setStatus("0005");
                base.setProduct(SHIP);

                return Response.status(200).entity(base).build();
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<SetShipBookingResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(SHIP);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response SetIssuedShip(SetShipIssuedRequest setShipIssuedRequest, Transaksi transaksi) {
        try {
            setShipIssuedRequest.setUserID(session.authKey().getUserID());
            setShipIssuedRequest.setAccessToken(session.authKey().getAccessToken());
            SetShipIssuedResponse response = objectMapper.readValue(this.process(shipissued, objectMapper.writeValueAsString(setShipIssuedRequest)), SetShipIssuedResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                transaksiService.save(transaksi.setST(Status.PAYMENT_SUCCESS)
                        .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis())));
                BaseResponse<SetShipIssuedResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(SHIP);
                base.setNtrans(transaksi.getNTRANS());
                base.setSaldoTerpotong(transaksi.getCHARGE());
                return Response.status(200).entity(base).build();
            } else {
                partnerDepositService.reverseSaldo(transaksi, objectMapper.writeValueAsString(response));
                BaseResponse<SetShipIssuedResponse> base = new BaseResponse<>();
                base.setStatus("0005");
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(SHIP);
                base.setNtrans(transaksi.getNTRANS());
                return Response.status(200).entity(base).build();
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<SetShipIssuedResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(SHIP);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetListShipBooking() {
        return null;
    }

    @Override
    public Response GetShipBookingDetail(GetShipBookingDetailRequest getShipBookingDetailRequest, Transaksi transaksi) {
        try {
            getShipBookingDetailRequest.setUserID(session.authKey().getUserID());
            getShipBookingDetailRequest.setAccessToken(session.authKey().getAccessToken());
            GetShipBookingDetailResponse response = objectMapper.readValue(this.process(shipDetailBooking, objectMapper.writeValueAsString(getShipBookingDetailRequest)), GetShipBookingDetailResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                BaseResponse<GetShipBookingDetailResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(SHIP);
                base.setNtrans(transaksi.getNTRANS());
                return Response.status(200).entity(base).build();
            } else {
                BaseResponse<GetShipBookingDetailResponse> base = new BaseResponse<>();
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(SHIP);
                base.setNtrans(transaksi.getNTRANS());
                return Response.status(200).entity(base).build();
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetShipBookingDetailResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(SHIP);
            return Response.status(200).entity(base).build();
        }
    }
}
