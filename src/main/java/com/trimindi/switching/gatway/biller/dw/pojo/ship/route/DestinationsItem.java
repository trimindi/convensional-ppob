package com.trimindi.switching.gatway.biller.dw.pojo.ship.route;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class DestinationsItem {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("destinationName")
    private String destinationName;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    @Override
    public String toString() {
        return
                "DestinationsItem{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",destinationName = '" + destinationName + '\'' +
                        "}";
    }
}