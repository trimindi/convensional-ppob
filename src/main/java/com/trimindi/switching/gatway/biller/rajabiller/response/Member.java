package com.trimindi.switching.gatway.biller.rajabiller.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class Member {
    private String name;

    private Value value;

    public Member() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ClassPojo [name = " + name + ", value = " + value + "]";
    }
}
