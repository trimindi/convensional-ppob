package com.trimindi.switching.gatway.biller.dw.pojo.airline.booking;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SetAirlineBookingRequest {

    @JsonProperty("insurance")
    private String insurance;

    @JsonProperty("contactAreaCodePhone")
    private String contactAreaCodePhone;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("contactFirstName")
    private String contactFirstName;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("searchKey")
    private String searchKey;

    @JsonProperty("schReturns")
    private List<SchDepartsItem> schReturns = new ArrayList<>();

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("schDeparts")
    private List<SchDepartsItem> schDeparts = new ArrayList<>();

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("contactTitle")
    private String contactTitle;

    @JsonProperty("paxChild")
    private String paxChild;

    @JsonProperty("paxInfant")
    private String paxInfant;

    @JsonProperty("contactCountryCodePhone")
    private String contactCountryCodePhone;

    @JsonProperty("paxDetails")
    private List<PaxDetailsItem> paxDetails;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("contactLastName")
    private String contactLastName;

    @JsonProperty("paxAdult")
    private String paxAdult;

    @JsonProperty("contactRemainingPhoneNo")
    private String contactRemainingPhoneNo;

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getContactAreaCodePhone() {
        return contactAreaCodePhone;
    }

    public void setContactAreaCodePhone(String contactAreaCodePhone) {
        this.contactAreaCodePhone = contactAreaCodePhone;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public List<SchDepartsItem> getSchReturns() {
        return schReturns;
    }

    public void setSchReturns(List<SchDepartsItem> schReturns) {
        this.schReturns = schReturns;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public List<SchDepartsItem> getSchDeparts() {
        return schDeparts;
    }

    public void setSchDeparts(List<SchDepartsItem> schDeparts) {
        this.schDeparts = schDeparts;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(String paxChild) {
        this.paxChild = paxChild;
    }

    public String getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(String paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getContactCountryCodePhone() {
        return contactCountryCodePhone;
    }

    public void setContactCountryCodePhone(String contactCountryCodePhone) {
        this.contactCountryCodePhone = contactCountryCodePhone;
    }

    public List<PaxDetailsItem> getPaxDetails() {
        return paxDetails;
    }

    public void setPaxDetails(List<PaxDetailsItem> paxDetails) {
        this.paxDetails = paxDetails;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(String paxAdult) {
        this.paxAdult = paxAdult;
    }

    public String getContactRemainingPhoneNo() {
        return contactRemainingPhoneNo;
    }

    public void setContactRemainingPhoneNo(String contactRemainingPhoneNo) {
        this.contactRemainingPhoneNo = contactRemainingPhoneNo;
    }

    @Override
    public String toString() {
        return
                "SetAirlineBookingRequest{" +
                        "insurance = '" + insurance + '\'' +
                        ",contactAreaCodePhone = '" + contactAreaCodePhone + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",contactFirstName = '" + contactFirstName + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",searchKey = '" + searchKey + '\'' +
                        ",schReturns = '" + schReturns + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",schDeparts = '" + schDeparts + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",contactTitle = '" + contactTitle + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",contactCountryCodePhone = '" + contactCountryCodePhone + '\'' +
                        ",paxDetails = '" + paxDetails + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",contactLastName = '" + contactLastName + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",contactRemainingPhoneNo = '" + contactRemainingPhoneNo + '\'' +
                        "}";
    }
}