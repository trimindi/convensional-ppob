package com.trimindi.switching.gatway.biller.dw.pojo.train.seat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetTrainSeatMapResponse {

    @JsonProperty("trainID")
    private String trainID;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonIgnore
    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("seats")
    private List<Seats> seats;

    @JsonIgnore
    @JsonProperty("userID")
    private String userID;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonIgnore
    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("trainNumber")
    private String trainNumber;

    @JsonProperty("status")
    private String status;

    public String getTrainID() {
        return trainID;
    }

    public void setTrainID(String trainID) {
        this.trainID = trainID;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public List<Seats> getSeats() {
        return seats;
    }

    public void setSeats(List<Seats> seats) {
        this.seats = seats;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetTrainSeatMapResponse{" +
                        "trainID = '" + trainID + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",seats = '" + seats + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",trainNumber = '" + trainNumber + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}