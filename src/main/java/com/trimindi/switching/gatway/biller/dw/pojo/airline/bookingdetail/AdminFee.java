package com.trimindi.switching.gatway.biller.dw.pojo.airline.bookingdetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class AdminFee {

    @JsonProperty("memberMarkup")
    private double memberMarkup;

    @JsonProperty("salesPrice")
    private double salesPrice;

    @JsonProperty("airlineMarkup")
    private double airlineMarkup;

    @JsonProperty("memberDiscount")
    private double memberDiscount;

    public double getMemberMarkup() {
        return memberMarkup;
    }

    public void setMemberMarkup(double memberMarkup) {
        this.memberMarkup = memberMarkup;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public double getAirlineMarkup() {
        return airlineMarkup;
    }

    public void setAirlineMarkup(double airlineMarkup) {
        this.airlineMarkup = airlineMarkup;
    }

    public double getMemberDiscount() {
        return memberDiscount;
    }

    public void setMemberDiscount(double memberDiscount) {
        this.memberDiscount = memberDiscount;
    }

    @Override
    public String toString() {
        return
                "AdminFee{" +
                        "memberMarkup = '" + memberMarkup + '\'' +
                        ",salesPrice = '" + salesPrice + '\'' +
                        ",airlineMarkup = '" + airlineMarkup + '\'' +
                        ",memberDiscount = '" + memberDiscount + '\'' +
                        "}";
    }
}