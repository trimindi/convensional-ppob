package com.trimindi.switching.gatway.biller.rajabiller;

import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.rajabiller.response.MethodResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Created by PC on 27/09/2017.
 */
@Component
public class RajabillerUtils {
    private static final Logger logger = LoggerFactory.getLogger(RajabillerUtils.class);
    private static JAXBContext jc;
    private static Unmarshaller unmarshaller;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Bean
    public Unmarshaller unmarshaller() {
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        return unmarshaller;
    }
}
