package com.trimindi.switching.gatway.biller.pdamtasik;

import java.util.List;

public class PdamTasikPaymentResponse {
    private String billname;
    private String address;
    private int totalamount;
    private String reff;
    private String billnumber;
    private int billqty;
    private int resultcode;
    private List<DetailItem> detail;
    private String type;
    private String ts;

    public String getBillname() {
        return billname;
    }

    public void setBillname(String billname) {
        this.billname = billname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(int totalamount) {
        this.totalamount = totalamount;
    }

    public String getReff() {
        return reff;
    }

    public void setReff(String reff) {
        this.reff = reff;
    }

    public String getBillnumber() {
        return billnumber;
    }

    public void setBillnumber(String billnumber) {
        this.billnumber = billnumber;
    }

    public int getBillqty() {
        return billqty;
    }

    public void setBillqty(int billqty) {
        this.billqty = billqty;
    }

    public int getResultcode() {
        return resultcode;
    }

    public void setResultcode(int resultcode) {
        this.resultcode = resultcode;
    }

    public List<DetailItem> getDetail() {
        return detail;
    }

    public void setDetail(List<DetailItem> detail) {
        this.detail = detail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    @Override
    public String toString() {
        return
                "PdamTasikPaymentResponse{" +
                        "billname = '" + billname + '\'' +
                        ",address = '" + address + '\'' +
                        ",totalamount = '" + totalamount + '\'' +
                        ",reff = '" + reff + '\'' +
                        ",billnumber = '" + billnumber + '\'' +
                        ",billqty = '" + billqty + '\'' +
                        ",resultcode = '" + resultcode + '\'' +
                        ",detail = '" + detail + '\'' +
                        ",type = '" + type + '\'' +
                        ",ts = '" + ts + '\'' +
                        "}";
    }
}