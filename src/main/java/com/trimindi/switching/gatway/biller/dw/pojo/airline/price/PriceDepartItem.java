package com.trimindi.switching.gatway.biller.dw.pojo.airline.price;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class PriceDepartItem {

    @JsonProperty("psDate")
    private String psDate;

    @JsonProperty("classFare")
    private String classFare;

    @JsonProperty("priceDetail")
    private List<PriceDetailItem> priceDetail;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("psDestination")
    private String psDestination;

    @JsonProperty("flightNumber")
    private String flightNumber;

    @JsonProperty("psOrigin")
    private String psOrigin;

    public String getPsDate() {
        return psDate;
    }

    public void setPsDate(String psDate) {
        this.psDate = psDate;
    }

    public String getClassFare() {
        return classFare;
    }

    public void setClassFare(String classFare) {
        this.classFare = classFare;
    }

    public List<PriceDetailItem> getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(List<PriceDetailItem> priceDetail) {
        this.priceDetail = priceDetail;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPsDestination() {
        return psDestination;
    }

    public void setPsDestination(String psDestination) {
        this.psDestination = psDestination;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getPsOrigin() {
        return psOrigin;
    }

    public void setPsOrigin(String psOrigin) {
        this.psOrigin = psOrigin;
    }

    @Override
    public String toString() {
        return
                "PriceDepartItem{" +
                        "psDate = '" + psDate + '\'' +
                        ",classFare = '" + classFare + '\'' +
                        ",priceDetail = '" + priceDetail + '\'' +
                        ",currency = '" + currency + '\'' +
                        ",psDestination = '" + psDestination + '\'' +
                        ",flightNumber = '" + flightNumber + '\'' +
                        ",psOrigin = '" + psOrigin + '\'' +
                        "}";
    }
}