package com.trimindi.switching.gatway.biller.dw.pojo.hotel.images.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelImageResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("images")
    private List<String> images = new ArrayList<>();

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("status")
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetHotelImageResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",images = '" + images + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}