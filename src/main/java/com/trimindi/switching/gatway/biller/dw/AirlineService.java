package com.trimindi.switching.gatway.biller.dw;

import com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage.GetAirlineBagageAndMealRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage.GetAirlineBagageResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.booking.SetAirlineBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.booking.SetAirlineBookingResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.bookingdetail.BookingDetailResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.daftar.AirlinesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.issued.SetAirlineIssuedRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.issued.SetArilineIssuedResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.natinal.CountriesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.price.GetAirlinePriceRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.price.GetAirlinePriceResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.route.GetAirlineRouteRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.route.RoutesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule.GetAirlineScheduleRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.seat.GetAirlineSeatRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.seat.GetAirlineSeatResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by sx on 18/12/17.
 * Copyright under comercial unit
 */
public interface AirlineService {
    BaseResponse<List<AirlinesItem>> getAirlineList();

    BaseResponse<List<CountriesItem>> getAirlineCountry();

    BaseResponse<List<RoutesItem>> getAirlineRoutes(GetAirlineRouteRequest getAirlineRouteRequest);

    Response getAirlineSchedule(GetAirlineScheduleRequest getAirlineScheduleRequest);

    BaseResponse<GetAirlineBagageResponse> getAirlineBagage(GetAirlineBagageAndMealRequest getAirlineBagageAndMealRequest);

    BaseResponse<GetAirlineSeatResponse> getAirlineSeatMap(GetAirlineSeatRequest getAirlineSeatRequest);

    BaseResponse<GetAirlinePriceResponse> getAilinePrice(GetAirlinePriceRequest getAirlinePriceRequest);

    BaseResponse<SetAirlineBookingResponse> setBookingAirline(SetAirlineBookingRequest setAirlineBookingRequest, PartnerCredential partnerCredential);

    BaseResponse<SetArilineIssuedResponse> setIssuedAirline(SetAirlineIssuedRequest setIssuedAirlineRequest, Transaksi transaksi);

    BaseResponse<BookingDetailResponse> getBookingDetail(BookingDetailRequest bookingDetailRequest, Transaksi transaksi);
}
