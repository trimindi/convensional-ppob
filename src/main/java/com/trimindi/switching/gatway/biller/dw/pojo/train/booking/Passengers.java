package com.trimindi.switching.gatway.biller.dw.pojo.train.booking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Passengers {

    @JsonProperty("wagonCode")
    private String wagonCode;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("name")
    private String name;

    @JsonProperty("type")
    private String type;

    @JsonProperty("wagonNumber")
    private String wagonNumber;

    @JsonProperty("birthDate")
    private String birthDate;

    @JsonProperty("seatNumber")
    private String seatNumber;

    @JsonProperty("IDNumber")
    private String iDNumber;

    public String getWagonCode() {
        return wagonCode;
    }

    public void setWagonCode(String wagonCode) {
        this.wagonCode = wagonCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWagonNumber() {
        return wagonNumber;
    }

    public void setWagonNumber(String wagonNumber) {
        this.wagonNumber = wagonNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getIDNumber() {
        return iDNumber;
    }

    public void setIDNumber(String iDNumber) {
        this.iDNumber = iDNumber;
    }

    @Override
    public String toString() {
        return
                "Passengers{" +
                        "wagonCode = '" + wagonCode + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",name = '" + name + '\'' +
                        ",type = '" + type + '\'' +
                        ",wagonNumber = '" + wagonNumber + '\'' +
                        ",birthDate = '" + birthDate + '\'' +
                        ",seatNumber = '" + seatNumber + '\'' +
                        ",iDNumber = '" + iDNumber + '\'' +
                        "}";
    }
}