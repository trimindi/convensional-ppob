package com.trimindi.switching.gatway.biller.dw.pojo.hotel.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class BookingDetail {

    @JsonProperty("breakFast")
    private String breakFast;

    @JsonProperty("totalPrice")
    private double totalPrice;

    @JsonProperty("cancelationPolicy")
    private String cancelationPolicy;

    @JsonProperty("hotelAddress")
    private String hotelAddress;

    @JsonProperty("cancelFee")
    private double cancelFee;

    @JsonProperty("hotelName")
    private String hotelName;

    @JsonProperty("hotelLogo")
    private String hotelLogo;

    @JsonProperty("checkInDate")
    private String checkInDate;

    @JsonProperty("requestDescription")
    private String requestDescription;

    @JsonProperty("roomName")
    private String roomName;

    @JsonProperty("voucherNo")
    private String voucherNo;

    @JsonProperty("osRefNo")
    private String osRefNo;

    @JsonProperty("checkOutDate")
    private String checkOutDate;

    @JsonProperty("roomNum")
    private int roomNum;

    @JsonProperty("hotelImages")
    private String hotelImages;

    @JsonProperty("bookingStatus")
    private String bookingStatus;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("refundDate")
    private String refundDate;

    @JsonProperty("cancelationRemark")
    private String cancelationRemark;

    @JsonProperty("reservationNo")
    private String reservationNo;

    @JsonProperty("refund")
    private double refund;

    public BookingDetail() {
    }

    public String getBreakFast() {
        return breakFast;
    }

    public BookingDetail setBreakFast(String breakFast) {
        this.breakFast = breakFast;
        return this;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public BookingDetail setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public String getCancelationPolicy() {
        return cancelationPolicy;
    }

    public BookingDetail setCancelationPolicy(String cancelationPolicy) {
        this.cancelationPolicy = cancelationPolicy;
        return this;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public BookingDetail setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
        return this;
    }

    public double getCancelFee() {
        return cancelFee;
    }

    public BookingDetail setCancelFee(double cancelFee) {
        this.cancelFee = cancelFee;
        return this;
    }

    public String getHotelName() {
        return hotelName;
    }

    public BookingDetail setHotelName(String hotelName) {
        this.hotelName = hotelName;
        return this;
    }

    public String getHotelLogo() {
        return hotelLogo;
    }

    public BookingDetail setHotelLogo(String hotelLogo) {
        this.hotelLogo = hotelLogo;
        return this;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public BookingDetail setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
        return this;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public BookingDetail setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
        return this;
    }

    public String getRoomName() {
        return roomName;
    }

    public BookingDetail setRoomName(String roomName) {
        this.roomName = roomName;
        return this;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public BookingDetail setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
        return this;
    }

    public String getOsRefNo() {
        return osRefNo;
    }

    public BookingDetail setOsRefNo(String osRefNo) {
        this.osRefNo = osRefNo;
        return this;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public BookingDetail setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
        return this;
    }

    public int getRoomNum() {
        return roomNum;
    }

    public BookingDetail setRoomNum(int roomNum) {
        this.roomNum = roomNum;
        return this;
    }

    public String getHotelImages() {
        return hotelImages;
    }

    public BookingDetail setHotelImages(String hotelImages) {
        this.hotelImages = hotelImages;
        return this;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public BookingDetail setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
        return this;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public BookingDetail setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
        return this;
    }

    public String getRefundDate() {
        return refundDate;
    }

    public BookingDetail setRefundDate(String refundDate) {
        this.refundDate = refundDate;
        return this;
    }

    public String getCancelationRemark() {
        return cancelationRemark;
    }

    public BookingDetail setCancelationRemark(String cancelationRemark) {
        this.cancelationRemark = cancelationRemark;
        return this;
    }

    public String getReservationNo() {
        return reservationNo;
    }

    public BookingDetail setReservationNo(String reservationNo) {
        this.reservationNo = reservationNo;
        return this;
    }

    public double getRefund() {
        return refund;
    }

    public BookingDetail setRefund(double refund) {
        this.refund = refund;
        return this;
    }

    @Override
    public String toString() {
        return
                "BookingDetail{" +
                        "breakFast = '" + breakFast + '\'' +
                        ",totalPrice = '" + totalPrice + '\'' +
                        ",cancelationPolicy = '" + cancelationPolicy + '\'' +
                        ",hotelAddress = '" + hotelAddress + '\'' +
                        ",cancelFee = '" + cancelFee + '\'' +
                        ",hotelName = '" + hotelName + '\'' +
                        ",hotelLogo = '" + hotelLogo + '\'' +
                        ",checkInDate = '" + checkInDate + '\'' +
                        ",requestDescription = '" + requestDescription + '\'' +
                        ",roomName = '" + roomName + '\'' +
                        ",voucherNo = '" + voucherNo + '\'' +
                        ",osRefNo = '" + osRefNo + '\'' +
                        ",checkOutDate = '" + checkOutDate + '\'' +
                        ",roomNum = '" + roomNum + '\'' +
                        ",hotelImages = '" + hotelImages + '\'' +
                        ",bookingStatus = '" + bookingStatus + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",refundDate = '" + refundDate + '\'' +
                        ",cancelationRemark = '" + cancelationRemark + '\'' +
                        ",reservationNo = '" + reservationNo + '\'' +
                        ",refund = '" + refund + '\'' +
                        "}";
    }
}