package com.trimindi.switching.gatway.biller.dw;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.DwSession;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.dw.pojo.train.booking.SetBookingTrainRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.booking.SetBookingTrainResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.daftar.GetTrainListRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.daftar.GetTrainListResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.daftar.Train;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.issue.SetIssuedTrainRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.issue.SetIssuedTrainResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.route.GetTrainRouteRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.route.GetTrainRouteResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.route.Route;
import com.trimindi.switching.gatway.biller.dw.pojo.train.schedule.GetTrainSchaduleRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.schedule.GetTrainSchaduleResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.seat.GetTrainSeatMapRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.seat.GetTrainSeatMapResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.takeseat.TakeSeatRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.takeseat.TakeSeatResponse;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by sx on 05/12/17.
 * Copyright under comercial unit
 */
@Service
public class KaiServiceImpl implements KaiService {
    public static final Logger logger = LoggerFactory.getLogger(KaiServiceImpl.class);
    public static final String TRAIN = "TRAIN";
    final
    ObjectMapper objectMapper;
    final
    CloseableHttpClient client;
    @Autowired
    private
    DwSession session;
    final
    PartnerDepositService partnerDepositService;

    final
    ProductItemService productItemService;
    final
    ProductFeeService productFeeService;
    @Value("${dw.url.train.list}")
    private String trainListPath;
    @Value("${dw.url.train.route}")
    private String trainRoute;
    @Value("${dw.url.train.schedule}")
    private String trainListschedule;
    @Value("${dw.url.train.seatmap}")
    private String trainseatmap;
    @Value("${dw.url.train.booking}")
    private String trainbooking;
    @Value("${dw.url.train.takeseat}")
    private String traintakeseat;
    @Value("${dw.url.train.issued}")
    private String trainissued;
    private final TransaksiService transaksiService;
    @Value("${dw.url.train.bookingdetail}")
    private String trainDetailBooking;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public KaiServiceImpl(ObjectMapper objectMapper, CloseableHttpClient client, PartnerDepositService partnerDepositService, ProductItemService productItemService, ProductFeeService productFeeService, TransaksiService transaksiService) {
        this.objectMapper = objectMapper;
        this.client = client;
        this.partnerDepositService = partnerDepositService;
        this.productItemService = productItemService;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
    }

    private String process(String url, String payload) throws IOException {
        logger.error("send req to       -> {}", url);
        logger.error("send req payload  -> {}", payload);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setEntity(new StringEntity(payload));
        CloseableHttpResponse execute = client.execute(httpPost);
        String respone = EntityUtils.toString(execute.getEntity(), StandardCharsets.UTF_8.name());
        logger.error("Response req  -> {}", respone);
        return respone;
    }

    @Override
    public BaseResponse<List<Train>> getTrainList() {
        try {
            GetTrainListRequest getTrainListRequest = new GetTrainListRequest();
            getTrainListRequest.setUserID(session.authKey().getUserID());
            getTrainListRequest.setAccessToken(session.authKey().getAccessToken());
            GetTrainListResponse response = objectMapper.readValue(this.process(trainListPath, objectMapper.writeValueAsString(getTrainListRequest)), GetTrainListResponse.class);
            BaseResponse<List<Train>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response.getTrains());
            } else {
                base.setStatus("0005");
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);

            BaseResponse<List<Train>> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }

    @Override
    public BaseResponse<List<Route>> getTrainRoutes(GetTrainRouteRequest getTrainRouteRequest) {
        try {
            getTrainRouteRequest.setUserID(session.authKey().getUserID());
            getTrainRouteRequest.setAccessToken(session.authKey().getAccessToken());
            GetTrainRouteResponse response = objectMapper.readValue(this.process(trainRoute, objectMapper.writeValueAsString(getTrainRouteRequest)), GetTrainRouteResponse.class);
            BaseResponse<List<Route>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response.getRoutes());
            } else {
                base.setStatus("0005");
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);

            BaseResponse<List<Route>> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }

    @Override
    public BaseResponse<GetTrainSchaduleResponse> getTrainSchedule(GetTrainSchaduleRequest getTrainSchaduleRequest) {
        try {
            getTrainSchaduleRequest.setUserID(session.authKey().getUserID());
            getTrainSchaduleRequest.setAccessToken(session.authKey().getAccessToken());
            GetTrainSchaduleResponse response = objectMapper.readValue(this.process(trainListschedule, objectMapper.writeValueAsString(getTrainSchaduleRequest)), GetTrainSchaduleResponse.class);
            BaseResponse<GetTrainSchaduleResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);

            BaseResponse<GetTrainSchaduleResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }

    @Override
    public BaseResponse<GetTrainSeatMapResponse> getTrainSeatMap(GetTrainSeatMapRequest getTrainSeatMapRequest) {
        try {
            getTrainSeatMapRequest.setUserID(session.authKey().getUserID());
            getTrainSeatMapRequest.setAccessToken(session.authKey().getAccessToken());
            GetTrainSeatMapResponse response = objectMapper.readValue(this.process(trainseatmap, objectMapper.writeValueAsString(getTrainSeatMapRequest)), GetTrainSeatMapResponse.class);
            BaseResponse<GetTrainSeatMapResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetTrainSeatMapResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }

    @Override
    public BaseResponse<TakeSeatResponse> setSeatTrain(TakeSeatRequest takeSeatRequest) {
        try {
            takeSeatRequest.setUserID(session.authKey().getUserID());
            takeSeatRequest.setAccessToken(session.authKey().getAccessToken());
            TakeSeatResponse response = objectMapper.readValue(this.process(traintakeseat, objectMapper.writeValueAsString(takeSeatRequest)), TakeSeatResponse.class);
            BaseResponse<TakeSeatResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<TakeSeatResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }

    @Override
    public BaseResponse<SetBookingTrainResponse> setBookingTrain(SetBookingTrainRequest setBookingTrainRequest, PartnerCredential partnerCredential) {
        try {
            setBookingTrainRequest.setUserID(session.authKey().getUserID());
            setBookingTrainRequest.setAccessToken(session.authKey().getAccessToken());
            String payload = this.process(trainbooking, objectMapper.writeValueAsString(setBookingTrainRequest));
            SetBookingTrainResponse response = objectMapper.readValue(payload, SetBookingTrainResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                ProductFee kai = productFeeService.findFeePartner(partnerCredential.getPartner_id(), TRAIN);
                Transaksi transaksi = new Transaksi();
                transaksi.setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                        .setDATE(new Timestamp(System.currentTimeMillis()))
                        .setUSERID(partnerCredential.getPartner_uid())
                        .setPARTNERID(partnerCredential.getPartner_id())
                        .setBILL_REF_NUMBER(response.getBookingCode())
                        .setDATA(response.getBookingDate())
                        .setST(Status.INQUIRY)
                        .setTAGIHAN(response.getTicketPrice())
                        .setDEBET(response.getSalesPrice() + kai.getFEE_CA())
                        .setCHARGE(response.getSalesPrice() + kai.getFEE_CA())
                        .setADMIN(response.getAdminFee())
                        .setPRODUCT(TRAIN)
                        .setDENOM(setBookingTrainRequest.getTrainID())
                        .setMSSIDN_NAME(setBookingTrainRequest.getContactName())
                        .setMSSIDN(setBookingTrainRequest.getContactPhone())
                        .setFEE_DEALER(kai.getFEE_CA())
                        .setFEE_DEALER(response.getAdminFee() - kai.getFEE_CA())
                        .setINQUIRY(payload);
                transaksiService.save(transaksi);
                response.setSalesPrice(transaksi.getCHARGE());
                response.setTrainMarkup(response.getAdminFee() - kai.getFEE_CA());
                BaseResponse<SetBookingTrainResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setNtrans(transaksi.getNTRANS());
                base.setProduct(TRAIN);
                base.setSaldoTerpotong(transaksi.getCHARGE());
                base.setFee(transaksi.getFEE_DEALER());
                base.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                base.setMessage("SUCCESS");
                return base;
            } else {
                BaseResponse<SetBookingTrainResponse> base = new BaseResponse<>();
                base.setMessage(response.getRespMessage());
                base.setStatus("0005");
                return base;
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<SetBookingTrainResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }

    @Override
    public BaseResponse<SetIssuedTrainResponse> setIssuedTrain(SetIssuedTrainRequest setIssuedTrainRequest, Transaksi transaksi) {
        try {
            setIssuedTrainRequest.setUserID(session.authKey().getUserID());
            setIssuedTrainRequest.setAccessToken(session.authKey().getAccessToken());
            SetIssuedTrainResponse response = objectMapper.readValue(this.process(trainissued, objectMapper.writeValueAsString(setIssuedTrainRequest)), SetIssuedTrainResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                transaksiService.save(transaksi.setST(Status.PAYMENT_SUCCESS)
                        .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis())));
                BaseResponse<SetIssuedTrainResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(TRAIN);
                base.setNtrans(transaksi.getNTRANS());
                base.setSaldoTerpotong(transaksi.getCHARGE());
                return base;
            } else {
                partnerDepositService.reverseSaldoTicket(transaksi, objectMapper.writeValueAsString(response));
                BaseResponse<SetIssuedTrainResponse> base = new BaseResponse<>();
                base.setStatus("0005");
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(TRAIN);
                base.setNtrans(transaksi.getNTRANS());
                return base;
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<SetIssuedTrainResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }

    @Override
    public BaseResponse<BookingDetailResponse> getBookingDetauils(BookingDetailRequest bookingDetailRequest, Transaksi transaksi) {
        try {
            bookingDetailRequest.setUserID(session.authKey().getUserID());
            bookingDetailRequest.setAccessToken(session.authKey().getAccessToken());
            BookingDetailResponse response = objectMapper.readValue(this.process(trainDetailBooking, objectMapper.writeValueAsString(bookingDetailRequest)), BookingDetailResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                BaseResponse<BookingDetailResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(TRAIN);
                base.setNtrans(transaksi.getNTRANS());
                return base;
            } else {
                BaseResponse<BookingDetailResponse> base = new BaseResponse<>();
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(TRAIN);
                base.setNtrans(transaksi.getNTRANS());
                return base;
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<BookingDetailResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(TRAIN);
            return base;
        }
    }
}
