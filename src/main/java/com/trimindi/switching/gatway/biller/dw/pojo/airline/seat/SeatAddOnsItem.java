package com.trimindi.switching.gatway.biller.dw.pojo.airline.seat;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SeatAddOnsItem {

    @JsonProperty("departTime")
    private String departTime;

    @JsonProperty("arrivalTime")
    private String arrivalTime;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("infos")
    private List<InfosItem> infos;

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<InfosItem> getInfos() {
        return infos;
    }

    public void setInfos(List<InfosItem> infos) {
        this.infos = infos;
    }

    @Override
    public String toString() {
        return
                "SeatAddOnsItem{" +
                        "departTime = '" + departTime + '\'' +
                        ",arrivalTime = '" + arrivalTime + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",infos = '" + infos + '\'' +
                        "}";
    }
}