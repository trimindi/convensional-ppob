package com.trimindi.switching.gatway.biller.dw.pojo.hotel;

/**
 * Created by sx on 29/01/18.
 * Copyright under comercial unit
 */
public enum RoomType {
    Single("Single"),
    Twin("Twin"),
    Double("Double"),
    Triple("Triple"),
    Quad("Quad");

    private String Type;

    RoomType(String type) {
        this.Type = type;
    }

    public String getType() {
        return Type;
    }

    public RoomType setType(String type) {
        Type = type;
        return this;
    }
}
