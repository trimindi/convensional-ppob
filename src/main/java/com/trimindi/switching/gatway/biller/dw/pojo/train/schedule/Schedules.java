package com.trimindi.switching.gatway.biller.dw.pojo.train.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class Schedules {

    @JsonProperty("availibilityClasses")
    private List<AvailibilityClasses> availibilityClasses;

    @JsonProperty("departTime")
    private String departTime;

    @JsonProperty("arrivalTime")
    private String arrivalTime;

    @JsonProperty("trainName")
    private String trainName;

    @JsonProperty("trainNumber")
    private String trainNumber;

    public List<AvailibilityClasses> getAvailibilityClasses() {
        return availibilityClasses;
    }

    public void setAvailibilityClasses(List<AvailibilityClasses> availibilityClasses) {
        this.availibilityClasses = availibilityClasses;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    @Override
    public String toString() {
        return
                "Schedules{" +
                        "availibilityClasses = '" + availibilityClasses + '\'' +
                        ",departTime = '" + departTime + '\'' +
                        ",arrivalTime = '" + arrivalTime + '\'' +
                        ",trainName = '" + trainName + '\'' +
                        ",trainNumber = '" + trainNumber + '\'' +
                        "}";
    }
}