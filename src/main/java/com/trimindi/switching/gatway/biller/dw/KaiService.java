package com.trimindi.switching.gatway.biller.dw;

import com.trimindi.switching.gatway.biller.dw.pojo.train.booking.SetBookingTrainRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.booking.SetBookingTrainResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.daftar.Train;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.issue.SetIssuedTrainRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.issue.SetIssuedTrainResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.route.GetTrainRouteRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.route.Route;
import com.trimindi.switching.gatway.biller.dw.pojo.train.schedule.GetTrainSchaduleRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.schedule.GetTrainSchaduleResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.seat.GetTrainSeatMapRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.seat.GetTrainSeatMapResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.takeseat.TakeSeatRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.train.takeseat.TakeSeatResponse;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;

import java.util.List;

/**
 * Created by sx on 05/12/17.
 * Copyright under comercial unit
 */
public interface KaiService {
    /**
     * Get Train List
     * Operation to get list of available train via API.
     * Get Train Routes
     * Operation to get list of train routes via API.
     * Get Train Schedule
     * Operation to get list of train schedule via API.
     * Get Train Seat Map
     * Operation to get list of train seat map via API.
     * Take Seat Train (only in KAI, other train include in booking feature)
     * Operation to take train seat.
     * Set Booking Train
     * Operation to set booking to selected train schedule via API.
     * Set Issued Train
     * Operation to set issued to booking train via API.
     * Get List Train Booking
     * Operation to get agent’s list of train booking via API.
     * Get Train Booking Detail
     * Operation to get information detail of agent’s train booking transaction via API.
     *
     * @return
     */
    BaseResponse<List<Train>> getTrainList();

    BaseResponse<List<Route>> getTrainRoutes(GetTrainRouteRequest getTrainRouteRequest);

    BaseResponse<GetTrainSchaduleResponse> getTrainSchedule(GetTrainSchaduleRequest getTrainSchaduleRequest);

    BaseResponse<GetTrainSeatMapResponse> getTrainSeatMap(GetTrainSeatMapRequest getTrainSeatMapRequest);

    BaseResponse<TakeSeatResponse> setSeatTrain(TakeSeatRequest takeSeatRequest);

    BaseResponse<SetBookingTrainResponse> setBookingTrain(SetBookingTrainRequest setBookingTrainRequest, PartnerCredential partnerCredential);

    BaseResponse<SetIssuedTrainResponse> setIssuedTrain(SetIssuedTrainRequest setIssuedTrainRequest, Transaksi transaksi);

    BaseResponse<BookingDetailResponse> getBookingDetauils(BookingDetailRequest bookingDetailRequest, Transaksi transaksi);


}
