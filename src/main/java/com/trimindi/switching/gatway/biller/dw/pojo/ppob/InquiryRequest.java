package com.trimindi.switching.gatway.biller.dw.pojo.ppob;

import com.fasterxml.jackson.annotation.JsonProperty;


public class InquiryRequest {

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("customerID")
    private String customerID;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("customerMSISDN")
    private String customerMSISDN;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCustomerMSISDN() {
        return customerMSISDN;
    }

    public void setCustomerMSISDN(String customerMSISDN) {
        this.customerMSISDN = customerMSISDN;
    }

    @Override
    public String toString() {
        return
                "InquiryRequest{" +
                        "productCode = '" + productCode + '\'' +
                        ",customerID = '" + customerID + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",customerMSISDN = '" + customerMSISDN + '\'' +
                        "}";
    }
}