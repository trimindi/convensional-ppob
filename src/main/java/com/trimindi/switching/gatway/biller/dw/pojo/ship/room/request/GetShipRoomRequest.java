package com.trimindi.switching.gatway.biller.dw.pojo.ship.room.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetShipRoomRequest {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("ticketBuyerName")
    private String ticketBuyerName;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("pax")
    private List<PaxItem> pax;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("ticketBuyerEmail")
    private String ticketBuyerEmail;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("ticketBuyerAddress")
    private String ticketBuyerAddress;

    @JsonProperty("ticketBuyerPhone")
    private String ticketBuyerPhone;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("family")
    private boolean family;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getTicketBuyerName() {
        return ticketBuyerName;
    }

    public void setTicketBuyerName(String ticketBuyerName) {
        this.ticketBuyerName = ticketBuyerName;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public List<PaxItem> getPax() {
        return pax;
    }

    public void setPax(List<PaxItem> pax) {
        this.pax = pax;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getTicketBuyerEmail() {
        return ticketBuyerEmail;
    }

    public void setTicketBuyerEmail(String ticketBuyerEmail) {
        this.ticketBuyerEmail = ticketBuyerEmail;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTicketBuyerAddress() {
        return ticketBuyerAddress;
    }

    public void setTicketBuyerAddress(String ticketBuyerAddress) {
        this.ticketBuyerAddress = ticketBuyerAddress;
    }

    public String getTicketBuyerPhone() {
        return ticketBuyerPhone;
    }

    public void setTicketBuyerPhone(String ticketBuyerPhone) {
        this.ticketBuyerPhone = ticketBuyerPhone;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public boolean isFamily() {
        return family;
    }

    public void setFamily(boolean family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return
                "GetShipRoomRequest{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",ticketBuyerName = '" + ticketBuyerName + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",pax = '" + pax + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",ticketBuyerEmail = '" + ticketBuyerEmail + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",ticketBuyerAddress = '" + ticketBuyerAddress + '\'' +
                        ",ticketBuyerPhone = '" + ticketBuyerPhone + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",family = '" + family + '\'' +
                        "}";
    }
}