package com.trimindi.switching.gatway.biller.dw.pojo.ship.room.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PaxItem {

    @JsonProperty("paxGender")
    private String paxGender;

    @JsonProperty("paxType")
    private String paxType;

    @JsonProperty("paxTotal")
    private int paxTotal;

    public String getPaxGender() {
        return paxGender;
    }

    public void setPaxGender(String paxGender) {
        this.paxGender = paxGender;
    }

    public String getPaxType() {
        return paxType;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    public int getPaxTotal() {
        return paxTotal;
    }

    public void setPaxTotal(int paxTotal) {
        this.paxTotal = paxTotal;
    }

    @Override
    public String toString() {
        return
                "PaxItem{" +
                        "paxGender = '" + paxGender + '\'' +
                        ",paxType = '" + paxType + '\'' +
                        ",paxTotal = '" + paxTotal + '\'' +
                        "}";
    }
}