package com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class HotelsItem {

    @JsonProperty("rooms")
    private List<RoomsItem> rooms;

    @JsonProperty("address")
    private String address;

    @JsonProperty("rating")
    private double rating;

    @JsonProperty("promoEndDate")
    private String promoEndDate;

    @JsonProperty("message")
    private String message;

    @JsonProperty("market")
    private String market;

    @JsonProperty("bookingDaysBefore")
    private int bookingDaysBefore;

    @JsonProperty("price")
    private double price;

    @JsonProperty("name")
    private String name;

    @JsonProperty("logo")
    private String logo;

    @JsonProperty("ID")
    private String iD;

    @JsonProperty("availabilityStatus")
    private boolean availabilityStatus;

    @JsonProperty("email")
    private String email;

    @JsonProperty("internalCode")
    private String internalCode;

    public List<RoomsItem> getRooms() {
        return rooms;
    }

    public HotelsItem setRooms(List<RoomsItem> rooms) {
        this.rooms = rooms;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public HotelsItem setAddress(String address) {
        this.address = address;
        return this;
    }

    public double getRating() {
        return rating;
    }

    public HotelsItem setRating(double rating) {
        this.rating = rating;
        return this;
    }

    public String getPromoEndDate() {
        return promoEndDate;
    }

    public HotelsItem setPromoEndDate(String promoEndDate) {
        this.promoEndDate = promoEndDate;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public HotelsItem setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getMarket() {
        return market;
    }

    public HotelsItem setMarket(String market) {
        this.market = market;
        return this;
    }

    public int getBookingDaysBefore() {
        return bookingDaysBefore;
    }

    public HotelsItem setBookingDaysBefore(int bookingDaysBefore) {
        this.bookingDaysBefore = bookingDaysBefore;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public HotelsItem setPrice(double price) {
        this.price = price;
        return this;
    }

    public String getName() {
        return name;
    }

    public HotelsItem setName(String name) {
        this.name = name;
        return this;
    }

    public String getLogo() {
        return logo;
    }

    public HotelsItem setLogo(String logo) {
        this.logo = logo;
        return this;
    }

    public String getiD() {
        return iD;
    }

    public HotelsItem setiD(String iD) {
        this.iD = iD;
        return this;
    }

    public boolean isAvailabilityStatus() {
        return availabilityStatus;
    }

    public HotelsItem setAvailabilityStatus(boolean availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public HotelsItem setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public HotelsItem setInternalCode(String internalCode) {
        this.internalCode = internalCode;
        return this;
    }

    @Override
    public String toString() {
        return "HotelsItem{" +
                "rooms=" + rooms +
                ", address='" + address + '\'' +
                ", rating=" + rating +
                ", promoEndDate='" + promoEndDate + '\'' +
                ", message='" + message + '\'' +
                ", market='" + market + '\'' +
                ", bookingDaysBefore=" + bookingDaysBefore +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", logo='" + logo + '\'' +
                ", iD='" + iD + '\'' +
                ", availabilityStatus=" + availabilityStatus +
                ", email='" + email + '\'' +
                ", internalCode='" + internalCode + '\'' +
                '}';
    }
}