package com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class FlightDetailItem {

    @JsonProperty("airlineCode")
    private String airlineCode;

    @JsonProperty("fdOrigin")
    private String fdOrigin;

    @JsonProperty("fdDestination")
    private String fdDestination;

    @JsonProperty("fdArrivalTime")
    private String fdArrivalTime;

    @JsonProperty("fdDepartTime")
    private String fdDepartTime;

    @JsonProperty("routeInfo")
    private String routeInfo;

    @JsonProperty("flightNumber")
    private String flightNumber;

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getFdOrigin() {
        return fdOrigin;
    }

    public void setFdOrigin(String fdOrigin) {
        this.fdOrigin = fdOrigin;
    }

    public String getFdDestination() {
        return fdDestination;
    }

    public void setFdDestination(String fdDestination) {
        this.fdDestination = fdDestination;
    }

    public String getFdArrivalTime() {
        return fdArrivalTime;
    }

    public void setFdArrivalTime(String fdArrivalTime) {
        this.fdArrivalTime = fdArrivalTime;
    }

    public String getFdDepartTime() {
        return fdDepartTime;
    }

    public void setFdDepartTime(String fdDepartTime) {
        this.fdDepartTime = fdDepartTime;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    public String toString() {
        return
                "FlightDetailItem{" +
                        "airlineCode = '" + airlineCode + '\'' +
                        ",fdOrigin = '" + fdOrigin + '\'' +
                        ",fdDestination = '" + fdDestination + '\'' +
                        ",fdArrivalTime = '" + fdArrivalTime + '\'' +
                        ",fdDepartTime = '" + fdDepartTime + '\'' +
                        ",routeInfo = '" + routeInfo + '\'' +
                        ",flightNumber = '" + flightNumber + '\'' +
                        "}";
    }
}