package com.trimindi.switching.gatway.biller.rajabiller.request;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 6/16/2017.
 */
@XmlRootElement
public class MethodCall {
    private Params params;

    private String methodName;

    public MethodCall(Params params, String methodName) {
        this.params = params;
        this.methodName = methodName;
    }

    public MethodCall() {
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public String toString() {
        return "ClassPojo [params = " + params + ", methodName = " + methodName + "]";
    }

    public static class Builder {
        private List<String> tempData;
        private String methodName;
        private Param[] listParam;

        public Builder() {
            tempData = new ArrayList<>();
        }

        public Builder setMethodName(String methodName) {
            this.methodName = methodName;
            return this;
        }

        public Builder addParam(String param) {
            this.tempData.add(param);
            return this;
        }

        public MethodCall build() {
            listParam = new Param[tempData.size()];
            for (int i = 0; i < tempData.size(); i++) {
                listParam[i] = new Param(new Value(tempData.get(i)));
            }
            Params p = new Params(listParam);
            return new MethodCall(p, methodName);
        }
    }
}
