package com.trimindi.switching.gatway.biller.dw.pojo.airline.seat;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GetAirlineSeatRequest {

    @JsonProperty("schReturn")
    private String schReturn;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("schDepart")
    private String schDepart;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("paxChild")
    private String paxChild;

    @JsonProperty("paxInfant")
    private String paxInfant;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("paxAdult")
    private String paxAdult;

    public String getSchReturn() {
        return schReturn;
    }

    public void setSchReturn(String schReturn) {
        this.schReturn = schReturn;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSchDepart() {
        return schDepart;
    }

    public void setSchDepart(String schDepart) {
        this.schDepart = schDepart;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(String paxChild) {
        this.paxChild = paxChild;
    }

    public String getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(String paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(String paxAdult) {
        this.paxAdult = paxAdult;
    }

    @Override
    public String toString() {
        return
                "GetAirlineSeatRequest{" +
                        "schReturn = '" + schReturn + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",schDepart = '" + schDepart + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        "}";
    }
}