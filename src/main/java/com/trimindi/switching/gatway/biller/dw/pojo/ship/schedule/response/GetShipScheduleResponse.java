package com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetShipScheduleResponse {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("destinationName")
    private String destinationName;

    @JsonProperty("schedules")
    private List<SchedulesItem> schedules;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("originName")
    private String originName;

    @JsonProperty("status")
    private String status;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public List<SchedulesItem> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<SchedulesItem> schedules) {
        this.schedules = schedules;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetShipScheduleResponse{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",destinationName = '" + destinationName + '\'' +
                        ",schedules = '" + schedules + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",originName = '" + originName + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}