package com.trimindi.switching.gatway.biller.dw.pojo.train.booking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PassengersItem {

    @JsonProperty("seat")
    private String seat;

    @JsonProperty("wagonCode")
    private String wagonCode;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("name")
    private String name;

    @JsonProperty("ID")
    private String iD;

    @JsonProperty("wagonNumber")
    private String wagonNumber;

    @JsonProperty("type")
    private String type;

    @JsonProperty("birthDate")
    private String birthDate;

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getWagonCode() {
        return wagonCode;
    }

    public void setWagonCode(String wagonCode) {
        this.wagonCode = wagonCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getWagonNumber() {
        return wagonNumber;
    }

    public void setWagonNumber(String wagonNumber) {
        this.wagonNumber = wagonNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return
                "PassengersItem{" +
                        "seat = '" + seat + '\'' +
                        ",wagonCode = '" + wagonCode + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",name = '" + name + '\'' +
                        ",iD = '" + iD + '\'' +
                        ",wagonNumber = '" + wagonNumber + '\'' +
                        ",type = '" + type + '\'' +
                        ",birthDate = '" + birthDate + '\'' +
                        "}";
    }
}