package com.trimindi.switching.gatway.biller.dw.pojo.ppob.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("period")
    private String period;

    @JsonProperty("additonalMessage")
    private Object additonalMessage;

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("billingReferenceID")
    private String billingReferenceID;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("customerID")
    private String customerID;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("customerName")
    private String customerName;

    @JsonProperty("status")
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Object getAdditonalMessage() {
        return additonalMessage;
    }

    public void setAdditonalMessage(Object additonalMessage) {
        this.additonalMessage = additonalMessage;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBillingReferenceID() {
        return billingReferenceID;
    }

    public void setBillingReferenceID(String billingReferenceID) {
        this.billingReferenceID = billingReferenceID;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "PaymentResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",period = '" + period + '\'' +
                        ",additonalMessage = '" + additonalMessage + '\'' +
                        ",productCode = '" + productCode + '\'' +
                        ",billingReferenceID = '" + billingReferenceID + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",customerID = '" + customerID + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",customerName = '" + customerName + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}