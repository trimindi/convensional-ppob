package com.trimindi.switching.gatway.biller.dw.pojo.hotel.country.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelCountryRequest {

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return
                "GetHotelCountryRequest{" +
                        "accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        "}";
    }
}