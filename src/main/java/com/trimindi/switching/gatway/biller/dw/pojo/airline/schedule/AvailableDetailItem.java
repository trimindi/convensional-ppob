package com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class AvailableDetailItem {

    @JsonProperty("availabityStatus")
    private String availabityStatus;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("classiId")
    private String classiId;

    @JsonProperty("cabin")
    private String cabin;

    @JsonProperty("flightClass")
    private String flightClass;
    @JsonProperty("airlineSegmentCode")
    private String airlineSegmentCode;

    public String getAirlineSegmentCode() {
        return airlineSegmentCode;
    }

    public AvailableDetailItem setAirlineSegmentCode(String airlineSegmentCode) {
        this.airlineSegmentCode = airlineSegmentCode;
        return this;
    }

    public String getClassiId() {
        return classiId;
    }

    public AvailableDetailItem setClassiId(String classiId) {
        this.classiId = classiId;
        return this;
    }

    public String getAvailabityStatus() {
        return availabityStatus;
    }

    public void setAvailabityStatus(String availabityStatus) {
        this.availabityStatus = availabityStatus;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public String getCabin() {
        return cabin;
    }

    public AvailableDetailItem setCabin(String cabin) {
        this.cabin = cabin;
        return this;
    }

    @Override
    public String toString() {
        return
                "AvailableDetailItem{" +
                        "availabityStatus = '" + availabityStatus + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",classiId = '" + classiId + '\'' +
                        ",cabin = '" + cabin + '\'' +
                        ",flightClass = '" + flightClass + '\'' +
                        "}";
    }
}