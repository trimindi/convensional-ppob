package com.trimindi.switching.gatway.biller.servindo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.models.*;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import com.trimindi.switching.gatway.utils.generator.ServindoRegIdGenerator;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Map;

/**
 * Created by PC on 21/07/2017.
 */
@Component
@Scope(value = "request")
public class ServindoService {
    public static final Logger logger = LoggerFactory.getLogger(ServindoService.class);
    private static HttpGet httpGet;
    private static URIBuilder builder;
    private final TransaksiService transaksiService;
    private final ProductItemService productItemService;
    private final ProductFeeService productFeeService;
    private final PartnerDepositService partnerDepositService;

    private final CloseableHttpClient client;

    private final ObjectMapper objectMapper;

    private final
    ServindoRegIdGenerator servindoRegIdGenerator;

    @Value("${servindo.ip}")
    private String host;
    @Value("${servindo.port}")
    private int port;
    @Value("${servindo.path}")
    private String path;
    @Value("${servindo.userid}")
    private String userid;
    @Value("${servindo.password}")
    private String password;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public ServindoService(TransaksiService transaksiService, ProductItemService productItemService, ProductFeeService productFeeService, PartnerDepositService partnerDepositService, CloseableHttpClient client, ObjectMapper objectMapper, ServindoRegIdGenerator servindoRegIdGenerator) {
        builder = new URIBuilder();
        this.transaksiService = transaksiService;
        this.productItemService = productItemService;
        this.productFeeService = productFeeService;
        this.partnerDepositService = partnerDepositService;
        this.client = client;
        this.objectMapper = objectMapper;
        this.servindoRegIdGenerator = servindoRegIdGenerator;
    }

    public void beliPulsa(@Suspended AsyncResponse toResponse, ProductItem product, Map<String, String> params, PartnerCredential partnerCredential) {
        final ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), product.getDENOM());
        Transaksi transaksi = null;

        String reg_id = servindoRegIdGenerator.nextTrace();
        try {
            transaksi = new Transaksi()
                    .setADMIN(0)
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setMSSIDN_NAME(params.get(Constanta.MSSIDN))
                    .setHOST_REF_NUMBER(reg_id)
                    .setMSSIDN(params.get(Constanta.MSSIDN))
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                    .setPRODUCT(product.getProduct_id())
                    .setTAGIHAN(product.getAMOUT() + productFee.getFEE_CA())
                    .setDENOM(product.getDENOM())
                    .setFEE_CA(productFee.getFEE_CA())
                    .setFEE_DEALER(0)
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setCHARGE(product.getAMOUT() + productFee.getFEE_CA())
                    .setDEBET(product.getAMOUT() + productFee.getFEE_CA())
                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setDENDA(0)
                    .setST(Status.PAYMENT_PROSESS);
            transaksiService.save(transaksi);
            if (partnerDepositService.bookingSaldo(transaksi)) {
                builder = new URIBuilder();
                builder.setScheme("http").setHost(host).setPort(port).setPath(path)
                        .setParameter("regid", reg_id)
                        .setParameter("userid", userid)
                        .setParameter("passwd", password)
                        .setParameter("msisd", params.get(Constanta.MSSIDN))
                        .setParameter("denom", product.getDENOM_BILLER());
                httpGet = new HttpGet(builder.build());
                CloseableHttpResponse response = client.execute(httpGet);
                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                ResponseServindo paketData = objectMapper.readValue(respone, ResponseServindo.class);
                BaseResponse<ResponseServindo> paymentResponse = new BaseResponse<>();
                switch (paketData.getStatus()) {
                    case 1:
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        if (!(Double.compare(Double.parseDouble(paketData.getHarga()), product.getAMOUT()) == 0)) {
                            if (Double.parseDouble(paketData.getHarga()) != 0) {
                                productItemService.updateHarga(transaksi.getDENOM(), Double.parseDouble(paketData.getHarga()));
                                logger.error("UPDATE NEW HARGA");
                            }
                        }
                        paymentResponse.setData(paketData);
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        paymentResponse.setMessage("Successful");
                        paymentResponse.setFee(0);
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    case 2:
                        transaksi.setST(Status.PAYMENT_PROSESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        toResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES.setSaldoTerpotong(transaksi.getCHARGE()).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setMssidn(params.get(Constanta.MSSIDN)).setNtrans(transaksi.getNTRANS())).build());
                        break;
                    default:
                        partnerDepositService.reverseSaldo(transaksi, respone);
                        toResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_FAILED.setMessage(paketData.getMessage()).setNtrans(transaksi.getNTRANS()).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(params.get(Constanta.MSSIDN))).build());
                        break;
                }
            } else {
                toResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS()).setMssidn(params.get(Constanta.MSSIDN)).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(params.get(Constanta.DENOM))).build());
            }
        } catch (Exception e) {
            slackSendMessage.sendMessage(e.getLocalizedMessage());
            partnerDepositService.reverseSaldo(transaksi, "");
            toResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(params.get(Constanta.DENOM))).build());
        }
    }

    public void beliData(AsyncResponse asyncResponse, ProductItem productItem, Map<String, String> params, PartnerCredential p) {
        final ProductFee productFee = productFeeService.findFeePartner(p.getPartner_id(), productItem.getDENOM());
        Transaksi transaksi = null;
        String reg_id = servindoRegIdGenerator.nextTrace();
        try {
            transaksi = new Transaksi()
                    .setPARTNERID(p.getPartner_id())
                    .setUSERID(p.getPartner_uid())
                    .setADMIN(0)
                    .setMSSIDN_NAME(params.get(Constanta.MSSIDN))
                    .setHOST_REF_NUMBER(reg_id)
                    .setMSSIDN(params.get(Constanta.MSSIDN))
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                    .setPRODUCT(productItem.getProduct_id())
                    .setTAGIHAN(productItem.getAMOUT() + productFee.getFEE_CA())
                    .setDENOM(productItem.getDENOM())
                    .setFEE_CA(productFee.getFEE_CA())
                    .setFEE_DEALER(0)
                    .setUSERID(p.getPartner_uid())
                    .setCHARGE(productItem.getAMOUT() + productFee.getFEE_CA())
                    .setDEBET(productItem.getAMOUT() + productFee.getFEE_CA())
                    .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                    .setPARTNERID(p.getPartner_id())
                    .setDENDA(0)
                    .setST(Status.PAYMENT_PROSESS);
            transaksiService.update(transaksi);
            if (partnerDepositService.bookingSaldo(transaksi)) {
                builder = new URIBuilder();
                builder.setScheme("http").setHost(host).setPort(port).setPath(path)
                        .setParameter("regid", reg_id)
                        .setParameter("userid", userid)
                        .setParameter("passwd", password)
                        .setParameter("msisd", params.get(Constanta.MSSIDN))
                        .setParameter("denom", productItem.getDENOM_BILLER());
                httpGet = new HttpGet(builder.build());
                CloseableHttpResponse response = client.execute(httpGet);
                String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
                ResponseServindo paketData = objectMapper.readValue(respone, ResponseServindo.class);
                paketData.setKodeProduk(productItem.getDENOM());
                PartnerDeposit partnerDeposit = partnerDepositService.findById(p.getPartner_id());
                BaseResponse<ResponseServindo> paymentResponse = new BaseResponse<>();
                switch (paketData.getStatus()) {
                    case 1:
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        if (!(Double.compare(Double.parseDouble(paketData.getHarga()), productItem.getAMOUT()) == 0)) {
                            logger.error("harga tidak sama");
                            if (Double.parseDouble(paketData.getHarga()) != 0) {
                                productItemService.updateHarga(transaksi.getDENOM(), Double.parseDouble(paketData.getHarga()));
                                logger.error("update harga");
                            }
                        }
                        paymentResponse.setData(paketData);
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        paymentResponse.setMessage("Successful");
                        asyncResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    case 2:
                        transaksi.setST(Status.PAYMENT_PROSESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(paketData.getTn())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_UNDER_PROSES.setSaldoTerpotong(transaksi.getCHARGE()).setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                        break;
                    default:
                        partnerDepositService.reverseSaldo(transaksi, respone);
                        asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_FAILED.setMessage(paketData.getMessage()).setMssidn(params.get(Constanta.MSSIDN)).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setNtrans(transaksi.getNTRANS()).setProduct(transaksi.getDENOM())).build());
                        break;
                }
            } else {
                asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS()).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(productItem.getDENOM())).build());
            }
        } catch (Exception e) {
            slackSendMessage.sendMessage(e.getLocalizedMessage());
            partnerDepositService.reverseSaldo(transaksi, "");
            asyncResponse.resume(Response.status(200).entity(ResponseCode.PAYMENT_FAILED.setNtrans(transaksi.getNTRANS()).setProduct(productItem.getDENOM())).build());
        }
    }
}
