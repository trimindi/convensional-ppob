package com.trimindi.switching.gatway.biller.dw.pojo.train.route;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetTrainRouteResponse {

    @JsonProperty("routes")
    private List<Route> routes;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("respTime")
    private String respTime;
    @JsonIgnore
    @JsonProperty("accessToken")
    private String accessToken;
    @JsonIgnore
    @JsonProperty("userID")
    private String userID;
    @JsonIgnore
    @JsonProperty("status")
    private String status;

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetTrainRouteResponse{" +
                        "routes = '" + routes + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}