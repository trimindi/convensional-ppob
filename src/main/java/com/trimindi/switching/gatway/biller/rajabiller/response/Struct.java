package com.trimindi.switching.gatway.biller.rajabiller.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class Struct {
    private Member[] member;

    public Struct() {
    }

    public Member[] getMember() {
        return member;
    }

    public void setMember(Member[] member) {
        this.member = member;
    }

    @Override
    public String toString() {
        return "ClassPojo [member = " + member + "]";
    }
}