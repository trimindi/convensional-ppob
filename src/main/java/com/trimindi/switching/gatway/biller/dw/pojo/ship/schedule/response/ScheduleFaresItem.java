package com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ScheduleFaresItem {

    @JsonProperty("infantFare")
    private int infantFare;

    @JsonProperty("adultFare")
    private int adultFare;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("childFare")
    private int childFare;

    @JsonProperty("class")
    private String jsonMemberClass;

    public int getInfantFare() {
        return infantFare;
    }

    public void setInfantFare(int infantFare) {
        this.infantFare = infantFare;
    }

    public int getAdultFare() {
        return adultFare;
    }

    public void setAdultFare(int adultFare) {
        this.adultFare = adultFare;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public int getChildFare() {
        return childFare;
    }

    public void setChildFare(int childFare) {
        this.childFare = childFare;
    }

    public String getJsonMemberClass() {
        return jsonMemberClass;
    }

    public void setJsonMemberClass(String jsonMemberClass) {
        this.jsonMemberClass = jsonMemberClass;
    }

    @Override
    public String toString() {
        return
                "ScheduleFaresItem{" +
                        "infantFare = '" + infantFare + '\'' +
                        ",adultFare = '" + adultFare + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",childFare = '" + childFare + '\'' +
                        ",class = '" + jsonMemberClass + '\'' +
                        "}";
    }
}