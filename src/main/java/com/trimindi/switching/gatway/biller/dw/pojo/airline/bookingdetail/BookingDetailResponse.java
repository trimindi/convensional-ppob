package com.trimindi.switching.gatway.biller.dw.pojo.airline.bookingdetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class BookingDetailResponse {

    @JsonProperty("flightDeparts")
    private List<FlightDepartsItem> flightDeparts;

    @JsonProperty("issuedDate")
    private String issuedDate;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("passengers")
    private List<PassengersItem> passengers;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("flightClass")
    private String flightClass;

    @JsonProperty("adminFee")
    private AdminFee adminFee;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("timeLimit")
    private String timeLimit;

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("flightReturns")
    private List<FlightDepartsItem> flightReturns;

    @JsonProperty("ticketStatus")
    private String ticketStatus;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("ticketDetail")
    private String ticketDetail;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("airline")
    private String airline;

    @JsonProperty("status")
    private String status;

    public List<FlightDepartsItem> getFlightDeparts() {
        return flightDeparts;
    }

    public void setFlightDeparts(List<FlightDepartsItem> flightDeparts) {
        this.flightDeparts = flightDeparts;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public List<PassengersItem> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<PassengersItem> passengers) {
        this.passengers = passengers;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public AdminFee getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(AdminFee adminFee) {
        this.adminFee = adminFee;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public List<FlightDepartsItem> getFlightReturns() {
        return flightReturns;
    }

    public void setFlightReturns(List<FlightDepartsItem> flightReturns) {
        this.flightReturns = flightReturns;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getTicketDetail() {
        return ticketDetail;
    }

    public void setTicketDetail(String ticketDetail) {
        this.ticketDetail = ticketDetail;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "BookingDetailResponse{" +
                        "flightDeparts = '" + flightDeparts + '\'' +
                        ",issuedDate = '" + issuedDate + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",passengers = '" + passengers + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",flightClass = '" + flightClass + '\'' +
                        ",adminFee = '" + adminFee + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",timeLimit = '" + timeLimit + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",flightReturns = '" + flightReturns + '\'' +
                        ",ticketStatus = '" + ticketStatus + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",ticketDetail = '" + ticketDetail + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",currency = '" + currency + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",airline = '" + airline + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}