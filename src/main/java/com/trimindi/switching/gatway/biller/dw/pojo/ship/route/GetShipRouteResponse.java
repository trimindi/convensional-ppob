package com.trimindi.switching.gatway.biller.dw.pojo.ship.route;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetShipRouteResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("destinations")
    private List<DestinationsItem> destinations;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("origins")
    private List<OriginsItem> origins;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("status")
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public List<DestinationsItem> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<DestinationsItem> destinations) {
        this.destinations = destinations;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public List<OriginsItem> getOrigins() {
        return origins;
    }

    public void setOrigins(List<OriginsItem> origins) {
        this.origins = origins;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetShipRouteResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",destinations = '" + destinations + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",origins = '" + origins + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}