package com.trimindi.switching.gatway.biller.dw.pojo.hotel.issued;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SetIssuedHotelResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("bookingStatus")
    private String bookingStatus;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("reservationNo")
    private String reservationNo;

    @JsonProperty("status")
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getReservationNo() {
        return reservationNo;
    }

    public void setReservationNo(String reservationNo) {
        this.reservationNo = reservationNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SetIssuedHotelResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",bookingStatus = '" + bookingStatus + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",reservationNo = '" + reservationNo + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}