package com.trimindi.switching.gatway.biller.dw.pojo.train.seat;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Detail {

    @JsonProperty("isAvailable")
    private boolean isAvailable;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("seatRow")
    private String seatRow;

    @JsonProperty("column")
    private int column;

    @JsonProperty("row")
    private int row;

    @JsonProperty("seatColumn")
    private String seatColumn;

    @JsonProperty("seatNumber")
    private String seatNumber;

    public boolean isIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getSeatRow() {
        return seatRow;
    }

    public void setSeatRow(String seatRow) {
        this.seatRow = seatRow;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getSeatColumn() {
        return seatColumn;
    }

    public void setSeatColumn(String seatColumn) {
        this.seatColumn = seatColumn;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public String toString() {
        return
                "Detail{" +
                        "isAvailable = '" + isAvailable + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",seatRow = '" + seatRow + '\'' +
                        ",column = '" + column + '\'' +
                        ",row = '" + row + '\'' +
                        ",seatColumn = '" + seatColumn + '\'' +
                        ",seatNumber = '" + seatNumber + '\'' +
                        "}";
    }
}