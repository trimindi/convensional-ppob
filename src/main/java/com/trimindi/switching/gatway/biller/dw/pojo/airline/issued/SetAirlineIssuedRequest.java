package com.trimindi.switching.gatway.biller.dw.pojo.airline.issued;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SetAirlineIssuedRequest {

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("airlineAccessCode")
    private String airlineAccessCode;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getAirlineAccessCode() {
        return airlineAccessCode;
    }

    public void setAirlineAccessCode(String airlineAccessCode) {
        this.airlineAccessCode = airlineAccessCode;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    @Override
    public String toString() {
        return
                "SetAirlineIssuedRequest{" +
                        "tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",airlineAccessCode = '" + airlineAccessCode + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        "}";
    }
}