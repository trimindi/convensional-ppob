package com.trimindi.switching.gatway.biller.dw.pojo.ppob.response;

/**
 * Created by sx on 22/01/18.
 * Copyright under comercial unit
 */
public class TvInquiryResponse {
    private String idpelanggan = "";
    private String namapelanggan = "";
    private String periode = "";
    private Integer tagihan = 0;
    private Integer admin = 0;
    private Integer totalTagihan = 0;
    private String keterangan = "";
    private Integer denda = 0;
    private Integer beban = 0;
    private Integer materai = 0;
    private Integer lainlain = 0;

    public TvInquiryResponse() {
    }

    public String getIdpelanggan() {
        return idpelanggan;
    }

    public TvInquiryResponse setIdpelanggan(String idpelanggan) {
        this.idpelanggan = idpelanggan;
        return this;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public TvInquiryResponse setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
        return this;
    }

    public String getPeriode() {
        return periode;
    }

    public TvInquiryResponse setPeriode(String periode) {
        this.periode = periode;
        return this;
    }

    public Integer getTagihan() {
        return tagihan;
    }

    public TvInquiryResponse setTagihan(Integer tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public Integer getAdmin() {
        return admin;
    }

    public TvInquiryResponse setAdmin(Integer admin) {
        this.admin = admin;
        return this;
    }

    public Integer getTotalTagihan() {
        return totalTagihan;
    }

    public TvInquiryResponse setTotalTagihan(Integer totalTagihan) {
        this.totalTagihan = totalTagihan;
        return this;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public TvInquiryResponse setKeterangan(String keterangan) {
        this.keterangan = keterangan;
        return this;
    }

    public Integer getDenda() {
        return denda;
    }

    public TvInquiryResponse setDenda(Integer denda) {
        this.denda = denda;
        return this;
    }

    public Integer getBeban() {
        return beban;
    }

    public TvInquiryResponse setBeban(Integer beban) {
        this.beban = beban;
        return this;
    }

    public Integer getMaterai() {
        return materai;
    }

    public TvInquiryResponse setMaterai(Integer materai) {
        this.materai = materai;
        return this;
    }

    public Integer getLainlain() {
        return lainlain;
    }

    public TvInquiryResponse setLainlain(Integer lainlain) {
        this.lainlain = lainlain;
        return this;
    }
}
