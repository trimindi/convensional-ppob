package com.trimindi.switching.gatway.biller.dw.pojo.airline.price;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetAirlinePriceRequest {

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("schReturns")
    private List<SchReturnsItem> schReturns;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("schDeparts")
    private List<SchDepartsItem> schDeparts;

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("paxChild")
    private String paxChild;

    @JsonProperty("paxInfant")
    private String paxInfant;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("promoCode")
    private String promoCode;

    @JsonProperty("paxAdult")
    private String paxAdult;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<SchReturnsItem> getSchReturns() {
        return schReturns;
    }

    public void setSchReturns(List<SchReturnsItem> schReturns) {
        this.schReturns = schReturns;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public List<SchDepartsItem> getSchDeparts() {
        return schDeparts;
    }

    public void setSchDeparts(List<SchDepartsItem> schDeparts) {
        this.schDeparts = schDeparts;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(String paxChild) {
        this.paxChild = paxChild;
    }

    public String getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(String paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(String paxAdult) {
        this.paxAdult = paxAdult;
    }

    @Override
    public String toString() {
        return
                "GetAirlinePriceRequest{" +
                        "origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",schReturns = '" + schReturns + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",schDeparts = '" + schDeparts + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",promoCode = '" + promoCode + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        "}";
    }
}