package com.trimindi.switching.gatway.biller.dw.pojo.hotel.issued;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SetIssuedHotelRequest {

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("reservationNo")
    private String reservationNo;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getReservationNo() {
        return reservationNo;
    }

    public void setReservationNo(String reservationNo) {
        this.reservationNo = reservationNo;
    }

    @Override
    public String toString() {
        return
                "SetIssuedHotelRequest{" +
                        "accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",reservationNo = '" + reservationNo + '\'' +
                        "}";
    }
}