package com.trimindi.switching.gatway.biller.dw.pojo.hotel.city.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelCityResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("cityFilter")
    private String cityFilter;

    @JsonProperty("cities")
    private List<CitiesItem> cities;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("countryID")
    private String countryID;

    @JsonProperty("status")
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getCityFilter() {
        return cityFilter;
    }

    public void setCityFilter(String cityFilter) {
        this.cityFilter = cityFilter;
    }

    public List<CitiesItem> getCities() {
        return cities;
    }

    public void setCities(List<CitiesItem> cities) {
        this.cities = cities;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetHotelCityResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",cityFilter = '" + cityFilter + '\'' +
                        ",cities = '" + cities + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",countryID = '" + countryID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}