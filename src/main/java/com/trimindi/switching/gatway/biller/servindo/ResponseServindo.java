package com.trimindi.switching.gatway.biller.servindo;

/**
 * Created by JacksonGenerator on 26/08/17.
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class ResponseServindo {
    @JsonProperty(value = "harga", access = JsonProperty.Access.WRITE_ONLY)
    private String harga;
    @JsonProperty(value = "tn", access = JsonProperty.Access.WRITE_ONLY)
    private String tn;
    @JsonProperty("sn")
    private String sn;
    @JsonProperty(value = "saldo", access = JsonProperty.Access.WRITE_ONLY)
    private String saldo;
    @JsonProperty("message")
    private String message;
    @JsonProperty(value = "kode_produk", access = JsonProperty.Access.WRITE_ONLY)
    private String kodeProduk;
    @JsonProperty(value = "reg_id", access = JsonProperty.Access.WRITE_ONLY)
    private String regId;
    @JsonProperty(value = "status", access = JsonProperty.Access.WRITE_ONLY)
    private Integer status;
    @JsonProperty(value = "timestamp", access = JsonProperty.Access.WRITE_ONLY)
    private Long timestamp;

    public ResponseServindo() {
    }
    public String getHarga() {
        return harga;
    }

    public ResponseServindo setHarga(String harga) {
        this.harga = harga;
        return this;
    }

    public String getTn() {
        return tn;
    }

    public ResponseServindo setTn(String tn) {
        this.tn = tn;
        return this;
    }

    public String getSn() {
        return sn;
    }

    public ResponseServindo setSn(String sn) {
        this.sn = sn;
        return this;
    }

    public String getSaldo() {
        return saldo;
    }

    public ResponseServindo setSaldo(String saldo) {
        this.saldo = saldo;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseServindo setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getKodeProduk() {
        return kodeProduk;
    }

    public ResponseServindo setKodeProduk(String kodeProduk) {
        this.kodeProduk = kodeProduk;
        return this;
    }

    public String getRegId() {
        return regId;
    }

    public ResponseServindo setRegId(String regId) {
        this.regId = regId;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public ResponseServindo setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public ResponseServindo setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}