package com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetAirlineBagageResponse {

    @JsonProperty("respMessage")
    @JsonIgnore
    private String respMessage;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("addOns")
    private List<BaggageAddOnsItem> addOns;

    @JsonProperty("accessToken")
    @JsonIgnore
    private String accessToken;

    @JsonProperty("userID")
    @JsonIgnore
    private String userID;

    @JsonProperty("status")
    @JsonIgnore
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public List<BaggageAddOnsItem> getAddOns() {
        return addOns;
    }

    public GetAirlineBagageResponse setAddOns(List<BaggageAddOnsItem> addOns) {
        this.addOns = addOns;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetAirlineBagageResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",baggageAddOns = '" + addOns + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}