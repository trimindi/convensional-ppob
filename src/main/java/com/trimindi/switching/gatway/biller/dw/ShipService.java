package com.trimindi.switching.gatway.biller.dw;

import com.trimindi.switching.gatway.biller.dw.pojo.ship.avability.request.GetShipAvabilityRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.request.SetShipBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.detail.request.GetShipBookingDetailRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.issued.request.SetShipIssuedRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.room.request.GetShipRoomRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.request.GetShipScheduleRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;

import javax.ws.rs.core.Response;

/**
 * Created by sx on 26/01/18.
 * Copyright under comercial unit
 */
public interface ShipService {

    Response GetShipRoutes();

    Response GetShipSchedule(GetShipScheduleRequest getShipScheduleRequest);

    Response GetShipAvailability(GetShipAvabilityRequest getShipAvabilityRequest);

    Response GetShipRoom(GetShipRoomRequest getShipRoomRequest);

    Response SetBookingShip(SetShipBookingRequest setShipBookingRequest, PartnerCredential partnerCredential);

    Response SetIssuedShip(SetShipIssuedRequest setShipIssuedRequest, Transaksi transaksi);

    Response GetListShipBooking();

    Response GetShipBookingDetail(GetShipBookingDetailRequest getShipBookingDetailRequest, Transaksi transaksi);
}
