package com.trimindi.switching.gatway.biller.dw.pojo.train.issue;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SetIssuedTrainResponse {

    @JsonProperty("trainID")
    private String trainID;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("departTime")
    private String departTime;

    @JsonProperty("origin")
    private String origin;

    @JsonIgnore
    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonIgnore
    @JsonProperty("accessToken")
    private String accessToken;
    @JsonIgnore
    @JsonProperty("userID")
    private String userID;

    @JsonProperty("status")
    private String status;

    public String getTrainID() {
        return trainID;
    }

    public void setTrainID(String trainID) {
        this.trainID = trainID;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SetIssuedTrainResponse{" +
                        "trainID = '" + trainID + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",departTime = '" + departTime + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}