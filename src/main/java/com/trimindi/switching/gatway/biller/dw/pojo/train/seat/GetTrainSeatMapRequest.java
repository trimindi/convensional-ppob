package com.trimindi.switching.gatway.biller.dw.pojo.train.seat;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GetTrainSeatMapRequest {

    @JsonProperty("trainID")
    private String trainID;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("trainNumber")
    private String trainNumber;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    public String getBookingDate() {
        return bookingDate;
    }

    public GetTrainSeatMapRequest setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
        return this;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public GetTrainSeatMapRequest setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
        return this;
    }

    public String getTrainID() {
        return trainID;
    }

    public void setTrainID(String trainID) {
        this.trainID = trainID;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return
                "GetTrainSeatMapRequest{" +
                        "trainID = '" + trainID + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",trainNumber = '" + trainNumber + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        "}";
    }
}