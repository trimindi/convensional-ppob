package com.trimindi.switching.gatway.biller.dw.pojo.train.takeseat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TakeSeatResponse {

    @JsonProperty("trainID")
    private String trainID;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("passengers")
    private List<PassengersItem> passengers;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("accessToken")
    @JsonIgnore
    private String accessToken;

    @JsonProperty("userID")
    @JsonIgnore
    private String userID;

    @JsonProperty("status")
    private String status;

    public String getTrainID() {
        return trainID;
    }

    public void setTrainID(String trainID) {
        this.trainID = trainID;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public List<PassengersItem> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<PassengersItem> passengers) {
        this.passengers = passengers;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "TakeSeatResponse{" +
                        "trainID = '" + trainID + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",passengers = '" + passengers + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}