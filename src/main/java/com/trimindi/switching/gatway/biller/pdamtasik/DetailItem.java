package com.trimindi.switching.gatway.biller.pdamtasik;

public class DetailItem {
    private String period;
    private int total;
    private String fine;
    private int billamount;
    private String usage;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public int getBillamount() {
        return billamount;
    }

    public void setBillamount(int billamount) {
        this.billamount = billamount;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return
                "DetailItem{" +
                        "period = '" + period + '\'' +
                        ",total = '" + total + '\'' +
                        ",fine = '" + fine + '\'' +
                        ",billamount = '" + billamount + '\'' +
                        ",usage = '" + usage + '\'' +
                        "}";
    }
}
