package com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GetShipScheduleRequest {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("departEndDate")
    private String departEndDate;

    @JsonProperty("departStartDate")
    private String departStartDate;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getDepartEndDate() {
        return departEndDate;
    }

    public void setDepartEndDate(String departEndDate) {
        this.departEndDate = departEndDate;
    }

    public String getDepartStartDate() {
        return departStartDate;
    }

    public void setDepartStartDate(String departStartDate) {
        this.departStartDate = departStartDate;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return
                "GetShipScheduleRequest{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",departEndDate = '" + departEndDate + '\'' +
                        ",departStartDate = '" + departStartDate + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        "}";
    }
}