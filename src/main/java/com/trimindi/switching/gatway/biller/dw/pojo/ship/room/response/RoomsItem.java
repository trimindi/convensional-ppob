package com.trimindi.switching.gatway.biller.dw.pojo.ship.room.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class RoomsItem {

    @JsonProperty("bed")
    private String bed;

    @JsonProperty("deck")
    private String deck;

    @JsonProperty("cabin")
    private String cabin;

    public String getBed() {
        return bed;
    }

    public void setBed(String bed) {
        this.bed = bed;
    }

    public String getDeck() {
        return deck;
    }

    public void setDeck(String deck) {
        this.deck = deck;
    }

    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    @Override
    public String toString() {
        return
                "RoomsItem{" +
                        "bed = '" + bed + '\'' +
                        ",deck = '" + deck + '\'' +
                        ",cabin = '" + cabin + '\'' +
                        "}";
    }
}