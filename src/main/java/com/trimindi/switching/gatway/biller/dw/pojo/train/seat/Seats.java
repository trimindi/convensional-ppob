package com.trimindi.switching.gatway.biller.dw.pojo.train.seat;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class Seats {

    @JsonProperty("totalRow")
    private double totalRow;

    @JsonProperty("wagonCode")
    private String wagonCode;

    @JsonProperty("totalColumn")
    private double totalColumn;

    @JsonProperty("detail")
    private List<Detail> detail;

    @JsonProperty("wagonNumber")
    private String wagonNumber;

    public double getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(double totalRow) {
        this.totalRow = totalRow;
    }

    public String getWagonCode() {
        return wagonCode;
    }

    public void setWagonCode(String wagonCode) {
        this.wagonCode = wagonCode;
    }

    public double getTotalColumn() {
        return totalColumn;
    }

    public void setTotalColumn(double totalColumn) {
        this.totalColumn = totalColumn;
    }

    public List<Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<Detail> detail) {
        this.detail = detail;
    }

    public String getWagonNumber() {
        return wagonNumber;
    }

    public void setWagonNumber(String wagonNumber) {
        this.wagonNumber = wagonNumber;
    }

    @Override
    public String toString() {
        return
                "Seats{" +
                        "totalRow = '" + totalRow + '\'' +
                        ",wagonCode = '" + wagonCode + '\'' +
                        ",totalColumn = '" + totalColumn + '\'' +
                        ",detail = '" + detail + '\'' +
                        ",wagonNumber = '" + wagonNumber + '\'' +
                        "}";
    }
}