package com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelSearchResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("hotels")
    private List<HotelsItem> hotels;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("status")
    private String status;

    public GetHotelSearchResponse() {
    }

    public String getRespMessage() {
        return respMessage;
    }

    public GetHotelSearchResponse setRespMessage(String respMessage) {
        this.respMessage = respMessage;
        return this;
    }

    public List<HotelsItem> getHotels() {
        return hotels;
    }

    public GetHotelSearchResponse setHotels(List<HotelsItem> hotels) {
        this.hotels = hotels;
        return this;
    }

    public String getRespTime() {
        return respTime;
    }

    public GetHotelSearchResponse setRespTime(String respTime) {
        this.respTime = respTime;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public GetHotelSearchResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public GetHotelSearchResponse setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public GetHotelSearchResponse setUserID(String userID) {
        this.userID = userID;
        return this;
    }

    @Override
    public String toString() {
        return
                "GetHotelSearchResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",hotels = '" + hotels + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}