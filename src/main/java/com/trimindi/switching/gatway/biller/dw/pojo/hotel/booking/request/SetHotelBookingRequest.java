package com.trimindi.switching.gatway.biller.dw.pojo.hotel.booking.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.request.RoomRequestItem;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SetHotelBookingRequest {

    @JsonProperty("guestLastName")
    @NotNull
    private String guestLastName;

    @JsonProperty("cityId")
    @NotNull
    private String cityId;

    @JsonProperty("hotelId")
    @NotNull
    private String hotelId;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("guestEmail")
    @NotNull
    private String guestEmail;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("checkInDate")
    @NotNull
    private String checkInDate;

    @JsonProperty("requestDescription")
    @NotNull
    private String requestDescription;

    @JsonProperty("paxPassport")
    @NotNull
    private String paxPassport;

    @JsonProperty("countryId")
    @NotNull
    private String countryId;

    @JsonProperty("roomId")
    @NotNull
    private String roomId;

    @JsonProperty("checkOutDate")
    @NotNull
    private String checkOutDate;

    @JsonProperty("roomRequest")
    private List<RoomRequestItem> roomRequest;

    @JsonProperty("guestFirstName")
    @NotNull
    private String guestFirstName;

    @JsonProperty("guestPhone")
    @NotNull
    private String guestPhone;

    @JsonProperty("guestTitle")
    @NotNull
    private String guestTitle;

    @JsonProperty("breakfast")
    @NotNull
    private String breakfast;

    @JsonProperty("internalCode")
    @NotNull
    private String internalCode;

    public String getGuestLastName() {
        return guestLastName;
    }

    public void setGuestLastName(String guestLastName) {
        this.guestLastName = guestLastName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public String getPaxPassport() {
        return paxPassport;
    }

    public void setPaxPassport(String paxPassport) {
        this.paxPassport = paxPassport;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public List<RoomRequestItem> getRoomRequest() {
        return roomRequest;
    }

    public void setRoomRequest(List<RoomRequestItem> roomRequest) {
        this.roomRequest = roomRequest;
    }

    public String getGuestFirstName() {
        return guestFirstName;
    }

    public void setGuestFirstName(String guestFirstName) {
        this.guestFirstName = guestFirstName;
    }

    public String getGuestPhone() {
        return guestPhone;
    }

    public void setGuestPhone(String guestPhone) {
        this.guestPhone = guestPhone;
    }

    public String getGuestTitle() {
        return guestTitle;
    }

    public void setGuestTitle(String guestTitle) {
        this.guestTitle = guestTitle;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    @Override
    public String toString() {
        return
                "SetHotelBookingRequest{" +
                        "guestLastName = '" + guestLastName + '\'' +
                        ",cityId = '" + cityId + '\'' +
                        ",hotelId = '" + hotelId + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",guestEmail = '" + guestEmail + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",checkInDate = '" + checkInDate + '\'' +
                        ",requestDescription = '" + requestDescription + '\'' +
                        ",paxPassport = '" + paxPassport + '\'' +
                        ",countryId = '" + countryId + '\'' +
                        ",roomId = '" + roomId + '\'' +
                        ",checkOutDate = '" + checkOutDate + '\'' +
                        ",roomRequest = '" + roomRequest + '\'' +
                        ",guestFirstName = '" + guestFirstName + '\'' +
                        ",guestPhone = '" + guestPhone + '\'' +
                        ",guestTitle = '" + guestTitle + '\'' +
                        ",breakfast = '" + breakfast + '\'' +
                        ",internalCode = '" + internalCode + '\'' +
                        "}";
    }
}