package com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class RoomsItem {
    /**
     * "ID":"12640",
     * "breakfast":"BABF",
     * "name":"Superior",
     * "isOnRequest":false,
     * "price":120000.00,
     * "bookingType":"Booking"
     */
    @JsonProperty("isOnRequest")
    private String isOnRequest;

    @JsonProperty("name")
    private String name;

    @JsonProperty("ID")
    private String ID;

    @JsonProperty("breakfast")
    private String breakfast;

    @JsonProperty("price")
    private Double price;
    @JsonProperty("bookingType")
    private String bookingType;

    public String getIsOnRequest() {
        return isOnRequest;
    }

    public RoomsItem setIsOnRequest(String isOnRequest) {
        this.isOnRequest = isOnRequest;
        return this;
    }

    public String getName() {
        return name;
    }

    public RoomsItem setName(String name) {
        this.name = name;
        return this;
    }

    public String getID() {
        return ID;
    }

    public RoomsItem setID(String ID) {
        this.ID = ID;
        return this;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public RoomsItem setBreakfast(String breakfast) {
        this.breakfast = breakfast;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public RoomsItem setPrice(Double price) {
        this.price = price;
        return this;
    }

    public String getBookingType() {
        return bookingType;
    }

    public RoomsItem setBookingType(String bookingType) {
        this.bookingType = bookingType;
        return this;
    }
}