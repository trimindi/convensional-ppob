package com.trimindi.switching.gatway.biller.dw;

import com.trimindi.switching.gatway.biller.dw.pojo.hotel.booking.request.SetHotelBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.request.GetHotelSearchRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.city.request.GetHotelCityRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.images.request.GetHotelImageRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.issued.SetIssuedHotelRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.request.GetHotelPriceRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.Transaksi;

import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by sx on 26/01/18.
 * Copyright under comercial unit
 */
public interface HotelService {
    /**
     * Get Hotel Country List
     * Operation to get list country to be used in hotel API request.
     * Get Hotel Passport List
     * Operation to get list passport to be used in airline API request.
     * Get Hotel City List
     * Operation to get list city to be used in airline API request.
     * Search Available Hotel
     * Operation to get list of available hotel via API.
     * Get Hotel Image List
     * Operation to get hotel image list.
     * Get Hotel Price Detail, Commission and Policy Info
     * Operation to get detail of price, commission and cancellation policy of hotel.
     * Set Hotel Booking
     * Operation to set booking to room selected. Get List Hotel Booking
     * Operation to get agent’s list of hotel booking via API.
     * Get Hotel Booking Detail
     * Operation to get information detail of agent’s hotel booking transaction via API.
     * Get Hotel Detail Information
     * Operation to get information detail of hotel via API.
     */
    Response GetHotelCountryList();

    Response GetHotelPassportList();

    Response GetHotelCityList(GetHotelCityRequest getHotelCityRequest);

    Response SearchAvailableHotel(GetHotelSearchRequest getHotelSearchRequest);

    Response GetHotelImageList(GetHotelImageRequest getHotelImageRequest);

    Response GetHotelPrice(GetHotelPriceRequest getHotelPriceRequest);

    Response SetHotelBooking(SetHotelBookingRequest setHotelBookingRequest, PartnerCredential partnerCredential);

    Response SetIssuedHotel(SetIssuedHotelRequest setIssuedHotelRequest, Transaksi transaksi) throws IOException;
    Response GetHotelBookingDetail(Transaksi transaksi);

}