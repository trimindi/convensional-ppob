package com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelSearchRequest {
    /**
     * {
     * "paxPassport":"MA05110065",
     * "countryId":"MA05110065",
     * "cityId":"MA07100118",
     * "checkInDate":"2016-02-14",
     * "checkOutDate":"2016-02-15",
     * "roomInfos":
     * [
     * {
     * "roomType": "Single",
     * "isRequestChildBed": "false",
     * "childNum":"0",
     * "childAges":[]
     * }
     * ]
     * }
     */
    @JsonProperty("roomRequest")
    private List<RoomRequestItem> roomRequest;


    @JsonProperty("checkOutDate")
    @NotNull
    private String checkOutDate;

    @JsonProperty("cityId")
    @NotNull
    private String cityId;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("checkInDate")
    @NotNull
    private String checkInDate;

    @JsonProperty("paxPassport")
    @NotNull
    private String paxPassport;

    @JsonProperty("countryId")
    @NotNull
    private String countryId;

    public List<RoomRequestItem> getRoomInfos() {
        return roomRequest;
    }

    public void setRoomInfos(List<RoomRequestItem> roomInfos) {
        this.roomRequest = roomInfos;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getPaxPassport() {
        return paxPassport;
    }

    public void setPaxPassport(String paxPassport) {
        this.paxPassport = paxPassport;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public List<RoomRequestItem> getRoomRequest() {
        return roomRequest;
    }

    public GetHotelSearchRequest setRoomRequest(List<RoomRequestItem> roomRequest) {
        this.roomRequest = roomRequest;
        return this;
    }

    @Override
    public String toString() {
        return "GetHotelSearchRequest{" +
                "roomRequest=" + roomRequest +
                ", checkOutDate='" + checkOutDate + '\'' +
                ", cityId='" + cityId + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", userID='" + userID + '\'' +
                ", checkInDate='" + checkInDate + '\'' +
                ", paxPassport='" + paxPassport + '\'' +
                ", countryId='" + countryId + '\'' +
                '}';
    }
}