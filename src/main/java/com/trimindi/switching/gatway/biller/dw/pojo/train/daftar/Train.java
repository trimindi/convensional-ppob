package com.trimindi.switching.gatway.biller.dw.pojo.train.daftar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Train {

    @JsonProperty("name")
    private String name;

    @JsonProperty("ID")
    private String iD;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    @Override
    public String toString() {
        return
                "Train{" +
                        "name = '" + name + '\'' +
                        ",iD = '" + iD + '\'' +
                        "}";
    }
}