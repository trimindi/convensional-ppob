package com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SegmentItem {

    @JsonProperty("flightDetail")
    private List<FlightDetailItem> flightDetail;

    @JsonProperty("availableDetail")
    private List<AvailableDetailItem> availableDetail;

    @JsonProperty("garudaNumber")
    private Object garudaNumber;

    public List<FlightDetailItem> getFlightDetail() {
        return flightDetail;
    }

    public void setFlightDetail(List<FlightDetailItem> flightDetail) {
        this.flightDetail = flightDetail;
    }

    public List<AvailableDetailItem> getAvailableDetail() {
        return availableDetail;
    }

    public void setAvailableDetail(List<AvailableDetailItem> availableDetail) {
        this.availableDetail = availableDetail;
    }

    public Object getGarudaNumber() {
        return garudaNumber;
    }

    public void setGarudaNumber(Object garudaNumber) {
        this.garudaNumber = garudaNumber;
    }

    @Override
    public String toString() {
        return
                "SegmentItem{" +
                        "flightDetail = '" + flightDetail + '\'' +
                        ",availableDetail = '" + availableDetail + '\'' +
                        ",garudaNumber = '" + garudaNumber + '\'' +
                        "}";
    }
}