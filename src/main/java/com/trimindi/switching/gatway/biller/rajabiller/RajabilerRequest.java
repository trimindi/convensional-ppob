package com.trimindi.switching.gatway.biller.rajabiller;

import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.rajabiller.response.MethodResponse;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.bpjs.InquiryBpjs;
import com.trimindi.switching.gatway.response.pasca.InquiryPhonePasca;
import com.trimindi.switching.gatway.response.pdam.Inquiry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * Created by PC on 6/19/2017.
 */
@Component
@Scope(value = "prototype")
public class RajabilerRequest {
    public static final Logger logger = LoggerFactory.getLogger(RajabilerRequest.class);
    private static JAXBContext jc;
    private static Unmarshaller unmarshaller;
    @Value("${rajabiler.uid}")
    private String UID;
    @Value("${rajabiler.pin}")
    private String PIN;
    @Autowired
    private SlackSendMessage slackSendMessage;

    public String inquiryPDAM(String kodeproduk, String idpel1, String idpel2, String idpel3, String ntrans) {
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.inq</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        return String.format(raw, kodeproduk, idpel1, idpel2, idpel3, UID, PIN, ntrans);
    }

    public String paymentPDAM(Transaksi transaksi) {
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.paydetail</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        StringReader stringReader = new StringReader(transaksi.getINQUIRY());
        try {
            MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
            Inquiry inquiry = new Inquiry(methodResponse);
            return String.format(raw, inquiry.getKODEPRODUK(), transaksi.getMSSIDN(), transaksi.getMSSIDN(), transaksi.getMSSIDN(), String.valueOf((int) inquiry.getTAGIHAN()), UID, PIN, inquiry.getREFF(), inquiry.getREF2(), inquiry.getPERIODE());
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        return "";
    }

    public String buyPulsa(String kodeproduk, String nohp, String ntrans) {
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.pulsa</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        return String.format(raw, kodeproduk, nohp, UID, PIN, ntrans);
    }

    public String inquiryTelkom(String kodeproduk, String area, String idpel, String ntrans, boolean speedy) {
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.inq</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        if (speedy) {
            return String.format(raw, kodeproduk, idpel, "", "", UID, PIN, ntrans);
        } else {
            return String.format(raw, kodeproduk, area, idpel, "", UID, PIN, ntrans);
        }
    }

    public String paymentTelokm(Transaksi transaksi) {
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            logger.error("JaxB Exception {}", e.getLocalizedMessage());
        }
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.paydetail</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        StringReader stringReader = new StringReader(transaksi.getINQUIRY());
        try {
            MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
            Inquiry inquiry = new Inquiry(methodResponse);
            if (transaksi.getDENOM().equalsIgnoreCase("SPEEDY")) {
                return String.format(raw, inquiry.getKODEPRODUK(), inquiry.getIDPELANGGAN1(), inquiry.getIDPELANGGAN(), inquiry.getIDPELANGGAN3(), String.valueOf((int) inquiry.getTAGIHAN()), UID, PIN, transaksi.getNTRANS(), inquiry.getREF2(), inquiry.getPERIODE());
            } else {
                return String.format(raw, inquiry.getKODEPRODUK(), inquiry.getIDPELANGGAN1(), inquiry.getIDPELANGGAN(), inquiry.getIDPELANGGAN3(), String.valueOf((int) inquiry.getTAGIHAN()), UID, PIN, transaksi.getNTRANS(), inquiry.getREF2(), inquiry.getPERIODE());
            }
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        return "";
    }

    public String inquiryPulsaPasca(String denom_biller, String idpel1, String idpel2, String idpel3, String ntrans) {
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.inq</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        return String.format(raw, denom_biller, idpel1, idpel2, idpel3, UID, PIN, ntrans);
    }

    public String paymentPulsaPasca(Transaksi transaksi) {
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.pay</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        StringReader stringReader = new StringReader(transaksi.getINQUIRY());
        try {
            MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
            InquiryPhonePasca inquiry = new InquiryPhonePasca(methodResponse);
            return String.format(raw, inquiry.getKodeProduct(), transaksi.getMSSIDN(), transaksi.getMSSIDN(), transaksi.getMSSIDN(), String.valueOf((int) inquiry.getTagihan()), UID, PIN, inquiry.getRef1(), inquiry.getRef2(), inquiry.getRef3());
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        return "";
    }

    public String inquiryBpjs(String denom_biller, String idpel, String ntrans) {
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.bpjsinq</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        return String.format(raw, denom_biller, idpel, 1, UID, PIN, ntrans);
    }

    public String paymentBpjs(Transaksi transaksi, Request req) {
        try {
            jc = JAXBContext.newInstance(MethodResponse.class);
            unmarshaller = jc.createUnmarshaller();
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        String raw = "<?xml version=\"1.0\"?><methodCall><methodName>rajabiller.bpjspay</methodName><params><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param><param><value><string>%s</string></value></param></params></methodCall>";
        StringReader stringReader = new StringReader(transaksi.getINQUIRY());
        try {
            MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
            InquiryBpjs inquiry = new InquiryBpjs(methodResponse);
            return String.format(raw, inquiry.getKodeProduct(), transaksi.getMSSIDN(), inquiry.getPeriode(), req.NOHP, String.valueOf((int) inquiry.getTagihan()), UID, PIN, inquiry.getRef1(), inquiry.getRef2());
        } catch (JAXBException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
        }
        return "";
    }
}
