package com.trimindi.switching.gatway.biller.dw.pojo.ppob.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InquiryResponse {

    @JsonProperty("penalty")
    private int penalty;

    @JsonProperty("sellPrice")
    private int sellPrice;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("billing")
    private int billing;

    @JsonProperty("policeNumber")
    private Object policeNumber;

    @JsonProperty("tenor")
    private Object tenor;

    @JsonProperty("BPJSDetail")
    private Object bPJSDetail;

    @JsonProperty("maxPayment")
    private int maxPayment;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("payment")
    private int payment;

    @JsonProperty("profit")
    private int profit;

    @JsonProperty("usageUnit")
    private Object usageUnit;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("period")
    private String period;

    @JsonProperty("billingReferenceID")
    private String billingReferenceID;

    @JsonProperty("lastPaidDueDate")
    private Object lastPaidDueDate;

    @JsonProperty("additionalMessage")
    private Object additionalMessage;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("customerName")
    private String customerName;

    @JsonProperty("adminBank")
    private int adminBank;

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("productGroup")
    private String productGroup;

    @JsonProperty("customerID")
    private String customerID;

    @JsonProperty("minPayment")
    private int minPayment;

    @JsonProperty("lastPaidPeriod")
    private Object lastPaidPeriod;

    @JsonProperty("customerMSISDN")
    private String customerMSISDN;

    @JsonProperty("status")
    private String status;

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public int getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(int sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getBilling() {
        return billing;
    }

    public void setBilling(int billing) {
        this.billing = billing;
    }

    public Object getPoliceNumber() {
        return policeNumber;
    }

    public void setPoliceNumber(Object policeNumber) {
        this.policeNumber = policeNumber;
    }

    public Object getTenor() {
        return tenor;
    }

    public void setTenor(Object tenor) {
        this.tenor = tenor;
    }

    public Object getBPJSDetail() {
        return bPJSDetail;
    }

    public void setBPJSDetail(Object bPJSDetail) {
        this.bPJSDetail = bPJSDetail;
    }

    public int getMaxPayment() {
        return maxPayment;
    }

    public void setMaxPayment(int maxPayment) {
        this.maxPayment = maxPayment;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    public Object getUsageUnit() {
        return usageUnit;
    }

    public void setUsageUnit(Object usageUnit) {
        this.usageUnit = usageUnit;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getBillingReferenceID() {
        return billingReferenceID;
    }

    public void setBillingReferenceID(String billingReferenceID) {
        this.billingReferenceID = billingReferenceID;
    }

    public Object getLastPaidDueDate() {
        return lastPaidDueDate;
    }

    public void setLastPaidDueDate(Object lastPaidDueDate) {
        this.lastPaidDueDate = lastPaidDueDate;
    }

    public Object getAdditionalMessage() {
        return additionalMessage;
    }

    public void setAdditionalMessage(Object additionalMessage) {
        this.additionalMessage = additionalMessage;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getAdminBank() {
        return adminBank;
    }

    public void setAdminBank(int adminBank) {
        this.adminBank = adminBank;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public int getMinPayment() {
        return minPayment;
    }

    public void setMinPayment(int minPayment) {
        this.minPayment = minPayment;
    }

    public Object getLastPaidPeriod() {
        return lastPaidPeriod;
    }

    public void setLastPaidPeriod(Object lastPaidPeriod) {
        this.lastPaidPeriod = lastPaidPeriod;
    }

    public String getCustomerMSISDN() {
        return customerMSISDN;
    }

    public void setCustomerMSISDN(String customerMSISDN) {
        this.customerMSISDN = customerMSISDN;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "InquiryResponse{" +
                        "penalty = '" + penalty + '\'' +
                        ",sellPrice = '" + sellPrice + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",billing = '" + billing + '\'' +
                        ",policeNumber = '" + policeNumber + '\'' +
                        ",tenor = '" + tenor + '\'' +
                        ",bPJSDetail = '" + bPJSDetail + '\'' +
                        ",maxPayment = '" + maxPayment + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",payment = '" + payment + '\'' +
                        ",profit = '" + profit + '\'' +
                        ",usageUnit = '" + usageUnit + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",period = '" + period + '\'' +
                        ",billingReferenceID = '" + billingReferenceID + '\'' +
                        ",lastPaidDueDate = '" + lastPaidDueDate + '\'' +
                        ",additionalMessage = '" + additionalMessage + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",customerName = '" + customerName + '\'' +
                        ",adminBank = '" + adminBank + '\'' +
                        ",productCode = '" + productCode + '\'' +
                        ",productGroup = '" + productGroup + '\'' +
                        ",customerID = '" + customerID + '\'' +
                        ",minPayment = '" + minPayment + '\'' +
                        ",lastPaidPeriod = '" + lastPaidPeriod + '\'' +
                        ",customerMSISDN = '" + customerMSISDN + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}