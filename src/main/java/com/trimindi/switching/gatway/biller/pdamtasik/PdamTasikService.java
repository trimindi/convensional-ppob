package com.trimindi.switching.gatway.biller.pdamtasik;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.biller.servindo.ServindoService;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.pdam.base.InquiryPdam;
import com.trimindi.switching.gatway.response.pdam.base.PaymentPdam;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by sx on 29/09/17.
 * Copyright under comercial unit
 */
@Service
public class PdamTasikService {
    public static final Logger logger = LoggerFactory.getLogger(ServindoService.class);
    private static HttpGet httpGet;
    private static URIBuilder builder;
    private final TransaksiService transaksiService;
    private final ProductItemService productItemService;
    private final ProductFeeService productFeeService;
    private final PartnerDepositService partnerDepositService;
    private final CloseableHttpClient client;
    private final ObjectMapper objectMapper;


    @Value("${pdam.tasik.ip}")
    private String host;
    @Value("${pdam.tasik.port}")
    private int port;
    @Value("${pdam.tasik.pathInquiry}")
    private String pathInquiry;
    @Value("${pdam.tasik.pathPayment}")
    private String pathPayment;
    @Value("${pdam.tasik.pathReverse}")
    private String pathReverse;
    @Value("${pdam.tasik.bankcode}")
    private String bankCode;
    private Transaksi transaksi;

    @Autowired
    public PdamTasikService(TransaksiService transaksiService, ProductItemService productItemService, ProductFeeService productFeeService, PartnerDepositService partnerDepositService, CloseableHttpClient client, ObjectMapper objectMapper) {
        builder = new URIBuilder();
        this.transaksiService = transaksiService;
        this.productItemService = productItemService;
        this.productFeeService = productFeeService;
        this.partnerDepositService = partnerDepositService;
        this.client = client;
        this.objectMapper = objectMapper;
    }

    public Response inquiry(ProductItem product, Map<String, String> params, PartnerCredential partnerCredential) throws IOException, URISyntaxException {
        try {
            builder = new URIBuilder();
            builder.setScheme("http").setHost(host).setPort(port).setPath(pathInquiry)
                    .setParameter("jenisbank", bankCode)
                    .setParameter("productcode", product.getDENOM_BILLER())
                    .setParameter("id", params.get(Constanta.MSSIDN));
            httpGet = new HttpGet(builder.build());
            CloseableHttpResponse response = client.execute(httpGet);
            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            logger.info(respone);
            PdamTasikInquiryResponse inquiryResponse = objectMapper.readValue(respone, PdamTasikInquiryResponse.class);
            switch (inquiryResponse.getResultcode()) {
                case 0:
                    ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), product.getDENOM());
                    transaksi = new Transaksi()
                            .setADMIN(product.getADMIN())
                            .setMSSIDN_NAME(inquiryResponse.getBillname())
                            .setMSSIDN(inquiryResponse.getBillnumber())
                            .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                            .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                            .setPRODUCT(product.getProduct_id())
                            .setTAGIHAN(inquiryResponse.getTotalamount())
                            .setDENOM(product.getDENOM())
                            .setFEE_CA((productFee.getFEE_CA() * inquiryResponse.getBillqty()))
                            .setFEE_DEALER(((product.getFEE_BILLER() - productFee.getFEE_CA()) * inquiryResponse.getBillqty()))
                            .setUSERID(partnerCredential.getPartner_uid())
                            .setINQUIRY(respone)
                            .setCHARGE((inquiryResponse.getTotalamount() + (product.getADMIN() * inquiryResponse.getBillqty()) - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * inquiryResponse.getBillqty())))
                            .setDEBET((inquiryResponse.getTotalamount() + (product.getADMIN() * inquiryResponse.getBillqty()) - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * inquiryResponse.getBillqty())))
                            .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                            .setPARTNERID(partnerCredential.getPartner_id())
                            .setDENDA(0)
                            .setST(Status.INQUIRY);
                    transaksiService.save(transaksi);
                    InquiryPdam inquiry = new InquiryPdam()
                            .setTagihan(inquiryResponse.getTotalamount())
                            .setAdmin((int) product.getADMIN())
                            .setPeriode(inquiryResponse.getDetail().stream().map(DetailItem::getPeriod).collect(Collectors.joining(",")))
                            .setType(inquiryResponse.getType())
                            .setIdpelanggan(inquiryResponse.getBillnumber())
                            .setAlamat(inquiryResponse.getAddress())
                            .setJumlahBill(inquiryResponse.getBillqty())
                            .setRincians(inquiryResponse.getDetail())
                            .setNamapelanggan(inquiryResponse.getBillname())
                            .setDenda(findDenda(inquiryResponse.getDetail()))
                            .setStandAwal(findStanAwal(inquiryResponse.getDetail()))
                            .setStandAkir(findStanAkir(inquiryResponse.getDetail()))
                            .setTotalTagihan((int) (transaksi.getTAGIHAN() + transaksi.getADMIN()));
                    BaseResponse<InquiryPdam> res = new BaseResponse<>();
                    res.setNtrans(transaksi.getNTRANS());
                    res.setData(inquiry);
                    res.setFee(transaksi.getFEE_DEALER()).setNtrans(transaksi.getNTRANS());
                    res.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                    res.setSaldoTerpotong(transaksi.getCHARGE());
                    res.setProduct(transaksi.getDENOM());
                    res.setMessage("Successful");
                    return Response.status(200).entity(res).build();
                case 7110:
                    return Response.status(200).entity(new ResponseCode("0005", "hanya bisa dilunasi di loket pdam , tagihan > 3")
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
                case 7111:
                    return Response.status(200).entity(new ResponseCode("0005", "Lakukan cek terlebih dahulu")
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
                case 7030:
                    return Response.status(200).entity(new ResponseCode("0005", "Tidak ada Tagihan")
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
                case 7020:
                    return Response.status(200).entity(new ResponseCode("0005", "Invalid account / Account not Found")
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
                case 7000:
                    return Response.status(200).entity(new ResponseCode("0005", "System maintenance")
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
                case 7004:
                    return Response.status(200).entity(ResponseCode.SERVER_TIMEOUT
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
                case 767:
                    return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
                default:
                    return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE
                            .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                            .build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE
                .setProduct(params.get(Constanta.DENOM)).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                .build();
    }


    public Response payment(Transaksi transaksi, PartnerCredential partnerCredential) throws URISyntaxException, IOException {
        ProductItem productItem = productItemService.findById(transaksi.getDENOM());
        builder = new URIBuilder();
        builder.setScheme("http").setHost(host).setPort(port).setPath(pathPayment)
                .setParameter("jenisbank", bankCode)
                .setParameter("productcode", productItem.getDENOM_BILLER())
                .setParameter("id", transaksi.getMSSIDN());
        httpGet = new HttpGet(builder.build());
        CloseableHttpResponse response = client.execute(httpGet);
        String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
        PdamTasikPaymentResponse paymentResponse = objectMapper.readValue(respone, PdamTasikPaymentResponse.class);
        switch (paymentResponse.getResultcode()) {
            case 0:
                transaksi.setST(Status.PAYMENT_SUCCESS)
                        .setPAYMENT(respone)
                        .setBILL_REF_NUMBER(paymentResponse.getReff())
                        .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                transaksiService.update(transaksi);
                PaymentPdam payment = new PaymentPdam()
                        .setTagihan(paymentResponse.getTotalamount())
                        .setAdmin((int) transaksi.getADMIN())
                        .setNamapelanggan(transaksi.getMSSIDN_NAME())
                        .setIdpelanggan(transaksi.getMSSIDN())
                        .setPeriode(paymentResponse.getDetail().stream().map(DetailItem::getPeriod).collect(Collectors.joining(",")))
                        .setReff(paymentResponse.getReff())
                        .setRincians(paymentResponse.getDetail())
                        .setReff(paymentResponse.getReff())
                        .setJumlahBill(paymentResponse.getBillqty())
                        .setType(paymentResponse.getType())
                        .setIdpelanggan(transaksi.getMSSIDN())
                        .setDenda((int) transaksi.getDENDA())
                        .setStandAwal(findStanAwal(paymentResponse.getDetail()))
                        .setStandAkir(findStanAkir(paymentResponse.getDetail()))
                        .setTotalTagihan((paymentResponse.getTotalamount() + (int) transaksi.getADMIN()));
                BaseResponse<PaymentPdam> res = new BaseResponse<>();
                res.setData(payment);
                res.setFee(transaksi.getFEE_DEALER());
                res.setNtrans(transaksi.getNTRANS());
                res.setSaldoTerpotong(transaksi.getCHARGE());
                res.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                res.setProduct(transaksi.getDENOM());
                res.setMessage("Successful");
                return Response.status(200).entity(res).build();
            case 7110:
                partnerDepositService.reverseSaldo(transaksi, respone);
                return Response.status(200).entity(new ResponseCode("0005", "hanya bisa dilunasi di loket pdam , tagihan > 3")
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            case 7111:
                partnerDepositService.reverseSaldo(transaksi, respone);
                doReverse(transaksi, productItem);
                return Response.status(200).entity(new ResponseCode("0005", "Lakukan cek terlebih dahulu")
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            case 7030:
                partnerDepositService.reverseSaldo(transaksi, respone);
                return Response.status(200).entity(new ResponseCode("0005", "Tidak ada Tagihan")
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            case 7020:
                partnerDepositService.reverseSaldo(transaksi, respone);
                return Response.status(200).entity(new ResponseCode("0005", "Invalid account / Account not Found")
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            case 7000:
                partnerDepositService.reverseSaldo(transaksi, respone);
                return Response.status(200).entity(new ResponseCode("0005", "System maintenance")
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            case 7004:
                partnerDepositService.reverseSaldo(transaksi, respone);
                doReverse(transaksi, productItem);
                return Response.status(200).entity(ResponseCode.SERVER_TIMEOUT
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            case 7066:
                partnerDepositService.reverseSaldo(transaksi, respone);
                doReverse(transaksi, productItem);
                return Response.status(200).entity(ResponseCode.SERVER_TIMEOUT
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            case 767:
                partnerDepositService.reverseSaldo(transaksi, respone);
                doReverse(transaksi, productItem);
                return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
            default:
                partnerDepositService.reverseSaldo(transaksi, respone);
                doReverse(transaksi, productItem);
                return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE
                        .setProduct(transaksi.getDENOM()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()))
                        .build();
        }
    }

    private Integer findDenda(List<DetailItem> detail) {
        Integer denda = 0;
        for (DetailItem aDetail : detail) {
            denda += (Objects.equals(aDetail.getFine(), "null")) ? 0 : Integer.parseInt(aDetail.getFine());
        }
        return denda;
    }

    private Integer findStanAkir(List<DetailItem> detail) {
        return Integer.valueOf(detail.get(detail.size() - 1).getUsage().split("-")[1]);
    }

    private Integer findStanAwal(List<DetailItem> detail) {
        return Integer.valueOf(detail.stream()
                .findFirst().get().getUsage()
                .split("-")[0]);
    }

    private void doReverse(Transaksi transaksi, ProductItem productItem) throws URISyntaxException, IOException {
        PdamTasikInquiryResponse res = objectMapper.readValue(transaksi.getINQUIRY(), PdamTasikInquiryResponse.class);
        builder = new URIBuilder();
        builder.setScheme("http").setHost(host).setPort(port).setPath(pathReverse)
                .setParameter("jenisbank", bankCode)
                .setParameter("jumlah", String.valueOf(res.getBillqty()))
                .setParameter("productcode", productItem.getDENOM_BILLER());

        final int[] i = {1};
        res.getDetail()
                .forEach(e -> {
                    builder.addParameter("id" + i[0], transaksi.getMSSIDN());
                    builder.setParameter("period" + i[0], e.getPeriod());
                    i[0]++;
                });
        httpGet = new HttpGet(builder.build());
        CloseableHttpResponse response = client.execute(httpGet);
    }
}
