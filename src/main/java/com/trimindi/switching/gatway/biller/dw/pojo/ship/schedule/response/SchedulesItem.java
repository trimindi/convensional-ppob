package com.trimindi.switching.gatway.biller.dw.pojo.ship.schedule.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SchedulesItem {

    @JsonProperty("scheduleFares")
    private List<ScheduleFaresItem> scheduleFares;

    @JsonProperty("destCall")
    private int destCall;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("arrivalDateTime")
    private String arrivalDateTime;

    @JsonProperty("shipName")
    private String shipName;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("routeInfo")
    private String routeInfo;

    @JsonProperty("departDateTime")
    private String departDateTime;

    public List<ScheduleFaresItem> getScheduleFares() {
        return scheduleFares;
    }

    public void setScheduleFares(List<ScheduleFaresItem> scheduleFares) {
        this.scheduleFares = scheduleFares;
    }

    public int getDestCall() {
        return destCall;
    }

    public void setDestCall(int destCall) {
        this.destCall = destCall;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getDepartDateTime() {
        return departDateTime;
    }

    public void setDepartDateTime(String departDateTime) {
        this.departDateTime = departDateTime;
    }

    @Override
    public String toString() {
        return
                "SchedulesItem{" +
                        "scheduleFares = '" + scheduleFares + '\'' +
                        ",destCall = '" + destCall + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",arrivalDateTime = '" + arrivalDateTime + '\'' +
                        ",shipName = '" + shipName + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",routeInfo = '" + routeInfo + '\'' +
                        ",departDateTime = '" + departDateTime + '\'' +
                        "}";
    }
}