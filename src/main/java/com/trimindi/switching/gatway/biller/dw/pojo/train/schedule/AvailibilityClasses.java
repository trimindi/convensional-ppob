package com.trimindi.switching.gatway.biller.dw.pojo.train.schedule;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class AvailibilityClasses {

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("price")
    private double price;

    @JsonProperty("availabilityStatus")
    private int availabilityStatus;

    @JsonProperty("availabilityClass")
    private String availabilityClass;

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(int availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    public String getAvailabilityClass() {
        return availabilityClass;
    }

    public void setAvailabilityClass(String availabilityClass) {
        this.availabilityClass = availabilityClass;
    }

    @Override
    public String toString() {
        return
                "AvailibilityClasses{" +
                        "subClass = '" + subClass + '\'' +
                        ",price = '" + price + '\'' +
                        ",availabilityStatus = '" + availabilityStatus + '\'' +
                        ",availabilityClass = '" + availabilityClass + '\'' +
                        "}";
    }
}