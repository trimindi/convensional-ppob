package com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SetShipBookingResponse {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("ticketPrice")
    private int ticketPrice;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("numCode")
    private String numCode;

    @JsonProperty("salesPrice")
    private int salesPrice;

    @JsonProperty("bookingDateTime")
    private String bookingDateTime;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("paxBookingDetails")
    private List<PaxBookingDetailsItem> paxBookingDetails;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("issuedDateTimeLimit")
    private String issuedDateTimeLimit;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("memberDiscount")
    private int memberDiscount;

    @JsonProperty("status")
    private String status;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public String getNumCode() {
        return numCode;
    }

    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getBookingDateTime() {
        return bookingDateTime;
    }

    public void setBookingDateTime(String bookingDateTime) {
        this.bookingDateTime = bookingDateTime;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public List<PaxBookingDetailsItem> getPaxBookingDetails() {
        return paxBookingDetails;
    }

    public void setPaxBookingDetails(List<PaxBookingDetailsItem> paxBookingDetails) {
        this.paxBookingDetails = paxBookingDetails;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getIssuedDateTimeLimit() {
        return issuedDateTimeLimit;
    }

    public void setIssuedDateTimeLimit(String issuedDateTimeLimit) {
        this.issuedDateTimeLimit = issuedDateTimeLimit;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public int getMemberDiscount() {
        return memberDiscount;
    }

    public void setMemberDiscount(int memberDiscount) {
        this.memberDiscount = memberDiscount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SetShipBookingResponse{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",ticketPrice = '" + ticketPrice + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",numCode = '" + numCode + '\'' +
                        ",salesPrice = '" + salesPrice + '\'' +
                        ",bookingDateTime = '" + bookingDateTime + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",paxBookingDetails = '" + paxBookingDetails + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",issuedDateTimeLimit = '" + issuedDateTimeLimit + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",memberDiscount = '" + memberDiscount + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}