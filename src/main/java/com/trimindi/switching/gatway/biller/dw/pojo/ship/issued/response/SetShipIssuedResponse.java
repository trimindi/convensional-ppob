package com.trimindi.switching.gatway.biller.dw.pojo.ship.issued.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SetShipIssuedResponse {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("bookingDateTime")
    private String bookingDateTime;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("bookingNumber")
    private String bookingNumber;

    @JsonProperty("status")
    private String status;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public String getBookingDateTime() {
        return bookingDateTime;
    }

    public void setBookingDateTime(String bookingDateTime) {
        this.bookingDateTime = bookingDateTime;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SetShipIssuedResponse{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",bookingDateTime = '" + bookingDateTime + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",bookingNumber = '" + bookingNumber + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}