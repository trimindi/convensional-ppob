package com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class BaggageInfosItem {

    @JsonProperty("fare")
    private double fare;

    @JsonProperty("code")
    private String code;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("desc")
    private String desc;

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return
                "BaggageInfosItem{" +
                        "fare = '" + fare + '\'' +
                        ",code = '" + code + '\'' +
                        ",currency = '" + currency + '\'' +
                        ",desc = '" + desc + '\'' +
                        "}";
    }
}