package com.trimindi.switching.gatway.biller.rajabiller.request;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/16/2017.
 */
@XmlRootElement
public class Param {
    private Value value;

    public Param() {
    }

    public Param(Value value) {
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ClassPojo [value = " + value + "]";
    }
}
