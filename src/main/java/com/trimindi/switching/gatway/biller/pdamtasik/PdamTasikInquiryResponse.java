package com.trimindi.switching.gatway.biller.pdamtasik;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PdamTasikInquiryResponse {

    @JsonProperty("billname")
    private String billname;

    @JsonProperty("address")
    private String address;

    @JsonProperty("totalamount")
    private int totalamount;

    @JsonProperty("billnumber")
    private String billnumber;

    @JsonProperty("billqty")
    private int billqty;

    @JsonProperty("resultcode")
    private int resultcode;

    @JsonProperty("detail")
    private List<DetailItem> detail;

    @JsonProperty("type")
    private String type;

    @JsonProperty("ts")
    private String ts;

    public String getBillname() {
        return billname;
    }

    public void setBillname(String billname) {
        this.billname = billname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(int totalamount) {
        this.totalamount = totalamount;
    }

    public String getBillnumber() {
        return billnumber;
    }

    public void setBillnumber(String billnumber) {
        this.billnumber = billnumber;
    }

    public int getBillqty() {
        return billqty;
    }

    public void setBillqty(int billqty) {
        this.billqty = billqty;
    }

    public int getResultcode() {
        return resultcode;
    }

    public void setResultcode(int resultcode) {
        this.resultcode = resultcode;
    }

    public List<DetailItem> getDetail() {
        return detail;
    }

    public void setDetail(List<DetailItem> detail) {
        this.detail = detail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    @Override
    public String toString() {
        return
                "PdamTasikInquiryResponse{" +
                        "billname = '" + billname + '\'' +
                        ",address = '" + address + '\'' +
                        ",totalamount = '" + totalamount + '\'' +
                        ",billnumber = '" + billnumber + '\'' +
                        ",billqty = '" + billqty + '\'' +
                        ",resultcode = '" + resultcode + '\'' +
                        ",detail = '" + detail + '\'' +
                        ",type = '" + type + '\'' +
                        ",ts = '" + ts + '\'' +
                        "}";
    }
}