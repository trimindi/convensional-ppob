package com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SetShipBookingRequest {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("paxDetails")
    private List<PaxDetailsItem> paxDetails;

    @JsonProperty("numCode")
    private String numCode;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("userID")
    private String userID;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public List<PaxDetailsItem> getPaxDetails() {
        return paxDetails;
    }

    public void setPaxDetails(List<PaxDetailsItem> paxDetails) {
        this.paxDetails = paxDetails;
    }

    public String getNumCode() {
        return numCode;
    }

    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return
                "SetShipBookingRequest{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",paxDetails = '" + paxDetails + '\'' +
                        ",numCode = '" + numCode + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",userID = '" + userID + '\'' +
                        "}";
    }
}