package com.trimindi.switching.gatway.biller.dw.pojo.ship.route;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class OriginsItem {

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("originName")
    private String originName;

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    @Override
    public String toString() {
        return
                "OriginsItem{" +
                        "originPort = '" + originPort + '\'' +
                        ",originName = '" + originName + '\'' +
                        "}";
    }
}