package com.trimindi.switching.gatway.biller.dw.pojo.airline.issued;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SetArilineIssuedResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("agentBalance")
    private double agentBalance;

    @JsonProperty("accessToken")
    @JsonIgnore
    private String accessToken;

    @JsonProperty("userID")
    @JsonIgnore
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("airlineAccessCode")
    private String airlineAccessCode;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("status")
    @JsonIgnore
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getAgentBalance() {
        return agentBalance;
    }

    public void setAgentBalance(double agentBalance) {
        this.agentBalance = agentBalance;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getAirlineAccessCode() {
        return airlineAccessCode;
    }

    public void setAirlineAccessCode(String airlineAccessCode) {
        this.airlineAccessCode = airlineAccessCode;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SetArilineIssuedResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",agentBalance = '" + agentBalance + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",airlineAccessCode = '" + airlineAccessCode + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}