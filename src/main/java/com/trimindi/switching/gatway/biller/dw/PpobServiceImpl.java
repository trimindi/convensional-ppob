package com.trimindi.switching.gatway.biller.dw;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.DwSession;
import com.trimindi.switching.gatway.biller.dw.pojo.ppob.InquiryRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ppob.PaymentRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.ppob.response.InquiryResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ppob.response.PaymentResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ppob.response.TvInquiryResponse;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Map;

/**
 * Created by sx on 22/01/18.
 * Copyright under comercial unit
 */
@Service
public class PpobServiceImpl implements PpobService {
    @Autowired
    private
    DwSession session;
    private final
    ProductFeeService productFeeService;
    private final
    ObjectMapper om;
    private Logger logger = LoggerFactory.getLogger(PpobServiceImpl.class);
    private final CloseableHttpClient client;
    @Value("${dw.url.inquiry}")
    private String inquiryPath;
    @Value("${dw.url.payment}")
    private String paymentPath;
    private final PartnerDepositService partnerDepositService;
    private final TransaksiService transaksiService;

    @Autowired
    public PpobServiceImpl(ProductFeeService productFeeService, ObjectMapper om, CloseableHttpClient client, PartnerDepositService partnerDepositService, TransaksiService transaksiService) {
        this.productFeeService = productFeeService;
        this.om = om;
        this.client = client;
        this.partnerDepositService = partnerDepositService;
        this.transaksiService = transaksiService;
    }

    @Override
    public Response inquiry(
            InquiryRequest inquiryRequest,
            ProductItem productItem,
            PartnerCredential partnerCredential,
            Map<String, String> params) throws IOException {
        inquiryRequest.setAccessToken(session.authKey().getAccessToken());
        inquiryRequest.setUserID(session.authKey().getUserID());
        String response = this.process(inquiryPath, om.writeValueAsString(inquiryRequest));
        InquiryResponse inquiryResponse = om.readValue(response, InquiryResponse.class);
        if (!inquiryResponse.getStatus().equalsIgnoreCase("SUCCESS")) {
            return Response.status(200).entity(
                    new ResponseCode("0005", inquiryResponse.getRespMessage())
                            .setSaldo(
                                    partnerDepositService
                                            .findById(partnerCredential.getPartner_id()).getBALANCE())
                            .setMssidn(inquiryRequest.getCustomerID())
                            .setProduct(productItem.getDENOM())).build();

        }
        ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), productItem.getDENOM());
        Transaksi transaksi = new Transaksi();
        transaksi.setADMIN(inquiryResponse.getAdminBank())
                .setMSSIDN_NAME(inquiryResponse.getCustomerName())
                .setHOST_REF_NUMBER("-")
                .setBILL_REF_NUMBER(inquiryResponse.getBillingReferenceID())
                .setMSSIDN(inquiryResponse.getCustomerID())
                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                .setPRODUCT(productItem.getProduct_id())
                .setTAGIHAN(inquiryResponse.getBilling())
                .setDENOM(productItem.getDENOM())
                .setFEE_CA(productFee.getFEE_CA())
                .setFEE_DEALER(((productItem.getFEE_BILLER() - productFee.getFEE_CA())))
                .setUSERID(partnerCredential.getPartner_uid())
                .setINQUIRY(response)
                .setCHARGE((inquiryResponse.getBilling() + inquiryResponse.getAdminBank() + inquiryResponse.getPenalty() - ((productItem.getFEE_BILLER() - productFee.getFEE_CA()))))
                .setDEBET((inquiryResponse.getBilling() + inquiryResponse.getAdminBank() + inquiryResponse.getPenalty() - ((productItem.getFEE_BILLER() - productFee.getFEE_CA()))))
                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                .setPARTNERID(partnerCredential.getPartner_id())
                .setDENDA(inquiryResponse.getPenalty())
                .setST(Status.INQUIRY);
        transaksiService.save(transaksi);
        TvInquiryResponse tvInquiryResponse = new TvInquiryResponse();
        tvInquiryResponse.setIdpelanggan(inquiryResponse.getCustomerID());
        tvInquiryResponse.setAdmin(inquiryResponse.getAdminBank());
        tvInquiryResponse.setNamapelanggan(inquiryResponse.getCustomerName());
        tvInquiryResponse.setPeriode(inquiryResponse.getPeriod());
        tvInquiryResponse.setDenda(inquiryResponse.getPenalty());
        tvInquiryResponse.setTagihan(inquiryResponse.getBilling());
        tvInquiryResponse.setKeterangan("Inquiry Sukses");
        Integer total = inquiryResponse.getAdminBank() +
                inquiryResponse.getBilling() +
                inquiryResponse.getPenalty();
        tvInquiryResponse.setTotalTagihan(total);
        BaseResponse<TvInquiryResponse> baseResponse = new BaseResponse<>();
        baseResponse.setData(tvInquiryResponse);
        baseResponse.setFee(transaksi.getFEE_DEALER());
        baseResponse.setNtrans(transaksi.getNTRANS());
        baseResponse.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
        baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
        baseResponse.setProduct(transaksi.getDENOM());
        baseResponse.setMessage("Successful");
        return Response.status(200).entity(baseResponse).build();
    }

    @Override
    public Response payment(Transaksi transaksi) throws IOException {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAccessToken(session.authKey().getAccessToken());
        paymentRequest.setUserID(session.authKey().getUserID());
        paymentRequest.setBillingReferenceID(transaksi.getBILL_REF_NUMBER());
        String response = this.process(paymentPath, om.writeValueAsString(paymentRequest));
        PaymentResponse paymentResponse = om.readValue(response, PaymentResponse.class);
        switch (paymentResponse.getStatus()) {
            case "REQUEST":
                transaksi
                        .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                        .setPAYMENT(response);
                transaksiService.save(transaksi);
                return Response.status(200).entity(
                        ResponseCode.PAYMENT_UNDER_PROSES.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(transaksi.getMSSIDN()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build();
            case "FAILED":
                partnerDepositService.reverseSaldo(transaksi, response);
                return Response.status(200).entity(
                        ResponseCode.PAYMENT_FAILED.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(transaksi.getMSSIDN()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build();
            case "PENDING":
                transaksi
                        .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                        .setPAYMENT(response);
                transaksiService.save(transaksi);
                return Response.status(200).entity(
                        ResponseCode.PAYMENT_UNDER_PROSES.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(transaksi.getMSSIDN()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build();
            case "SUCCESS":
                transaksi.setST(Status.PAYMENT_SUCCESS)
                        .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                        .setPAYMENT(response);
                transaksiService.save(transaksi);
                InquiryResponse inquiryResponse = om.readValue(transaksi.getINQUIRY(), InquiryResponse.class);
                TvInquiryResponse tvInquiryResponse = new TvInquiryResponse();
                tvInquiryResponse.setIdpelanggan(inquiryResponse.getCustomerID());
                tvInquiryResponse.setAdmin(inquiryResponse.getAdminBank());
                tvInquiryResponse.setNamapelanggan(inquiryResponse.getCustomerName());
                tvInquiryResponse.setPeriode(inquiryResponse.getPeriod());
                tvInquiryResponse.setDenda(inquiryResponse.getPenalty());
                tvInquiryResponse.setTagihan(inquiryResponse.getBilling());
                tvInquiryResponse.setKeterangan("Payment Sukses");
                Integer total = inquiryResponse.getAdminBank() +
                        inquiryResponse.getBilling() +
                        inquiryResponse.getPenalty();
                tvInquiryResponse.setTotalTagihan(total);
                BaseResponse<TvInquiryResponse> baseResponse = new BaseResponse<>();
                baseResponse.setData(tvInquiryResponse);
                baseResponse.setFee(transaksi.getFEE_DEALER());
                baseResponse.setNtrans(transaksi.getNTRANS());
                baseResponse.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
                baseResponse.setProduct(transaksi.getDENOM());
                baseResponse.setMessage("Successful");
                return Response.status(200).entity(baseResponse).build();
            default:
                partnerDepositService.reverseSaldo(transaksi, response);
                return Response.status(200).entity(
                        ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(transaksi.getMSSIDN()).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build();
        }
    }

    private String process(String url, String payload) throws IOException {
        logger.error("send req to       -> {}", url);
        logger.error("send req payload  -> {}", payload);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setEntity(new StringEntity(payload));
        CloseableHttpResponse execute = client.execute(httpPost);
        String respone = EntityUtils.toString(execute.getEntity(), StandardCharsets.UTF_8.name());
        logger.error("Response req  -> {}", respone);
        return respone;
    }
}
