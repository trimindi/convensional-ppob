package com.trimindi.switching.gatway.biller.dw.pojo.train.schedule;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetTrainSchaduleResponse {

    @JsonProperty("trainID")
    private String trainID;

    @JsonIgnore
    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonIgnore
    @JsonProperty("accessToken")
    private String accessToken;
    @JsonIgnore
    @JsonProperty("userID")
    private String userID;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonProperty("destinationFull")
    private String destinationFull;

    @JsonProperty("schedules")
    private List<Schedules> schedules;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("originFull")
    private String originFull;

    @JsonProperty("status")
    private String status;

    public String getTrainID() {
        return trainID;
    }

    public void setTrainID(String trainID) {
        this.trainID = trainID;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getDestinationFull() {
        return destinationFull;
    }

    public void setDestinationFull(String destinationFull) {
        this.destinationFull = destinationFull;
    }

    public List<Schedules> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedules> schedules) {
        this.schedules = schedules;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    public String getOriginFull() {
        return originFull;
    }

    public void setOriginFull(String originFull) {
        this.originFull = originFull;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetTrainSchaduleResponse{" +
                        "trainID = '" + trainID + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",destinationFull = '" + destinationFull + '\'' +
                        ",schedules = '" + schedules + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",originFull = '" + originFull + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}