package com.trimindi.switching.gatway.biller.dw.pojo.ship.room.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetShipRoomResponse {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("rooms")
    private List<RoomsItem> rooms;

    @JsonProperty("ticketBuyerName")
    private String ticketBuyerName;

    @JsonProperty("ticketPrice")
    private int ticketPrice;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("numCode")
    private String numCode;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("ticketBuyerEmail")
    private String ticketBuyerEmail;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("ticketBuyerAddress")
    private String ticketBuyerAddress;

    @JsonProperty("ticketBuyerPhone")
    private String ticketBuyerPhone;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("bookTimeLimit")
    private int bookTimeLimit;

    @JsonProperty("status")
    private String status;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public List<RoomsItem> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomsItem> rooms) {
        this.rooms = rooms;
    }

    public String getTicketBuyerName() {
        return ticketBuyerName;
    }

    public void setTicketBuyerName(String ticketBuyerName) {
        this.ticketBuyerName = ticketBuyerName;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public String getNumCode() {
        return numCode;
    }

    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getTicketBuyerEmail() {
        return ticketBuyerEmail;
    }

    public void setTicketBuyerEmail(String ticketBuyerEmail) {
        this.ticketBuyerEmail = ticketBuyerEmail;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTicketBuyerAddress() {
        return ticketBuyerAddress;
    }

    public void setTicketBuyerAddress(String ticketBuyerAddress) {
        this.ticketBuyerAddress = ticketBuyerAddress;
    }

    public String getTicketBuyerPhone() {
        return ticketBuyerPhone;
    }

    public void setTicketBuyerPhone(String ticketBuyerPhone) {
        this.ticketBuyerPhone = ticketBuyerPhone;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public int getBookTimeLimit() {
        return bookTimeLimit;
    }

    public void setBookTimeLimit(int bookTimeLimit) {
        this.bookTimeLimit = bookTimeLimit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetShipRoomResponse{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",rooms = '" + rooms + '\'' +
                        ",ticketBuyerName = '" + ticketBuyerName + '\'' +
                        ",ticketPrice = '" + ticketPrice + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",numCode = '" + numCode + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",ticketBuyerEmail = '" + ticketBuyerEmail + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",ticketBuyerAddress = '" + ticketBuyerAddress + '\'' +
                        ",ticketBuyerPhone = '" + ticketBuyerPhone + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",bookTimeLimit = '" + bookTimeLimit + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}