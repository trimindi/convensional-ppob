package com.trimindi.switching.gatway.biller.dw.pojo.airline.bookingdetail;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class FlightDepartsItem {

    @JsonProperty("fdOrigin")
    private String fdOrigin;

    @JsonProperty("fdDestination")
    private String fdDestination;

    @JsonProperty("fdArrivalTime")
    private String fdArrivalTime;

    @JsonProperty("fdDepartTime")
    private String fdDepartTime;

    @JsonProperty("flightNumber")
    private String flightNumber;

    @JsonProperty("fdFlightClass")
    private String fdFlightClass;

    public String getFdOrigin() {
        return fdOrigin;
    }

    public void setFdOrigin(String fdOrigin) {
        this.fdOrigin = fdOrigin;
    }

    public String getFdDestination() {
        return fdDestination;
    }

    public void setFdDestination(String fdDestination) {
        this.fdDestination = fdDestination;
    }

    public String getFdArrivalTime() {
        return fdArrivalTime;
    }

    public void setFdArrivalTime(String fdArrivalTime) {
        this.fdArrivalTime = fdArrivalTime;
    }

    public String getFdDepartTime() {
        return fdDepartTime;
    }

    public void setFdDepartTime(String fdDepartTime) {
        this.fdDepartTime = fdDepartTime;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFdFlightClass() {
        return fdFlightClass;
    }

    public void setFdFlightClass(String fdFlightClass) {
        this.fdFlightClass = fdFlightClass;
    }

    @Override
    public String toString() {
        return
                "FlightDepartsItem{" +
                        "fdOrigin = '" + fdOrigin + '\'' +
                        ",fdDestination = '" + fdDestination + '\'' +
                        ",fdArrivalTime = '" + fdArrivalTime + '\'' +
                        ",fdDepartTime = '" + fdDepartTime + '\'' +
                        ",flightNumber = '" + flightNumber + '\'' +
                        ",fdFlightClass = '" + fdFlightClass + '\'' +
                        "}";
    }
}