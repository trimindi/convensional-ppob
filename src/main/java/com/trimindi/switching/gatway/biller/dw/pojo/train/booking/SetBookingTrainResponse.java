package com.trimindi.switching.gatway.biller.dw.pojo.train.booking;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SetBookingTrainResponse {

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("salesPrice")
    private double salesPrice;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("adminFee")
    private double adminFee;

    @JsonIgnore
    @JsonProperty("userID")
    private String userID;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonProperty("destinationFull")
    private String destinationFull;

    @JsonProperty("arrivalTime")
    private String arrivalTime;

    @JsonProperty("trainName")
    private String trainName;

    @JsonIgnore
    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("passengers")
    private List<PassengersItem> passengers;

    @JsonProperty("departTime")
    private String departTime;

    @JsonProperty("ticketPrice")
    private double ticketPrice;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("issuedTimeLimit")
    private String issuedTimeLimit;

    @JsonIgnore
    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("trainMarkup")
    private double trainMarkup;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("trainNumber")
    private String trainNumber;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("availabilityClass")
    private String availabilityClass;

    @JsonProperty("originFull")
    private String originFull;

    @JsonProperty("status")
    private String status;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(double adminFee) {
        this.adminFee = adminFee;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public String getDestinationFull() {
        return destinationFull;
    }

    public void setDestinationFull(String destinationFull) {
        this.destinationFull = destinationFull;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public List<PassengersItem> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<PassengersItem> passengers) {
        this.passengers = passengers;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getIssuedTimeLimit() {
        return issuedTimeLimit;
    }

    public void setIssuedTimeLimit(String issuedTimeLimit) {
        this.issuedTimeLimit = issuedTimeLimit;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public double getTrainMarkup() {
        return trainMarkup;
    }

    public void setTrainMarkup(double trainMarkup) {
        this.trainMarkup = trainMarkup;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getAvailabilityClass() {
        return availabilityClass;
    }

    public void setAvailabilityClass(String availabilityClass) {
        this.availabilityClass = availabilityClass;
    }

    public String getOriginFull() {
        return originFull;
    }

    public void setOriginFull(String originFull) {
        this.originFull = originFull;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SetBookingTrainResponse{" +
                        "origin = '" + origin + '\'' +
                        ",salesPrice = '" + salesPrice + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",adminFee = '" + adminFee + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",destinationFull = '" + destinationFull + '\'' +
                        ",arrivalTime = '" + arrivalTime + '\'' +
                        ",trainName = '" + trainName + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",passengers = '" + passengers + '\'' +
                        ",departTime = '" + departTime + '\'' +
                        ",ticketPrice = '" + ticketPrice + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",issuedTimeLimit = '" + issuedTimeLimit + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",trainMarkup = '" + trainMarkup + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",trainNumber = '" + trainNumber + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",availabilityClass = '" + availabilityClass + '\'' +
                        ",originFull = '" + originFull + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}