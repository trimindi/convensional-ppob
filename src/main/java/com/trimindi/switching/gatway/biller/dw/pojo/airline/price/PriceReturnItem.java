package com.trimindi.switching.gatway.biller.dw.pojo.airline.price;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class PriceReturnItem {

    @JsonProperty("pdDate")
    private String pdDate;

    @JsonProperty("classFare")
    private String classFare;

    @JsonProperty("pdOrigin")
    private String pdOrigin;

    @JsonProperty("priceDetail")
    private List<PriceDetailItem> priceDetail;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("pdDestination")
    private String pdDestination;

    @JsonProperty("flightNumber")
    private String flightNumber;

    public String getPdDate() {
        return pdDate;
    }

    public void setPdDate(String pdDate) {
        this.pdDate = pdDate;
    }

    public String getClassFare() {
        return classFare;
    }

    public void setClassFare(String classFare) {
        this.classFare = classFare;
    }

    public String getPdOrigin() {
        return pdOrigin;
    }

    public void setPdOrigin(String pdOrigin) {
        this.pdOrigin = pdOrigin;
    }

    public List<PriceDetailItem> getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(List<PriceDetailItem> priceDetail) {
        this.priceDetail = priceDetail;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPdDestination() {
        return pdDestination;
    }

    public void setPdDestination(String pdDestination) {
        this.pdDestination = pdDestination;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    public String toString() {
        return
                "PriceReturnItem{" +
                        "pdDate = '" + pdDate + '\'' +
                        ",classFare = '" + classFare + '\'' +
                        ",pdOrigin = '" + pdOrigin + '\'' +
                        ",priceDetail = '" + priceDetail + '\'' +
                        ",currency = '" + currency + '\'' +
                        ",pdDestination = '" + pdDestination + '\'' +
                        ",flightNumber = '" + flightNumber + '\'' +
                        "}";
    }
}