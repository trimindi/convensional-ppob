package com.trimindi.switching.gatway.biller.dw.pojo.pulsa;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 10/03/18.
 * Copyright under comercial unit
 */
@XmlRootElement(name = "pulsamatic")
public class PulsaMaticRequest {
    private String trxid;
    private String sign;
    private String time;
    private String msisdn;
    private String command;
    private String userid;
    private String vtype;

    public PulsaMaticRequest() {
    }

    public String getTrxid() {
        return trxid;
    }

    public PulsaMaticRequest setTrxid(String trxid) {
        this.trxid = trxid;
        return this;
    }

    public String getSign() {
        return sign;
    }

    public PulsaMaticRequest setSign(String sign) {
        this.sign = sign;
        return this;
    }

    public String getTime() {
        return time;
    }

    public PulsaMaticRequest setTime(String time) {
        this.time = time;
        return this;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public PulsaMaticRequest setMsisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public String getCommand() {
        return command;
    }

    public PulsaMaticRequest setCommand(String command) {
        this.command = command;
        return this;
    }

    public String getUserid() {
        return userid;
    }

    public PulsaMaticRequest setUserid(String userid) {
        this.userid = userid;
        return this;
    }

    public String getVtype() {
        return vtype;
    }

    public PulsaMaticRequest setVtype(String vtype) {
        this.vtype = vtype;
        return this;
    }
}