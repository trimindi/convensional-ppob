package com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelPriceResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("totalPrice")
    private double totalPrice;

    @JsonProperty("cancelPolicy")
    private String cancelPolicy;

    @JsonProperty("cityID")
    private String cityID;

    @JsonProperty("hotelID")
    private String hotelID;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("checkInDate")
    private String checkInDate;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("paxPassport")
    private String paxPassport;

    @JsonProperty("countryID")
    private String countryID;

    @JsonProperty("roomID")
    private String roomID;

    @JsonProperty("isAgentDebtOverdue")
    private boolean isAgentDebtOverdue;

    @JsonProperty("checkOutDate")
    private String checkOutDate;

    @JsonProperty("roomRequest")
    private List<RoomRequestItem> roomRequest;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("isEnableBooking")
    private boolean isEnableBooking;

    @JsonProperty("commission")
    private double commission;

    @JsonProperty("breakfast")
    private String breakfast;

    @JsonProperty("internalCode")
    private String internalCode;

    @JsonProperty("status")
    private String status;

    public GetHotelPriceResponse() {
    }

    public String getRespMessage() {
        return respMessage;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public String getCancelPolicy() {
        return cancelPolicy;
    }

    public String getCityID() {
        return cityID;
    }

    public String getHotelID() {
        return hotelID;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public String getPaxPassport() {
        return paxPassport;
    }

    public String getCountryID() {
        return countryID;
    }

    public String getRoomID() {
        return roomID;
    }

    public boolean isAgentDebtOverdue() {
        return isAgentDebtOverdue;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public List<RoomRequestItem> getRoomRequest() {
        return roomRequest;
    }

    public String getRespTime() {
        return respTime;
    }

    public GetHotelPriceResponse setAgentDebtOverdue(boolean agentDebtOverdue) {
        isAgentDebtOverdue = agentDebtOverdue;
        return this;
    }

    public double getCommission() {
        return commission;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public String getStatus() {
        return status;
    }

    public boolean isEnableBooking() {
        return isEnableBooking;
    }

    public GetHotelPriceResponse setEnableBooking(boolean enableBooking) {
        isEnableBooking = enableBooking;
        return this;
    }

    public GetHotelPriceResponse setRespMessage(String respMessage) {
        this.respMessage = respMessage;
        return this;
    }

    public GetHotelPriceResponse setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public GetHotelPriceResponse setCancelPolicy(String cancelPolicy) {
        this.cancelPolicy = cancelPolicy;
        return this;
    }

    public GetHotelPriceResponse setCityID(String cityID) {
        this.cityID = cityID;
        return this;
    }

    public GetHotelPriceResponse setHotelID(String hotelID) {
        this.hotelID = hotelID;
        return this;
    }

    public GetHotelPriceResponse setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public GetHotelPriceResponse setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
        return this;
    }

    public GetHotelPriceResponse setUserID(String userID) {
        this.userID = userID;
        return this;
    }

    public GetHotelPriceResponse setPaxPassport(String paxPassport) {
        this.paxPassport = paxPassport;
        return this;
    }

    public GetHotelPriceResponse setCountryID(String countryID) {
        this.countryID = countryID;
        return this;
    }

    public GetHotelPriceResponse setRoomID(String roomID) {
        this.roomID = roomID;
        return this;
    }

    public GetHotelPriceResponse setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
        return this;
    }

    public GetHotelPriceResponse setRoomRequest(List<RoomRequestItem> roomRequest) {
        this.roomRequest = roomRequest;
        return this;
    }

    public GetHotelPriceResponse setRespTime(String respTime) {
        this.respTime = respTime;
        return this;
    }

    public GetHotelPriceResponse setCommission(double commission) {
        this.commission = commission;
        return this;
    }

    public GetHotelPriceResponse setBreakfast(String breakfast) {
        this.breakfast = breakfast;
        return this;
    }

    public GetHotelPriceResponse setInternalCode(String internalCode) {
        this.internalCode = internalCode;
        return this;
    }

    public GetHotelPriceResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        return
                "GetHotelPriceResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",totalPrice = '" + totalPrice + '\'' +
                        ",cancelPolicy = '" + cancelPolicy + '\'' +
                        ",cityID = '" + cityID + '\'' +
                        ",hotelID = '" + hotelID + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",checkInDate = '" + checkInDate + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",paxPassport = '" + paxPassport + '\'' +
                        ",countryID = '" + countryID + '\'' +
                        ",roomID = '" + roomID + '\'' +
                        ",isAgentDebtOverdue = '" + isAgentDebtOverdue + '\'' +
                        ",checkOutDate = '" + checkOutDate + '\'' +
                        ",roomRequest = '" + roomRequest + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",isEnableBooking = '" + isEnableBooking + '\'' +
                        ",commission = '" + commission + '\'' +
                        ",breakfast = '" + breakfast + '\'' +
                        ",internalCode = '" + internalCode + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}