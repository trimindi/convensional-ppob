package com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.RoomType;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class RoomRequestItem {

    @JsonProperty("childNum")
    @NotNull
    private String childNum;

    @JsonProperty("isRequestChildBed")
    @NotNull
    private String isRequestChildBed;

    @JsonProperty("childAges")
    private List<Integer> childAges = new ArrayList<>();

    @JsonProperty("roomType")
    @NotNull
    private RoomType roomType;

    public String getChildNum() {
        return childNum;
    }

    public void setChildNum(String childNum) {
        this.childNum = childNum;
    }

    public String getIsRequestChildBed() {
        return isRequestChildBed;
    }

    public void setIsRequestChildBed(String isRequestChildBed) {
        this.isRequestChildBed = isRequestChildBed;
    }

    public List<Integer> getChildAges() {
        return childAges;
    }

    public RoomRequestItem setChildAges(List<Integer> childAges) {
        this.childAges = childAges;
        return this;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public RoomRequestItem setRoomType(RoomType roomType) {
        this.roomType = roomType;
        return this;
    }

    @Override
    public String toString() {
        return
                "RoomRequestItem{" +
                        "childNum = '" + childNum + '\'' +
                        ",isRequestChildBed = '" + isRequestChildBed + '\'' +
                        ",childAges = '" + childAges + '\'' +
                        ",roomType = '" + roomType + '\'' +
                        "}";
    }
}