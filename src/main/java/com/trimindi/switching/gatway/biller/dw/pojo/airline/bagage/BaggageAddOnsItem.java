package com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class BaggageAddOnsItem {

    @JsonProperty("baggageInfos")
    private List<BaggageInfosItem> baggageInfos;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("mealInfos")
    private List<MealInfosItem> mealInfos;

    @JsonProperty("isEnableNoBaggage")
    private boolean isEnableNoBaggage;

    public List<BaggageInfosItem> getBaggageInfos() {
        return baggageInfos;
    }

    public void setBaggageInfos(List<BaggageInfosItem> baggageInfos) {
        this.baggageInfos = baggageInfos;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<MealInfosItem> getMealInfos() {
        return mealInfos;
    }

    public void setMealInfos(List<MealInfosItem> mealInfos) {
        this.mealInfos = mealInfos;
    }

    public boolean isIsEnableNoBaggage() {
        return isEnableNoBaggage;
    }

    public void setIsEnableNoBaggage(boolean isEnableNoBaggage) {
        this.isEnableNoBaggage = isEnableNoBaggage;
    }

    @Override
    public String toString() {
        return
                "BaggageAddOnsItem{" +
                        "baggageInfos = '" + baggageInfos + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",mealInfos = '" + mealInfos + '\'' +
                        ",isEnableNoBaggage = '" + isEnableNoBaggage + '\'' +
                        "}";
    }
}