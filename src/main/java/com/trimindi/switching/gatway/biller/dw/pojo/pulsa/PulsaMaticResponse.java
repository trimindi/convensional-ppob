package com.trimindi.switching.gatway.biller.dw.pojo.pulsa;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 10/03/18.
 * Copyright under comercial unit
 */
@XmlRootElement(name = "pulsamatic")
public class PulsaMaticResponse {
    private String date;
    private String result;
    private String message;
    private String trxid;
    private String partner_trxid;

    public PulsaMaticResponse() {
    }

    public String getDate() {
        return date;
    }

    public PulsaMaticResponse setDate(String date) {
        this.date = date;
        return this;
    }

    public String getResult() {
        return result;
    }

    public PulsaMaticResponse setResult(String result) {
        this.result = result;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public PulsaMaticResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getTrxid() {
        return trxid;
    }

    public PulsaMaticResponse setTrxid(String trxid) {
        this.trxid = trxid;
        return this;
    }

    public String getPartner_trxid() {
        return partner_trxid;
    }

    public PulsaMaticResponse setPartner_trxid(String partner_trxid) {
        this.partner_trxid = partner_trxid;
        return this;
    }
}
