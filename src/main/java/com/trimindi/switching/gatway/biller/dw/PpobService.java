package com.trimindi.switching.gatway.biller.dw;

import com.trimindi.switching.gatway.biller.dw.pojo.ppob.InquiryRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.models.Transaksi;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Map;

/**
 * Created by sx on 22/01/18.
 * Copyright under comercial unit
 */
public interface PpobService {
    Response inquiry(
            InquiryRequest inquiryRequest,
            ProductItem productItem,
            PartnerCredential partnerCredential,
            Map<String, String> params) throws IOException;

    Response payment(Transaksi transaksi) throws IOException;
}
