package com.trimindi.switching.gatway.biller.rajabiller.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/18/2017.
 */
@XmlRootElement
public class Value {
    private String string;
    private Struct struct;
    private Array array;

    public Value() {
    }

    public String getString() {
        return string;
    }

    public Value setString(String string) {
        this.string = string;
        return this;
    }

    public Struct getStruct() {
        return struct;
    }

    public Value setStruct(Struct struct) {
        this.struct = struct;
        return this;
    }

    public Array getArray() {
        return array;
    }

    public Value setArray(Array array) {
        this.array = array;
        return this;
    }
}