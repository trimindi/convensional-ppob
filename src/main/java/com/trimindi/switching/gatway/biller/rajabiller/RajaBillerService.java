package com.trimindi.switching.gatway.biller.rajabiller;

import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.rajabiller.response.MethodResponse;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.models.*;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.bpjs.InquiryBpjs;
import com.trimindi.switching.gatway.response.bpjs.PaymentBpjs;
import com.trimindi.switching.gatway.response.pasca.InquiryPhonePasca;
import com.trimindi.switching.gatway.response.pasca.PaymentPhonePasca;
import com.trimindi.switching.gatway.response.pdam.Inquiry;
import com.trimindi.switching.gatway.response.pdam.Payment;
import com.trimindi.switching.gatway.response.pdam.base.InquiryPdam;
import com.trimindi.switching.gatway.response.pdam.base.PaymentPdam;
import com.trimindi.switching.gatway.response.telkom.PaymentTelkom;
import com.trimindi.switching.gatway.response.telkom.base.InquiryTelkom;
import com.trimindi.switching.gatway.response.telkom.base.PaymentTelkomNew;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Constanta;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Objects;

/**
 * Created by PC on 6/19/2017.
 */

@Component
@Scope(value = "request")
public class RajaBillerService {
    public static final Logger logger = LoggerFactory.getLogger(RajaBillerService.class);
    private static HttpPost httpPost;

    final
    RajabilerRequest rajabilerRequest;
    private final TransaksiService transaksiService;
    private final ProductFeeService productFeeService;
    private final PartnerDepositService partnerDepositService;
    private final CloseableHttpClient client;
    @Value("${rajabiler.host}")
    String host;
    @Autowired
    private Unmarshaller unmarshaller;
    @Autowired
    private SlackSendMessage slackSendMessage;

    @Autowired
    public RajaBillerService(RajabilerRequest rajabilerRequest, TransaksiService transaksiService, ProductFeeService productFeeService, PartnerDepositService partnerDepositService, CloseableHttpClient client) {
        this.rajabilerRequest = rajabilerRequest;
        this.transaksiService = transaksiService;
        this.productFeeService = productFeeService;
        this.partnerDepositService = partnerDepositService;
        this.client = client;
    }

    public void InquiryPDAM(@Suspended final AsyncResponse toResponse, final ProductItem product, final Map<String, String> params, final PartnerCredential partnerCredential) {
        httpPost = new HttpPost(host);
        final ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), product.getDENOM());
        try {
            Transaksi transaksi = new Transaksi()
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setUSERID(partnerCredential.getPartner_uid());
            transaksiService.save(transaksi);
            StringEntity entity;
            entity = new StringEntity(rajabilerRequest.inquiryPDAM(product.getDENOM_BILLER(), params.get(Constanta.MSSIDN), params.get(Constanta.MSSIDN), params.get(Constanta.MSSIDN), transaksi.getNTRANS()));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                Inquiry inquiry = new Inquiry(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(partnerCredential.getPartner_id());
                switch (inquiry.getSTATUS()) {
                    case "00":
                        if (inquiry.getTAGIHAN() == 0) {
                            toResponse.resume(Response.status(200).entity(new ResponseCode("0088", "TAGIHAN SUDAH TERBAYAR").setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
                            return;
                        }
                        int jml = 1;
                        if (inquiry.getPERIODE().contains(",")) {
                            String[] jum = inquiry.getPERIODE().split(",");
                            jml = jum.length;
                        }
                        transaksi.setADMIN(inquiry.getADMIN())
                                .setMSSIDN_NAME(inquiry.getNAMAPELANGGAN())
                                .setHOST_REF_NUMBER(inquiry.getREF2())
                                .setBILL_REF_NUMBER(inquiry.getREFF())
                                .setMSSIDN(inquiry.getIDPELANGGAN1())
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setPRODUCT(product.getProduct_id())
                                .setTAGIHAN(inquiry.getTAGIHAN())
                                .setDENOM(product.getDENOM())
                                .setFEE_CA((productFee.getFEE_CA() * jml))
                                .setFEE_DEALER(((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml))
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setINQUIRY(respone)
                                .setCHARGE((inquiry.getTAGIHAN() + inquiry.getADMIN() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setDEBET((inquiry.getTAGIHAN() + inquiry.getADMIN() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setDENDA(0)
                                .setST(Status.INQUIRY);
                        transaksiService.update(transaksi);
                        InquiryPdam inquiryPdam = new InquiryPdam()
                                .setAdmin((int) transaksi.getADMIN())
                                .setJumlahBill(jml)
                                .setNamapelanggan(inquiry.getNAMAPELANGGAN())
                                .setPeriode(inquiry.getPERIODE())
                                .setTagihan((int) inquiry.getTAGIHAN())
                                .setIdpelanggan(inquiry.getIDPELANGGAN())
                                .setTotalTagihan((int) (inquiry.getTAGIHAN() + inquiry.getADMIN()));
                        BaseResponse<InquiryPdam> inquiryResponse = new BaseResponse<>();
                        inquiryResponse.setData(inquiryPdam);
                        inquiryResponse.setFee(transaksi.getFEE_DEALER()).setNtrans(transaksi.getNTRANS());
                        inquiryResponse.setSaldo(partnerDeposit.getBALANCE());
                        inquiryResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        inquiryResponse.setProduct(transaksi.getDENOM());
                        inquiryResponse.setMessage("Successful");
                        toResponse.resume(Response.status(200).entity(inquiryResponse).build());
                        break;
                    default:
                        transaksiService.deleteById(transaksi.getNTRANS());
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", inquiry.getKETERANGAN()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
                        break;
                }
            } catch (JAXBException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                partnerDepositService.reverseSaldo(transaksi, respone);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
        }
    }

    public void PaymentPDAM(@Suspended AsyncResponse toResponse, Transaksi transaksi, Map<String, String> params) {
        httpPost = new HttpPost(host);
        Payment payment;
        String respone = null;
        try {
            String req = rajabilerRequest.paymentPDAM(transaksi);
            StringEntity entity = new StringEntity(req);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                payment = new Payment(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(transaksi.getPARTNERID());
                switch (payment.getSTATUS()) {
                    case "00":
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(payment.getREFF())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        int jml = 1;
                        if (payment.getPERIODE().contains(",")) {
                            String[] jum = payment.getPERIODE().split(",");
                            jml = jum.length;
                        }
                        PaymentPdam paymentPdam = new PaymentPdam()
                                .setAdmin((int) payment.getADMIN())
                                .setJumlahBill(jml)
                                .setReff(payment.getREFF())
                                .setPeriode(payment.getPERIODE())
                                .setNamapelanggan(payment.getNAMAPELANGGAN())
                                .setIdpelanggan(payment.getIDPELANGGAN())
                                .setTagihan((int) payment.getTAGIHAN())
                                .setTotalTagihan((int) (payment.getTAGIHAN() + payment.getADMIN()))
                                .setStandAwal((Objects.equals(payment.getSTANDAWAL(), "")) ? 0 : Integer.valueOf(payment.getSTANDAWAL()))
                                .setStandAkir((Objects.equals(payment.getSTANDAKHIR(), "")) ? 0 : Integer.valueOf(payment.getSTANDAKHIR()))
                                .setTotalTagihan((int) (payment.getTAGIHAN() + payment.getADMIN()));
                        BaseResponse<PaymentPdam> paymentResponse = new BaseResponse<>();

                        paymentResponse.setData(paymentPdam);
                        paymentResponse.setFee(transaksi.getFEE_DEALER());
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        paymentResponse.setMessage("Successful");
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverseSaldo(transaksi, respone);
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", payment.getKETERANGAN()).setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                        break;
                }
            } catch (JAXBException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                partnerDepositService.reverseSaldo(transaksi, respone);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            partnerDepositService.reverseSaldo(transaksi, respone);
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setMssidn(params.get(Constanta.MSSIDN)).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
        }
    }

    public void InquiryTelkom(@Suspended AsyncResponse toResponse, final ProductItem product, final Map<String, String> params, final PartnerCredential partnerCredential) {
        httpPost = new HttpPost(host);
        final ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), product.getDENOM());
        try {
            Transaksi transaksi = new Transaksi()
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setPARTNERID(partnerCredential.getPartner_id());
            transaksiService.save(transaksi);
            StringEntity entity;
            entity = new StringEntity(rajabilerRequest.inquiryTelkom(product.getDENOM_BILLER(), params.get(Constanta.AREA), params.get(Constanta.MSSIDN), transaksi.getNTRANS(), product.getDENOM().equalsIgnoreCase("SPEEDY")));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                Inquiry inquiry = new Inquiry(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(partnerCredential.getPartner_id());
                switch (inquiry.getSTATUS()) {
                    case "00":
                        if (inquiry.getTAGIHAN() == 0) {
                            toResponse.resume(Response.status(200).entity(new ResponseCode("0088", "TAGIHAN SUDAH TERBAYAR").setSaldo(partnerDeposit.getBALANCE()).setProduct(product.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
                            return;
                        }
                        int jml = 1;
                        if (inquiry.getPERIODE().contains(",")) {
                            String[] jum = inquiry.getPERIODE().split(",");
                            jml = jum.length;
                        }
                        String idpel = "";
                        if (inquiry.getIDPELANGGAN().equals(inquiry.getIDPELANGGAN1())) {
                            idpel = inquiry.getIDPELANGGAN1();
                        } else {
                            idpel = inquiry.getIDPELANGGAN1() + " " + inquiry.getIDPELANGGAN();
                        }
                        transaksi.setADMIN(inquiry.getADMIN())
                                .setMSSIDN_NAME(inquiry.getNAMAPELANGGAN())
                                .setHOST_REF_NUMBER(inquiry.getREF2())
                                .setBILL_REF_NUMBER(inquiry.getREFF())
                                .setMSSIDN(idpel)
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setPRODUCT(product.getProduct_id())
                                .setTAGIHAN((inquiry.getTAGIHAN()))
                                .setDENOM(product.getDENOM())
                                .setFEE_CA((productFee.getFEE_CA() * jml))
                                .setFEE_DEALER(((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml))
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setINQUIRY(respone)
                                .setCHARGE((inquiry.getTAGIHAN() + inquiry.getADMIN() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setDEBET((inquiry.getTAGIHAN() + inquiry.getADMIN() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setDENDA(0)
                                .setST(Status.INQUIRY);
                        transaksiService.update(transaksi);
                        InquiryTelkom inquiryTelkom = new InquiryTelkom()
                                .setAdmin((int) inquiry.getADMIN())
                                .setJumlahBill(jml)
                                .setNamapelanggan(inquiry.getNAMAPELANGGAN())
                                .setPeriode(inquiry.getPERIODE())
                                .setTagihan((int) inquiry.getTAGIHAN())
                                .setTotalTagihan((int) (inquiry.getTAGIHAN() + inquiry.getADMIN()))
                                .setIdpelanggan(idpel);
                        BaseResponse<InquiryTelkom> inquiryResponse = new BaseResponse<>();
                        inquiryResponse.setData(inquiryTelkom);
                        inquiryResponse.setFee(transaksi.getFEE_DEALER()).setNtrans(transaksi.getNTRANS());
                        inquiryResponse.setSaldo(partnerDeposit.getBALANCE());
                        inquiryResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        inquiryResponse.setProduct(transaksi.getDENOM());
                        inquiryResponse.setMessage("Successful");
                        toResponse.resume(Response.status(200).entity(inquiryResponse).build());
                        break;
                    default:
                        transaksiService.deleteById(transaksi.getNTRANS());
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", inquiry.getKETERANGAN()).setMssidn(params.get(Constanta.MSSIDN)).setSaldo(partnerDeposit.getBALANCE()).setProduct(product.getDENOM())).build());
                        break;
                }
            } catch (JAXBException e) {
                partnerDepositService.reverseSaldo(transaksi, respone);
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
        }
    }

    public void PaymentTelkom(@Suspended AsyncResponse toResponse, Transaksi transaksi, Map<String, String> params) {
        httpPost = new HttpPost(host);
        String respone = null;
        PaymentTelkom payment;
        try {
            StringEntity entity = new StringEntity(rajabilerRequest.paymentTelokm(transaksi));
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                payment = new PaymentTelkom(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(transaksi.getPARTNERID());
                switch (payment.getSTATUS()) {
                    case "00":
                        int jml = 1;
                        if (payment.getPERIODE().contains(",")) {
                            String[] jum = payment.getPERIODE().split(",");
                            jml = jum.length;
                        }
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setBILL_REF_NUMBER(payment.getREF1())
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        String idpel = (payment.getIDPELANGGAN().equals(payment.getIDPELANGGAN1())) ? (payment.getIDPELANGGAN1() + " " + payment.getIDPELANGGAN()) : payment.getIDPELANGGAN();

                        PaymentTelkomNew paymentTelkom = new PaymentTelkomNew()
                                .setAdmin((int) payment.getADMIN())
                                .setJumlahBill(jml)
                                .setNamapelanggan(payment.getNAMAPELANGGAN())
                                .setPeriode(payment.getPERIODE())
                                .setReff(payment.getREF1())
                                .setTagihan((int) payment.getTAGIHAN())
                                .setTotalTagihan((int) (payment.getTAGIHAN() + payment.getADMIN()))
                                .setIdpelanggan(idpel);
                        BaseResponse<PaymentTelkomNew> paymentResponse = new BaseResponse<>();
                        paymentResponse.setData(paymentTelkom);
                        paymentResponse.setFee(transaksi.getFEE_DEALER());
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        paymentResponse.setMessage("Successful");
                        paymentResponse.setProduct(transaksi.getDENOM());
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverseSaldo(transaksi, respone);
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", payment.getKETERANGAN()).setSaldo(partnerDeposit.getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                        break;
                }
            } catch (JAXBException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                partnerDepositService.reverseSaldo(transaksi, respone);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            partnerDepositService.reverseSaldo(transaksi, respone);
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
        }
    }

    public void InquiryPulsaPasca(@Suspended AsyncResponse toResponse, final ProductItem product, final Map<String, String> params, final PartnerCredential partnerCredential) {
        httpPost = new HttpPost(host);
        final ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), product.getDENOM());
        try {
            Transaksi transaksi = new Transaksi()
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setUSERID(partnerCredential.getPartner_uid());
            transaksiService.save(transaksi);
            StringEntity entity;
            String payload = rajabilerRequest.inquiryPulsaPasca(product.getDENOM_BILLER(), params.get(Constanta.MSSIDN), params.get(Constanta.MSSIDN), params.get(Constanta.MSSIDN), transaksi.getNTRANS());
            entity = new StringEntity(payload);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                InquiryPhonePasca inquiry = new InquiryPhonePasca(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(partnerCredential.getPartner_id());
                switch (inquiry.getSTATUS()) {
                    case "00":
                        if (inquiry.getTagihan() == 0) {
                            toResponse.resume(Response.status(200).entity(new ResponseCode("0088", "TAGIHAN SUDAH TERBAYAR").setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
                            return;
                        }
                        int jml = 1;
                        if (inquiry.getPeriode().contains(",")) {
                            String[] jum = inquiry.getPeriode().split(",");
                            jml = jum.length;
                        }
                        transaksi.setADMIN(inquiry.getAdmin())
                                .setMSSIDN_NAME(inquiry.getNamapelanggan())
                                .setHOST_REF_NUMBER("-")
                                .setBILL_REF_NUMBER("-")
                                .setMSSIDN(inquiry.getIdpelanggan())
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setPRODUCT(product.getProduct_id())
                                .setTAGIHAN(inquiry.getTagihan())
                                .setDENOM(product.getDENOM())
                                .setFEE_CA((productFee.getFEE_CA() * jml))
                                .setFEE_DEALER(((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml))
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setINQUIRY(respone)
                                .setCHARGE((inquiry.getTagihan() + inquiry.getAdmin() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setDEBET((inquiry.getTagihan() + inquiry.getAdmin() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setDENDA(0)
                                .setST(Status.INQUIRY);
                        transaksiService.update(transaksi);
                        BaseResponse<InquiryPhonePasca> inquiryResponse = new BaseResponse<>();
                        inquiryResponse.setData(inquiry);
                        inquiryResponse.setFee(transaksi.getFEE_DEALER()).setNtrans(transaksi.getNTRANS());
                        inquiryResponse.setSaldo(partnerDeposit.getBALANCE());
                        inquiryResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        inquiryResponse.setProduct(transaksi.getDENOM());
                        inquiryResponse.setMessage("Successful");
                        toResponse.resume(Response.status(200).entity(inquiryResponse).build());
                        break;
                    default:
                        transaksiService.deleteById(transaksi.getNTRANS());
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", inquiry.getKeterangan()).setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
                        break;
                }
            } catch (JAXBException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                partnerDepositService.reverseSaldo(transaksi, respone);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            logger.error("IO Exception Inquiry Pdam Exception {}", e.getLocalizedMessage());
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
        }
    }

    public void PaymentPulsaPasca(@Suspended AsyncResponse toResponse, Transaksi transaksi, Map<String, String> params) {
        httpPost = new HttpPost(host);
        String respone = null;
        try {
            String payload = rajabilerRequest.paymentPulsaPasca(transaksi);
            StringEntity entity = new StringEntity(payload);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                PaymentPhonePasca payment = new PaymentPhonePasca(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(transaksi.getPARTNERID());
                switch (payment.getStatus()) {
                    case "00":
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        BaseResponse<PaymentPhonePasca> paymentResponse = new BaseResponse<>();
                        paymentResponse.setData(payment);
                        paymentResponse.setFee(transaksi.getFEE_DEALER());
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        paymentResponse.setMessage("Successful");
                        paymentResponse.setProduct(transaksi.getDENOM());
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverseSaldo(transaksi, respone);
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", payment.getKeterangan()).setSaldo(partnerDeposit.getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                        break;
                }
            } catch (JAXBException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                partnerDepositService.reverseSaldo(transaksi, respone);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            partnerDepositService.reverseSaldo(transaksi, respone);
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
        }
    }

    public void InquiryBpjs(@Suspended AsyncResponse toResponse, final ProductItem product, final Map<String, String> params, final PartnerCredential partnerCredential) {
        httpPost = new HttpPost(host);
        final ProductFee productFee = productFeeService.findFeePartner(partnerCredential.getPartner_id(), product.getDENOM());
        try {
            Transaksi transaksi = new Transaksi()
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setPARTNERID(partnerCredential.getPartner_id());
            transaksiService.save(transaksi);
            StringEntity entity;
            String payload = rajabilerRequest.inquiryBpjs(product.getDENOM_BILLER(), params.get(Constanta.MSSIDN), transaksi.getNTRANS());
            entity = new StringEntity(payload);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            String respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                InquiryBpjs inquiry = new InquiryBpjs(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(partnerCredential.getPartner_id());
                switch (inquiry.getSTATUS()) {
                    case "00":
                        if (inquiry.getTagihan() == 0) {
                            toResponse.resume(Response.status(200).entity(new ResponseCode("0088", "TAGIHAN SUDAH TERBAYAR").setSaldo(partnerDeposit.getBALANCE()).setProduct(product.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
                            return;
                        }
                        int jml = 1;
                        if (inquiry.getPeriode().contains(",")) {
                            String[] jum = inquiry.getPeriode().split(",");
                            jml = jum.length;
                        }
                        transaksi.setADMIN(inquiry.getAdmin())
                                .setMSSIDN_NAME(inquiry.getNamapelanggan())
                                .setHOST_REF_NUMBER("-")
                                .setBILL_REF_NUMBER("-")
                                .setMSSIDN(inquiry.getIdpelanggan())
                                .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                                .setMERCHANT_ID(params.get(Constanta.MACHINE_TYPE))
                                .setPRODUCT(product.getProduct_id())
                                .setTAGIHAN((inquiry.getTagihan()))
                                .setDENOM(product.getDENOM())
                                .setFEE_CA((productFee.getFEE_CA() * jml))
                                .setFEE_DEALER(((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml))
                                .setUSERID(partnerCredential.getPartner_uid())
                                .setINQUIRY(respone)
                                .setCHARGE((inquiry.getTagihan() + inquiry.getAdmin() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setDEBET((inquiry.getTagihan() + inquiry.getAdmin() - ((product.getFEE_BILLER() - productFee.getFEE_CA()) * jml)))
                                .setIP_ADDRESS(params.get(Constanta.IP_ADDRESS))
                                .setPARTNERID(partnerCredential.getPartner_id())
                                .setDENDA(0)
                                .setST(Status.INQUIRY);
                        transaksiService.update(transaksi);
                        BaseResponse<InquiryBpjs> inquiryResponse = new BaseResponse<>();
                        inquiryResponse.setData(inquiry);
                        inquiryResponse.setFee(transaksi.getFEE_DEALER()).setNtrans(transaksi.getNTRANS());
                        inquiryResponse.setSaldo(partnerDeposit.getBALANCE());
                        inquiryResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        inquiryResponse.setProduct(transaksi.getDENOM());
                        inquiryResponse.setMessage("Successful");
                        toResponse.resume(Response.status(200).entity(inquiryResponse).build());
                        break;
                    default:
                        transaksiService.deleteById(transaksi.getNTRANS());
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", inquiry.getKeterangan()).setMssidn(params.get(Constanta.MSSIDN)).setSaldo(partnerDeposit.getBALANCE()).setProduct(product.getDENOM())).build());
                        break;
                }
            } catch (JAXBException e) {
                partnerDepositService.reverseSaldo(transaksi, respone);
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(product.getDENOM())).build());
        }
    }

    public void PaymentBpjs(@Suspended AsyncResponse toResponse, Transaksi transaksi, Map<String, String> params, Request req) {
        httpPost = new HttpPost(host);
        String respone = null;
        try {
            String payload = rajabilerRequest.paymentBpjs(transaksi, req);
            StringEntity entity = new StringEntity(payload);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/xml");
            httpPost.setHeader("Content-type", "application/xml");
            CloseableHttpResponse response = client.execute(httpPost);
            respone = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
            MethodResponse methodResponse;
            StringReader stringReader = new StringReader(respone);
            try {
                methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
                PaymentBpjs payment = new PaymentBpjs(methodResponse);
                PartnerDeposit partnerDeposit = partnerDepositService.findById(transaksi.getPARTNERID());
                switch (payment.getStatus()) {
                    case "00":
                        transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setPAYMENT(respone)
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
                        transaksiService.update(transaksi);
                        BaseResponse<PaymentBpjs> paymentResponse = new BaseResponse<>();
                        paymentResponse.setData(payment);
                        paymentResponse.setFee(transaksi.getFEE_DEALER());
                        paymentResponse.setNtrans(transaksi.getNTRANS());
                        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
                        paymentResponse.setSaldo(partnerDeposit.getBALANCE());
                        paymentResponse.setProduct(transaksi.getDENOM());
                        paymentResponse.setMessage("Successful");
                        paymentResponse.setProduct(transaksi.getDENOM());
                        toResponse.resume(Response.status(200).entity(paymentResponse).build());
                        break;
                    default:
                        partnerDepositService.reverseSaldo(transaksi, respone);
                        toResponse.resume(Response.status(200).entity(new ResponseCode("0005", payment.getKeterangan()).setSaldo(partnerDeposit.getBALANCE()).setMssidn(params.get(Constanta.MSSIDN)).setProduct(transaksi.getDENOM()).setNtrans(transaksi.getNTRANS())).build());
                        break;
                }
            } catch (JAXBException e) {
                slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
                partnerDepositService.reverseSaldo(transaksi, respone);
                toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            partnerDepositService.reverseSaldo(transaksi, respone);
            toResponse.resume(Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE()).setProduct(transaksi.getDENOM()).setMssidn(params.get(Constanta.MSSIDN))).build());
        }
    }
}
