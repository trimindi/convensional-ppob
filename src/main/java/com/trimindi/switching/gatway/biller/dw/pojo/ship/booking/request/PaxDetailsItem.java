package com.trimindi.switching.gatway.biller.dw.pojo.ship.booking.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PaxDetailsItem {

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("paxGender")
    private String paxGender;

    @JsonProperty("paxType")
    private String paxType;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("ID")
    private String iD;

    @JsonProperty("birthDate")
    private String birthDate;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPaxGender() {
        return paxGender;
    }

    public void setPaxGender(String paxGender) {
        this.paxGender = paxGender;
    }

    public String getPaxType() {
        return paxType;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return
                "PaxDetailsItem{" +
                        "firstName = '" + firstName + '\'' +
                        ",lastName = '" + lastName + '\'' +
                        ",paxGender = '" + paxGender + '\'' +
                        ",paxType = '" + paxType + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",iD = '" + iD + '\'' +
                        ",birthDate = '" + birthDate + '\'' +
                        "}";
    }
}