package com.trimindi.switching.gatway.biller.dw.pojo.airline.booking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SchDepartsItem {

    @JsonProperty("detailSchedule")
    private String detailSchedule;

    @JsonProperty("schDepartTime")
    private String schDepartTime;

    @JsonProperty("airlineCode")
    private String airlineCode;

    @JsonProperty("flightClass")
    private String flightClass;

    @JsonProperty("garudaAvailability")
    private String garudaAvailability;

    @JsonProperty("schOrigin")
    private String schOrigin;

    @JsonProperty("schDestination")
    private String schDestination;

    @JsonProperty("flightNumber")
    private String flightNumber;

    @JsonProperty("schArrivalTime")
    private String schArrivalTime;

    @JsonProperty("garudaNumber")
    private String garudaNumber;

    public String getDetailSchedule() {
        return detailSchedule;
    }

    public void setDetailSchedule(String detailSchedule) {
        this.detailSchedule = detailSchedule;
    }

    public String getSchDepartTime() {
        return schDepartTime;
    }

    public void setSchDepartTime(String schDepartTime) {
        this.schDepartTime = schDepartTime;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public String getGarudaAvailability() {
        return garudaAvailability;
    }

    public void setGarudaAvailability(String garudaAvailability) {
        this.garudaAvailability = garudaAvailability;
    }

    public String getSchOrigin() {
        return schOrigin;
    }

    public void setSchOrigin(String schOrigin) {
        this.schOrigin = schOrigin;
    }

    public String getSchDestination() {
        return schDestination;
    }

    public void setSchDestination(String schDestination) {
        this.schDestination = schDestination;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getSchArrivalTime() {
        return schArrivalTime;
    }

    public void setSchArrivalTime(String schArrivalTime) {
        this.schArrivalTime = schArrivalTime;
    }

    public String getGarudaNumber() {
        return garudaNumber;
    }

    public void setGarudaNumber(String garudaNumber) {
        this.garudaNumber = garudaNumber;
    }

    @Override
    public String toString() {
        return
                "SchDepartsItem{" +
                        "detailSchedule = '" + detailSchedule + '\'' +
                        ",schDepartTime = '" + schDepartTime + '\'' +
                        ",airlineCode = '" + airlineCode + '\'' +
                        ",flightClass = '" + flightClass + '\'' +
                        ",garudaAvailability = '" + garudaAvailability + '\'' +
                        ",schOrigin = '" + schOrigin + '\'' +
                        ",schDestination = '" + schDestination + '\'' +
                        ",flightNumber = '" + flightNumber + '\'' +
                        ",schArrivalTime = '" + schArrivalTime + '\'' +
                        ",garudaNumber = '" + garudaNumber + '\'' +
                        "}";
    }
}