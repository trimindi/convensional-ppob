package com.trimindi.switching.gatway.biller.dw.pojo.airline.seat;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class InfosItem {

    @JsonProperty("seatType")
    private String seatType;

    @JsonProperty("isOpen")
    private boolean isOpen;

    @JsonProperty("seatDesignator")
    private String seatDesignator;

    @JsonProperty("compartment")
    private String compartment;

    @JsonProperty("X")
    private int X;

    @JsonProperty("width")
    private int width;

    @JsonProperty("Y")
    private int Y;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("seatPrice")
    private double seatPrice;

    @JsonProperty("seatText")
    private String seatText;

    @JsonProperty("assignable")
    private boolean assignable;

    @JsonProperty("height")
    private int height;

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public boolean isIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public String getSeatDesignator() {
        return seatDesignator;
    }

    public void setSeatDesignator(String seatDesignator) {
        this.seatDesignator = seatDesignator;
    }

    public String getCompartment() {
        return compartment;
    }

    public void setCompartment(String compartment) {
        this.compartment = compartment;
    }

    public int getX() {
        return X;
    }

    public void setX(int X) {
        this.X = X;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getY() {
        return Y;
    }

    public void setY(int Y) {
        this.Y = Y;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getSeatPrice() {
        return seatPrice;
    }

    public void setSeatPrice(double seatPrice) {
        this.seatPrice = seatPrice;
    }

    public String getSeatText() {
        return seatText;
    }

    public void setSeatText(String seatText) {
        this.seatText = seatText;
    }

    public boolean isAssignable() {
        return assignable;
    }

    public void setAssignable(boolean assignable) {
        this.assignable = assignable;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return
                "InfosItem{" +
                        "seatType = '" + seatType + '\'' +
                        ",isOpen = '" + isOpen + '\'' +
                        ",seatDesignator = '" + seatDesignator + '\'' +
                        ",compartment = '" + compartment + '\'' +
                        ",X = '" + X + '\'' +
                        ",width = '" + width + '\'' +
                        ",Y = '" + Y + '\'' +
                        ",currency = '" + currency + '\'' +
                        ",seatPrice = '" + seatPrice + '\'' +
                        ",seatText = '" + seatText + '\'' +
                        ",assignable = '" + assignable + '\'' +
                        ",height = '" + height + '\'' +
                        "}";
    }
}