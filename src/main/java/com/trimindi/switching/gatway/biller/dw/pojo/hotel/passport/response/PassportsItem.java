package com.trimindi.switching.gatway.biller.dw.pojo.hotel.passport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PassportsItem {

    @JsonProperty("ID")
    private String iD;

    @JsonProperty("Name")
    private String name;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return
                "PassportsItem{" +
                        "iD = '" + iD + '\'' +
                        ",name = '" + name + '\'' +
                        "}";
    }
}