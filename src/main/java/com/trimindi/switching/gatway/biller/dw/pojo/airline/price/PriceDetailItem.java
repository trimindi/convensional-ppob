package com.trimindi.switching.gatway.biller.dw.pojo.airline.price;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PriceDetailItem {

    @JsonProperty("bagInfo")
    private Object bagInfo;

    @JsonProperty("priceInfo")
    private Object priceInfo;

    @JsonProperty("paxType")
    private String paxType;

    @JsonProperty("baseFare")
    private double baseFare;

    @JsonProperty("totalFare")
    private double totalFare;

    @JsonProperty("tax")
    private double tax;

    public Object getBagInfo() {
        return bagInfo;
    }

    public void setBagInfo(Object bagInfo) {
        this.bagInfo = bagInfo;
    }

    public Object getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(Object priceInfo) {
        this.priceInfo = priceInfo;
    }

    public String getPaxType() {
        return paxType;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    public double getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(double baseFare) {
        this.baseFare = baseFare;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    @Override
    public String toString() {
        return
                "PriceDetailItem{" +
                        "bagInfo = '" + bagInfo + '\'' +
                        ",priceInfo = '" + priceInfo + '\'' +
                        ",paxType = '" + paxType + '\'' +
                        ",baseFare = '" + baseFare + '\'' +
                        ",totalFare = '" + totalFare + '\'' +
                        ",tax = '" + tax + '\'' +
                        "}";
    }
}