package com.trimindi.switching.gatway.biller.dw.pojo.train.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class BookingDetailResponse {

    @JsonProperty("trainID")
    private String trainID;

    @JsonProperty("issuedDate")
    private String issuedDate;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("salesPrice")
    private double salesPrice;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("markupFee")
    private double markupFee;

    @JsonProperty("trainClass")
    private String trainClass;

    @JsonProperty("adminFee")
    private double adminFee;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("destinationFull")
    private String destinationFull;

    @JsonProperty("arrivalTime")
    private String arrivalTime;

    @JsonProperty("trainName")
    private String trainName;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("passengers")
    private List<PassengersItem> passengers;

    @JsonProperty("departTime")
    private String departTime;

    @JsonProperty("ticketPrice")
    private double ticketPrice;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("contactName")
    private String contactName;

    @JsonProperty("issuedTimeLimit")
    private String issuedTimeLimit;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("ticketStatus")
    private String ticketStatus;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("trainNumber")
    private String trainNumber;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("contactPhone")
    private String contactPhone;

    @JsonProperty("originFull")
    private String originFull;

    @JsonProperty("status")
    private String status;

    public String getTrainID() {
        return trainID;
    }

    public void setTrainID(String trainID) {
        this.trainID = trainID;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getMarkupFee() {
        return markupFee;
    }

    public void setMarkupFee(double markupFee) {
        this.markupFee = markupFee;
    }

    public String getTrainClass() {
        return trainClass;
    }

    public void setTrainClass(String trainClass) {
        this.trainClass = trainClass;
    }

    public double getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(double adminFee) {
        this.adminFee = adminFee;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDestinationFull() {
        return destinationFull;
    }

    public void setDestinationFull(String destinationFull) {
        this.destinationFull = destinationFull;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public List<PassengersItem> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<PassengersItem> passengers) {
        this.passengers = passengers;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getIssuedTimeLimit() {
        return issuedTimeLimit;
    }

    public void setIssuedTimeLimit(String issuedTimeLimit) {
        this.issuedTimeLimit = issuedTimeLimit;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getOriginFull() {
        return originFull;
    }

    public void setOriginFull(String originFull) {
        this.originFull = originFull;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "BookingDetailResponse{" +
                        "trainID = '" + trainID + '\'' +
                        ",issuedDate = '" + issuedDate + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",salesPrice = '" + salesPrice + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",markupFee = '" + markupFee + '\'' +
                        ",trainClass = '" + trainClass + '\'' +
                        ",adminFee = '" + adminFee + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",destinationFull = '" + destinationFull + '\'' +
                        ",arrivalTime = '" + arrivalTime + '\'' +
                        ",trainName = '" + trainName + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",passengers = '" + passengers + '\'' +
                        ",departTime = '" + departTime + '\'' +
                        ",ticketPrice = '" + ticketPrice + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",contactName = '" + contactName + '\'' +
                        ",issuedTimeLimit = '" + issuedTimeLimit + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",ticketStatus = '" + ticketStatus + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",trainNumber = '" + trainNumber + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",contactPhone = '" + contactPhone + '\'' +
                        ",originFull = '" + originFull + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}