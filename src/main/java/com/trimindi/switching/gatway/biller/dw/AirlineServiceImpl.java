package com.trimindi.switching.gatway.biller.dw;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindi.switching.gatway.DwSession;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage.GetAirlineBagageAndMealRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.bagage.GetAirlineBagageResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.booking.SetAirlineBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.booking.SetAirlineBookingResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.bookingdetail.BookingDetailResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.daftar.AirlinesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.daftar.GetAirlineListRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.daftar.GetAirlineListResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.issued.SetAirlineIssuedRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.issued.SetArilineIssuedResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.natinal.CountriesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.natinal.GetAirlineNationalityListResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.price.GetAirlinePriceRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.price.GetAirlinePriceResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.route.GetAirlineRouteRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.route.GetAirlineRouteResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.route.RoutesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule.GetAirlineScheduleRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.schedule.GetAirlineScheduleResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.seat.GetAirlineSeatRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.airline.seat.GetAirlineSeatResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.train.detail.BookingDetailRequest;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by sx on 18/12/17.
 * Copyright under comercial unit
 */
@Service
public class AirlineServiceImpl implements AirlineService {
    public static final Logger logger = LoggerFactory.getLogger(KaiServiceImpl.class);
    public static final String AIRLINE = "AIRLINE";
    private final
    ObjectMapper objectMapper;
    private final
    CloseableHttpClient client;
    @Autowired
    private
    DwSession session;
    @Value("${dw.username}")
    private String username;
    @Value("${dw.password}")
    private String password;
    private final TransaksiService transaksiService;
    @Value("${dw.url.airline.list}")
    private String airlineListPath;
    @Value("${dw.url.airline.national}")
    private String airlinenational;
    @Value("${dw.url.airline.bagage}")
    private String airlinebagage;
    @Value("${dw.url.airline.route}")
    private String airlineRoute;
    @Value("${dw.url.airline.schedule}")
    private String airlineListschedule;
    @Value("${dw.url.airline.seatmap}")
    private String airlineseatmap;
    @Value("${dw.url.airline.price}")
    private String airlineprice;
    @Value("${dw.url.airline.booking}")
    private String airlinebooking;
    @Value("${dw.url.airline.issued}")
    private String airlineissued;
    @Autowired
    private ProductFeeService productFeeService;
    @Autowired
    private PartnerDepositService partnerDepositService;
    @Autowired
    private SlackSendMessage slackSendMessage;
    @Value("${dw.url.airline.bookingdetail}")
    private String airlinebookingdetail;

    @Autowired
    public AirlineServiceImpl(ObjectMapper objectMapper, CloseableHttpClient client, TransaksiService transaksiService) {
        this.objectMapper = objectMapper;
        this.client = client;
        this.transaksiService = transaksiService;
    }

    @Override
    public BaseResponse<List<AirlinesItem>> getAirlineList() {
        try {
            GetAirlineListRequest request = new GetAirlineListRequest();
            request.setAccessToken(session.authKey().getAccessToken());
            request.setUserID(session.authKey().getUserID());
            GetAirlineListResponse response = objectMapper.readValue(this.process(airlineListPath, objectMapper.writeValueAsString(request)), GetAirlineListResponse.class);
            BaseResponse<List<AirlinesItem>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setData(response.getAirlines());
            } else {
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<List<AirlinesItem>> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }

    @Override
    public BaseResponse<List<CountriesItem>> getAirlineCountry() {
        try {
            GetAirlineListRequest request = new GetAirlineListRequest();
            request.setAccessToken(session.authKey().getAccessToken());
            request.setUserID(session.authKey().getUserID());
            GetAirlineNationalityListResponse response = objectMapper.readValue(this.process(airlinenational, objectMapper.writeValueAsString(request)), GetAirlineNationalityListResponse.class);
            BaseResponse<List<CountriesItem>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setData(response.getCountries());
            } else {
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<List<CountriesItem>> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }

    @Override
    public BaseResponse<List<RoutesItem>> getAirlineRoutes(GetAirlineRouteRequest request) {
        try {
            request.setAccessToken(session.authKey().getAccessToken());
            request.setUserID(session.authKey().getUserID());
            GetAirlineRouteResponse response = objectMapper.readValue(this.process(airlineRoute, objectMapper.writeValueAsString(request)), GetAirlineRouteResponse.class);
            BaseResponse<List<RoutesItem>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setData(response.getRoutes());
            } else {
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<List<RoutesItem>> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }

    @Override
    public Response getAirlineSchedule(GetAirlineScheduleRequest getAirlineScheduleRequest) {
        try {
            getAirlineScheduleRequest.setUserID(session.authKey().getUserID());
            getAirlineScheduleRequest.setAccessToken(session.authKey().getAccessToken());
            GetAirlineScheduleResponse response = objectMapper.readValue(this.process(airlineListschedule, objectMapper.writeValueAsString(getAirlineScheduleRequest)), GetAirlineScheduleResponse.class);
            BaseResponse<GetAirlineScheduleResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setData(response);
                return Response.status(200).entity(base).build();
            } else {
                if (response.getRespMessage().equalsIgnoreCase("airline access code is empty or not valid")) {
                    BaseResponse<String> err = new BaseResponse<>();
                    err.setData(response.getAirlineAccessCode());
                    err.setStatus("9966");
                    err.setProduct(AIRLINE);
                    err.setMessage(response.getRespMessage());
                    return Response.status(200).entity(err).build();
                } else {
                    base.setStatus("0005");
                    base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
                    base.setProduct(AIRLINE);
                    return Response.status(200).entity(base).build();
                }
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetAirlineScheduleResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public BaseResponse<GetAirlineBagageResponse> getAirlineBagage(GetAirlineBagageAndMealRequest getAirlineBagageAndMealRequest) {
        try {
            getAirlineBagageAndMealRequest.setUserID(session.authKey().getUserID());
            getAirlineBagageAndMealRequest.setAccessToken(session.authKey().getAccessToken());
            GetAirlineBagageResponse response = objectMapper.readValue(this.process(airlinebagage, objectMapper.writeValueAsString(getAirlineBagageAndMealRequest)), GetAirlineBagageResponse.class);
            BaseResponse<GetAirlineBagageResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetAirlineBagageResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }

    @Override
    public BaseResponse<GetAirlineSeatResponse> getAirlineSeatMap(GetAirlineSeatRequest getAirlineSeatRequest) {
        try {
            getAirlineSeatRequest.setUserID(session.authKey().getUserID());
            getAirlineSeatRequest.setAccessToken(session.authKey().getAccessToken());
            GetAirlineSeatResponse response = objectMapper.readValue(this.process(airlineseatmap, objectMapper.writeValueAsString(getAirlineSeatRequest)), GetAirlineSeatResponse.class);
            BaseResponse<GetAirlineSeatResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);

            BaseResponse<GetAirlineSeatResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }

    @Override
    public BaseResponse<GetAirlinePriceResponse> getAilinePrice(GetAirlinePriceRequest getAirlinePriceRequest) {
        try {
            getAirlinePriceRequest.setUserID(session.authKey().getUserID());
            getAirlinePriceRequest.setAccessToken(session.authKey().getAccessToken());
            GetAirlinePriceResponse response = objectMapper.readValue(this.process(airlineprice, objectMapper.writeValueAsString(getAirlinePriceRequest)), GetAirlinePriceResponse.class);
            BaseResponse<GetAirlinePriceResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setMessage(response.getRespMessage());
            }
            return base;
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetAirlinePriceResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }


    @Override
    public BaseResponse<SetAirlineBookingResponse> setBookingAirline(SetAirlineBookingRequest setAirlineBookingRequest, PartnerCredential partnerCredential) {
        try {
            setAirlineBookingRequest.setUserID(session.authKey().getUserID());
            setAirlineBookingRequest.setAccessToken(session.authKey().getAccessToken());
            String payload = this.process(airlinebooking, objectMapper.writeValueAsString(setAirlineBookingRequest));
            SetAirlineBookingResponse response = objectMapper.readValue(payload, SetAirlineBookingResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                ProductFee airline = productFeeService.findFeePartner(partnerCredential.getPartner_id(), AIRLINE);
                Transaksi transaksi = new Transaksi();
                transaksi.setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                        .setDATE(new Timestamp(System.currentTimeMillis()))
                        .setUSERID(partnerCredential.getPartner_uid())
                        .setPARTNERID(partnerCredential.getPartner_id())
                        .setBILL_REF_NUMBER(response.getBookingCode())
                        .setDATA(response.getBookingDate())
                        .setST(Status.INQUIRY)
                        .setTAGIHAN(response.getTicketPrice())
                        .setDEBET(response.getSalesPrice() + airline.getFEE_CA())
                        .setCHARGE(response.getSalesPrice() + airline.getFEE_CA())
                        .setPRODUCT(AIRLINE)
                        .setMSSIDN(response.getBookingCode())
                        .setMSSIDN_NAME(response.getBookingCode())
                        .setADMIN(response.getAirlineAdminFee())
                        .setFEE_DEALER(airline.getFEE_CA())
                        .setFEE_DEALER(response.getAirlineAdminFee() - airline.getFEE_CA())
                        .setINQUIRY(payload);
                transaksiService.save(transaksi);
                response.setSalesPrice(transaksi.getCHARGE());
                BaseResponse<SetAirlineBookingResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setNtrans(transaksi.getNTRANS());
                base.setProduct(AIRLINE);
                base.setSaldoTerpotong(transaksi.getCHARGE());
                base.setFee(transaksi.getFEE_DEALER());
                base.setMessage(response.getRespMessage());
                return base;
            } else {
                BaseResponse<SetAirlineBookingResponse> base = new BaseResponse<>();
                base.setStatus("0005");
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                return base;
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<SetAirlineBookingResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }

    @Override
    public BaseResponse<SetArilineIssuedResponse> setIssuedAirline(SetAirlineIssuedRequest setIssuedAirlineRequest, Transaksi transaksi) {
        try {
            setIssuedAirlineRequest.setUserID(session.authKey().getUserID());
            setIssuedAirlineRequest.setAccessToken(session.authKey().getAccessToken());
            SetArilineIssuedResponse response = objectMapper.readValue(this.process(airlineissued, objectMapper.writeValueAsString(setIssuedAirlineRequest)), SetArilineIssuedResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                BaseResponse<SetArilineIssuedResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setMessage(response.getRespMessage());
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setNtrans(transaksi.getNTRANS());
                return base;
            } else {
                partnerDepositService.reverseSaldo(transaksi, objectMapper.writeValueAsString(response));
                BaseResponse<SetArilineIssuedResponse> base = new BaseResponse<>();
                base.setMessage(response.getRespMessage());
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setNtrans(transaksi.getNTRANS());
                return base;
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<SetArilineIssuedResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            return base;
        }
    }

    @Override
    public BaseResponse<BookingDetailResponse> getBookingDetail(BookingDetailRequest bookingDetailRequest, Transaksi transaksi) {
        try {
            bookingDetailRequest.setUserID(session.authKey().getUserID());
            bookingDetailRequest.setAccessToken(session.authKey().getAccessToken());
            BookingDetailResponse response = objectMapper.readValue(this.process(airlinebookingdetail, objectMapper.writeValueAsString(bookingDetailRequest)), BookingDetailResponse.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                BaseResponse<BookingDetailResponse> base = new BaseResponse<>();
                base.setData(response);
                base.setMessage(response.getRespMessage());
                base.setProduct(AIRLINE);
                base.setNtrans(transaksi.getNTRANS());
                return base;
            } else {
                partnerDepositService.reverseSaldo(transaksi, objectMapper.writeValueAsString(response));
                BaseResponse<BookingDetailResponse> base = new BaseResponse<>();
                base.setMessage(response.getRespMessage());
                base.setStatus("0005");
                base.setProduct(AIRLINE);
                base.setNtrans(transaksi.getNTRANS());
                return base;
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<BookingDetailResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(AIRLINE);
            base.setNtrans(transaksi.getNTRANS());
            return base;
        }
    }

    private String process(String url, String payload) throws IOException {
        logger.error("send req to       -> {}", url);
        logger.error("send req payload  -> {}", payload);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setEntity(new StringEntity(payload));
        CloseableHttpResponse execute = client.execute(httpPost);
        String respone = EntityUtils.toString(execute.getEntity(), StandardCharsets.UTF_8.name());
        logger.error("Response req      -> {}", respone);
        return respone;
    }
}
