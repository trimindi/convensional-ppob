package com.trimindi.switching.gatway.biller.dw.pojo.ship.avability.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GetShipAvabilityResponse {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("availabilityInfo")
    private String availabilityInfo;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("status")
    private String status;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getAvailabilityInfo() {
        return availabilityInfo;
    }

    public void setAvailabilityInfo(String availabilityInfo) {
        this.availabilityInfo = availabilityInfo;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetShipAvabilityResponse{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",availabilityInfo = '" + availabilityInfo + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}