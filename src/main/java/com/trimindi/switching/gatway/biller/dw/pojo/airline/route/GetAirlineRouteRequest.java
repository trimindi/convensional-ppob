package com.trimindi.switching.gatway.biller.dw.pojo.airline.route;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class GetAirlineRouteRequest {

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    @Override
    public String toString() {
        return
                "GetAirlineRouteRequest{" +
                        "accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        "}";
    }
}