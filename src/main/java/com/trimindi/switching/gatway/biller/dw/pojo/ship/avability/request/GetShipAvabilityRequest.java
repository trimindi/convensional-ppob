package com.trimindi.switching.gatway.biller.dw.pojo.ship.avability.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetShipAvabilityRequest {

    @JsonProperty("destinationPort")
    private String destinationPort;

    @JsonProperty("destinationCall")
    private int destinationCall;

    @JsonProperty("subClass")
    private String subClass;

    @JsonProperty("pax")
    private List<PaxItem> pax;

    @JsonProperty("originPort")
    private String originPort;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("shipNumber")
    private String shipNumber;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("originCall")
    private int originCall;

    @JsonProperty("userID")
    private String userID;

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public int getDestinationCall() {
        return destinationCall;
    }

    public void setDestinationCall(int destinationCall) {
        this.destinationCall = destinationCall;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public List<PaxItem> getPax() {
        return pax;
    }

    public void setPax(List<PaxItem> pax) {
        this.pax = pax;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(String shipNumber) {
        this.shipNumber = shipNumber;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getOriginCall() {
        return originCall;
    }

    public void setOriginCall(int originCall) {
        this.originCall = originCall;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return
                "GetShipAvabilityRequest{" +
                        "destinationPort = '" + destinationPort + '\'' +
                        ",destinationCall = '" + destinationCall + '\'' +
                        ",subClass = '" + subClass + '\'' +
                        ",pax = '" + pax + '\'' +
                        ",originPort = '" + originPort + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",shipNumber = '" + shipNumber + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",originCall = '" + originCall + '\'' +
                        ",userID = '" + userID + '\'' +
                        "}";
    }
}