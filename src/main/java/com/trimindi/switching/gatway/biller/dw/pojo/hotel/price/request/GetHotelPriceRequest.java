package com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelPriceRequest {

    @JsonProperty("checkOutDate")
    @NotNull
    private String checkOutDate;

    @JsonProperty("roomRequest")
    private List<RoomRequestItem> roomRequest;

    @JsonProperty("cityId")
    @NotNull
    private String cityId;

    @JsonProperty("hotelId")
    @NotNull

    private String hotelId;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("breakfast")
    @NotNull
    private String breakfast;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("checkInDate")
    @NotNull
    private String checkInDate;

    @JsonProperty("paxPassport")
    @NotNull
    private String paxPassport;

    @JsonProperty("countryId")
    @NotNull
    private String countryId;

    @JsonProperty("internalCode")
    @NotNull

    private String internalCode;

    @JsonProperty("roomId")
    @NotNull
    private String roomId;

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public List<RoomRequestItem> getRoomRequest() {
        return roomRequest;
    }

    public void setRoomRequest(List<RoomRequestItem> roomRequest) {
        this.roomRequest = roomRequest;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getPaxPassport() {
        return paxPassport;
    }

    public void setPaxPassport(String paxPassport) {
        this.paxPassport = paxPassport;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    @Override
    public String toString() {
        return
                "GetHotelPriceRequest{" +
                        "checkOutDate = '" + checkOutDate + '\'' +
                        ",roomRequest = '" + roomRequest + '\'' +
                        ",cityId = '" + cityId + '\'' +
                        ",hotelId = '" + hotelId + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",breakfast = '" + breakfast + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",checkInDate = '" + checkInDate + '\'' +
                        ",paxPassport = '" + paxPassport + '\'' +
                        ",countryId = '" + countryId + '\'' +
                        ",internalCode = '" + internalCode + '\'' +
                        ",roomId = '" + roomId + '\'' +
                        "}";
    }
}