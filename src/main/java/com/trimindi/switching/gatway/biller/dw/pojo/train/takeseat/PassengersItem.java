package com.trimindi.switching.gatway.biller.dw.pojo.train.takeseat;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PassengersItem {

    @JsonProperty("wagonCode")
    private String wagonCode;

    @JsonProperty("name")
    private String name;

    @JsonProperty("type")
    private String type;

    @JsonProperty("wagonNumber")
    private String wagonNumber;

    @JsonProperty("seatNumber")
    private String seatNumber;

    public String getWagonCode() {
        return wagonCode;
    }

    public void setWagonCode(String wagonCode) {
        this.wagonCode = wagonCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWagonNumber() {
        return wagonNumber;
    }

    public void setWagonNumber(String wagonNumber) {
        this.wagonNumber = wagonNumber;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public String toString() {
        return
                "PassengersItem{" +
                        "wagonCode = '" + wagonCode + '\'' +
                        ",name = '" + name + '\'' +
                        ",type = '" + type + '\'' +
                        ",wagonNumber = '" + wagonNumber + '\'' +
                        ",seatNumber = '" + seatNumber + '\'' +
                        "}";
    }
}