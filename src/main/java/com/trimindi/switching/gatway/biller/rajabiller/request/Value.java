package com.trimindi.switching.gatway.biller.rajabiller.request;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/16/2017.
 */
@XmlRootElement
public class Value {
    private String string;

    public Value(String string) {
        this.string = string;
    }

    public Value() {
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return "ClassPojo [string = " + string + "]";
    }
}
