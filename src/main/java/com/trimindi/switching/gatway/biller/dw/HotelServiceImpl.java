package com.trimindi.switching.gatway.biller.dw;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.trimindi.switching.gatway.DwSession;
import com.trimindi.switching.gatway.SlackSendMessage;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.booking.request.SetHotelBookingRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.booking.response.SetHotelBookingResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.request.GetHotelSearchRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.response.GetHotelSearchResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.cari.response.HotelsItem;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.city.request.GetHotelCityRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.city.response.CitiesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.city.response.GetHotelCityResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.country.response.CountriesItem;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.country.response.GetHotelCountryResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.detail.BookingDetail;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.detail.GetHotelBookingDetail;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.images.request.GetHotelImageRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.images.response.GetHotelImageResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.issued.SetIssuedHotelRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.issued.SetIssuedHotelResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.passport.response.GetHotelPassportResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.passport.response.PassportsItem;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.request.GetHotelPriceRequest;
import com.trimindi.switching.gatway.biller.dw.pojo.hotel.price.response.GetHotelPriceResponse;
import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.services.deposit.PartnerDepositService;
import com.trimindi.switching.gatway.services.fee.ProductFeeService;
import com.trimindi.switching.gatway.services.product.ProductItemService;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by sx on 26/01/18.
 * Copyright under comercial unit
 */
@Service
public class HotelServiceImpl implements HotelService {
    public static final Logger logger = LoggerFactory.getLogger(HotelServiceImpl.class);
    private static final String HOTEL = "HOTEL";
    final
    ObjectMapper objectMapper;
    final
    CloseableHttpClient client;
    final
    PartnerDepositService partnerDepositService;
    final
    ProductItemService productItemService;
    final
    ProductFeeService productFeeService;
    @Autowired
    private
    DwSession session;
    private final TransaksiService transaksiService;
    private final SlackSendMessage slackSendMessage;
    @Value("${dw.url.hotel.county}")
    private String hotelCountry;
    @Value("${dw.url.hotel.pasport}")
    private String hotelPasport;
    @Value("${dw.url.hotel.city}")
    private String hotelCity;
    @Value("${dw.url.hotel.search}")
    private String hotelSearch;
    @Value("${dw.url.hotel.image}")
    private String hotelImage;
    @Value("${dw.url.hotel.price}")
    private String hotelPrice;
    @Value("${dw.url.hotel.booking}")
    private String hotelBooking;
    @Value("${dw.url.hotel.bookingdetail}")
    private String hotelBookingDetail;
    @Value("${dw.url.hotel.issued}")
    private String hotelIssued;

    @Autowired
    public HotelServiceImpl(ObjectMapper objectMapper, CloseableHttpClient client, PartnerDepositService partnerDepositService, ProductItemService productItemService, ProductFeeService productFeeService, TransaksiService transaksiService, SlackSendMessage slackSendMessage) {
        this.objectMapper = objectMapper;
        this.client = client;
        this.partnerDepositService = partnerDepositService;
        this.productItemService = productItemService;
        this.productFeeService = productFeeService;
        this.transaksiService = transaksiService;
        this.slackSendMessage = slackSendMessage;
    }

    private String process(String url, String payload) throws IOException {
        logger.error("send req to       -> {}", url);
        logger.error("send req payload  -> {}", payload);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setEntity(new StringEntity(payload));
        CloseableHttpResponse execute = client.execute(httpPost);
        String respone = EntityUtils.toString(execute.getEntity(), StandardCharsets.UTF_8.name());
        logger.error("Response req  -> {}", respone);
        return respone;
    }

    @Override
    public Response GetHotelCountryList() {
        try {
            ObjectNode objectNode = objectMapper.createObjectNode();
            objectNode.put("userID", session.authKey().getUserID());
            objectNode.put("accessToken", session.authKey().getAccessToken());
            GetHotelCountryResponse response = objectMapper.readValue(this.process(hotelCountry, objectMapper.writeValueAsString(objectNode)), GetHotelCountryResponse.class);
            BaseResponse<List<CountriesItem>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response.getCountries());
                base.setProduct(HOTEL);
            } else {
                base.setStatus("0005");
                base.setProduct(HOTEL);
                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelCountryResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetHotelPassportList() {
        try {
            ObjectNode objectNode = objectMapper.createObjectNode();
            objectNode.put("userID", session.authKey().getUserID());
            objectNode.put("accessToken", session.authKey().getAccessToken());
            GetHotelPassportResponse response = objectMapper.readValue(this.process(hotelPasport, objectMapper.writeValueAsString(objectNode)), GetHotelPassportResponse.class);
            BaseResponse<List<PassportsItem>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response.getPassports());
                base.setProduct(HOTEL);

            } else {
                base.setStatus("0005");
                base.setProduct(HOTEL);
                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelCountryResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetHotelCityList(GetHotelCityRequest getHotelCityRequest) {
        try {
            getHotelCityRequest.setAccessToken(session.authKey().getAccessToken());
            getHotelCityRequest.setUserID(session.authKey().getUserID());
            GetHotelCityResponse response = objectMapper.readValue(this.process(hotelCity, objectMapper.writeValueAsString(getHotelCityRequest)), GetHotelCityResponse.class);
            BaseResponse<List<CitiesItem>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response.getCities());
                base.setProduct(HOTEL);
            } else {
                base.setStatus("0005");
                base.setProduct(HOTEL);
                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelCountryResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response SearchAvailableHotel(GetHotelSearchRequest getHotelSearchRequest) {
        try {
            getHotelSearchRequest.setAccessToken(session.authKey().getAccessToken());
            getHotelSearchRequest.setUserID(session.authKey().getUserID());
            GetHotelSearchResponse response = objectMapper.readValue(this.process(hotelSearch, objectMapper.writeValueAsString(getHotelSearchRequest)), GetHotelSearchResponse.class);
            BaseResponse<List<HotelsItem>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setData(response.getHotels());
                base.setProduct(HOTEL);

            } else {
                base.setStatus("0005");
                base.setProduct(HOTEL);

                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelCountryResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetHotelImageList(GetHotelImageRequest getHotelImageRequest) {
        try {
            getHotelImageRequest.setAccessToken(session.authKey().getAccessToken());
            getHotelImageRequest.setUserID(session.authKey().getUserID());
            GetHotelImageResponse response = objectMapper.readValue(this.process(hotelImage, objectMapper.writeValueAsString(getHotelImageRequest)), GetHotelImageResponse.class);
            BaseResponse<List<String>> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(HOTEL);

                base.setData(response.getImages());
            } else {
                base.setStatus("0005");
                base.setProduct(HOTEL);

                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelCountryResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response GetHotelPrice(GetHotelPriceRequest getHotelPriceRequest) {
        try {
            getHotelPriceRequest.setAccessToken(session.authKey().getAccessToken());
            getHotelPriceRequest.setUserID(session.authKey().getUserID());
            GetHotelPriceResponse response = objectMapper.readValue(this.process(hotelPrice, objectMapper.writeValueAsString(getHotelPriceRequest)), GetHotelPriceResponse.class);
            BaseResponse<GetHotelPriceResponse> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                base.setMessage(response.getRespMessage());
                base.setProduct(HOTEL);

                base.setData(response);
            } else {
                base.setStatus("0005");
                base.setProduct(HOTEL);

                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelCountryResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }

    private GetHotelPriceResponse checkPrice(SetHotelBookingRequest setHotelBookingRequest) throws IOException {
        GetHotelPriceRequest getHotelPriceRequest = new GetHotelPriceRequest();
        getHotelPriceRequest.setAccessToken(session.authKey().getAccessToken());
        getHotelPriceRequest.setUserID(session.authKey().getUserID());
        getHotelPriceRequest.setBreakfast(setHotelBookingRequest.getBreakfast());
        getHotelPriceRequest.setCheckInDate(setHotelBookingRequest.getCheckInDate());
        getHotelPriceRequest.setCityId(setHotelBookingRequest.getCityId());
        getHotelPriceRequest.setInternalCode(setHotelBookingRequest.getInternalCode());
        getHotelPriceRequest.setRoomId(setHotelBookingRequest.getRoomId());
        getHotelPriceRequest.setPaxPassport(setHotelBookingRequest.getPaxPassport());
        getHotelPriceRequest.setRoomRequest(setHotelBookingRequest.getRoomRequest());
        getHotelPriceRequest.setCheckOutDate(setHotelBookingRequest.getCheckOutDate());
        getHotelPriceRequest.setHotelId(setHotelBookingRequest.getHotelId());
        getHotelPriceRequest.setCountryId(setHotelBookingRequest.getCountryId());
        return objectMapper.readValue(this.process(hotelPrice, objectMapper.writeValueAsString(getHotelPriceRequest)), GetHotelPriceResponse.class);
    }

    @Override
    public Response SetHotelBooking(SetHotelBookingRequest setHotelBookingRequest, PartnerCredential partnerCredential) {
        try {
            setHotelBookingRequest.setAccessToken(session.authKey().getAccessToken());
            setHotelBookingRequest.setUserID(session.authKey().getUserID());
            GetHotelPriceResponse getHotelPriceResponse = checkPrice(setHotelBookingRequest);
            if (getHotelPriceResponse == null) {
                return Response.status(200).entity(ResponseCode.PAYMENT_FAILED).build();
            }
            if (!getHotelPriceResponse.getRespMessage().equalsIgnoreCase("SUCCESS")) {
                return Response.status(200).entity(ResponseCode.PAYMENT_FAILED).build();
            }
            ProductFee kai = productFeeService.findFeePartner(partnerCredential.getPartner_id(), HOTEL);
            Transaksi transaksi = new Transaksi()
                    .setTIME_INQUIRY(new Timestamp(System.currentTimeMillis()))
                    .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()))
                    .setDATE(new Timestamp(System.currentTimeMillis()))
                    .setUSERID(partnerCredential.getPartner_uid())
                    .setPARTNERID(partnerCredential.getPartner_id())
                    .setST(Status.INQUIRY)
                    .setTAGIHAN(getHotelPriceResponse.getTotalPrice())
                    .setDEBET(getHotelPriceResponse.getTotalPrice() + kai.getFEE_CA())
                    .setCHARGE(getHotelPriceResponse.getTotalPrice() + kai.getFEE_CA())
                    .setADMIN(0)
                    .setPRODUCT(HOTEL)
                    .setDENOM(HOTEL)
                    .setMSSIDN_NAME(setHotelBookingRequest.getGuestFirstName() + " " + setHotelBookingRequest.getGuestLastName())
                    .setMSSIDN(setHotelBookingRequest.getGuestPhone())
                    .setFEE_CA(kai.getFEE_CA())
                    .setFEE_DEALER(kai.getFEE_CA())
                    .setINQUIRY(objectMapper.writeValueAsString(getHotelPriceResponse));
            if (!getHotelPriceResponse.isEnableBooking()) {
                transaksi.setST(Status.PAYMENT_PROSESS);
                transaksiService.save(transaksi);
                if (partnerDepositService.bookingSaldo(transaksi)) {
                    String payload = this.process(hotelBooking, objectMapper.writeValueAsString(setHotelBookingRequest));
                    SetHotelBookingResponse response = objectMapper.readValue(payload, SetHotelBookingResponse.class);
                    BaseResponse<SetHotelBookingResponse> base = new BaseResponse<>();
                    if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                        transaksiService.save(transaksi.setST(Status.PAYMENT_SUCCESS)
                                .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis())));
                        base.setData(response);
                        base.setNtrans(transaksi.getNTRANS());
                        base.setProduct(HOTEL);
                        base.setSaldoTerpotong(transaksi.getCHARGE());
                        base.setFee(transaksi.getFEE_DEALER());
                        base.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                        base.setMessage("SUCCESS");
                    } else {
                        partnerDepositService.reverseSaldo(transaksi, objectMapper.writeValueAsString(response));
                        base.setNtrans(transaksi.getNTRANS());
                        base.setStatus("0005");
                        base.setProduct(HOTEL);
                        base.setMessage(response.getRespMessage());
                    }
                    return Response.status(200).entity(base).build();
                } else {
                    return Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();

                }
            } else {
                transaksiService.save(transaksi);
                String payload = this.process(hotelBooking, objectMapper.writeValueAsString(setHotelBookingRequest));
                SetHotelBookingResponse response = objectMapper.readValue(payload, SetHotelBookingResponse.class);
                BaseResponse<SetHotelBookingResponse> base = new BaseResponse<>();
                if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                    transaksi.setINQUIRY(payload);
                    transaksi.setBILL_REF_NUMBER(response.getReservationNo());
                    transaksiService.save(transaksi);
                    base.setData(response);
                    base.setNtrans(transaksi.getNTRANS());
                    base.setProduct(HOTEL);
                    base.setSaldoTerpotong(transaksi.getCHARGE());
                    base.setFee(transaksi.getFEE_DEALER());
                    base.setSaldo(partnerDepositService.findById(partnerCredential.getPartner_id()).getBALANCE());
                    base.setMessage("SUCCESS");
                } else {
                    base.setNtrans(transaksi.getNTRANS());
                    base.setStatus("0005");
                    base.setProduct(HOTEL);
                    base.setMessage(response.getRespMessage());
                }
                return Response.status(200).entity(base).build();
            }
        } catch (Exception e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelCountryResponse> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }

    @Override
    public Response SetIssuedHotel(SetIssuedHotelRequest setIssuedHotelRequest, Transaksi transaksi) throws IOException {
        setIssuedHotelRequest.setAccessToken(session.authKey().getAccessToken());
        setIssuedHotelRequest.setUserID(session.authKey().getUserID());
        if (partnerDepositService.bookingSaldo(transaksi)) {
            String payload = this.process(hotelIssued, objectMapper.writeValueAsString(setIssuedHotelRequest));
            SetIssuedHotelResponse response = objectMapper.readValue(payload, SetIssuedHotelResponse.class);
            BaseResponse<String> base = new BaseResponse<>();
            if (response.getStatus().equalsIgnoreCase("SUCCESS") && response.getBookingStatus().trim().equalsIgnoreCase("Accept")) {
                transaksiService.save(transaksi.setST(Status.PAYMENT_SUCCESS)
                        .setTIME_PAYMENT(new Timestamp(System.currentTimeMillis())));
                base.setData(response.getBookingStatus().trim());
                base.setNtrans(transaksi.getNTRANS());
                base.setProduct(HOTEL);
                base.setSaldoTerpotong(transaksi.getCHARGE());
                base.setFee(transaksi.getFEE_DEALER());
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage("SUCCESS");
            } else {
                partnerDepositService.reverseSaldo(transaksi, objectMapper.writeValueAsString(response));
                base.setNtrans(transaksi.getNTRANS());
                base.setStatus("0005");
                base.setProduct(HOTEL);
                base.setData(response.getBookingStatus().trim());
                base.setMessage(response.getRespMessage());
            }
            return Response.status(200).entity(base).build();
        } else {
            return Response.status(200).entity(ResponseCode.PARAMETER_SALDO.setNtrans(transaksi.getNTRANS())).build();

        }
    }

    @Override
    public Response GetHotelBookingDetail(Transaksi transaksi) {
        try {
            ObjectNode objectNode = objectMapper.createObjectNode();
            objectNode.put("userID", session.authKey().getUserID());
            objectNode.put("accessToken", session.authKey().getAccessToken());
            objectNode.put("reservationNo", transaksi.getBILL_REF_NUMBER());
            GetHotelBookingDetail response = objectMapper.readValue(this.process(hotelBookingDetail, objectMapper.writeValueAsString(objectNode)), GetHotelBookingDetail.class);
            if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
                BaseResponse<BookingDetail> base = new BaseResponse<>();
                base.setData(response.getBookingDetail());
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(HOTEL);
                base.setNtrans(transaksi.getNTRANS());
                return Response.status(200).entity(base).build();
            } else {
                BaseResponse<GetHotelBookingDetail> base = new BaseResponse<>();
                base.setSaldo(partnerDepositService.findById(transaksi.getPARTNERID()).getBALANCE());
                base.setMessage(response.getRespMessage());
                base.setProduct(HOTEL);
                base.setNtrans(transaksi.getNTRANS());
                return Response.status(200).entity(base).build();
            }
        } catch (IOException e) {
            slackSendMessage.sendMessageStachTrace(e.getLocalizedMessage(), e);
            BaseResponse<GetHotelBookingDetail> base = new BaseResponse<>();
            base.setStatus("0005");
            base.setMessage(ResponseCode.SERVER_UNAVAILABLE.getMessage());
            base.setProduct(HOTEL);
            return Response.status(200).entity(base).build();
        }
    }


}
