package com.trimindi.switching.gatway.biller.dw.pojo.airline.booking;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SetAirlineBookingResponse {

    @JsonProperty("flightDeparts")
    private List<FlightDepartsItem> flightDeparts;

    @JsonProperty("airlineAdminFee")
    private double airlineAdminFee;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("salesPrice")
    private double salesPrice;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("userID")
    @JsonIgnore
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonProperty("segment")
    private int segment;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("memberDiscount")
    private double memberDiscount;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("ticketPrice")
    private double ticketPrice;

    @JsonProperty("memberAdminFee")
    private double memberAdminFee;

    @JsonProperty("accessToken")
    @JsonIgnore
    private String accessToken;

    @JsonProperty("timeLimit")
    private String timeLimit;

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("flightReturns")
    private List<FlightDepartsItem> flightReturns;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("bookingCode")
    private String bookingCode;

    @JsonProperty("detail")
    private String detail;

    @JsonProperty("status")
    @JsonIgnore
    private String status;

    public List<FlightDepartsItem> getFlightDeparts() {
        return flightDeparts;
    }

    public void setFlightDeparts(List<FlightDepartsItem> flightDeparts) {
        this.flightDeparts = flightDeparts;
    }

    public double getAirlineAdminFee() {
        return airlineAdminFee;
    }

    public void setAirlineAdminFee(double airlineAdminFee) {
        this.airlineAdminFee = airlineAdminFee;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public double getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(double salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public int getSegment() {
        return segment;
    }

    public void setSegment(int segment) {
        this.segment = segment;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    public double getMemberDiscount() {
        return memberDiscount;
    }

    public void setMemberDiscount(double memberDiscount) {
        this.memberDiscount = memberDiscount;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public double getMemberAdminFee() {
        return memberAdminFee;
    }

    public void setMemberAdminFee(double memberAdminFee) {
        this.memberAdminFee = memberAdminFee;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public List<FlightDepartsItem> getFlightReturns() {
        return flightReturns;
    }

    public void setFlightReturns(List<FlightDepartsItem> flightReturns) {
        this.flightReturns = flightReturns;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SetAirlineBookingResponse{" +
                        "flightDeparts = '" + flightDeparts + '\'' +
                        ",airlineAdminFee = '" + airlineAdminFee + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",salesPrice = '" + salesPrice + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",segment = '" + segment + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",currency = '" + currency + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",memberDiscount = '" + memberDiscount + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",ticketPrice = '" + ticketPrice + '\'' +
                        ",memberAdminFee = '" + memberAdminFee + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",timeLimit = '" + timeLimit + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",flightReturns = '" + flightReturns + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",bookingCode = '" + bookingCode + '\'' +
                        ",detail = '" + detail + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}