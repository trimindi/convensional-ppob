package com.trimindi.switching.gatway.biller.dw.pojo.hotel.booking.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class SetHotelBookingResponse {

    @JsonProperty("totalPrice")
    private double totalPrice;

    @JsonProperty("hotelAddress")
    private String hotelAddress;

    @JsonProperty("hotelID")
    private String hotelID;

    @JsonProperty("checkInDate")
    private String checkInDate;

    @JsonProperty("requestDescription")
    private String requestDescription;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("paxPassport")
    private String paxPassport;

    @JsonProperty("checkOutDate")
    private String checkOutDate;

    @JsonProperty("roomNum")
    private int roomNum;

    @JsonProperty("respTime")
    private String respTime;

    @JsonProperty("bookingStatus")
    private String bookingStatus;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("cityID")
    private String cityID;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("hotelName")
    private String hotelName;

    @JsonProperty("countryID")
    private String countryID;

    @JsonProperty("roomID")
    private String roomID;

    @JsonProperty("roomName")
    private String roomName;

    @JsonProperty("voucherNo")
    private String voucherNo;

    @JsonProperty("osRefNo")
    private String osRefNo;

    @JsonProperty("roomRequest")
    private List<RoomRequestItem> roomRequest;

    @JsonProperty("bookingDate")
    private String bookingDate;

    @JsonProperty("breakfast")
    private String breakfast;

    @JsonProperty("internalCode")
    private String internalCode;

    @JsonProperty("reservationNo")
    private String reservationNo;

    @JsonProperty("status")
    private String status;

    public SetHotelBookingResponse() {
    }

    public SetHotelBookingResponse setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public SetHotelBookingResponse setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
        return this;
    }

    public SetHotelBookingResponse setHotelID(String hotelID) {
        this.hotelID = hotelID;
        return this;
    }

    public SetHotelBookingResponse setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
        return this;
    }

    public SetHotelBookingResponse setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
        return this;
    }

    public SetHotelBookingResponse setUserID(String userID) {
        this.userID = userID;
        return this;
    }

    public SetHotelBookingResponse setPaxPassport(String paxPassport) {
        this.paxPassport = paxPassport;
        return this;
    }

    public SetHotelBookingResponse setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
        return this;
    }

    public SetHotelBookingResponse setRoomNum(int roomNum) {
        this.roomNum = roomNum;
        return this;
    }

    public SetHotelBookingResponse setRespTime(String respTime) {
        this.respTime = respTime;
        return this;
    }

    public SetHotelBookingResponse setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
        return this;
    }

    public SetHotelBookingResponse setRespMessage(String respMessage) {
        this.respMessage = respMessage;
        return this;
    }

    public SetHotelBookingResponse setCityID(String cityID) {
        this.cityID = cityID;
        return this;
    }

    public SetHotelBookingResponse setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public SetHotelBookingResponse setHotelName(String hotelName) {
        this.hotelName = hotelName;
        return this;
    }

    public SetHotelBookingResponse setCountryID(String countryID) {
        this.countryID = countryID;
        return this;
    }

    public SetHotelBookingResponse setRoomID(String roomID) {
        this.roomID = roomID;
        return this;
    }

    public SetHotelBookingResponse setRoomName(String roomName) {
        this.roomName = roomName;
        return this;
    }

    public SetHotelBookingResponse setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
        return this;
    }

    public SetHotelBookingResponse setOsRefNo(String osRefNo) {
        this.osRefNo = osRefNo;
        return this;
    }

    public SetHotelBookingResponse setRoomRequest(List<RoomRequestItem> roomRequest) {
        this.roomRequest = roomRequest;
        return this;
    }

    public SetHotelBookingResponse setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
        return this;
    }

    public SetHotelBookingResponse setBreakfast(String breakfast) {
        this.breakfast = breakfast;
        return this;
    }

    public SetHotelBookingResponse setInternalCode(String internalCode) {
        this.internalCode = internalCode;
        return this;
    }

    public SetHotelBookingResponse setReservationNo(String reservationNo) {
        this.reservationNo = reservationNo;
        return this;
    }

    public SetHotelBookingResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public String getHotelID() {
        return hotelID;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public String getPaxPassport() {
        return paxPassport;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public int getRoomNum() {
        return roomNum;
    }

    public String getRespTime() {
        return respTime;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public String getCityID() {
        return cityID;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getCountryID() {
        return countryID;
    }

    public String getRoomID() {
        return roomID;
    }

    public String getRoomName() {
        return roomName;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public String getOsRefNo() {
        return osRefNo;
    }

    public List<RoomRequestItem> getRoomRequest() {
        return roomRequest;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public String getReservationNo() {
        return reservationNo;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "SetHotelBookingResponse{" +
                        "totalPrice = '" + totalPrice + '\'' +
                        ",hotelAddress = '" + hotelAddress + '\'' +
                        ",hotelID = '" + hotelID + '\'' +
                        ",checkInDate = '" + checkInDate + '\'' +
                        ",requestDescription = '" + requestDescription + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",paxPassport = '" + paxPassport + '\'' +
                        ",checkOutDate = '" + checkOutDate + '\'' +
                        ",roomNum = '" + roomNum + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",bookingStatus = '" + bookingStatus + '\'' +
                        ",respMessage = '" + respMessage + '\'' +
                        ",cityID = '" + cityID + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",hotelName = '" + hotelName + '\'' +
                        ",countryID = '" + countryID + '\'' +
                        ",roomID = '" + roomID + '\'' +
                        ",roomName = '" + roomName + '\'' +
                        ",voucherNo = '" + voucherNo + '\'' +
                        ",osRefNo = '" + osRefNo + '\'' +
                        ",roomRequest = '" + roomRequest + '\'' +
                        ",bookingDate = '" + bookingDate + '\'' +
                        ",breakfast = '" + breakfast + '\'' +
                        ",internalCode = '" + internalCode + '\'' +
                        ",reservationNo = '" + reservationNo + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}