package com.trimindi.switching.gatway.biller.dw.pojo.train.route;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Route {

    @JsonProperty("destinationFull")
    private String destinationFull;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("originFull")
    private String originFull;

    public String getDestinationFull() {
        return destinationFull;
    }

    public void setDestinationFull(String destinationFull) {
        this.destinationFull = destinationFull;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOriginFull() {
        return originFull;
    }

    public void setOriginFull(String originFull) {
        this.originFull = originFull;
    }

    @Override
    public String toString() {
        return
                "Route{" +
                        "destinationFull = '" + destinationFull + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",originFull = '" + originFull + '\'' +
                        "}";
    }
}