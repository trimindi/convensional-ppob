package com.trimindi.switching.gatway.biller.dw.pojo.airline.price;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetAirlinePriceResponse {

    @JsonProperty("respMessage")
    @JsonIgnore
    private String respMessage;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("destination")
    private String destination;

    @JsonProperty("priceReturn")
    private List<PriceReturnItem> priceReturn;

    @JsonProperty("searchKey")
    private Object searchKey;

    @JsonProperty("accessToken")
    @JsonIgnore
    private String accessToken;

    @JsonProperty("sumFare")
    private double sumFare;

    @JsonProperty("userID")
    @JsonIgnore
    private String userID;

    @JsonProperty("airlineID")
    private String airlineID;

    @JsonProperty("tripType")
    private String tripType;

    @JsonProperty("returnDate")
    private String returnDate;

    @JsonProperty("paxChild")
    private int paxChild;

    @JsonProperty("paxInfant")
    private int paxInfant;

    @JsonProperty("priceDepart")
    private List<PriceDepartItem> priceDepart;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("departDate")
    private String departDate;

    @JsonProperty("promoCode")
    private String promoCode;

    @JsonProperty("paxAdult")
    private int paxAdult;

    @JsonProperty("status")
    @JsonIgnore
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<PriceReturnItem> getPriceReturn() {
        return priceReturn;
    }

    public void setPriceReturn(List<PriceReturnItem> priceReturn) {
        this.priceReturn = priceReturn;
    }

    public Object getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(Object searchKey) {
        this.searchKey = searchKey;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public double getSumFare() {
        return sumFare;
    }

    public void setSumFare(double sumFare) {
        this.sumFare = sumFare;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(String airlineID) {
        this.airlineID = airlineID;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public int getPaxChild() {
        return paxChild;
    }

    public void setPaxChild(int paxChild) {
        this.paxChild = paxChild;
    }

    public int getPaxInfant() {
        return paxInfant;
    }

    public void setPaxInfant(int paxInfant) {
        this.paxInfant = paxInfant;
    }

    public List<PriceDepartItem> getPriceDepart() {
        return priceDepart;
    }

    public void setPriceDepart(List<PriceDepartItem> priceDepart) {
        this.priceDepart = priceDepart;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public int getPaxAdult() {
        return paxAdult;
    }

    public void setPaxAdult(int paxAdult) {
        this.paxAdult = paxAdult;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetAirlinePriceResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",origin = '" + origin + '\'' +
                        ",destination = '" + destination + '\'' +
                        ",priceReturn = '" + priceReturn + '\'' +
                        ",searchKey = '" + searchKey + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",sumFare = '" + sumFare + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",airlineID = '" + airlineID + '\'' +
                        ",tripType = '" + tripType + '\'' +
                        ",returnDate = '" + returnDate + '\'' +
                        ",paxChild = '" + paxChild + '\'' +
                        ",paxInfant = '" + paxInfant + '\'' +
                        ",priceDepart = '" + priceDepart + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",departDate = '" + departDate + '\'' +
                        ",promoCode = '" + promoCode + '\'' +
                        ",paxAdult = '" + paxAdult + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}