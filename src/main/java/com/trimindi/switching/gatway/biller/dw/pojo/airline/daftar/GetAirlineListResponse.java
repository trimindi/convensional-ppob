package com.trimindi.switching.gatway.biller.dw.pojo.airline.daftar;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetAirlineListResponse {

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("respTime")
    @JsonIgnore
    private String respTime;

    @JsonProperty("airlines")
    private List<AirlinesItem> airlines;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    @JsonIgnore
    private String userID;

    @JsonProperty("status")
    @JsonIgnore
    private String status;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public List<AirlinesItem> getAirlines() {
        return airlines;
    }

    public void setAirlines(List<AirlinesItem> airlines) {
        this.airlines = airlines;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "GetAirlineListResponse{" +
                        "respMessage = '" + respMessage + '\'' +
                        ",respTime = '" + respTime + '\'' +
                        ",airlines = '" + airlines + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}