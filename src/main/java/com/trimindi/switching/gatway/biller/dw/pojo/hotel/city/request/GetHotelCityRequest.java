package com.trimindi.switching.gatway.biller.dw.pojo.hotel.city.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

@Generated("com.robohorse.robopojogenerator")
public class GetHotelCityRequest {

    @JsonProperty("cityNameFilter")
    @NotNull
    private String cityNameFilter;

    @JsonProperty("accessToken")
    private String accessToken;

    @JsonProperty("userID")
    private String userID;

    @JsonProperty("countryID")
    @NotNull
    private String countryID;

    public String getCityNameFilter() {
        return cityNameFilter;
    }

    public void setCityNameFilter(String cityNameFilter) {
        this.cityNameFilter = cityNameFilter;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    @Override
    public String toString() {
        return
                "GetHotelCityRequest{" +
                        " cityNameFilter  = '" + cityNameFilter + '\'' +
                        ",accessToken = '" + accessToken + '\'' +
                        ",userID = '" + userID + '\'' +
                        ",countryID = '" + countryID + '\'' +
                        "}";
    }
}