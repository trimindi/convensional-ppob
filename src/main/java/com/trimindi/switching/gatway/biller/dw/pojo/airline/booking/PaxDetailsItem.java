package com.trimindi.switching.gatway.biller.dw.pojo.airline.booking;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class PaxDetailsItem {

    @JsonProperty("passportNumber")
    private String passportNumber;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("parent")
    private String parent;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("addOns")
    private List<AddOnsItem> addOns;

    @JsonProperty("passportExpiredDate")
    private String passportExpiredDate;

    @JsonProperty("passportIssuedCountry")
    private String passportIssuedCountry;

    @JsonProperty("title")
    private String title;

    @JsonProperty("type")
    private String type;

    @JsonProperty("birthDate")
    private String birthDate;

    @JsonProperty("passportIssuedDate")
    private String passportIssuedDate;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("nationality")
    private String nationality;

    @JsonProperty("birthCountry")
    private String birthCountry;

    @JsonProperty("IDNumber")
    private String iDNumber;

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<AddOnsItem> getAddOns() {
        return addOns;
    }

    public void setAddOns(List<AddOnsItem> addOns) {
        this.addOns = addOns;
    }

    public String getPassportExpiredDate() {
        return passportExpiredDate;
    }

    public void setPassportExpiredDate(String passportExpiredDate) {
        this.passportExpiredDate = passportExpiredDate;
    }

    public String getPassportIssuedCountry() {
        return passportIssuedCountry;
    }

    public void setPassportIssuedCountry(String passportIssuedCountry) {
        this.passportIssuedCountry = passportIssuedCountry;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPassportIssuedDate() {
        return passportIssuedDate;
    }

    public void setPassportIssuedDate(String passportIssuedDate) {
        this.passportIssuedDate = passportIssuedDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthCountry() {
        return birthCountry;
    }

    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    public String getIDNumber() {
        return iDNumber;
    }

    public void setIDNumber(String iDNumber) {
        this.iDNumber = iDNumber;
    }

    @Override
    public String toString() {
        return
                "PaxDetailsItem{" +
                        "passportNumber = '" + passportNumber + '\'' +
                        ",lastName = '" + lastName + '\'' +
                        ",parent = '" + parent + '\'' +
                        ",gender = '" + gender + '\'' +
                        ",addOns = '" + addOns + '\'' +
                        ",passportExpiredDate = '" + passportExpiredDate + '\'' +
                        ",passportIssuedCountry = '" + passportIssuedCountry + '\'' +
                        ",title = '" + title + '\'' +
                        ",type = '" + type + '\'' +
                        ",birthDate = '" + birthDate + '\'' +
                        ",passportIssuedDate = '" + passportIssuedDate + '\'' +
                        ",firstName = '" + firstName + '\'' +
                        ",nationality = '" + nationality + '\'' +
                        ",birthCountry = '" + birthCountry + '\'' +
                        ",iDNumber = '" + iDNumber + '\'' +
                        "}";
    }
}