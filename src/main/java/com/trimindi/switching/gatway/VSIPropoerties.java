package com.trimindi.switching.gatway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by sx on 16/10/17.
 * Copyright under comercial unit
 */
@Configuration
public class VSIPropoerties {
    @Value("${vsi.partner.id}")
    public String PARTNER_ID;
    @Value("${vsi.bank.code}")
    public String BANK_CODE;
    @Value("${vsi.terminal.id}")
    public String TERMINAL_ID;
    @Value("${vsi.switcher.id}")
    public String SWITCHER_ID;
    @Value("${vsi.signon}")
    public String SIGN_ON;
    @Value("${vsi.signoff}")
    public String SIGN_OFF;
    @Value("${vsi.echo}")
    public String ECHO_TEST;
    @Value("${iso8583.vsi.connection.timeout}")
    public String TIMOUT;
    @Value("${vsi.pan.multifinance}")
    public String PAN_MULTIFINANCE;
    @Value("${vsi.pan.telkom}")
    public String PAN_TELKOM;
}
