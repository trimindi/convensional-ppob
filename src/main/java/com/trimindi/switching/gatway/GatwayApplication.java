package com.trimindi.switching.gatway;

import net.gpedro.integrations.slack.SlackApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCaching
@SpringBootApplication
@EnableScheduling
public class GatwayApplication {
    @Value("${logging.slack.webhook.url}")
    private String url;

    public static void main(String[] args) {
        SpringApplication.run(GatwayApplication.class, args);
    }

    @Bean
    public SlackApi slackApi() {
        return new SlackApi(url);
    }

}
