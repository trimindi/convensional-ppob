package com.trimindi.switching.gatway.exeption;


import com.trimindi.switching.gatway.utils.constanta.ResponseCode;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by PC on 6/12/2017.
 */
@Provider
public class CustomNotAllowedException implements ExceptionMapper<NotAllowedException> {

    @Override
    public Response toResponse(NotAllowedException e) {
        return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build();
    }
}
