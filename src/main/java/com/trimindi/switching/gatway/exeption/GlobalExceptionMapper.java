package com.trimindi.switching.gatway.exeption;

import com.trimindi.switching.gatway.utils.constanta.ResponseCode;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by sx on 17/04/18.
 * Copyright under comercial unit
 */
@Provider
public class GlobalExceptionMapper implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception e) {

        ResponseCode responseCode = new ResponseCode();
        responseCode.setStatus("0068");
        responseCode.setMessage(e.getMessage());
        return Response.status(200).entity(responseCode).build();
    }
}
