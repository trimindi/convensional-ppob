package com.trimindi.switching.gatway.exeption;

import com.trimindi.switching.gatway.utils.constanta.ResponseCode;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by PC on 6/17/2017.
 */
@Provider
public class CustomBadRequestExeption implements ExceptionMapper<BadRequestException> {

    @Override
    public Response toResponse(BadRequestException e) {
        return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build();
    }
}
