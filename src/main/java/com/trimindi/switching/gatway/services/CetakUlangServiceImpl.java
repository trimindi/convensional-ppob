package com.trimindi.switching.gatway.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.biller.dw.pojo.ppob.response.InquiryResponse;
import com.trimindi.switching.gatway.biller.dw.pojo.ppob.response.TvInquiryResponse;
import com.trimindi.switching.gatway.biller.pdamtasik.DetailItem;
import com.trimindi.switching.gatway.biller.pdamtasik.PdamTasikPaymentResponse;
import com.trimindi.switching.gatway.biller.rajabiller.response.MethodResponse;
import com.trimindi.switching.gatway.biller.servindo.ResponseServindo;
import com.trimindi.switching.gatway.client.client.Iso8583Client;
import com.trimindi.switching.gatway.client.client.Iso8583ClientCustom;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.response.BaseResponse;
import com.trimindi.switching.gatway.response.bpjs.PaymentBpjs;
import com.trimindi.switching.gatway.response.pasca.PaymentPhonePasca;
import com.trimindi.switching.gatway.response.pdam.Payment;
import com.trimindi.switching.gatway.response.pdam.base.PaymentPdam;
import com.trimindi.switching.gatway.response.postpaid.Rincian;
import com.trimindi.switching.gatway.response.telkom.PaymentTelkom;
import com.trimindi.switching.gatway.response.telkom.base.PaymentTelkomNew;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.PostpaidHelper;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.generator.VSIMessageGenerator;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.ParsingHelper;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorMultifinance;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by PC on 27/09/2017.
 */
@Service
public class CetakUlangServiceImpl implements CetakUlangService {
    private static final Logger logger = LoggerFactory.getLogger(CetakUlangServiceImpl.class);
    StringReader stringReader;

    final
    ObjectMapper objectMapper;
    ResponseServindo responseServindo;
    private final Unmarshaller unmarshaller;
    private List<Rules> rules;
    private final Iso8583Client<IsoMessage> iso8583Client;
    private final TransaksiService transaksiService;
    private final Iso8583ClientCustom<IsoMessage> iso8583ClientCustom;
    private final VSIMessageGenerator vsiMessageGenerator;
    private final Map<String, ResponseCode> responseCode;

    @Autowired
    public CetakUlangServiceImpl(ObjectMapper objectMapper,
                                 Unmarshaller unmarshaller,
                                 @Qualifier("aranetClient") Iso8583Client<IsoMessage> iso8583Client,
                                 TransaksiService transaksiService,
                                 Iso8583ClientCustom<IsoMessage> iso8583ClientCustom,
                                 VSIMessageGenerator vsiMessageGenerator,
                                 @Qualifier("vsiResponseCode") Map<String, ResponseCode> responseCode) {
        this.objectMapper = objectMapper;
        this.unmarshaller = unmarshaller;
        this.iso8583Client = iso8583Client;
        this.transaksiService = transaksiService;
        this.iso8583ClientCustom = iso8583ClientCustom;
        this.vsiMessageGenerator = vsiMessageGenerator;
        this.responseCode = responseCode;
    }

    @Override
    public Response cetakUlangPdamRajabiller(Transaksi transaksi) throws JAXBException {
        stringReader = new StringReader(transaksi.getPAYMENT());
        MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
        Payment payment = new Payment(methodResponse);
        int jml = 1;
        if (payment.getPERIODE().contains(",")) {
            String[] jum = payment.getPERIODE().split(",");
            jml = jum.length;
        }
        PaymentPdam paymentPdam = new PaymentPdam()
                .setAdmin((int) payment.getADMIN())
                .setJumlahBill(jml)
                .setReff(payment.getREFF())
                .setPeriode(payment.getPERIODE())
                .setNamapelanggan(payment.getNAMAPELANGGAN())
                .setIdpelanggan(payment.getIDPELANGGAN())
                .setTagihan((int) payment.getTAGIHAN())
                .setTotalTagihan((int) payment.getTAGIHAN())
                .setStandAwal((Objects.equals(payment.getSTANDAWAL(), "")) ? 0 : Integer.valueOf(payment.getSTANDAWAL()))
                .setStandAkir((Objects.equals(payment.getSTANDAKHIR(), "")) ? 0 : Integer.valueOf(payment.getSTANDAKHIR()))
                .setTotalTagihan((int) (payment.getTAGIHAN() + payment.getADMIN()));
        BaseResponse<PaymentPdam> paymentResponse = new BaseResponse<>();
        paymentResponse.setData(paymentPdam);
        paymentResponse.setFee(transaksi.getFEE_DEALER());
        paymentResponse.setNtrans(transaksi.getNTRANS());
        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
        paymentResponse.setSaldo(0);
        paymentResponse.setProduct(transaksi.getDENOM());
        paymentResponse.setMessage("Successful");
        paymentResponse.setWaktu(transaksi.getTIME_PAYMENT());
        return Response.status(200).entity(paymentResponse).build();
    }

    @Override
    public Response cetakUlangPulsaPascaRajabiller(Transaksi transaksi) throws JAXBException {
        stringReader = new StringReader(transaksi.getPAYMENT());
        MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
        PaymentPhonePasca payment = new PaymentPhonePasca(methodResponse);
        BaseResponse<PaymentPhonePasca> paymentResponse = new BaseResponse<>();
        paymentResponse.setData(payment);
        paymentResponse.setFee(transaksi.getFEE_DEALER());
        paymentResponse.setNtrans(transaksi.getNTRANS());
        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
        paymentResponse.setSaldo(0);
        paymentResponse.setProduct(transaksi.getDENOM());
        paymentResponse.setMessage("Successful");
        paymentResponse.setWaktu(transaksi.getTIME_PAYMENT());
        return Response.status(200).entity(paymentResponse).build();
    }

    @Override
    public Response cetakUlangBpjsRajabiller(Transaksi transaksi) throws JAXBException {
        stringReader = new StringReader(transaksi.getPAYMENT());
        MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
        PaymentBpjs payment = new PaymentBpjs(methodResponse);
        BaseResponse<PaymentBpjs> paymentResponse = new BaseResponse<>();
        paymentResponse.setData(payment);
        paymentResponse.setFee(transaksi.getFEE_DEALER());
        paymentResponse.setNtrans(transaksi.getNTRANS());
        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
        paymentResponse.setSaldo(0);
        paymentResponse.setProduct(transaksi.getDENOM());
        paymentResponse.setMessage("Successful");
        paymentResponse.setWaktu(transaksi.getTIME_PAYMENT());
        return Response.status(200).entity(paymentResponse).build();
    }

    @Override
    public Response cetakUlangTelkomRajabiller(Transaksi transaksi) throws JAXBException {
        stringReader = new StringReader(transaksi.getPAYMENT());
        MethodResponse methodResponse = (MethodResponse) unmarshaller.unmarshal(stringReader);
        PaymentTelkom payment = new PaymentTelkom(methodResponse);
        int jml = 1;
        if (payment.getPERIODE().contains(",")) {
            String[] jum = payment.getPERIODE().split(",");
            jml = jum.length;
        }
        String idpel = (payment.getIDPELANGGAN().equals(payment.getIDPELANGGAN1())) ? (payment.getIDPELANGGAN1() + " " + payment.getIDPELANGGAN()) : payment.getIDPELANGGAN();

        PaymentTelkomNew paymentTelkom = new PaymentTelkomNew()
                .setAdmin((int) payment.getADMIN())
                .setJumlahBill(jml)
                .setNamapelanggan(payment.getNAMAPELANGGAN())
                .setPeriode(payment.getPERIODE())
                .setReff(payment.getREF1())
                .setTagihan((int) payment.getTAGIHAN())
                .setTotalTagihan((int) (payment.getTAGIHAN() + payment.getADMIN()))
                .setIdpelanggan(idpel);
        BaseResponse<PaymentTelkomNew> paymentResponse = new BaseResponse<>();
        paymentResponse.setData(paymentTelkom);
        paymentResponse.setFee(transaksi.getFEE_DEALER());
        paymentResponse.setNtrans(transaksi.getNTRANS());
        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
        paymentResponse.setSaldo(0);
        paymentResponse.setProduct(transaksi.getDENOM());
        paymentResponse.setMessage("Successful");
        paymentResponse.setWaktu(transaksi.getTIME_PAYMENT());
        return Response.status(200).entity(paymentResponse).build();
    }

    @Override
    public Response cetakUlangPlnPrepaidAranet(Transaksi transaksi) {
        IsoMessage isoMsg = new IsoMessage();
        try {
            isoMsg = iso8583Client.getIsoMessageFactory().parseMessage(transaksi.getPAYMENT().getBytes(), 0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        rules = ParsingHelper.parsingRulesPrepaid(isoMsg, true);
        com.trimindi.switching.gatway.response.prepaid.Payment prepaid = new com.trimindi.switching.gatway.response.prepaid.Payment(rules, true);
        prepaid.setTagihan(transaksi.getTAGIHAN());
        BaseResponse<com.trimindi.switching.gatway.response.prepaid.Payment> baseResponse1 = new BaseResponse<>();
        baseResponse1.setData(prepaid);
        baseResponse1.setSaldoTerpotong(transaksi.getCHARGE());
        baseResponse1.setProduct(transaksi.getDENOM());
        baseResponse1.setNtrans(transaksi.getNTRANS());
        baseResponse1.setFee(transaksi.getFEE_DEALER());
        baseResponse1.setSaldoTerpotong(0);
        baseResponse1.setMessage("CETAK ULANG KE " + (transaksi.getPRT() + 1));
        baseResponse1.setWaktu(transaksi.getTIME_PAYMENT());
        transaksi.setPRT(transaksi.getPRT() + 1);
        transaksiService.update(transaksi);
        return Response.status(200).entity(baseResponse1).build();
    }

    @Override
    public Response cetakUlangPlnPostpaidAranet(Transaksi transaksi) {
        IsoMessage isoMsg = new IsoMessage();
        try {
            isoMsg = iso8583Client.getIsoMessageFactory().parseMessage(transaksi.getPAYMENT().getBytes(), 0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        rules = ParsingHelper.parsingRulesPostPaid(isoMsg, true);
        com.trimindi.switching.gatway.response.postpaid.Payment payment = new com.trimindi.switching.gatway.response.postpaid.Payment(rules);
        String bit48 = isoMsg.getObjectValue(48);
        int legth = new SDE.Builder().setPayload(bit48).setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, true)).calculate();
        String rincian = bit48.substring(legth, bit48.length());
        int start = 0;
        int leghtRincian = 115;
        List<Rincian> rincians = new ArrayList<>();
        for (int i = 0; i < payment.getBillStatus(); i++) {
            String parsRincian = rincian.substring(start, start + leghtRincian);
            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
            Rincian r = new Rincian(rc);
            rincians.add(r);
            start += leghtRincian;
        }
        payment.setPeriod(PostpaidHelper.generatePeriode(rincians));
        payment.setMeter(PostpaidHelper.generateStandMeter(rincians));
        BaseResponse<com.trimindi.switching.gatway.response.postpaid.Payment> baseResponse = new BaseResponse<>();
        payment.setRincians(rincians);
        payment.setTagihan(transaksi.getTAGIHAN());
        payment.setDenda(transaksi.getDENDA());
        baseResponse.setData(payment);
        baseResponse.setProduct(transaksi.getDENOM());
        baseResponse.setNtrans(transaksi.getNTRANS());
        baseResponse.setFee(transaksi.getFEE_DEALER());
        baseResponse.setSaldoTerpotong(0);
        baseResponse.setMessage("CETAK ULANG KE " + (transaksi.getPRT() + 1));
        baseResponse.setWaktu(transaksi.getTIME_PAYMENT());
        transaksi.setPRT(transaksi.getPRT() + 1);
        transaksiService.update(transaksi);
        return Response.status(200).entity(baseResponse).build();
    }

    @Override
    public Response cetakUlangPlnNontaglistAranet(Transaksi transaksi) {
        IsoMessage isoMsg = new IsoMessage();
        try {
            isoMsg = iso8583Client.getIsoMessageFactory().parseMessage(transaksi.getPAYMENT().getBytes(), 0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        rules = ParsingHelper.parsingRulesNontaglist(isoMsg, true);
        com.trimindi.switching.gatway.response.nontaglist.Payment nontaglist = new com.trimindi.switching.gatway.response.nontaglist.Payment(rules);
        BaseResponse<com.trimindi.switching.gatway.response.nontaglist.Payment> baseResponse2 = new BaseResponse<>();
        baseResponse2.setSaldoTerpotong(transaksi.getCHARGE());
        baseResponse2.setProduct(transaksi.getPRODUCT());
        baseResponse2.setNtrans(transaksi.getNTRANS());
        baseResponse2.setData(nontaglist);
        baseResponse2.setFee(transaksi.getFEE_DEALER());
        baseResponse2.setSaldoTerpotong(0);
        baseResponse2.setMessage("CETAK ULANG KE " + (transaksi.getPRT() + 1));
        baseResponse2.setWaktu(transaksi.getTIME_PAYMENT());
        transaksi.setPRT(transaksi.getPRT() + 1);
        transaksiService.update(transaksi);
        return Response.status(200).entity(baseResponse2).build();
    }

    @Override
    public Response cetakUlangPulsaServindo(Transaksi transaksi) throws IOException {
        responseServindo = objectMapper.readValue(transaksi.getPAYMENT(), ResponseServindo.class);
        BaseResponse<ResponseServindo> paymentResponse = new BaseResponse<>();
        paymentResponse.setData(responseServindo);
        paymentResponse.setNtrans(transaksi.getNTRANS());
        paymentResponse.setSaldo(0);
        paymentResponse.setMessage("Successful");
        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
        paymentResponse.setProduct(transaksi.getDENOM());
        paymentResponse.setWaktu(transaksi.getTIME_PAYMENT());

        return Response.status(200).entity(paymentResponse).build();
    }

    @Override
    public Response cetakUlangDataServindo(Transaksi transaksi) throws IOException {
        responseServindo = objectMapper.readValue(transaksi.getPAYMENT(), ResponseServindo.class);
        BaseResponse<ResponseServindo> paymentResponse = new BaseResponse<>();
        paymentResponse.setData(responseServindo);
        paymentResponse.setNtrans(transaksi.getNTRANS());
        paymentResponse.setSaldo(0);
        paymentResponse.setMessage("Successful");
        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
        paymentResponse.setProduct(transaksi.getDENOM());
        paymentResponse.setWaktu(transaksi.getTIME_PAYMENT());

        return Response.status(200).entity(paymentResponse).build();
    }

    @Override
    public Response cetakUlangPdamTasik(Transaksi transaksi) throws IOException {
        PdamTasikPaymentResponse paymentResponse = objectMapper.readValue(transaksi.getPAYMENT(), PdamTasikPaymentResponse.class);
        PaymentPdam payment = new PaymentPdam()
                .setTagihan(paymentResponse.getTotalamount())
                .setAdmin((int) transaksi.getADMIN())
                .setNamapelanggan(transaksi.getMSSIDN_NAME())
                .setIdpelanggan(transaksi.getMSSIDN())
                .setPeriode(paymentResponse.getDetail().stream().map(DetailItem::getPeriod).collect(Collectors.joining(",")))
                .setReff(paymentResponse.getReff())
                .setRincians(paymentResponse.getDetail())
                .setReff(paymentResponse.getReff())
                .setJumlahBill(paymentResponse.getBillqty())
                .setType(paymentResponse.getType())
                .setStandAwal(findStanAwal(paymentResponse.getDetail()))
                .setStandAkir(findStanAkir(paymentResponse.getDetail()))
                .setDenda(findDenda(paymentResponse.getDetail()))
                .setIdpelanggan(transaksi.getMSSIDN())
                .setTotalTagihan((int) (transaksi.getADMIN() + transaksi.getTAGIHAN()));
        BaseResponse<PaymentPdam> res = new BaseResponse<>();
        res.setData(payment);
        res.setFee(transaksi.getFEE_DEALER());
        res.setNtrans(transaksi.getNTRANS());
        res.setSaldoTerpotong(transaksi.getCHARGE());
        res.setSaldo(0);
        res.setProduct(transaksi.getDENOM());
        res.setMessage("Successful");
        res.setWaktu(transaksi.getTIME_PAYMENT());

        return Response.status(200).entity(res).build();
    }

    @Override
    public void cetakUlangMultifinance(@Suspended AsyncResponse asyncResponse, Transaksi transaksi) {

        IsoMessage isoMsg = null;
        try {
            isoMsg = iso8583ClientCustom.getIsoMessageFactory().parseMessage(transaksi.getPAYMENT().getBytes(), 0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(isoMsg.getObjectValue(48))
                .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(48, true))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(isoMsg.getObjectValue(62))
                .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(62, true))
                .generate();
        bit48.addAll(bit62);
        com.trimindi.switching.gatway.response.mutifinance.Payment prepaid = new com.trimindi.switching.gatway.response.mutifinance.Payment(bit48);
        BaseResponse<com.trimindi.switching.gatway.response.mutifinance.Payment> baseResponse1 = new BaseResponse<>();
        baseResponse1.setData(prepaid);
        baseResponse1.setSaldoTerpotong(transaksi.getCHARGE());
        baseResponse1.setProduct(transaksi.getDENOM());
        baseResponse1.setNtrans(transaksi.getNTRANS());
        baseResponse1.setFee(transaksi.getFEE_DEALER());
        baseResponse1.setSaldoTerpotong(0);
        baseResponse1.setMessage("CETAK ULANG KE " + (transaksi.getPRT() + 1));
        baseResponse1.setWaktu(transaksi.getTIME_PAYMENT());
        transaksi.setPRT(transaksi.getPRT() + 1);
        transaksiService.update(transaksi);
        asyncResponse.resume(Response.status(200).entity(baseResponse1).build());
    }

    @Override
    public Response cetakUlangTvDharmawisata(Transaksi transaksi) throws IOException {
        InquiryResponse inquiryResponse = objectMapper.readValue(transaksi.getINQUIRY(), InquiryResponse.class);
        TvInquiryResponse tvInquiryResponse = new TvInquiryResponse();
        tvInquiryResponse.setIdpelanggan(inquiryResponse.getCustomerID());
        tvInquiryResponse.setAdmin(inquiryResponse.getAdminBank());
        tvInquiryResponse.setNamapelanggan(inquiryResponse.getCustomerName());
        tvInquiryResponse.setPeriode(inquiryResponse.getPeriod());
        tvInquiryResponse.setDenda(inquiryResponse.getPenalty());
        tvInquiryResponse.setTagihan(inquiryResponse.getBilling());
        tvInquiryResponse.setKeterangan("Payment Sukses");
        Integer total = inquiryResponse.getAdminBank() +
                inquiryResponse.getBilling() +
                inquiryResponse.getPenalty();
        tvInquiryResponse.setTotalTagihan(total);
        BaseResponse<TvInquiryResponse> baseResponse = new BaseResponse<>();
        baseResponse.setData(tvInquiryResponse);
        baseResponse.setFee(transaksi.getFEE_DEALER());
        baseResponse.setNtrans(transaksi.getNTRANS());
        baseResponse.setSaldo(0);
        baseResponse.setSaldoTerpotong(transaksi.getCHARGE());
        baseResponse.setProduct(transaksi.getDENOM());
        baseResponse.setMessage("Successful");
        return Response.status(200).entity(baseResponse).build();
    }

    @Override
    public Response cetakUlangPulsaDI(Transaksi transaksi) {
        Map<String, String> r = new HashMap<>();
        r.put("sn", transaksi.getBILL_REF_NUMBER());
        r.put("message", String.format("SUKSES! Trx %s %s berhasil. SN: %s", transaksi.getDENOM(), transaksi.getMSSIDN(), transaksi.getBILL_REF_NUMBER()));
        BaseResponse<Map<String, String>> paymentResponse = new BaseResponse<>();
        paymentResponse.setData(r);
        paymentResponse.setNtrans(transaksi.getNTRANS());
        paymentResponse.setSaldo(0);
        paymentResponse.setMessage("Successful");
        paymentResponse.setSaldoTerpotong(transaksi.getCHARGE());
        paymentResponse.setProduct(transaksi.getDENOM());
        paymentResponse.setWaktu(transaksi.getTIME_PAYMENT());
        return Response.status(200).entity(paymentResponse).build();
    }


    private Integer findStanAkir(List<DetailItem> detail) {
        return Integer.valueOf(detail.get(detail.size()).getUsage().split("-")[1]);
    }

    private Integer findStanAwal(List<DetailItem> detail) {
        return Integer.valueOf(detail.stream()
                .findFirst().get().getUsage()
                .split("-")[0]);
    }

    private Integer findDenda(List<DetailItem> detail) {
        Integer denda = 0;
        for (int i = 0; i < detail.size(); i++) {
            denda += (Objects.equals(detail.get(i).getFine(), "null")) ? 0 : Integer.parseInt(detail.get(i).getFine());
        }
        return denda;
    }
}
