package com.trimindi.switching.gatway.services;

import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.repository.PartnerCredentialRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional
public class PartnerCredentialImpl implements PartnerCredentialService {

    PartnerCredentialRepository partnerCredentialRepository;

    public PartnerCredentialImpl(PartnerCredentialRepository partnerCredentialRepository) {
        this.partnerCredentialRepository = partnerCredentialRepository;
    }

    @Override
    @Cacheable(value = "partnerFindCache", key = "#p0")
    public PartnerCredential findById(String id) {
        return partnerCredentialRepository.findOne(id);
    }

    @Override
    public List<PartnerCredential> findAll() {
        return partnerCredentialRepository.findAll();
    }

    @Override
    public PartnerCredential save(PartnerCredential values) {
        return partnerCredentialRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        partnerCredentialRepository.delete(value);
    }

    @Override
    public void update(PartnerCredential values) {
        partnerCredentialRepository.save(values);
    }

}
