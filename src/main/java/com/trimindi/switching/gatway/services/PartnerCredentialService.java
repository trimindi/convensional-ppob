package com.trimindi.switching.gatway.services;

import com.trimindi.switching.gatway.models.PartnerCredential;
import com.trimindi.switching.gatway.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface PartnerCredentialService extends BaseService<PartnerCredential, String> {
}
