package com.trimindi.switching.gatway.services.deposit;

import com.trimindi.switching.gatway.models.PartnerDeposit;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.base.BaseService;

import javax.transaction.Transactional;

/**
 * Created by PC on 12/08/2017.
 */
public interface PartnerDepositService extends BaseService<PartnerDeposit, String> {
    @Transactional
    boolean bookingSaldo(Transaksi transaksi);
    void reverseSaldo(Transaksi transaksi, String response);

    void reverseSaldoTicket(Transaksi transaksi, String string);
}
