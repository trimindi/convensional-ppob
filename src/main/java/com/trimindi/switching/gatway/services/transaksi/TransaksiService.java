package com.trimindi.switching.gatway.services.transaksi;

import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.services.base.BaseService;

import java.util.List;

/**
 * Created by PC on 10/08/2017.
 */

public interface TransaksiService extends BaseService<Transaksi, String> {
    Double findTotalFeePartner(String partner);

    Boolean isTransaksiReadyToPay(String ntrans);

    Transaksi findByBILL_REF_NUMBER(String reg_id);

    List<Transaksi> check_if_transaksi_under_progress(String mssidn, String product);

    void delete_old_inquiry();
}
