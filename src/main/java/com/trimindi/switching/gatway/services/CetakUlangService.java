package com.trimindi.switching.gatway.services;

import com.trimindi.switching.gatway.models.Transaksi;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import java.io.IOException;


/**
 * Created by PC on 27/09/2017.
 */
public interface CetakUlangService {
    Response cetakUlangPdamRajabiller(Transaksi transaksi) throws JAXBException;

    Response cetakUlangPulsaPascaRajabiller(Transaksi transaksi) throws JAXBException;

    Response cetakUlangBpjsRajabiller(Transaksi transaksi) throws JAXBException;

    Response cetakUlangTelkomRajabiller(Transaksi transaksi) throws JAXBException;

    Response cetakUlangPlnPrepaidAranet(Transaksi transaksi);

    Response cetakUlangPlnPostpaidAranet(Transaksi transaksi);

    Response cetakUlangPlnNontaglistAranet(Transaksi transaksi);

    Response cetakUlangPulsaServindo(Transaksi transaksi) throws IOException;

    Response cetakUlangDataServindo(Transaksi transaksi) throws IOException;

    Response cetakUlangPdamTasik(Transaksi transaksi) throws IOException;

    void cetakUlangMultifinance(@Suspended AsyncResponse asyncResponse, Transaksi transaksi);

    Response cetakUlangTvDharmawisata(Transaksi transaksi) throws IOException;

    Response cetakUlangPulsaDI(Transaksi transaksi);
}
