package com.trimindi.switching.gatway.services.deposit;

import com.trimindi.switching.gatway.models.HistoryTransaction;
import com.trimindi.switching.gatway.models.PartnerDeposit;
import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.repository.HistoryTransactionRepository;
import com.trimindi.switching.gatway.repository.PartnerDepositRepository;
import com.trimindi.switching.gatway.services.transaksi.TransaksiService;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional
public class PartnerDepositServiceImpl implements PartnerDepositService {

    private final PartnerDepositRepository partnerDepositRepository;
    private final TransaksiService transaksiService;
    private final HistoryTransactionRepository historyTransactionRepository;

    public PartnerDepositServiceImpl(
            PartnerDepositRepository partnerDepositRepository,
            TransaksiService transaksiService, HistoryTransactionRepository historyTransactionRepository) {
        this.transaksiService = transaksiService;
        this.partnerDepositRepository = partnerDepositRepository;
        this.historyTransactionRepository = historyTransactionRepository;
    }

    @Override
    public PartnerDeposit findById(String id) {

        return partnerDepositRepository.findOne(id);
    }

    @Override
    public List<PartnerDeposit> findAll() {
        return partnerDepositRepository.findAll();
    }

    @Override
    public PartnerDeposit save(PartnerDeposit values) {
        return partnerDepositRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        partnerDepositRepository.delete(value);
    }

    @Override
    public void update(PartnerDeposit values) {
        if (partnerDepositRepository.exists(values.getPartner_id())) {
            partnerDepositRepository.save(values);
        }
    }

    @Override
    @Transactional
    public boolean bookingSaldo(Transaksi transaction) {
        try {
            PartnerDeposit partnerDeposito = findById(transaction.getPARTNERID());
            if (partnerDeposito.getBALANCE() < transaction.getCHARGE()) {
                return false;
            }
            double before = partnerDeposito.getBALANCE();
            double saldo = partnerDeposito.getBALANCE() - transaction.getCHARGE();
            partnerDeposito.setBALANCE(saldo);
            partnerDeposito.setKELUAR(partnerDeposito.getKELUAR() + transaction.getCHARGE());
            partnerDepositRepository.save(partnerDeposito);
            transaction.setDEBET(transaction.getCHARGE());
            transaction.setSALDO(saldo);
            transaction.setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
            transaction.setST(Status.PAYMENT_PROSESS);
            transaksiService.update(transaction);
            historyTransactionRepository.save(new HistoryTransaction()
                    .setNominal(transaction.getCHARGE())
                    .setCreateAt(new Timestamp(System.currentTimeMillis()))
                    .setNtrans(transaction.getNTRANS())
                    .setAction("BOOKING")
                    .setBefore(before)
                    .setAfter(saldo));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public void reverseSaldo(Transaksi transaction, String response) {
        try {
            if (Objects.equals(transaction.getST(), Status.PAYMENT_PROSESS)) {
                PartnerDeposit partnerDeposit = findById(transaction.getPARTNERID());
                double before = partnerDeposit.getBALANCE();
                double saldo = partnerDeposit.getBALANCE() + transaction.getCHARGE();
                partnerDeposit.setBALANCE(saldo);
                partnerDeposit.setKELUAR(partnerDeposit.getKELUAR() - transaction.getCHARGE());
                partnerDepositRepository.save(partnerDeposit);
                transaction.setPAYMENT(response);
                transaction.setKREDIT(transaction.getCHARGE());
                transaction.setSALDO(saldo);
                transaction.setST(Status.PAYMENT_FAILED);
                transaksiService.update(transaction);
                historyTransactionRepository.save(new HistoryTransaction()
                        .setNominal(transaction.getCHARGE())
                        .setCreateAt(new Timestamp(System.currentTimeMillis()))
                        .setNtrans(transaction.getNTRANS())
                        .setAction("REVERSE")
                        .setBefore(before)
                        .setAfter(saldo));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reverseSaldoTicket(Transaksi transaction, String response) {
        try {
            if (Objects.equals(transaction.getST(), Status.PAYMENT_PROSESS)) {
                PartnerDeposit partnerDeposit = findById(transaction.getPARTNERID());
                double before = partnerDeposit.getBALANCE();
                double saldo = partnerDeposit.getBALANCE() + transaction.getCHARGE();
                partnerDeposit.setBALANCE(saldo);
                partnerDeposit.setKELUAR(partnerDeposit.getKELUAR() - transaction.getCHARGE());
                partnerDepositRepository.save(partnerDeposit);
                transaction.setPAYMENT(response);
                transaction.setSALDO(saldo);
                transaction.setST(Status.INQUIRY);
                transaksiService.update(transaction);
                historyTransactionRepository.save(new HistoryTransaction()
                        .setNominal(transaction.getCHARGE())
                        .setCreateAt(new Timestamp(System.currentTimeMillis()))
                        .setNtrans(transaction.getNTRANS())
                        .setAction("REVERSE")
                        .setBefore(before)
                        .setAfter(saldo));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
