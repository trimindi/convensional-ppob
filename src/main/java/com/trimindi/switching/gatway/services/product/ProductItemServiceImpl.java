package com.trimindi.switching.gatway.services.product;

import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.repository.ProductItemRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
public class ProductItemServiceImpl implements ProductItemService {
    private final ProductItemRepository productItemRepository;

    public ProductItemServiceImpl(ProductItemRepository productItemRepository) {
        this.productItemRepository = productItemRepository;
    }

    @Override
    @Cacheable(value = "productFindCache", key = "#id")
    public ProductItem findById(String id) {
        return productItemRepository.findOne(id);
    }

    @Override
    public List<ProductItem> findAll() {
        return productItemRepository.findAll();
    }

    @Override
    public ProductItem save(ProductItem values) {
        return productItemRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        productItemRepository.delete(value);
    }

    @Override
    public void update(ProductItem values) {
        productItemRepository.delete(values);
    }

    @Override
    public void updateHarga(String denom, double hargabaru) {
        ProductItem p = findById(denom);
        p.setAMOUT(hargabaru);
        productItemRepository.save(p);
    }
}
