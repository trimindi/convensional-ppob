package com.trimindi.switching.gatway.services.fee;

import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.repository.ProductPriceRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional(readOnly = true)
public class ProductFeeServiceImpl implements ProductFeeService {


    private final ProductPriceRepository productProceRepository;

    public ProductFeeServiceImpl(ProductPriceRepository productPriceRepository) {
        this.productProceRepository = productPriceRepository;
    }

    @Override
    @Cacheable(value = "productFeeFindCache", key = "#id")
    public ProductFee findById(String id) {
        return productProceRepository.findOne(id);
    }

    @Override
    public List<ProductFee> findAll() {
        return productProceRepository.findAll();
    }

    @Override
    public ProductFee save(ProductFee values) {
        return productProceRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        productProceRepository.delete(value);
    }

    @Override
    public void update(ProductFee values) {
        productProceRepository.save(values);
    }

    @Override
    public ProductFee findFeePartner(String partner, String denom) {
        return productProceRepository.findByPrice(partner, denom);
    }
}
