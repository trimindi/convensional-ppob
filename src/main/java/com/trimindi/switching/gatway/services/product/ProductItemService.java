package com.trimindi.switching.gatway.services.product;

import com.trimindi.switching.gatway.models.ProductItem;
import com.trimindi.switching.gatway.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */

public interface ProductItemService extends BaseService<ProductItem, String> {
    void updateHarga(String denom, double hargabaru);
}
