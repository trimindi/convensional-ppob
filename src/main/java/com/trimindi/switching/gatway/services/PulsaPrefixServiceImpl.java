package com.trimindi.switching.gatway.services;

import com.trimindi.switching.gatway.repository.PulsaPrefixRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Created by PC on 04/09/2017.
 */
@Service
public class PulsaPrefixServiceImpl implements PulsaPrefixService {

    @Autowired
    PulsaPrefixRepository pulsaPrefixRepository;

    @Override
    @Cacheable(value = "isValidNumberProductCache", key = "{#product,#number}")
    public boolean isValidNumberProduct(String product, String number) {
        return pulsaPrefixRepository.findByProduct(product.substring(0, 2), number.substring(0, 4)).size() > 0;
    }
}
