package com.trimindi.switching.gatway.services.fee;

import com.trimindi.switching.gatway.models.ProductFee;
import com.trimindi.switching.gatway.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface ProductFeeService extends BaseService<ProductFee, String> {
    ProductFee findFeePartner(String partner, String denom);
}
