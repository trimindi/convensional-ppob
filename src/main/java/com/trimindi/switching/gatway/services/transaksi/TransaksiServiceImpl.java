package com.trimindi.switching.gatway.services.transaksi;

import com.trimindi.switching.gatway.models.Transaksi;
import com.trimindi.switching.gatway.repository.TransaksiRepository;
import com.trimindi.switching.gatway.utils.constanta.Status;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional
public class TransaksiServiceImpl implements TransaksiService {


    private TransaksiRepository transaksiRepository;

    public TransaksiServiceImpl(TransaksiRepository transaksiRepository) {
        this.transaksiRepository = transaksiRepository;
    }

    @Override
    public Transaksi findById(String id) {
        return transaksiRepository.findOne(id);
    }

    @Override
    public List<Transaksi> findAll() {
        return transaksiRepository.findAll();
    }

    @Override
    public Transaksi save(Transaksi values) {
        return transaksiRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        transaksiRepository.delete(value);
    }

    @Override
    @Transactional
    public void update(Transaksi values) {
        transaksiRepository.save(values);
    }

    @Override
    public Double findTotalFeePartner(String partner) {
        return transaksiRepository.findTotalFee(partner);
    }

    @Override
    public Boolean isTransaksiReadyToPay(String ntrans) {
        Transaksi transaksi = transaksiRepository.findOne(ntrans);
        switch (transaksi.getST()) {
            case Status.INQUIRY:
                return true;
            case Status.PAYMENT_FAILED:
                return false;
            case Status.PAYMENT_SUCCESS:
                return false;
            case Status.PAYMENT_PROSESS:
                return false;
        }
        return false;
    }

    @Override
    public Transaksi findByBILL_REF_NUMBER(String reg_id) {
        return transaksiRepository.findByBILL_REF_NUMBER(reg_id);
    }

    @Override
    public List<Transaksi> check_if_transaksi_under_progress(String mssidn, String product) {
        return transaksiRepository.checkIfTransactionUnderProses(mssidn, product, new Date());
    }

    @Override
    public void delete_old_inquiry() {
        Date date = new Date(System.currentTimeMillis() - 180 * 1000);
        transaksiRepository.deleteAllByST(date);
    }

}
