package com.trimindi.switching.gatway;

import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by sx on 08/01/18.
 * Copyright under comercial unit
 */
public @Component
class SlackSendMessage {

    @Value("${logging.slack.webhook.name}")
    String name;

    @Autowired
    SlackApi slackApi;
    public static final Logger logger = LoggerFactory.getLogger(SlackSendMessage.class);


    public void sendMessageStachTrace(String message, Exception ex) {
        StringBuilder stringBuilder = new StringBuilder();
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        stringBuilder.append("*");
        stringBuilder.append(name + "*\n");
        stringBuilder.append(message + "\n");
        stringBuilder.append("*Stack Trace :*\n");
        stringBuilder.append("```");
        stringBuilder.append(errors.toString());
        stringBuilder.append("```");
        slackApi.call(new SlackMessage(name, stringBuilder.toString()));
    }

    public void sendMessage(String message) {
        slackApi.call(new SlackMessage(name, message));
    }
}
