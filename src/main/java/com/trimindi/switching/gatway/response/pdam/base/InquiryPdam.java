package com.trimindi.switching.gatway.response.pdam.base;

import com.trimindi.switching.gatway.biller.pdamtasik.DetailItem;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by sx on 04/10/17.
 * Copyright under comercial unit
 */
@XmlRootElement
public class InquiryPdam {
    private String idpelanggan = "";
    private String namapelanggan = "";
    private String tarif = "";
    private String periode = "";
    private String alamat = "";
    private String type = "";
    private Integer tagihan = 0;
    private Integer admin = 0;
    private Integer beban = 0;
    private Integer totalTagihan = 0;
    private String keterangan = "";
    private Integer jumlahBill = 0;
    private Integer standAwal = 0;
    private Integer standAkir = 0;
    private Integer pemakaian = 0;
    private Integer materai = 0;
    private Integer denda = 0;
    private String lainlain = "";
    private List<RinciansPDAM> rincians = new ArrayList<>();

    public Integer getMaterai() {
        return materai;
    }

    public InquiryPdam setMaterai(Integer materai) {
        this.materai = materai;
        return this;
    }

    public Integer getDenda() {
        return denda;
    }

    public InquiryPdam setDenda(Integer denda) {
        this.denda = denda;
        return this;
    }

    public InquiryPdam() {
    }

    public Integer getStandAwal() {
        return standAwal;
    }

    public InquiryPdam setStandAwal(Integer standAwal) {
        this.standAwal = standAwal;
        return this;
    }

    public Integer getStandAkir() {
        return standAkir;
    }

    public InquiryPdam setStandAkir(Integer standAkir) {
        this.standAkir = standAkir;
        return this;
    }

    public Integer getPemakaian() {
        return standAkir - standAwal;
    }

    public InquiryPdam setPemakaian(Integer pemakaian) {
        this.pemakaian = pemakaian;
        return this;
    }

    public String getLainlain() {
        return lainlain;
    }

    public InquiryPdam setLainlain(String lainlain) {
        this.lainlain = lainlain;
        return this;
    }

    public String getIdpelanggan() {
        return idpelanggan;
    }

    public InquiryPdam setIdpelanggan(String idpelanggan) {
        this.idpelanggan = idpelanggan;
        return this;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public InquiryPdam setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
        return this;
    }

    public String getTarif() {
        return tarif;
    }

    public InquiryPdam setTarif(String tarif) {
        this.tarif = tarif;
        return this;
    }

    public String getPeriode() {
        return periode;
    }

    public InquiryPdam setPeriode(String periode) {
        this.periode = periode;
        return this;
    }

    public String getAlamat() {
        return alamat;
    }

    public InquiryPdam setAlamat(String alamat) {
        this.alamat = alamat;
        return this;
    }

    public String getType() {
        return type;
    }

    public InquiryPdam setType(String type) {
        this.type = type;
        return this;
    }

    public Integer getTagihan() {
        return tagihan;
    }

    public InquiryPdam setTagihan(Integer tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public Integer getAdmin() {
        return admin;
    }

    public InquiryPdam setAdmin(Integer admin) {
        this.admin = admin;
        return this;
    }

    public Integer getBeban() {
        return beban;
    }

    public InquiryPdam setBeban(Integer beban) {
        this.beban = beban;
        return this;
    }

    public Integer getTotalTagihan() {
        return totalTagihan;
    }

    public InquiryPdam setTotalTagihan(Integer totalTagihan) {
        this.totalTagihan = totalTagihan;
        return this;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public InquiryPdam setKeterangan(String keterangan) {
        this.keterangan = keterangan;
        return this;
    }

    public Integer getJumlahBill() {
        return jumlahBill;
    }

    public InquiryPdam setJumlahBill(Integer jumlahBill) {
        this.jumlahBill = jumlahBill;
        return this;
    }

    public List<RinciansPDAM> getRincians() {
        return rincians;
    }

    public InquiryPdam setRincians(List<DetailItem> detail) {
        this.rincians = detail.stream()
                .map(v -> new RinciansPDAM(v.getPeriod(), (v.getFine() == null ? 0 : Double.valueOf(v.getFine())), v.getBillamount(), v.getUsage()))
                .collect(Collectors.toList());
        return this;
    }
}
