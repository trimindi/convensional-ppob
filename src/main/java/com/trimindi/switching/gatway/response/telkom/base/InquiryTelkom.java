package com.trimindi.switching.gatway.response.telkom.base;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 06/10/17.
 * Copyright under comercial unit
 */
@XmlRootElement
public class InquiryTelkom {
    private String idpelanggan = "";
    private String namapelanggan = "";
    private String periode = "";
    private Integer tagihan = 0;
    private Integer admin = 0;
    private Integer totalTagihan = 0;
    private String keterangan = "";
    private Integer jumlahBill = 0;
    private Integer denda = 0;
    private String layanan = "TelkomPay";
    private String divre = "";
    private String datel = "";
    private Integer beban = 0;
    private Integer materai = 0;
    private Integer lainlain = 0;
    private String npwp = "";
    public InquiryTelkom() {
    }

    public String getNpwp() {
        return npwp;
    }

    public InquiryTelkom setNpwp(String npwp) {
        this.npwp = npwp;
        return this;
    }

    public Integer getBeban() {
        return beban;
    }

    public InquiryTelkom setBeban(Integer beban) {
        this.beban = beban;
        return this;
    }

    public Integer getMaterai() {
        return materai;
    }

    public InquiryTelkom setMaterai(Integer materai) {
        this.materai = materai;
        return this;
    }

    public Integer getLainlain() {
        return lainlain;
    }

    public InquiryTelkom setLainlain(Integer lainlain) {
        this.lainlain = lainlain;
        return this;
    }

    public String getIdpelanggan() {
        return idpelanggan;
    }

    public InquiryTelkom setIdpelanggan(String idpelanggan) {
        this.idpelanggan = idpelanggan;
        return this;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public InquiryTelkom setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
        return this;
    }

    public String getPeriode() {
        return periode;
    }

    public InquiryTelkom setPeriode(String periode) {
        this.periode = periode;
        return this;
    }

    public Integer getTagihan() {
        return tagihan;
    }

    public InquiryTelkom setTagihan(Integer tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public Integer getAdmin() {
        return admin;
    }

    public InquiryTelkom setAdmin(Integer admin) {
        this.admin = admin;
        return this;
    }

    public Integer getTotalTagihan() {
        return totalTagihan;
    }

    public InquiryTelkom setTotalTagihan(Integer totalTagihan) {
        this.totalTagihan = totalTagihan;
        return this;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public InquiryTelkom setKeterangan(String keterangan) {
        this.keterangan = keterangan;
        return this;
    }

    public Integer getJumlahBill() {
        return jumlahBill;
    }

    public InquiryTelkom setJumlahBill(Integer jumlahBill) {
        this.jumlahBill = jumlahBill;
        return this;
    }

    public Integer getDenda() {
        return denda;
    }

    public InquiryTelkom setDenda(Integer denda) {
        this.denda = denda;
        return this;
    }

    public String getLayanan() {
        return layanan;
    }

    public InquiryTelkom setLayanan(String layanan) {
        this.layanan = layanan;
        return this;
    }

    public String getDivre() {
        return divre;
    }

    public InquiryTelkom setDivre(String divre) {
        this.divre = divre;
        return this;
    }

    public String getDatel() {
        return datel;
    }

    public InquiryTelkom setDatel(String datel) {
        this.datel = datel;
        return this;
    }
}
