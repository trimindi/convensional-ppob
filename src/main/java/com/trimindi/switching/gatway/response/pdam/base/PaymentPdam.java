package com.trimindi.switching.gatway.response.pdam.base;

import com.trimindi.switching.gatway.biller.pdamtasik.DetailItem;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by sx on 04/10/17.
 * Copyright under comercial unit
 */
@XmlRootElement
public class PaymentPdam {
    private String idpelanggan = "";
    private String namapelanggan = "";
    private String tarif = "";
    private String periode = "";
    private String alamat = "";
    private String type = "";
    private Integer tagihan = 0;
    private Integer admin = 0;
    private Integer beban = 0;
    private Integer totalTagihan = 0;
    private String keterangan = "";
    private Integer jumlahBill = 0;
    private Integer standAwal = 0;
    private Integer standAkir = 0;
    private Integer pemakaian = 0;
    private Integer materai = 0;
    private Integer denda = 0;
    private Integer lainlain = 0;
    private List<RinciansPDAM> rincians = new ArrayList<>();
    private String reff = "";

    public Integer getMaterai() {
        return materai;
    }

    public PaymentPdam setMaterai(Integer materai) {
        this.materai = materai;
        return this;
    }

    public Integer getDenda() {
        return denda;
    }

    public PaymentPdam setDenda(Integer denda) {
        this.denda = denda;
        return this;
    }

    public String getIdpelanggan() {
        return idpelanggan;
    }

    public Integer getStandAwal() {
        return standAwal;
    }

    public PaymentPdam setStandAwal(Integer standAwal) {
        this.standAwal = standAwal;
        return this;
    }

    public Integer getStandAkir() {
        return standAkir;
    }

    public PaymentPdam setStandAkir(Integer standAkir) {
        this.standAkir = standAkir;
        return this;
    }

    public Integer getPemakaian() {
        return standAkir - standAwal;
    }

    public PaymentPdam setPemakaian(Integer pemakaian) {
        this.pemakaian = pemakaian;
        return this;
    }

    public Integer getLainlain() {
        return lainlain;
    }

    public PaymentPdam setLainlain(Integer lainlain) {
        this.lainlain = lainlain;
        return this;
    }

    public PaymentPdam setIdpelanggan(String idpelanggan) {
        this.idpelanggan = idpelanggan;
        return this;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public PaymentPdam setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
        return this;
    }

    public String getTarif() {
        return tarif;
    }

    public PaymentPdam setTarif(String tarif) {
        this.tarif = tarif;
        return this;
    }

    public String getPeriode() {
        return periode;
    }

    public PaymentPdam setPeriode(String periode) {
        this.periode = periode;
        return this;
    }

    public String getAlamat() {
        return alamat;
    }

    public PaymentPdam setAlamat(String alamat) {
        this.alamat = alamat;
        return this;
    }

    public String getType() {
        return type;
    }

    public PaymentPdam setType(String type) {
        this.type = type;
        return this;
    }

    public Integer getTagihan() {
        return tagihan;
    }

    public PaymentPdam setTagihan(Integer tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public Integer getAdmin() {
        return admin;
    }

    public PaymentPdam setAdmin(Integer admin) {
        this.admin = admin;
        return this;
    }

    public Integer getBeban() {
        return beban;
    }

    public PaymentPdam setBeban(Integer beban) {
        this.beban = beban;
        return this;
    }

    public Integer getTotalTagihan() {
        return totalTagihan;
    }

    public PaymentPdam setTotalTagihan(Integer totalTagihan) {
        this.totalTagihan = totalTagihan;
        return this;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public PaymentPdam setKeterangan(String keterangan) {
        this.keterangan = keterangan;
        return this;
    }

    public Integer getJumlahBill() {
        return jumlahBill;
    }

    public PaymentPdam setJumlahBill(Integer jumlahBill) {
        this.jumlahBill = jumlahBill;
        return this;
    }

    public List<RinciansPDAM> getRincians() {
        return rincians;
    }

    public PaymentPdam setRincians(List<DetailItem> detail) {
        this.rincians = detail.stream()
                .map(v -> new RinciansPDAM(v.getPeriod(), (v.getFine() == null ? 0 : Double.valueOf(v.getFine())), v.getBillamount(), v.getUsage()))
                .collect(Collectors.toList());
        return this;
    }

    public String getReff() {
        return reff;
    }

    public PaymentPdam setReff(String reff) {
        this.reff = reff;
        return this;
    }
}
