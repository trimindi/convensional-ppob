package com.trimindi.switching.gatway.response.bpjs;

import com.trimindi.switching.gatway.biller.rajabiller.response.*;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 28/11/17.
 * Copyright under comercial unit
 */
@XmlRootElement
public class PaymentBpjs {
    private String idpelanggan = "";
    private String namapelanggan = "";
    private String periode = "";
    private Integer tagihan = 0;
    private Integer admin = 0;
    private Integer totalTagihan = 0;
    private String keterangan = "";
    private Integer denda = 0;
    private Integer beban = 0;
    private Integer materai = 0;
    private Integer lainlain = 0;
    private String idreff = "";
    private String status;

    public String getStatus() {
        return status;
    }

    public PaymentBpjs setStatus(String status) {
        this.status = status;
        return this;
    }

    public PaymentBpjs() {
    }

    public PaymentBpjs(MethodResponse methodResponse) {
        Data d = methodResponse.getParams().getParam().getValue().getArray().getData();
        Value[] v = d.getValue();
        this.idpelanggan = v[2].getString();
        this.namapelanggan = v[5].getString();
        this.periode = v[6].getString();
        this.tagihan = Integer.valueOf(v[7].getString());
        this.admin = Integer.valueOf(v[8].getString());
        this.totalTagihan = this.tagihan + this.admin;
        this.keterangan = v[15].getString();
        this.status = v[14].getString();
        Struct struct = v[19].getStruct();
        Member[] member = struct.getMember();
        this.idreff = member[2].getValue().getString();
    }

    public String getIdpelanggan() {
        return idpelanggan;
    }

    public PaymentBpjs setIdpelanggan(String idpelanggan) {
        this.idpelanggan = idpelanggan;
        return this;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public PaymentBpjs setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
        return this;
    }

    public String getPeriode() {
        return periode;
    }

    public PaymentBpjs setPeriode(String periode) {
        this.periode = periode;
        return this;
    }

    public Integer getTagihan() {
        return tagihan;
    }

    public PaymentBpjs setTagihan(Integer tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public Integer getAdmin() {
        return admin;
    }

    public PaymentBpjs setAdmin(Integer admin) {
        this.admin = admin;
        return this;
    }

    public Integer getTotalTagihan() {
        return totalTagihan;
    }

    public PaymentBpjs setTotalTagihan(Integer totalTagihan) {
        this.totalTagihan = totalTagihan;
        return this;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public PaymentBpjs setKeterangan(String keterangan) {
        this.keterangan = keterangan;
        return this;
    }

    public Integer getDenda() {
        return denda;
    }

    public PaymentBpjs setDenda(Integer denda) {
        this.denda = denda;
        return this;
    }

    public Integer getBeban() {
        return beban;
    }

    public PaymentBpjs setBeban(Integer beban) {
        this.beban = beban;
        return this;
    }

    public Integer getMaterai() {
        return materai;
    }

    public PaymentBpjs setMaterai(Integer materai) {
        this.materai = materai;
        return this;
    }

    public Integer getLainlain() {
        return lainlain;
    }

    public PaymentBpjs setLainlain(Integer lainlain) {
        this.lainlain = lainlain;
        return this;
    }

    public String getIdreff() {
        return idreff;
    }

    public PaymentBpjs setIdreff(String idreff) {
        this.idreff = idreff;
        return this;
    }
}
