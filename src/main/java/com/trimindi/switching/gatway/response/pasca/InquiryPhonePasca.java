package com.trimindi.switching.gatway.response.pasca;

import com.trimindi.switching.gatway.biller.rajabiller.response.Data;
import com.trimindi.switching.gatway.biller.rajabiller.response.MethodResponse;
import com.trimindi.switching.gatway.biller.rajabiller.response.Value;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by sx on 28/11/17.
 * Copyright under comercial unit
 */
@XmlRootElement
public class InquiryPhonePasca {
    private String idpelanggan = "";
    private String namapelanggan = "";
    private String periode = "";
    private Integer tagihan = 0;
    private Integer admin = 0;
    private Integer totalTagihan = 0;
    private String keterangan = "";
    private Integer denda = 0;
    private Integer beban = 0;
    private Integer materai = 0;
    private Integer lainlain = 0;
    private String STATUS;
    private String kodeProduct;
    private String ref1;
    private String ref2;
    private String ref3;
    public InquiryPhonePasca() {
    }

    public InquiryPhonePasca(MethodResponse methodResponse) {
        Data d = methodResponse.getParams().getParam().getValue().getArray().getData();
        Value[] v = d.getValue();
        this.idpelanggan = v[2].getString();
        this.namapelanggan = v[5].getString();
        this.periode = v[6].getString();
        this.tagihan = Integer.valueOf(v[7].getString());
        this.admin = Integer.valueOf(v[8].getString());
        this.totalTagihan = this.tagihan + this.admin;
        this.keterangan = v[15].getString();
        this.STATUS = v[14].getString();
        this.ref1 = v[11].getString();
        this.ref2 = v[12].getString();
        this.ref3 = v[13].getString();
        this.kodeProduct = v[0].getString();
    }

    @XmlTransient
    public String getRef1() {
        return ref1;
    }

    public InquiryPhonePasca setRef1(String ref1) {
        this.ref1 = ref1;
        return this;
    }

    @XmlTransient
    public String getRef2() {
        return ref2;
    }

    public InquiryPhonePasca setRef2(String ref2) {
        this.ref2 = ref2;
        return this;
    }

    @XmlTransient
    public String getRef3() {
        return ref3;
    }

    public InquiryPhonePasca setRef3(String ref3) {
        this.ref3 = ref3;
        return this;
    }

    @XmlTransient
    public String getKodeProduct() {
        return kodeProduct;
    }

    public InquiryPhonePasca setKodeProduct(String kodeProduct) {
        this.kodeProduct = kodeProduct;
        return this;
    }

    @XmlTransient
    public String getSTATUS() {
        return STATUS;
    }

    public InquiryPhonePasca setSTATUS(String STATUS) {
        this.STATUS = STATUS;
        return this;
    }

    public String getIdpelanggan() {
        return idpelanggan;
    }

    public InquiryPhonePasca setIdpelanggan(String idpelanggan) {
        this.idpelanggan = idpelanggan;
        return this;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public InquiryPhonePasca setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
        return this;
    }

    public String getPeriode() {
        return periode;
    }

    public InquiryPhonePasca setPeriode(String periode) {
        this.periode = periode;
        return this;
    }

    public Integer getTagihan() {
        return tagihan;
    }

    public InquiryPhonePasca setTagihan(Integer tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public Integer getAdmin() {
        return admin;
    }

    public InquiryPhonePasca setAdmin(Integer admin) {
        this.admin = admin;
        return this;
    }

    public Integer getTotalTagihan() {
        return totalTagihan;
    }

    public InquiryPhonePasca setTotalTagihan(Integer totalTagihan) {
        this.totalTagihan = totalTagihan;
        return this;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public InquiryPhonePasca setKeterangan(String keterangan) {
        this.keterangan = keterangan;
        return this;
    }

    public Integer getDenda() {
        return denda;
    }

    public InquiryPhonePasca setDenda(Integer denda) {
        this.denda = denda;
        return this;
    }

    public Integer getBeban() {
        return beban;
    }

    public InquiryPhonePasca setBeban(Integer beban) {
        this.beban = beban;
        return this;
    }

    public Integer getMaterai() {
        return materai;
    }

    public InquiryPhonePasca setMaterai(Integer materai) {
        this.materai = materai;
        return this;
    }

    public Integer getLainlain() {
        return lainlain;
    }

    public InquiryPhonePasca setLainlain(Integer lainlain) {
        this.lainlain = lainlain;
        return this;
    }
}
