package com.trimindi.switching.gatway.response.pdam;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/19/2017.
 */
@XmlRootElement
public class PaymentResponse {
    private String code = "0000";
    private String ntrans;
    private double tagihan;
    private double fee;
    private double totalBayar;
    private double saldo;
    private double totalFee;
    private Payment data;
    private String product;

    public PaymentResponse() {
    }

    public String getProduct() {
        return product;
    }

    public PaymentResponse setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getCode() {
        return code;
    }

    public PaymentResponse setCode(String code) {
        this.code = code;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public PaymentResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }


    public double getTagihan() {
        return tagihan;
    }

    public PaymentResponse setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public PaymentResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getTotalBayar() {
        return totalBayar;
    }

    public PaymentResponse setTotalBayar(double totalBayar) {
        this.totalBayar = totalBayar;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PaymentResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public PaymentResponse setTotalFee(double totalFee) {
        this.totalFee = totalFee;
        return this;
    }

    public Payment getData() {
        return data;
    }

    public PaymentResponse setData(Payment data) {
        this.data = data;
        return this;
    }
}
