package com.trimindi.switching.gatway.response;

import com.trimindi.switching.gatway.utils.TimestampAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Timestamp;

/**
 * Created by PC on 29/08/2017.
 */
@XmlRootElement
public class BaseResponse<T> {
    @XmlElement(name = "waktu", required = true, nillable = true)
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    private Timestamp waktu = new Timestamp(System.currentTimeMillis());
    private String status = "0000";
    private String ntrans = "";
    private double saldoTerpotong = 0;
    private double saldo = 0;
    private String product = "";
    private String message = "";
    private double fee = 0;
    private T data;

    public BaseResponse() {
    }

    public double getFee() {
        return fee;
    }

    public BaseResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public BaseResponse setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public BaseResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public Timestamp getWaktu() {
        return waktu;
    }

    public BaseResponse setWaktu(Timestamp waktu) {
        this.waktu = waktu;
        return this;
    }

    public double getSaldoTerpotong() {
        return saldoTerpotong;
    }

    public BaseResponse setSaldoTerpotong(double saldoTerpotong) {
        this.saldoTerpotong = saldoTerpotong;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public BaseResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public BaseResponse setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public BaseResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public BaseResponse setData(T data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "status='" + status + '\'' +
                ", ntrans='" + ntrans + '\'' +
                ", saldoTerpotong=" + saldoTerpotong +
                ", saldo=" + saldo +
                ", product='" + product + '\'' +
                ", message='" + message + '\'' +
                ", fee=" + fee +
                ", data=" + data +
                '}';
    }
}
