package com.trimindi.switching.gatway.response.pdam;

import com.trimindi.switching.gatway.biller.rajabiller.response.Data;
import com.trimindi.switching.gatway.biller.rajabiller.response.MethodResponse;
import com.trimindi.switching.gatway.biller.rajabiller.response.Value;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by PC on 6/19/2017.
 */

@XmlRootElement
public class Inquiry {
    private String KODEPRODUK;
    private String WAKTU;
    private String IDPELANGGAN1;
    private String IDPELANGGAN;
    private String IDPELANGGAN3;
    private String NAMAPELANGGAN;
    private String PERIODE;
    private double TAGIHAN;
    private double ADMIN;
    private String UID;
    private String PIN;
    private String REFF;
    private String REF2;
    private String REF3;
    private String STATUS;
    private String KETERANGAN;
    private String SALDOTERPOTONG;
    private String SISASALDO;
    private String URLSTRUK;

    public Inquiry() {
    }

    public Inquiry(MethodResponse methodResponse) {
        Data d = methodResponse.getParams().getParam().getValue().getArray().getData();
        Value[] v = d.getValue();
        this.KODEPRODUK = v[0].getString();
        this.WAKTU = v[1].getString();
        this.IDPELANGGAN1 = v[2].getString();
        this.IDPELANGGAN = v[3].getString();
        this.IDPELANGGAN3 = v[4].getString();
        this.NAMAPELANGGAN = v[5].getString();
        this.PERIODE = v[6].getString();
        this.TAGIHAN = Double.parseDouble(v[7].getString());
        this.ADMIN = Double.parseDouble(v[8].getString());
        this.UID = v[9].getString();
        this.PIN = v[10].getString();
        this.REFF = v[11].getString();
        this.REF2 = v[12].getString();
        this.REF3 = v[13].getString();
        this.STATUS = v[14].getString();
        this.KETERANGAN = v[15].getString();
        this.SALDOTERPOTONG = v[16].getString();
        this.SISASALDO = v[17].getString();
        this.URLSTRUK = v[18].getString();
    }


    @XmlTransient
    public String getKODEPRODUK() {
        return KODEPRODUK;
    }

    public Inquiry setKODEPRODUK(String KODEPRODUK) {
        this.KODEPRODUK = KODEPRODUK;
        return this;
    }

    @XmlTransient
    public String getWAKTU() {
        return WAKTU;
    }

    public Inquiry setWAKTU(String WAKTU) {
        this.WAKTU = WAKTU;
        return this;
    }

    @XmlTransient
    public String getIDPELANGGAN1() {
        return IDPELANGGAN1;
    }

    public Inquiry setIDPELANGGAN1(String IDPELANGGAN1) {
        this.IDPELANGGAN1 = IDPELANGGAN1;
        return this;
    }


    public String getIDPELANGGAN() {
        return IDPELANGGAN;
    }

    public Inquiry setIDPELANGGAN(String IDPELANGGAN) {
        this.IDPELANGGAN = IDPELANGGAN;
        return this;
    }

    @XmlTransient
    public String getIDPELANGGAN3() {
        return IDPELANGGAN3;
    }

    public Inquiry setIDPELANGGAN3(String IDPELANGGAN3) {
        this.IDPELANGGAN3 = IDPELANGGAN3;
        return this;
    }

    public String getNAMAPELANGGAN() {
        return NAMAPELANGGAN;
    }

    public Inquiry setNAMAPELANGGAN(String NAMAPELANGGAN) {
        this.NAMAPELANGGAN = NAMAPELANGGAN;
        return this;
    }

    public String getPERIODE() {
        return PERIODE;
    }

    public Inquiry setPERIODE(String PERIODE) {
        this.PERIODE = PERIODE;
        return this;
    }

    public double getTAGIHAN() {
        return TAGIHAN;
    }

    public Inquiry setTAGIHAN(double TAGIHAN) {
        this.TAGIHAN = TAGIHAN;
        return this;
    }

    public double getADMIN() {
        return ADMIN;
    }

    public Inquiry setADMIN(double ADMIN) {
        this.ADMIN = ADMIN;
        return this;
    }


    @XmlTransient
    public String getUID() {
        return UID;
    }


    public Inquiry setUID(String UID) {
        this.UID = UID;
        return this;
    }


    @XmlTransient
    public String getPIN() {
        return PIN;
    }

    public Inquiry setPIN(String PIN) {
        this.PIN = PIN;
        return this;
    }

    @XmlTransient
    public String getREFF() {
        return REFF;
    }

    public Inquiry setREFF(String REFF) {
        this.REFF = REFF;
        return this;
    }

    @XmlTransient
    public String getREF2() {
        return REF2;
    }

    public Inquiry setREF2(String REF2) {
        this.REF2 = REF2;
        return this;
    }

    @XmlTransient
    public String getREF3() {
        return REF3;
    }

    public Inquiry setREF3(String REF3) {
        this.REF3 = REF3;
        return this;
    }

    @XmlTransient
    public String getSTATUS() {
        return STATUS;
    }

    public Inquiry setSTATUS(String STATUS) {
        this.STATUS = STATUS;
        return this;
    }

    public String getKETERANGAN() {
        return KETERANGAN;
    }

    public Inquiry setKETERANGAN(String KETERANGAN) {
        this.KETERANGAN = KETERANGAN;
        return this;
    }


    @XmlTransient
    public String getSALDOTERPOTONG() {
        return SALDOTERPOTONG;
    }

    public Inquiry setSALDOTERPOTONG(String SALDOTERPOTONG) {
        this.SALDOTERPOTONG = SALDOTERPOTONG;
        return this;
    }


    @XmlTransient
    public String getSISASALDO() {
        return SISASALDO;
    }

    public Inquiry setSISASALDO(String SISASALDO) {
        this.SISASALDO = SISASALDO;
        return this;
    }

    @XmlTransient
    public String getURLSTRUK() {
        return URLSTRUK;
    }

    public Inquiry setURLSTRUK(String URLSTRUK) {
        this.URLSTRUK = URLSTRUK;
        return this;
    }
}
