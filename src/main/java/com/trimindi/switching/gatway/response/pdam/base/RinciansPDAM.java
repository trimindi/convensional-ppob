package com.trimindi.switching.gatway.response.pdam.base;

/**
 * Created by sx on 04/10/17.
 * Copyright under comercial unit
 */
public class RinciansPDAM {
    private String periode = "";
    private double denda = 0L;
    private double tagihan = 0L;
    private String usage = "";

    public RinciansPDAM(String periode, double denda, double tagihan, String usage) {
        this.periode = periode;
        this.denda = denda;
        this.tagihan = tagihan;
        this.usage = usage;
    }

    public RinciansPDAM() {
    }

    public String getPeriode() {
        return periode;
    }

    public RinciansPDAM setPeriode(String periode) {
        this.periode = periode;
        return this;
    }

    public double getDenda() {
        return denda;
    }

    public RinciansPDAM setDenda(double denda) {
        this.denda = denda;
        return this;
    }

    public double getTagihan() {
        return tagihan;
    }

    public RinciansPDAM setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public String getUsage() {
        return usage;
    }

    public RinciansPDAM setUsage(String usage) {
        this.usage = usage;
        return this;
    }
}
