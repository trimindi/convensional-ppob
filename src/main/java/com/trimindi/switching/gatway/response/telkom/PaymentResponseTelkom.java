package com.trimindi.switching.gatway.response.telkom;

/**
 * Created by PC on 6/21/2017.
 */

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/19/2017.
 */
@XmlRootElement
public class PaymentResponseTelkom {
    private String code = "0000";
    private String ntrans;
    private double tagihan;
    private double fee;
    private double totalBayar;
    private double saldo;
    private double totalFee;
    private PaymentTelkom data;
    private String product;

    public PaymentResponseTelkom() {
    }

    public String getProduct() {
        return product;
    }

    public PaymentResponseTelkom setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getCode() {
        return code;
    }

    public PaymentResponseTelkom setCode(String code) {
        this.code = code;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public PaymentResponseTelkom setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getTagihan() {
        return tagihan;
    }

    public PaymentResponseTelkom setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public PaymentResponseTelkom setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getTotalBayar() {
        return totalBayar;
    }

    public PaymentResponseTelkom setTotalBayar(double totalBayar) {
        this.totalBayar = totalBayar;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PaymentResponseTelkom setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public PaymentResponseTelkom setTotalFee(double totalFee) {
        this.totalFee = totalFee;
        return this;
    }

    public PaymentTelkom getData() {
        return data;
    }

    public PaymentResponseTelkom setData(PaymentTelkom data) {
        this.data = data;
        return this;
    }
}
