package com.trimindi.switching.gatway.response.mutifinance;

import com.trimindi.switching.gatway.utils.generator.BaseHelper;
import com.trimindi.switching.gatway.utils.iso.models.Rules;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sx on 19/10/17.
 * Copyright under comercial unit
 */
public class Inquiry extends BaseHelper implements Serializable {
    private String SwitcherID;
    private String BillerCode;
    private String CustomerID;
    private String GWReferenceNumber;
    private String SWReferenceNumber;
    private String CustomerName;
    private String ProductCategory;
    private Integer MinorUnit;
    private Double BillAmount;
    private Double StampDuty;
    private Double PPN;
    private Double AdminCharges;
    private String BillerReferenceNumber;
    private String PTName;
    private String BranchName;
    private String ItemMerkType;
    private String ChasisNumber;
    private String CarNumber;
    private String Tenor;
    private String LastPaidPeriod;
    private String LastPaidDueDate;
    private Integer MinorUnit2;
    private Double OSInstallmentAmount;
    private Double ODInstallmentPeriod;
    private Double ODInstallmentAmount;
    private Double ODPenaltyFee;
    private Double BillerAdminFee;
    private Double MiscFee;
    private Double MinimumPayAmount;
    private Double MaximumPayAmount;

    public Inquiry() {
    }

    public Inquiry(List<Rules> rules) {
        this.SwitcherID = (String) rules.get(0).getResults();
        this.BillerCode = (String) rules.get(1).getResults();
        this.CustomerID = (String) rules.get(2).getResults();
        this.GWReferenceNumber = (String) rules.get(3).getResults();
        this.SWReferenceNumber = (String) rules.get(4).getResults();
        this.CustomerName = (String) rules.get(5).getResults();
        this.ProductCategory = (String) rules.get(6).getResults();
        this.MinorUnit = Integer.valueOf((String) rules.get(7).getResults());
        this.BillAmount = numberMinorUnit((String) rules.get(8).getResults(), this.getMinorUnit());
        this.StampDuty = numberMinorUnit((String) rules.get(9).getResults(), this.getMinorUnit());
        this.PPN = numberMinorUnit((String) rules.get(10).getResults(), this.getMinorUnit());
        this.AdminCharges = Double.valueOf((String) rules.get(11).getResults());
        this.BillerReferenceNumber = (String) rules.get(12).getResults();
        this.PTName = (String) rules.get(13).getResults();
        this.BranchName = (String) rules.get(14).getResults();
        this.ItemMerkType = (String) rules.get(15).getResults();
        this.ChasisNumber = (String) rules.get(16).getResults();
        this.CarNumber = (String) rules.get(17).getResults();
        this.Tenor = (String) rules.get(18).getResults();
        this.LastPaidPeriod = (String) rules.get(19).getResults();
        this.LastPaidDueDate = (String) rules.get(20).getResults();
        this.MinorUnit2 = Integer.valueOf((String) rules.get(21).getResults());
        this.OSInstallmentAmount = numberMinorUnit((String) rules.get(22).getResults(), this.getMinorUnit2());
        this.ODInstallmentPeriod = numberMinorUnit((String) rules.get(23).getResults(), this.getMinorUnit2());
        this.ODInstallmentAmount = numberMinorUnit((String) rules.get(24).getResults(), this.getMinorUnit2());
        this.ODPenaltyFee = numberMinorUnit((String) rules.get(25).getResults(), this.getMinorUnit2());
        this.BillerAdminFee = numberMinorUnit((String) rules.get(26).getResults(), this.getMinorUnit2());
        this.MiscFee = numberMinorUnit((String) rules.get(27).getResults(), this.getMinorUnit2());
        this.MinimumPayAmount = numberMinorUnit((String) rules.get(28).getResults(), this.getMinorUnit2());
        this.MaximumPayAmount = numberMinorUnit((String) rules.get(29).getResults(), this.getMinorUnit2());
    }

    @Override
    public String toString() {
        return "Inquiry{" +
                "SwitcherID='" + SwitcherID + '\'' +
                ", BillerCode='" + BillerCode + '\'' +
                ", CustomerID='" + CustomerID + '\'' +
                ", GWReferenceNumber='" + GWReferenceNumber + '\'' +
                ", SWReferenceNumber='" + SWReferenceNumber + '\'' +
                ", CustomerName='" + CustomerName + '\'' +
                ", ProductCategory='" + ProductCategory + '\'' +
                ", MinorUnit=" + MinorUnit +
                ", BillAmount=" + BillAmount +
                ", StampDuty=" + StampDuty +
                ", PPN=" + PPN +
                ", AdminCharges=" + AdminCharges +
                ", BillerReferenceNumber='" + BillerReferenceNumber + '\'' +
                ", PTName='" + PTName + '\'' +
                ", BranchName='" + BranchName + '\'' +
                ", ItemMerkType='" + ItemMerkType + '\'' +
                ", ChasisNumber='" + ChasisNumber + '\'' +
                ", CarNumber='" + CarNumber + '\'' +
                ", Tenor='" + Tenor + '\'' +
                ", LastPaidPeriod='" + LastPaidPeriod + '\'' +
                ", LastPaidDueDate='" + LastPaidDueDate + '\'' +
                ", MinorUnit2=" + MinorUnit2 +
                ", OSInstallmentAmount='" + OSInstallmentAmount + '\'' +
                ", ODInstallmentPeriod='" + ODInstallmentPeriod + '\'' +
                ", ODInstallmentAmount='" + ODInstallmentAmount + '\'' +
                ", ODPenaltyFee='" + ODPenaltyFee + '\'' +
                ", BillerAdminFee=" + BillerAdminFee +
                ", MiscFee='" + MiscFee + '\'' +
                ", MinimumPayAmount='" + MinimumPayAmount + '\'' +
                ", MaximumPayAmount='" + MaximumPayAmount + '\'' +
                '}';
    }

    public String getSwitcherID() {
        return SwitcherID;
    }

    public Inquiry setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
        return this;
    }

    public String getBillerCode() {
        return BillerCode;
    }

    public Inquiry setBillerCode(String billerCode) {
        BillerCode = billerCode;
        return this;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public Inquiry setCustomerID(String customerID) {
        CustomerID = customerID;
        return this;
    }

    public String getGWReferenceNumber() {
        return GWReferenceNumber;
    }

    public Inquiry setGWReferenceNumber(String GWReferenceNumber) {
        this.GWReferenceNumber = GWReferenceNumber;
        return this;
    }

    public String getSWReferenceNumber() {
        return SWReferenceNumber;
    }

    public Inquiry setSWReferenceNumber(String SWReferenceNumber) {
        this.SWReferenceNumber = SWReferenceNumber;
        return this;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public Inquiry setCustomerName(String customerName) {
        CustomerName = customerName;
        return this;
    }

    public String getProductCategory() {
        return ProductCategory;
    }

    public Inquiry setProductCategory(String productCategory) {
        ProductCategory = productCategory;
        return this;
    }

    public Integer getMinorUnit() {
        return MinorUnit;
    }

    public Inquiry setMinorUnit(Integer minorUnit) {
        MinorUnit = minorUnit;
        return this;
    }

    public Double getBillAmount() {
        return BillAmount;
    }

    public Inquiry setBillAmount(Double billAmount) {
        BillAmount = billAmount;
        return this;
    }

    public Double getStampDuty() {
        return StampDuty;
    }

    public Inquiry setStampDuty(Double stampDuty) {
        StampDuty = stampDuty;
        return this;
    }

    public Double getPPN() {
        return PPN;
    }

    public Inquiry setPPN(Double PPN) {
        this.PPN = PPN;
        return this;
    }

    public Double getAdminCharges() {
        return AdminCharges;
    }

    public Inquiry setAdminCharges(Double adminCharges) {
        AdminCharges = adminCharges;
        return this;
    }

    public String getBillerReferenceNumber() {
        return BillerReferenceNumber;
    }

    public Inquiry setBillerReferenceNumber(String billerReferenceNumber) {
        BillerReferenceNumber = billerReferenceNumber;
        return this;
    }

    public String getPTName() {
        return PTName;
    }

    public Inquiry setPTName(String PTName) {
        this.PTName = PTName;
        return this;
    }

    public String getBranchName() {
        return BranchName;
    }

    public Inquiry setBranchName(String branchName) {
        BranchName = branchName;
        return this;
    }

    public String getItemMerkType() {
        return ItemMerkType;
    }

    public Inquiry setItemMerkType(String itemMerkType) {
        ItemMerkType = itemMerkType;
        return this;
    }

    public String getChasisNumber() {
        return ChasisNumber;
    }

    public Inquiry setChasisNumber(String chasisNumber) {
        ChasisNumber = chasisNumber;
        return this;
    }

    public String getCarNumber() {
        return CarNumber;
    }

    public Inquiry setCarNumber(String carNumber) {
        CarNumber = carNumber;
        return this;
    }

    public String getTenor() {
        return Tenor;
    }

    public Inquiry setTenor(String tenor) {
        Tenor = tenor;
        return this;
    }

    public String getLastPaidPeriod() {
        return LastPaidPeriod;
    }

    public Inquiry setLastPaidPeriod(String lastPaidPeriod) {
        LastPaidPeriod = lastPaidPeriod;
        return this;
    }

    public String getLastPaidDueDate() {
        return LastPaidDueDate;
    }

    public Inquiry setLastPaidDueDate(String lastPaidDueDate) {
        LastPaidDueDate = lastPaidDueDate;
        return this;
    }

    public Integer getMinorUnit2() {
        return MinorUnit2;
    }

    public Inquiry setMinorUnit2(Integer minorUnit2) {
        MinorUnit2 = minorUnit2;
        return this;
    }

    public Double getOSInstallmentAmount() {
        return OSInstallmentAmount;
    }

    public Inquiry setOSInstallmentAmount(Double OSInstallmentAmount) {
        this.OSInstallmentAmount = OSInstallmentAmount;
        return this;
    }

    public Double getODInstallmentPeriod() {
        return ODInstallmentPeriod;
    }

    public Inquiry setODInstallmentPeriod(Double ODInstallmentPeriod) {
        this.ODInstallmentPeriod = ODInstallmentPeriod;
        return this;
    }

    public Double getODInstallmentAmount() {
        return ODInstallmentAmount;
    }

    public Inquiry setODInstallmentAmount(Double ODInstallmentAmount) {
        this.ODInstallmentAmount = ODInstallmentAmount;
        return this;
    }

    public Double getODPenaltyFee() {
        return ODPenaltyFee;
    }

    public Inquiry setODPenaltyFee(Double ODPenaltyFee) {
        this.ODPenaltyFee = ODPenaltyFee;
        return this;
    }

    public Double getBillerAdminFee() {
        return BillerAdminFee;
    }

    public Inquiry setBillerAdminFee(Double billerAdminFee) {
        BillerAdminFee = billerAdminFee;
        return this;
    }

    public Double getMiscFee() {
        return MiscFee;
    }

    public Inquiry setMiscFee(Double miscFee) {
        MiscFee = miscFee;
        return this;
    }

    public Double getMinimumPayAmount() {
        return MinimumPayAmount;
    }

    public Inquiry setMinimumPayAmount(Double minimumPayAmount) {
        MinimumPayAmount = minimumPayAmount;
        return this;
    }

    public Double getMaximumPayAmount() {
        return MaximumPayAmount;
    }

    public Inquiry setMaximumPayAmount(Double maximumPayAmount) {
        MaximumPayAmount = maximumPayAmount;
        return this;
    }
}
