/*
 * Copyright 2014 The FIX.io Project
 *
 * The FIX.io Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package com.trimindi.switching.gatway.client.netty.pipeline;

import com.solab.iso8583.MessageFactory;
import com.trimindi.switching.gatway.client.ConnectorConfiguration;
import com.trimindi.switching.gatway.client.ConnectorConfigurer;
import com.trimindi.switching.gatway.client.netty.codec.Iso8583DecoderCustom;
import com.trimindi.switching.gatway.client.netty.codec.Iso8583EncoderCustom;
import io.netty.bootstrap.AbstractBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.timeout.IdleStateHandler;

public class Iso8583ChannelInitializerCustom<C extends Channel, B extends AbstractBootstrap, G extends ConnectorConfiguration> extends ChannelInitializer<C> {

    public static final int DEFAULT_LENGTH_HEADER_LENGTH = 0;

    private final G configuration;
    private final ConnectorConfigurer<G, B> configurer;
    private final EventLoopGroup workerGroup;
    private final MessageFactory isoMessageFactory;
    private final ChannelHandler[] customChannelHandlers;
    private final Iso8583EncoderCustom isoMessageEncoder;
    private ChannelHandler loggingHandler;
    private int headerLength = DEFAULT_LENGTH_HEADER_LENGTH;

    public Iso8583ChannelInitializerCustom(
            G configuration,
            ConnectorConfigurer<G, B> configurer,
            EventLoopGroup workerGroup,
            MessageFactory isoMessageFactory,
            ChannelHandler... customChannelHandlers) {
        this.configuration = configuration;
        this.configurer = configurer;
        this.workerGroup = workerGroup;
        this.isoMessageFactory = isoMessageFactory;
        this.customChannelHandlers = customChannelHandlers;

        this.isoMessageEncoder = createIso8583Encoder(headerLength);
        this.loggingHandler = createLoggingHandler(configuration);
    }

    @Override
    public void initChannel(C ch) throws Exception {
        final ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast("delimiterBasedFrameDecoder", new DelimiterBasedFrameDecoder(
                configuration.getMaxFrameLength(), true, false, new ByteBuf[]{Unpooled.wrappedBuffer(new byte[]{-0x1})}));
        pipeline.addLast("iso8583Decoder", createIso8583Decoder(isoMessageFactory));

        pipeline.addLast("iso8583Encoder", isoMessageEncoder);

        if (configuration.addLoggingHandler()) {
            pipeline.addLast(workerGroup, "logging", loggingHandler);
        }

        if (configuration.replyOnError()) {
            pipeline.addLast(workerGroup, "replyOnError", loggingHandler);
        }

        pipeline.addLast("idleState", new IdleStateHandler(0, 0, configuration.getIdleTimeout()));
        pipeline.addLast("idleEventHandler", new IdleEventHandlerCustom(isoMessageFactory));
        if (customChannelHandlers != null) {
            pipeline.addLast(workerGroup, customChannelHandlers);
        }

        if (configurer != null) {
            configurer.configurePipeline(pipeline, configuration);
        }
    }

    protected Iso8583EncoderCustom createIso8583Encoder(int lengthHeaderLength) {
        return new Iso8583EncoderCustom(lengthHeaderLength);
    }

    protected Iso8583DecoderCustom createIso8583Decoder(final MessageFactory messageFactory) {
        return new Iso8583DecoderCustom(messageFactory);
    }


    protected ChannelHandler createLoggingHandler(G configuration) {
        return new IsoMessageLoggingHandler(LogLevel.ERROR,
                configuration.logSensitiveData(),
                configuration.logFieldDescription(),
                configuration.getSensitiveDataFields());
    }


}
