package com.trimindi.switching.gatway.client.netty.codec;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.List;

public class Iso8583DecoderCustom extends ByteToMessageDecoder {
    public static final Logger logger = LoggerFactory.getLogger(Iso8583DecoderCustom.class);
    private final MessageFactory messageFactory;

    public Iso8583DecoderCustom(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List out) throws Exception {
        if (!byteBuf.isReadable()) {
            return;
        }
//        ByteArrayOutputStream os = new ByteArrayOutputStream();
//        char a = 255;
//        while (byteBuf.isReadable()) {
//            int read = byteBuf.readByte();
//            if (read == (byte) a) {
//                logger.error("FOUND -1");
//                break;
//            }
//            os.write(read);
//        }
//        logger.error(os.toString(StandardCharsets.UTF_8.name()));

        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);
        final IsoMessage isoMessage = messageFactory.parseMessage(bytes, 0);
        if (isoMessage != null) {
            out.add(isoMessage);
        } else {
            throw new ParseException("Can't parse ISO8583 message", 0);
        }
    }
}
