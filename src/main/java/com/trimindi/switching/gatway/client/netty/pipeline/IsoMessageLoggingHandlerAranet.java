package com.trimindi.switching.gatway.client.netty.pipeline;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoValue;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketAddress;
import java.util.Arrays;
import java.util.Properties;

/**
 * Created by sx on 05/03/18.
 * Copyright under comercial unit
 */
@ChannelHandler.Sharable
public class IsoMessageLoggingHandlerAranet extends LoggingHandler {
    public static final Logger logger = LoggerFactory.getLogger(IsoMessageLoggingHandlerAranet.class);
    private static final char MASK_CHAR = '*';
    private static final int[] DEFAULT_MASKED_FIELDS = {
            41
    };
    private static char[] MASKED_VALUE = "***".toCharArray();
    private static String[] FIELD_NAMES = new String[128];

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("exceptionCaught".toUpperCase() + cause.getMessage());
        ctx.close();
        throw new Exception("Request Failed.");
    }

    static {
        try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("iso8583fields.properties")) {
            final Properties properties = new Properties();
            properties.load(stream);
            properties.forEach((key, value) -> {
                int field = Integer.parseInt((String) key);
                FIELD_NAMES[field - 1] = (String) value;
            });
        } catch (IOException | NumberFormatException e) {
            throw new IllegalStateException("Unable to load ISO8583 field descriptions", e);
        }
    }

    private final boolean printSensitiveData;
    private final boolean printFieldDescriptions;
    private final int[] maskedFields;
    ChannelGroup allConnected =
            new DefaultChannelGroup("all-connected", GlobalEventExecutor.INSTANCE);
    private SocketAddress sa;

    public IsoMessageLoggingHandlerAranet(LogLevel level,
                                          boolean printSensitiveData,
                                          boolean printFieldDescriptions,
                                          int... maskedFields) {
        super(level);
        this.printSensitiveData = printSensitiveData;
        this.printFieldDescriptions = printFieldDescriptions;
        this.maskedFields = (maskedFields != null && maskedFields.length > 0) ? maskedFields : DEFAULT_MASKED_FIELDS;
    }

    public IsoMessageLoggingHandlerAranet(LogLevel level) {
        this(level, true, true);
    }

    private static char[] maskPAN(String fullPan) {
        char[] maskedPan = fullPan.toCharArray();
        for (int i = 6; i < maskedPan.length - 4; i++) {
            maskedPan[i] = MASK_CHAR;
        }
        return maskedPan;
    }
//
//    @Override
//    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//        super.exceptionCaught(ctx, cause);
//        allConnected.disconnect();
//        logger.debug("Disconnect from upstream server " + cause.getLocalizedMessage());
//    }
//
//    @Override
//    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        allConnected.add(ctx.channel());
//        StringBuilder sb = new StringBuilder();
//        sb.append("Active connection ");
//        sb.append(allConnected.size()).append(" On Local Address ")
//                .append(ctx.channel().localAddress().toString())
//                .append(" On Remote Upstream ")
//                .append(ctx.channel().remoteAddress().toString());
//        logger.error(sb.toString());
//    }
//
//    @Override
//    public void connect(ChannelHandlerContext ctx, SocketAddress remoteAddress, SocketAddress localAddress, ChannelPromise promise) throws Exception {
//        if (sa == null) {
//            sa = localAddress;
//        }
//        super.connect(ctx, remoteAddress, sa, promise);
//    }
//
//    @Override
//    public void close(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
//        super.close(ctx, promise);
//        logger.error("Active connection " + allConnected.size());
//    }
//
//    @Override
//    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
//        allConnected.forEach(ChannelOutboundInvoker::close);
//        StringBuilder sb = new StringBuilder();
//        sb.append("Inactive connection ");
//        sb.append(allConnected.size()).append(" On Local Address ")
//                .append(ctx.channel().localAddress().toString())
//                .append(" On Remote Upstream ")
//                .append(ctx.channel().remoteAddress().toString());
//        logger.error(sb.toString());
//    }
//
//    @Override
//    public void flush(ChannelHandlerContext ctx) throws Exception {
//        ctx.flush();
//    }
//
//    @Override
//    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
//        ctx.write(msg, promise);
//        logger.error(format(ctx, "WRITE ", msg));
//    }

    @Override
    protected String format(ChannelHandlerContext ctx, String eventName, Object arg) {
        if (arg instanceof IsoMessage) {
            return super.format(ctx, eventName, formatIsoMessage((IsoMessage) arg));
        } else {
            return super.format(ctx, eventName, arg);
        }
    }

    private String formatIsoMessage(IsoMessage m) {
        StringBuilder sb = new StringBuilder();
        if (printSensitiveData) {
            sb.append("Message: ").append(m.debugString()).append("\n");
        }
        sb.append("MTI: 0x").append(String.format("%04x", m.getType()));
        for (int i = 2; i < 128; i++) {
            if (m.hasField(i)) {
                final IsoValue<Object> field = m.getField(i);
                sb.append("\n  ").append(i)
                        .append(": [");

                if (printFieldDescriptions) {
                    sb.append(FIELD_NAMES[i - 1]).append(':');
                }

                char[] formattedValue;
                if (printSensitiveData) {
                    formattedValue = field.toString().toCharArray();
                } else {
                    if (i == 2) {
                        formattedValue = maskPAN(field.toString());
                    } else if (Arrays.binarySearch(maskedFields, i) >= 0) {
                        formattedValue = MASKED_VALUE;
                    } else {
                        formattedValue = field.toString().toCharArray();
                    }

                }
                sb.append(field.getType()).append('(').append(field.getLength())
                        .append(")] = '").append(formattedValue).append('\'');

            }
        }
        return sb.toString();
    }
}

