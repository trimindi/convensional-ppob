package com.trimindi.switching.gatway.client.netty.pipeline;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by sx on 08/02/18.
 * Copyright under comercial unit
 */
public class ErrorHandler extends ChannelDuplexHandler {
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.disconnect();
        ctx.disconnect();
        super.exceptionCaught(ctx, cause);
    }

    @Override
    public boolean isSharable() {
        return true;
    }


}
