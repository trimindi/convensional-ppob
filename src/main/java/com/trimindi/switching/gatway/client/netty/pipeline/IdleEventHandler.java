package com.trimindi.switching.gatway.client.netty.pipeline;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

import static com.trimindi.switching.gatway.utils.generator.BaseHelper.date14;

@ChannelHandler.Sharable
public class IdleEventHandler extends ChannelInboundHandlerAdapter {

    private final MessageFactory isoMessageFactory;


    public IdleEventHandler(MessageFactory isoMessageFactory) {
        this.isoMessageFactory = isoMessageFactory;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state() == IdleState.ALL_IDLE) {
                final IsoMessage echoMessage = createEchoMessage();
                ctx.writeAndFlush(echoMessage);
            }
        }
    }

    private IsoMessage createEchoMessage() {
        IsoMessage isoMessage = isoMessageFactory.newMessage(0x2800);
        isoMessage.setField(12, IsoType.ALPHA.value(date14(), 14));
        isoMessage.setField(33, IsoType.LLVAR.value("4412705"));
        isoMessage.setField(40, IsoType.ALPHA.value("003", 3));
        isoMessage.setField(41, IsoType.ALPHA.value("TMC0000000000001", 16));
        isoMessage.setField(48, IsoType.LLLLVAR.value("0000000"));
        return isoMessage;
    }
}
