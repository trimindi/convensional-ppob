package com.trimindi.switching.gatway.client.netty.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Iso8583DecoderCustomNew extends DelimiterBasedFrameDecoder {
    public static final Logger logger = LoggerFactory.getLogger(Iso8583DecoderCustomNew.class);


    public Iso8583DecoderCustomNew(int maxFrameLength, boolean stripDelimiter, ByteBuf delimiter) {
        super(maxFrameLength, stripDelimiter, delimiter);
    }

    public Iso8583DecoderCustomNew(int maxFrameLength, boolean stripDelimiter, boolean failFast, ByteBuf delimiter) {
        super(maxFrameLength, stripDelimiter, failFast, delimiter);
    }

    public Iso8583DecoderCustomNew(int maxFrameLength, ByteBuf... delimiters) {
        super(maxFrameLength, delimiters);
    }

    public Iso8583DecoderCustomNew(int maxFrameLength, boolean stripDelimiter, ByteBuf... delimiters) {
        super(maxFrameLength, stripDelimiter, delimiters);
    }

    public Iso8583DecoderCustomNew(int maxFrameLength, boolean stripDelimiter, boolean failFast, ByteBuf... delimiters) {
        super(maxFrameLength, stripDelimiter, failFast, delimiters);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf buffer) throws Exception {
        return super.decode(ctx, buffer);
    }
}
