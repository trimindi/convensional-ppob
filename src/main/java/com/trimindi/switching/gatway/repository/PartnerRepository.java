package com.trimindi.switching.gatway.repository;

import com.trimindi.switching.gatway.models.Partner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PC on 08/08/2017.
 */
@Repository
public interface PartnerRepository extends CrudRepository<Partner,String> {
}
