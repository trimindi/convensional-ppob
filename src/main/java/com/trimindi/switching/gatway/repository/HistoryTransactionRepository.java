package com.trimindi.switching.gatway.repository;

import com.trimindi.switching.gatway.models.HistoryTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by sx on 30/11/17.
 * Copyright under comercial unit
 */
@Repository
public interface HistoryTransactionRepository extends JpaRepository<HistoryTransaction, Long> {
}
