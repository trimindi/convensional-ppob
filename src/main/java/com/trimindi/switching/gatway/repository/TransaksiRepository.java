package com.trimindi.switching.gatway.repository;

import com.trimindi.switching.gatway.models.Transaksi;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PC on 27/07/2017.
 */
@Repository
public interface TransaksiRepository extends CrudRepository<Transaksi,String> {
    @Query(value = "SELECT '*' FROM mst_transaksi t WHERE t.MSSIDN = :mssidn and t.ST = '01'",nativeQuery = true)
    Transaksi checkTransaksi(@Param("mssidn") String mssidn);

    @Query(value = "SELECT COALESCE(sum(t.fee),0) FROM mst_transaksi t WHERE t.PARTNERID = :partnerid AND t.st = '02'", nativeQuery = true)
    double findTotalFee(@Param("partnerid") String partnerid);

    List<Transaksi> findAll();

    @Query(value = "SELECT * FROM mst_transaksi t WHERE t.HOST_REF_NUMBER = :bill_ref_number", nativeQuery = true)
    Transaksi findByBILL_REF_NUMBER(@Param("bill_ref_number") String bill_ref_number);

    @Query(value = "SELECT * FROM mst_transaksi t WHERE t.MSSIDN = :mssidn AND t.DENOM = :product AND t.DATE = :date AND t.ST = '01'", nativeQuery = true)
    List<Transaksi> checkIfTransactionUnderProses(@Param("mssidn") String mssidn, @Param("product") String product, @Param("date") Date date);

    @Modifying
    @Query(value = "DELETE FROM mst_transaksi t WHERE t.TIME_INQUIRY < :time_old AND t.ST = '00'", nativeQuery = true)
    void deleteAllByST(@Param("time_old") Date timeOld);
}
