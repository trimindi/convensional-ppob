package com.trimindi.switching.gatway.repository;

import com.trimindi.switching.gatway.models.PulsaPrefix;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PC on 04/09/2017.
 */
@Repository
public interface PulsaPrefixRepository extends JpaRepository<PulsaPrefix, Long> {
    @Query(value = "SELECT p FROM PulsaPrefix p WHERE p.product LIKE CONCAT('%',:product,'%') AND p.prefix = :prefix")
    List<PulsaPrefix> findByProduct(@Param("product") String product, @Param("prefix") String prefix);
}
