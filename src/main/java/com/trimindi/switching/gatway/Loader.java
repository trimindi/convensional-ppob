package com.trimindi.switching.gatway;

import com.solab.iso8583.IsoMessage;
import com.trimindi.switching.gatway.client.IsoMessageListener;
import com.trimindi.switching.gatway.client.client.Iso8583Client;
import com.trimindi.switching.gatway.utils.constanta.ResponseCode;
import com.trimindi.switching.gatway.utils.generator.MessageGenerator;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Created by sx on 10/10/17.
 * Copyright under comercial unit
 */
@Component
public class Loader {
    private final Iso8583Client<IsoMessage> client;
    private final MessageGenerator messageGenerator;
    private final Map<String, ResponseCode> responseCode;
    private Logger logger = LoggerFactory.getLogger(Loader.class);
    @Value("${service.aranet}")
    private boolean active;

    @Autowired
    public Loader(@Qualifier("aranetClient") Iso8583Client<IsoMessage> client, @Qualifier("aranetMessageGenerator") MessageGenerator messageGenerator, @Qualifier("aranetResponseCode") Map<String, ResponseCode> responseCode) {
        this.client = client;
        this.messageGenerator = messageGenerator;
        this.responseCode = responseCode;
    }

    @PostConstruct
    public void singOn() {
        if (active) {
            client.init();
            client.addMessageListener(new IsoMessageListener<IsoMessage>() {
                @Override
                public boolean applies(IsoMessage isoMessage) {
                    return isoMessage.getType() == 0x2810;
                }

                @Override
                public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        switch (isoMessage.getObjectValue(40).toString()) {
                            case "001":
                                logger.error("SUCCESS SIGN ON KE SERVER");
                                break;
                            case "002":
                                logger.error("SUCCESS SIGN OFF KE SERVER");
                                break;
                            case "003":
                                logger.error("CONNECTION ESTABILISED");
                                break;
                        }
                    } else {
                        logger.error("FAILED CONNECT TO SERVER " + responseCode.get(isoMessage.getObjectValue(39).toString()).getMessage());
                    }
                    return false;
                }
            });
            client.connectAsync();
            if (client.isConnected()) {
                IsoMessage message = messageGenerator.signON();
                client.sendAsync(message);
            }
        }

    }
}
