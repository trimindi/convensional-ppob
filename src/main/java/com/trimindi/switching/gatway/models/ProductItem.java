package com.trimindi.switching.gatway.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * Created by sx on 11/05/17.
 */

@Entity
@Table(name = "mst_product_item")
@XmlRootElement
public class ProductItem implements Serializable {

    @Column(name = "FK_PRODUCT_ID")
    private String product_id;
    @Column(name = "FK_BILLER")
    private String fk_biller;
    @Id
    private String DENOM;
    private String NAME;
    private String DESKRIPSI;
    private double AMOUT;
    private double FEE_BILLER;
    @Column(name = "ACTIVE")
    private int ACTIVE;
    private double ADMIN;
    private String ROUTE;
    private String DENOM_BILLER;

    public ProductItem() {
    }

    public String getROUTE() {
        return ROUTE;
    }

    public ProductItem setROUTE(String ROUTE) {
        this.ROUTE = ROUTE;
        return this;
    }

    public String getDENOM_BILLER() {
        return DENOM_BILLER;
    }

    public ProductItem setDENOM_BILLER(String DENOM_BILLER) {
        this.DENOM_BILLER = DENOM_BILLER;
        return this;
    }

    public boolean isACTIVE() {
        return ACTIVE == 1;
    }

    public String getFk_biller() {
        return fk_biller;
    }

    public ProductItem setFk_biller(String fk_biller) {
        this.fk_biller = fk_biller;
        return this;
    }

    @XmlTransient
    public String getProduct_id() {
        return product_id;
    }

    public ProductItem setProduct_id(String product_id) {
        this.product_id = product_id;
        return this;
    }

    public String getDENOM() {
        return DENOM;
    }

    public ProductItem setDENOM(String DENOM) {
        this.DENOM = DENOM;
        return this;
    }

    public double getFEE_BILLER() {
        return FEE_BILLER;
    }

    public ProductItem setFEE_BILLER(double FEE_BILLER) {
        this.FEE_BILLER = FEE_BILLER;
        return this;
    }

    public int getACTIVE() {
        return ACTIVE;
    }

    public ProductItem setACTIVE(int ACTIVE) {
        this.ACTIVE = ACTIVE;
        return this;
    }

    public String getNAME() {
        return NAME;
    }

    public ProductItem setNAME(String NAME) {
        this.NAME = NAME;
        return this;
    }

    public String getDESKRIPSI() {
        return DESKRIPSI;
    }

    public ProductItem setDESKRIPSI(String DESKRIPSI) {
        this.DESKRIPSI = DESKRIPSI;
        return this;
    }

    public double getAMOUT() {
        return AMOUT;
    }

    public ProductItem setAMOUT(double AMOUT) {
        this.AMOUT = AMOUT;
        return this;
    }

    public double getADMIN() {
        return ADMIN;
    }

    public ProductItem setADMIN(double ADMIN) {
        this.ADMIN = ADMIN;
        return this;
    }
}
