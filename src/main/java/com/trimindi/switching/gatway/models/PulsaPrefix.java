package com.trimindi.switching.gatway.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by PC on 04/09/2017.
 */
@Entity
@Table(name = "pulsa_prefix")
public class PulsaPrefix implements Serializable {
    @Id
    private Long Id;
    private String product;
    private String prefix;

    public PulsaPrefix() {
    }

    public Long getId() {
        return Id;
    }

    public PulsaPrefix setId(Long id) {
        Id = id;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public PulsaPrefix setProduct(String product) {
        this.product = product;
        return this;
    }

    public String getPrefix() {
        return prefix;
    }

    public PulsaPrefix setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }
}
