package com.trimindi.switching.gatway.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by sx on 30/11/17.
 * Copyright under comercial unit
 */
@Entity
@Table(name = "history_transaction")
public class HistoryTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq_gen")
    @SequenceGenerator(name = "order_seq_gen", sequenceName = "history_transaction_id_seq")
    private Long id;
    private String ntrans;
    private double before;
    private double after;
    private double nominal;
    private String action;
    private Timestamp createAt;

    public HistoryTransaction() {
    }

    public HistoryTransaction(String ntrans, double before, double after, double nominal, Timestamp createAt) {
        this.ntrans = ntrans;
        this.before = before;
        this.after = after;
        this.nominal = nominal;
        this.createAt = createAt;
    }

    public String getAction() {
        return action;
    }

    public HistoryTransaction setAction(String action) {
        this.action = action;
        return this;
    }

    public Long getId() {
        return id;
    }

    public HistoryTransaction setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public HistoryTransaction setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getBefore() {
        return before;
    }

    public HistoryTransaction setBefore(double before) {
        this.before = before;
        return this;
    }

    public double getAfter() {
        return after;
    }

    public HistoryTransaction setAfter(double after) {
        this.after = after;
        return this;
    }

    public double getNominal() {
        return nominal;
    }

    public HistoryTransaction setNominal(double nominal) {
        this.nominal = nominal;
        return this;
    }

    public Timestamp getCreateAt() {
        return createAt;
    }

    public HistoryTransaction setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
        return this;
    }
}
