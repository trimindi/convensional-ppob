package com.trimindi.switching.gatway.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by HP on 19/05/2017.
 */
@Entity
@Table(name = "mst_product_price")
public class ProductFee implements Serializable {
    @Id
    private int ID;
    private String PARTNER_ID;
    private String PRODUCT_ITEM;
    private double ADMIN;
    private double FEE_CA;
    public ProductFee() {

    }

    public int getID() {
        return ID;
    }

    public ProductFee setID(int ID) {
        this.ID = ID;
        return this;
    }

    public String getPARTNER_ID() {
        return PARTNER_ID;
    }

    public ProductFee setPARTNER_ID(String PARTNER_ID) {
        this.PARTNER_ID = PARTNER_ID;
        return this;
    }

    public String getPRODUCT_ITEM() {
        return PRODUCT_ITEM;
    }

    public ProductFee setPRODUCT_ITEM(String PRODUCT_ITEM) {
        this.PRODUCT_ITEM = PRODUCT_ITEM;
        return this;
    }

    public double getADMIN() {
        return ADMIN;
    }

    public ProductFee setADMIN(double ADMIN) {
        this.ADMIN = ADMIN;
        return this;
    }

    public double getFEE_CA() {
        return FEE_CA;
    }

    public ProductFee setFEE_CA(double FEE_CA) {
        this.FEE_CA = FEE_CA;
        return this;
    }
}
