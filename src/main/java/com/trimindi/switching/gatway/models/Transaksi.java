package com.trimindi.switching.gatway.models;

import com.trimindi.switching.gatway.utils.TimestampAdapter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by sx on 14/05/17.
 */
@XmlRootElement
@Entity(name = "mst_transaksi")
public class Transaksi {
    @Id
    @Column(name = "NTRANS")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    private String NTRANS;
    private String MSSIDN;
    private String MSSIDN_NAME;
    private String MONTH;
    private String YEAR;
    @Column(name = "FK_MERCHANT_ID")
    private String MERCHANT_ID;
    private Timestamp DATE = null;
    private Timestamp TIME_INQUIRY = null;
    private Timestamp TIME_PAYMENT = null;
    private String DENOM;
    private String PARTNERID;
    private String HOST_REF_NUMBER;
    private String BILL_REF_NUMBER;
    private double TAGIHAN;
    private double ADMIN;
    private double DENDA;
    private double CHARGE;
    private double FEE_DEALER;
    private double FEE_CA;
    private double DEBET;
    private double KREDIT;
    private double SALDO;
    @Column(name = "RES_INQUIRY")
    private String INQUIRY;
    @Column(name = "RES_PAYMENT")
    private String PAYMENT;
    private String USERID;
    private String IP_ADDRESS;
    private String DATA;
    private String ST;
    private String STAN;
    private int PRT;
    private String PRODUCT;

    public Transaksi() {
        this.MONTH = new SimpleDateFormat("MM").format(new java.util.Date());
        this.YEAR = new SimpleDateFormat("YYYY").format(new java.util.Date());
        this.DATE = new Timestamp(System.currentTimeMillis());
        this.TIME_INQUIRY = new Timestamp(System.currentTimeMillis());
    }

    public String getSTAN() {
        return STAN;
    }

    public Transaksi setSTAN(String STAN) {
        this.STAN = STAN;
        return this;
    }

    public String getPARTNERID() {
        return PARTNERID;
    }

    public Transaksi setPARTNERID(String PARTNERID) {
        this.PARTNERID = PARTNERID;
        return this;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public Transaksi setPRODUCT(String PRODUCT) {
        this.PRODUCT = PRODUCT;
        return this;
    }

    public String getNTRANS() {
        return NTRANS;
    }

    public Transaksi setNTRANS(String NTRANS) {
        this.NTRANS = NTRANS;
        return this;
    }

    public String getMSSIDN() {
        return MSSIDN;
    }

    public Transaksi setMSSIDN(String MSSIDN) {
        this.MSSIDN = MSSIDN;
        return this;
    }

    public String getMSSIDN_NAME() {
        return MSSIDN_NAME;
    }

    public Transaksi setMSSIDN_NAME(String MSSIDN_NAME) {
        this.MSSIDN_NAME = MSSIDN_NAME;
        return this;
    }

    public String getMONTH() {
        return MONTH;
    }

    public Transaksi setMONTH(String MONTH) {
        this.MONTH = MONTH;
        return this;
    }

    public String getYEAR() {
        return YEAR;
    }

    public Transaksi setYEAR(String YEAR) {
        this.YEAR = YEAR;
        return this;
    }

    public String getMERCHANT_ID() {
        return MERCHANT_ID;
    }

    public Transaksi setMERCHANT_ID(String MERCHANT_ID) {
        this.MERCHANT_ID = MERCHANT_ID;
        return this;
    }

    @XmlJavaTypeAdapter(TimestampAdapter.class)
    public Timestamp getDATE() {
        return DATE;
    }

    public Transaksi setDATE(Timestamp DATE) {
        this.DATE = DATE;
        return this;
    }
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    public Timestamp getTIME_INQUIRY() {
        return TIME_INQUIRY;
    }

    public Transaksi setTIME_INQUIRY(Timestamp TIME_INQUIRY) {
        this.TIME_INQUIRY = TIME_INQUIRY;
        return this;
    }
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    public Timestamp getTIME_PAYMENT() {
        return TIME_PAYMENT;
    }

    public Transaksi setTIME_PAYMENT(Timestamp TIME_PAYMENT) {
        this.TIME_PAYMENT = TIME_PAYMENT;
        return this;
    }

    public String getDENOM() {
        return DENOM;
    }

    public Transaksi setDENOM(String DENOM) {
        this.DENOM = DENOM;
        return this;
    }

    @XmlTransient
    public String getHOST_REF_NUMBER() {
        return HOST_REF_NUMBER;
    }

    public Transaksi setHOST_REF_NUMBER(String HOST_REF_NUMBER) {
        this.HOST_REF_NUMBER = HOST_REF_NUMBER;
        return this;
    }

    @XmlTransient
    public String getBILL_REF_NUMBER() {
        return BILL_REF_NUMBER;
    }

    public Transaksi setBILL_REF_NUMBER(String BILL_REF_NUMBER) {
        this.BILL_REF_NUMBER = BILL_REF_NUMBER;
        return this;
    }

    public double getTAGIHAN() {
        return TAGIHAN;
    }

    public Transaksi setTAGIHAN(double TAGIHAN) {
        this.TAGIHAN = TAGIHAN;
        return this;
    }

    public double getADMIN() {
        return ADMIN;
    }

    public Transaksi setADMIN(double ADMIN) {
        this.ADMIN = ADMIN;
        return this;
    }

    public double getDENDA() {
        return DENDA;
    }

    public Transaksi setDENDA(double DENDA) {
        this.DENDA = DENDA;
        return this;
    }

    public double getCHARGE() {
        return CHARGE;
    }

    public Transaksi setCHARGE(double CHARGE) {
        this.CHARGE = CHARGE;
        return this;
    }

    public double getFEE_CA() {
        return FEE_CA;
    }

    public Transaksi setFEE_CA(double FEE_CA) {
        this.FEE_CA = FEE_CA;
        return this;
    }

    public double getDEBET() {
        return DEBET;
    }

    public Transaksi setDEBET(double DEBET) {
        this.DEBET = DEBET;
        return this;
    }

    public double getKREDIT() {
        return KREDIT;
    }

    public Transaksi setKREDIT(double KREDIT) {
        this.KREDIT = KREDIT;
        return this;
    }

    public double getSALDO() {
        return SALDO;
    }

    public Transaksi setSALDO(double SALDO) {
        this.SALDO = SALDO;
        return this;
    }

    @XmlTransient
    public String getINQUIRY() {
        return INQUIRY;
    }

    public Transaksi setINQUIRY(String INQUIRY) {
        this.INQUIRY = INQUIRY;
        return this;
    }

    @XmlTransient
    public String getPAYMENT() {
        return PAYMENT;
    }

    public Transaksi setPAYMENT(String PAYMENT) {
        this.PAYMENT = PAYMENT;
        return this;
    }

    public String getUSERID() {
        return USERID;
    }

    public Transaksi setUSERID(String USERID) {
        this.USERID = USERID;
        return this;
    }

    @XmlTransient
    public String getDATA() {
        return DATA;
    }

    public Transaksi setDATA(String DATA) {
        this.DATA = DATA;
        return this;
    }

    public String getST() {
        return ST;
    }

    public Transaksi setST(String ST) {
        this.ST = ST;
        return this;
    }

    public int getPRT() {
        return PRT;
    }

    public Transaksi setPRT(int PRT) {
        this.PRT = PRT;
        return this;
    }

    public double getFEE_DEALER() {
        return FEE_DEALER;
    }

    public Transaksi setFEE_DEALER(double FEE_DEALER) {
        this.FEE_DEALER = FEE_DEALER;
        return this;
    }

    public String getIP_ADDRESS() {
        return IP_ADDRESS;
    }

    public Transaksi setIP_ADDRESS(String IP_ADDRESS) {
        this.IP_ADDRESS = IP_ADDRESS;
        return this;
    }

    @Override
    public String toString() {
        return "Transaksi{" +
                "NTRANS='" + NTRANS + '\'' +
                ", MSSIDN='" + MSSIDN + '\'' +
                ", MSSIDN_NAME='" + MSSIDN_NAME + '\'' +
                ", MONTH='" + MONTH + '\'' +
                ", YEAR='" + YEAR + '\'' +
                ", MERCHANT_ID='" + MERCHANT_ID + '\'' +
                ", DATE=" + DATE +
                ", TIME_INQUIRY=" + TIME_INQUIRY +
                ", TIME_PAYMENT=" + TIME_PAYMENT +
                ", DENOM='" + DENOM + '\'' +
                ", PARTNERID='" + PARTNERID + '\'' +
                ", HOST_REF_NUMBER='" + HOST_REF_NUMBER + '\'' +
                ", BILL_REF_NUMBER='" + BILL_REF_NUMBER + '\'' +
                ", TAGIHAN=" + TAGIHAN +
                ", ADMIN=" + ADMIN +
                ", DENDA=" + DENDA +
                ", CHARGE=" + CHARGE +
                ", FEE_DEALER=" + FEE_DEALER +
                ", FEE_CA=" + FEE_CA +
                ", DEBET=" + DEBET +
                ", KREDIT=" + KREDIT +
                ", SALDO=" + SALDO +
                ", ST='" + ST + '\'' +
                ", PRT=" + PRT +
                ", PRODUCT='" + PRODUCT + '\'' +
                '}';
    }
}
