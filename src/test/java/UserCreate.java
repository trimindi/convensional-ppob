import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sx on 20/12/17.
 * Copyright under comercial unit
 */
public class UserCreate {

    @Test
    public void createSign() {
        String username = "TAKTIS";
        String pass = "@TAKTISPPOB";
        String merchant_type = "6021";
        String date = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        String sign = DigestUtils.sha1Hex(username + date + pass + merchant_type);
        System.out.println("userid :" + username);
        System.out.println("time   :" + date);
        System.out.println("sign   :" + sign);
    }

    @Test
    public void date() throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX").parse("2018-01-29T16:10:48.304182+07:00");
        System.out.println(new SimpleDateFormat("yyyy-MM-dd").format(date));
    }

    @Test
    public void parse() {
        String data = "idpel:517103002889|nmpel:HOSNAN|periode:JUL11,AGT11|tagihan:34905|usage:735900-743700|billqty:2|tarifdaya:R1/450|plnref:9562E91DFFEE45C18D13B6EFD92AB172|mkmref:00000000021051113018229011120109|nonsub:0|billtot:02|charge:35405|saldo:10000000";

        Map<String, String> payload = new HashMap<>();
        String[] res = data.split("\\|");
        for (int i = 0; i < res.length; i++) {
            System.out.println(res[i]);
            String[] _res2 = res[i].split("\\:");
            System.out.println(_res2.length);
            payload.put(_res2[0], _res2[1]);
        }
        System.out.println(payload);
    }
}
