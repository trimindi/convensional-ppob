import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.parse.ConfigParser;
import com.trimindi.switching.gatway.biller.dw.pojo.train.daftar.GetTrainListRequest;
import com.trimindi.switching.gatway.controllers.Request;
import com.trimindi.switching.gatway.utils.generator.BaseHelper;
import com.trimindi.switching.gatway.utils.iso.models.Rules;
import com.trimindi.switching.gatway.utils.iso.parsing.SDE;
import com.trimindi.switching.gatway.utils.rules.response.ResponseRulesGeneratorMultifinance;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by sx on 04/10/17.
 * Copyright under comercial unit
 */
public class ParsingTest {

    @Test
    public void test() throws IOException, ParseException {
        String msg = "2210503200418281000605860013600000000835600000000000023201711326060502017112260210700000000750611390000TRIMINDI00000001194VI105V300        201010041100565D01C93D564A5A8B8D2D06B6EA35A48388B708D4804BD58A08306C9E1B9B88Subscriber Du'mmy                                 01000000083400000000000000000000000000000000000160026623000000000000000000000000000000PT MEGA CENTRAL FINANCE  Purwakarta MCF                YAMAHA V IXION 2008                       MH33C10018K047130        T2160AU   017013200905160000003570000000000000714000000000120000000000000000000000000000000000714000000000834000059Bukti pembayaran ini adalah SAH. Pengaduan hub 08041123888.";
        MessageFactory<IsoMessage> messageMessageFactory = ConfigParser.createFromClasspathConfig("spec-vsi.xml");
        IsoMessage isoMessage = messageMessageFactory.parseMessage(msg.getBytes(), 0);
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(isoMessage.getObjectValue(48))
                .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(48, true))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(isoMessage.getObjectValue(62))
                .setRules(ResponseRulesGeneratorMultifinance.inquiryResponse(62, true))
                .generate();
        bit48.addAll(bit62);

        com.trimindi.switching.gatway.response.mutifinance.Payment prepaid = new com.trimindi.switching.gatway.response.mutifinance.Payment(bit48);
        System.out.println(prepaid.toString());
        System.out.println(BaseHelper.date14());

    }

    @Test
    public void generateDw() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
//        String username = "YECX0Z0CZC";
//        String password = "YEC252401C";
//        String token = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
//        String pass = DigestUtils.md5Hex(password);
//        String securityCode = DigestUtils.md5Hex(token + pass);
//        LoginRequest loginRequest = new LoginRequest();
//        loginRequest.setUserID(username);
//        loginRequest.setToken(token);
//        loginRequest.setSecurityCode(securityCode);
//        System.out.println(objectMapper.writeValueAsString(loginRequest));

        String payload = "{\n" +
                "\t\"ACTION\": \"PLN.PAYMENT\",\n" +
                "\t\"MSSIDN\": \"539610102102\",\n" +
                "\t\"PRODUCT\": \"PLNPP\",\n" +
                "\t\"NTRANSss\":\"ff80808163153ee0016316db2b790018\"\n" +
                "}";
        Request request = objectMapper.readValue(payload, Request.class);
        System.out.println(request.toString());
    }

    @Test
    public void generat() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String username = "YECX0Z0CZC";
        String accessToken = "5238a0e02a7161e6a777fe25ba0fbefc";
        GetTrainListRequest loginRequest = new GetTrainListRequest();
        loginRequest.setUserID(username);
        loginRequest.setAccessToken(accessToken);
        System.out.println(objectMapper.writeValueAsString(loginRequest));
    }

}
